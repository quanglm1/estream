﻿using System;
using System.Web;
using System.Web.Mvc;
using Model;

namespace eStream.Common.CommonClass
{
    public class ActionLogFilter : ActionFilterAttribute
    {

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var descriptor = filterContext.ActionDescriptor;
            if (descriptor.GetCustomAttributes(typeof(SkipActionLogFilterAttribute), false).Length > 0)
            {
                return;
            }

            string controller = descriptor.ControllerDescriptor.ControllerName;
            string actionName = descriptor.ActionName;
            var user = SessionManager.GetUserInfoBO();
            if (user == null || user.IsAdmin.GetValueOrDefault() == 0)
            {
                string userName = null;
                int? userID = null;
                if (user != null)
                {
                    userName = user.UserName;
                    userID = user.UserID;
                }
                using (var context = new Entities())
                {
                    ActionLog actionLog = new ActionLog();
                    actionLog.UserID = userID;
                    actionLog.UserName = userName;
                    actionLog.ActionName = actionName;
                    actionLog.ActionTime = DateTime.Now;
                    actionLog.ControllerName = controller;
                    actionLog.IPAddress = GetIpValue();
                    context.ActionLog.Add(actionLog);
                    context.SaveChanges();
                }
            }
        }
        private string GetIpValue()
        {
            string ipAdd = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (string.IsNullOrEmpty(ipAdd))
            {
                ipAdd = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
            return ipAdd;
        }

    }
}