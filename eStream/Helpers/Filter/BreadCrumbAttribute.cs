﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eStream.Helpers.Filter
{
    public class BreadCrumbAttribute : ActionFilterAttribute
    {
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string AreaName { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            filterContext.Controller.ViewBag.displayActionName = ActionName;
            filterContext.Controller.ViewBag.displayControllerName = ControllerName;
            filterContext.Controller.ViewBag.displayAreaName = AreaName;
        }
    }
}