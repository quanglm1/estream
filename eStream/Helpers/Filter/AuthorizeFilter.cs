﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace eStream.Common.CommonClass
{
    public class AuthorizeFilter : AuthorizeAttribute
    {
        public const string TOKEN_KEY = "AuthenticationToken";
        private static ConcurrentDictionary<string, string> DicDomainHash = new ConcurrentDictionary<string, string>();

        public AuthorizeFilter() : base()
        {

        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return base.AuthorizeCore(httpContext);
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (!filterContext.ActionDescriptor.IsDefined(typeof(SkipDomainHashFilterAttribute), true))
            {
                var request = HttpContext.Current.Request;
                var domain = request.Url.Authority;

                var actualDomainHash = string.Empty;
                if (DicDomainHash.ContainsKey(domain))
                {
                    actualDomainHash = DicDomainHash[domain];
                }
                else
                {
                    actualDomainHash = Crypto.ComputeSha256Hash(domain, GlobalConstant.VERY_SECRET + GlobalConstant.DOMAIN_SECRET);
                    DicDomainHash[domain] = actualDomainHash;
                }
                if (actualDomainHash != GlobalConstant.DOMAIN_HASH)
                {
                    if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
                    {
                        if (filterContext.HttpContext.Request.IsAjaxRequest())
                        {
                            throw new BusinessException("Domain không hợp hệ");
                        }
                        filterContext.Result = new RedirectToRouteResult(
                          new RouteValueDictionary
                               {
                            { "controller", "Home" },
                            { "action", "DomainError" }
                               });
                        return;
                    }
                }
            }
            base.OnAuthorization(filterContext);
            if (filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true))
            {
                return;
            }
            else
            {
                if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
                {
                    if (filterContext.HttpContext.Request.IsAjaxRequest())
                    {
                        throw new BusinessException("Bạn không được cấp quyền thao tác chức năng");
                    }
                    throw new UnauthorizedAccessException();
                }
            }
        }
    }
}