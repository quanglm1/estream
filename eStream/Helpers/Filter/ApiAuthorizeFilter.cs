﻿using Jwt;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace eStream.Common.CommonClass
{
    public class ApiAuthorizeFilter : AuthorizeAttribute
    {
        private static readonly string TOKEN_KEY = ConfigurationManager.AppSettings["API_TOKEN_KEY"];
        private static readonly string ALLOWED_USER = ConfigurationManager.AppSettings["API_ALLOWED_USER"];
        public ApiAuthorizeFilter() : base()
        {

        }

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (!IsAuthorized(actionContext))
            {
                var response = new HttpResponseMessage(HttpStatusCode.Forbidden)
                {
                    Content = new StringContent("{\"message\":\"Forbidden\"}", Encoding.UTF8, "application/json")
                };
                throw new HttpResponseException(response);
            }
        }

        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            var author = actionContext.Request.Headers.Authorization;
            if (author == null)
            {
                return false;
            }
            string token = author.Parameter;
            if (string.IsNullOrWhiteSpace(token))
            {
                return false;
            }
            try
            {
                var decoded = JsonWebToken.DecodeToObject<Dictionary<string, object>>(token, TOKEN_KEY);
                object sub = null;
                if (decoded.TryGetValue("sub", out sub))
                {
                    if (sub != null)
                    {
                        var users = ALLOWED_USER.Split(',');
                        if (ALLOWED_USER.Contains(sub.ToString()))
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
            catch (SignatureVerificationException)
            {
                return false;
            }
        }
    }
}