﻿using eStream.Common.CommonClass;
using eStream.Helpers.Filter;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.IO;
using eStream.Common.CommonExcel;
using System.Web;

namespace eStream.Controllers
{
    public class ReportTimeController : BaseController
    {
        private readonly Entities _context = new Entities();
        // GET: /Device/
        [BreadCrumb(ControllerName = "Thời gian hoạt động", AreaName = "Báo cáo")]
        public ActionResult Index()
        {
            PrepareDataSearch(_context);
            ViewData["fmType"] = GlobalConstant.IS_NOT_ACTIVE;
            return View();
        }

        [BreadCrumb(ControllerName = "Thời gian phát FM", AreaName = "Báo cáo")]
        public ActionResult Fm()
        {
            PrepareDataSearch(_context);
            ViewData["fmType"] = GlobalConstant.IS_ACTIVE;
            return View("Index");
        }

        public FileResult Export(int year, int month, int unitID = 0, int fmType = GlobalConstant.IS_NOT_ACTIVE)
        {
            var user = SessionManager.GetUserInfoBO();
            if (unitID == 0)
            {
                unitID = user.UnitID.Value;
            }
            var unit = _context.Unit.Find(unitID);
            if (unit == null)
            {
                throw new BusinessException("Không tồn tại đơn vị");
            }
            string sUnitID = "/" + unitID + "/";
            var units = _context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE
            && x.UnitLevel != GlobalConstant.UNIT_TYPE_VILLAGE && x.UnitPath.Contains(sUnitID)).OrderBy(x => x.UnitPath).ToList();
            var listData = (from p in _context.DevicePlayingTime
                            join d in _context.Device on p.DeviceID equals d.DeviceID
                            join u in _context.Unit on d.UnitID equals u.UnitID
                            where (u.UnitID == unitID || (user.IsSupperAdmin == GlobalConstant.IS_ACTIVE || u.UnitPath.Contains(sUnitID)))
                            && p.Year == year && p.Month == month && u.Status == GlobalConstant.IS_ACTIVE && d.Status != GlobalConstant.IS_NOT_ACTIVE
                            && ((fmType == GlobalConstant.IS_NOT_ACTIVE && (d.FmType == null || d.FmType == GlobalConstant.IS_NOT_ACTIVE)) || d.FmType == GlobalConstant.IS_ACTIVE)
                            group new { p, u } by new { u.UnitID, u.UnitPath, p.DeviceID, p.Date } into g
                            select new
                            {
                                g.Key.DeviceID,
                                g.Key.UnitID,
                                g.Key.UnitPath,
                                g.Key.Date,
                                TotalTime = g.Sum(x => x.p.TotalMinus)
                            }).ToList();
            var listDevice = (from d in _context.Device
                              join u in _context.Unit on d.UnitID equals u.UnitID
                              where (u.UnitID == unitID || (user.IsSupperAdmin == GlobalConstant.IS_ACTIVE || u.UnitPath.Contains(sUnitID)))
                              && u.Status == GlobalConstant.IS_ACTIVE && d.Status != GlobalConstant.IS_NOT_ACTIVE
                              && ((fmType == GlobalConstant.IS_NOT_ACTIVE && (d.FmType == null || d.FmType == GlobalConstant.IS_NOT_ACTIVE)) || d.FmType == GlobalConstant.IS_ACTIVE)
                              select new
                              {
                                  d.DeviceID,
                                  d.DeviceName,
                                  u.UnitID,
                                  u.UnitPath
                              }).ToList();
            var parentIds = unit.ParentID == null ? new List<int>() : unit.UnitPath.Split('/').Where(x => !string.IsNullOrWhiteSpace(x)).Select(x => int.Parse(x)).ToList();
            // Bo chinh no ra
            if (parentIds.Any())
            {
                parentIds.RemoveAt(parentIds.Count - 1);
            }
            var parents = _context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE && x.UnitLevel != GlobalConstant.UNIT_TYPE_PROVINCE && parentIds.Contains(x.UnitID)).ToList();
            int days = DateTime.DaysInMonth(year, month);
            string fileName = "Bieu mau bao cao thoi gian hoat dong " + days + ".xlsx";
            string filePath = Server.MapPath("~/Templates/") + fileName;
            Dictionary<int, int> dicPos = new Dictionary<int, int>();
            Dictionary<int, int> dicDevicePos = new Dictionary<int, int>();
            using (MemoryStream ms = new MemoryStream())
            {
                using (FileStream file = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                {
                    byte[] bytes = new byte[file.Length];
                    file.Read(bytes, 0, (int)file.Length);
                    ms.Write(bytes, 0, (int)file.Length);

                    IXVTWorkbook oBook = XVTExcel.OpenWorkbook(ms);
                    IXVTWorksheet tempSheet = oBook.GetSheet(1);
                    int lastCol = 34 - (31 - days);
                    string lastColName = GlobalCommon.GetExcelColumnName(lastCol);
                    var disTemp = tempSheet.GetRange(6, 1, 6, lastCol);
                    var comTemp = tempSheet.GetRange(7, 1, 7, lastCol);
                    var vilTemp = tempSheet.GetRange(8, 1, 8, lastCol);
                    var sumTemp = tempSheet.GetRange(9, 1, 9, lastCol);
                    IXVTWorksheet sheet = oBook.CopySheet(tempSheet, "BCTG");
                    sheet.DeleteRow(9);
                    int startData = 6;
                    string donvi = unit.UnitName.ToUpper();
                    string reportName = donvi + " THÁNG " + month + " NĂM " + year;
                    sheet.SetCellValue(2, 1, reportName);
                    if (fmType == GlobalConstant.IS_ACTIVE)
                    {
                        sheet.SetCellValue(1, 1, "BÁO CÁO THỜI GIAN HOẠT ĐỘNG HỆ THỐNG FM");
                    }
                    var listDicstrictID = new List<int>();
                    var listProvinceDeviceID = new List<int>();
                    var dicParent = new Dictionary<int, List<int>>();
                    if (parents.Any())
                    {
                        units.InsertRange(0, parents);
                    }
                    for (var i = 0; i < units.Count; i++)
                    {
                        var u = units[i];
                        if (u.UnitLevel != GlobalConstant.UNIT_TYPE_DISTRICT && u.ParentID.HasValue)
                        {
                            if (dicParent.ContainsKey(u.ParentID.Value))
                            {
                                dicParent[u.ParentID.Value].Add(u.UnitID);
                            }
                            else
                            {
                                dicParent[u.ParentID.Value] = new List<int>() { u.UnitID };
                            }
                        }
                        switch (u.UnitLevel)
                        {
                            case GlobalConstant.UNIT_TYPE_DISTRICT:
                                disTemp.CopyTo(sheet.GetRange(startData, 1, startData, lastCol));
                                listDicstrictID.Add(u.UnitID);
                                break;
                            case GlobalConstant.UNIT_TYPE_COMMUNE:
                                comTemp.CopyTo(sheet.GetRange(startData, 1, startData, lastCol));
                                break;
                        }
                        if (u.UnitLevel != GlobalConstant.UNIT_TYPE_PROVINCE)
                        {
                            sheet.SetCellValue(startData, 2, u.UnitName);
                            sheet.SetCellFormula(startData, 3, "SUM(D" + startData + ":" + lastColName + startData + ")");
                            dicPos[u.UnitID] = startData;
                            startData++;
                        }
                        var unitData = listData.Where(x => (u.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE && x.UnitPath.Contains("/" + u.UnitID + "/"))
                        || x.UnitID == u.UnitID).ToList();
                        var listUnitDevice = listDevice.Where(x => (u.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE && x.UnitPath.Contains("/" + u.UnitID + "/"))
                        || x.UnitID == u.UnitID).ToList();
                        for (int j = 0; j < listUnitDevice.Count; j++)
                        {
                            vilTemp.CopyTo(sheet.GetRange(startData, 1, startData, lastCol));
                            var device = listUnitDevice[j];
                            sheet.SetCellValue(startData, 1, (j + 1).ToString());
                            sheet.SetCellValue(startData, 2, device.DeviceName);
                            sheet.SetCellFormula(startData, 3, "SUM(D" + startData + ":" + lastColName + startData + ")");
                            var deviceData = unitData.Where(x => x.DeviceID == device.DeviceID).ToList();
                            for (int day = 1; day <= days; day++)
                            {
                                var playingTime = deviceData.Where(x => x.Date == day).Sum(x => x.TotalTime);
                                if (playingTime.HasValue)
                                {
                                    sheet.SetCellValue(startData, 3 + day, Math.Round(playingTime.Value / 60, 1).ToString());
                                }
                            }
                            if (u.UnitLevel == GlobalConstant.UNIT_TYPE_PROVINCE)
                            {
                                listProvinceDeviceID.Add(device.DeviceID);
                            }
                            dicDevicePos[device.DeviceID] = startData;
                            startData++;
                        }
                    }
                    sumTemp.CopyTo(sheet.GetRange(startData, 1, startData, lastCol));
                    sheet.SetCellFormula(startData, 3, "SUM(D" + startData + ":" + lastColName + startData + ")");
                    if (listDicstrictID.Count > 0)
                    {
                        string fSum = string.Empty;
                        foreach (var deviceID in listProvinceDeviceID)
                        {
                            fSum += ", {0}" + dicDevicePos[deviceID];
                        }
                        foreach (var disID in listDicstrictID)
                        {
                            fSum += ", {0}" + dicPos[disID];
                        }
                        fSum = "SUM(" + fSum.Substring(2) + ")";
                        for (int day = 1; day <= days; day++)
                        {
                            sheet.SetCellFormula(startData, 3 + day, string.Format(fSum, GlobalCommon.GetExcelColumnName(day + 3)));
                        }
                    }
                    // Sum
                    int disIndex = 0;
                    foreach (var parentID in dicParent.Keys)
                    {
                        var parent = units.Where(x => x.UnitID == parentID).FirstOrDefault();
                        var listDisDevice = listDevice.Where(x => x.UnitID == parentID).ToList();
                        if (parent.UnitLevel == GlobalConstant.UNIT_TYPE_DISTRICT)
                        {
                            var listChildrentID = dicParent[parentID];
                            disIndex++;
                            sheet.SetCellValue(dicPos[parentID], 1, GlobalCommon.GetExcelColumnName(disIndex));
                            string dSum = string.Empty;
                            foreach (var device in listDisDevice)
                            {
                                dSum += ", {0}" + dicDevicePos[device.DeviceID]; ;
                            }
                            foreach (var childID in listChildrentID)
                            {
                                dSum += ", {0}" + dicPos[childID]; ;
                            }
                            dSum = "SUM(" + dSum.Substring(2) + ")";
                            for (int day = 1; day <= days; day++)
                            {
                                sheet.SetCellFormula(dicPos[parentID], 3 + day, string.Format(dSum, GlobalCommon.GetExcelColumnName(day + 3)));
                            }
                            if (listChildrentID.Any())
                            {
                                for (var j = 0; j < listChildrentID.Count; j++)
                                {
                                    var listComDevice = listDevice.Where(x => x.UnitPath.Contains("/" + listChildrentID[j] + "/")).ToList();
                                    if (listComDevice.Any())
                                    {
                                        string tmp = "{0}" + dicDevicePos[listComDevice[0].DeviceID] + ":" + "{0}" + dicDevicePos[listComDevice[listComDevice.Count - 1].DeviceID];
                                        string cSum = "SUM(" + tmp + ")";
                                        for (int day = 1; day <= days; day++)
                                        {
                                            sheet.SetCellFormula(dicPos[listChildrentID[j]], 3 + day, string.Format(cSum, GlobalCommon.GetExcelColumnName(day + 3)));
                                        }
                                    }
                                    sheet.SetCellValue(dicPos[listChildrentID[j]], 1, GlobalCommon.ToRoman(j + 1));
                                }
                            }
                        }
                    }
                    tempSheet.Delete();
                    Stream streamToWrite = oBook.ToStream();
                    byte[] bytesToWrite = new byte[streamToWrite.Length];
                    streamToWrite.Read(bytesToWrite, 0, bytesToWrite.Length);
                    string newFile = (fmType == GlobalConstant.IS_ACTIVE ? "Thoi gian phat FM don vi " : "Thoi gian hoat dong don vi ") + GlobalCommon.RemoveSign4VietnameseString(unit.UnitName) + " nam " + year + " thang " + month + ".xlsx";
                    return File(bytesToWrite.ToArray(), MimeMapping.GetMimeMapping(fileName), newFile);
                }
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}