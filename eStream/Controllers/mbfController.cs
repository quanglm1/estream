﻿using eStream.Common.CommonClass;
using eStream.Common.CommonObject;
using eStream.Dto;
using Model;
using NLog;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace eStream.Controllers
{
    [ApiAuthorizeFilter]
    public class mbfController : ApiController
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private const int CONNECTED = 1;
        private const int NOT_CONNECTED = 2;

        [HttpGet]
        public MbfUnitDto[] units(int parent_id = 0, int level_type = 0, string name = null)
        {
            using (var context = new Entities())
            {
                var query = context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE);
                if (parent_id > 0)
                {
                    query = query.Where(x => x.ParentID == parent_id);
                }
                if (level_type > 0)
                {
                    query = query.Where(x => x.UnitLevel == level_type);
                }
                if (!string.IsNullOrWhiteSpace(name))
                {
                    name = name.Trim().ToLower();
                    query = query.Where(x => x.UnitName.ToLower().Contains(name));
                }
                return query.Select(x => new MbfUnitDto
                {
                    id = x.UnitID,
                    level_type = x.UnitLevel,
                    name = x.UnitName,
                    parent_id = x.ParentID,
                    phone_number = x.PhoneNumber,
                    email = x.Email,
                    path = x.UnitPath
                }).ToArray();
            }
        }

        [HttpGet]
        public MbfDeviceDto devices(int unit_id = 0, string code = null, string name = null, int page = 1, int size = 10)
        {
            if (unit_id == 0)
            {
                return null;
            }
            using (var context = new Entities())
            {
                var query = context.Device.Where(x => x.Status != GlobalConstant.IS_NOT_ACTIVE);
                string sUnitID = "/" + unit_id + "/";
                query = query.Where(o => context.Unit.Any(u => u.Status != GlobalConstant.IS_NOT_ACTIVE
                        && u.UnitID == o.UnitID && u.UnitPath.Contains(sUnitID)));
                if (!string.IsNullOrWhiteSpace(code))
                {
                    code = code.Trim().ToLower();
                    query = query.Where(x => x.DeviceCode.ToLower().Contains(code));
                }
                if (!string.IsNullOrWhiteSpace(name))
                {
                    name = name.Trim().ToLower();
                    query = query.Where(x => x.DeviceName.ToLower().Contains(name));
                }
                var total = query.Count();
                var devices = query.OrderBy(x => x.DeviceCode).Skip((page - 1) * size).Take(size).Select(x => new MbfDevice
                {
                    id = x.DeviceID,
                    code = x.DeviceCode,
                    name = x.DeviceName,
                    status = x.Status,
                    unit_id = x.UnitID,
                    unit_name = context.Unit.Where(u => u.UnitID == x.UnitID).Select(u => u.UnitName).FirstOrDefault()
                }).ToArray();
                return new MbfDeviceDto
                {
                    data = devices,
                    total = total,
                };
            }
        }

        [HttpGet]
        public MbfDeviceStatusDto device_statuses(int unit_id = 0, string device_code = null, string device_name = null,
                int status = 0, int connect = 0, int page = 1, int size = 10)
        {
            if (unit_id == 0)
            {
                return null;
            }
            using (var context = new Entities())
            {
                var query = context.Device.Where(x => x.Status != GlobalConstant.IS_NOT_ACTIVE);
                string sUnitID = "/" + unit_id + "/";
                query = query.Where(o => context.Unit.Any(u => u.Status != GlobalConstant.IS_NOT_ACTIVE
                        && u.UnitID == o.UnitID && u.UnitPath.Contains(sUnitID)));
                if (!string.IsNullOrWhiteSpace(device_code))
                {
                    device_code = device_code.Trim().ToLower();
                    query = query.Where(x => x.DeviceCode.ToLower().Contains(device_code));
                }
                if (!string.IsNullOrWhiteSpace(device_name))
                {
                    device_name = device_name.Trim().ToLower();
                    query = query.Where(x => x.DeviceName.ToLower().Contains(device_name));
                }
                int total = 0;
                List<MbfDeviceStatus> devices = null;
                List<StatusBO> listDeviceStatus = null;
                int[] deviceIds = null;
                if (status == 0 && connect == 0)
                {
                    devices = query.OrderBy(x => x.DeviceCode).Skip((page - 1) * size).Take(size).Select(x => new MbfDeviceStatus
                    {
                        id = x.DeviceID,
                        code = x.DeviceCode,
                        name = x.DeviceName,
                        status = x.Status,
                        unit_id = x.UnitID,
                        unit_name = context.Unit.Where(u => u.UnitID == x.UnitID).Select(u => u.UnitName).FirstOrDefault()
                    }).ToList();
                    total = query.Count();
                }
                else
                {
                    SystemConfig sc = context.SystemConfig.FirstOrDefault();
                    int timeout = sc != null ? sc.PlayingTimeout.GetValueOrDefault() : 0;
                    var listDevice = query.Select(x => new MbfDeviceStatus
                    {
                        id = x.DeviceID,
                        code = x.DeviceCode,
                        name = x.DeviceName,
                        status = x.Status,
                        unit_id = x.UnitID,
                        unit_name = context.Unit.Where(u => u.UnitID == x.UnitID).Select(u => u.UnitName).FirstOrDefault()
                    }).ToList();

                    deviceIds = listDevice.Select(x => x.id).ToArray();
                    listDeviceStatus = ManagerController.GetDeviceStatus(context, deviceIds);
                    Dictionary<int, int> dicStatus = new Dictionary<int, int>();
                    foreach (StatusBO nowStatus in listDeviceStatus)
                    {
                        if (status > 0 && !ManagerController.IsValidStatus(status, nowStatus))
                        {
                            continue;
                        }
                        if (connect > 0 && !ManagerController.IsValidConnect(connect, nowStatus))
                        {
                            continue;
                        }
                        dicStatus[nowStatus.ID] = GlobalConstant.IS_ACTIVE;
                    }
                    devices = new List<MbfDeviceStatus>();
                    foreach (var device in listDevice)
                    {
                        if (dicStatus.ContainsKey(device.id))
                        {
                            devices.Add(device);
                        }
                    }
                    total = devices.Count;
                    devices = devices.Skip((page - 1) * size).Take(size).ToList();
                }
                foreach (var d in devices)
                {
                    var statusBO = listDeviceStatus.Where(x => x.ID == d.id).FirstOrDefault();
                    if (statusBO != null)
                    {
                        if (statusBO.IsOnline == GlobalConstant.IS_ACTIVE)
                        {
                            d.connect = CONNECTED;
                        }
                        else
                        {
                            d.connect = NOT_CONNECTED;
                        }
                        if (statusBO.IsPlaying == GlobalConstant.IS_ACTIVE)
                        {
                            d.status = GlobalConstant.DEVICE_STATUS_PLAYING;
                        }
                        else if (statusBO.IsStop == GlobalConstant.IS_ACTIVE)
                        {
                            d.status = GlobalConstant.DEVICE_STATUS_STOPPED;
                        }
                        else
                        {
                            d.status = GlobalConstant.DEVICE_STATUS_UNDEFINED;
                        }
                        d.playing_type = statusBO.PlayingType;
                        d.playing_source = statusBO.PlayingSource;
                        if (statusBO.FmStatus == null || statusBO.FmStatus.GetValueOrDefault() < 0)
                        {
                            d.fm_status = GlobalConstant.DEVICE_STATUS_UNDEFINED;
                        }
                        else if (statusBO.FmStatus == GlobalConstant.IS_ACTIVE)
                        {
                            d.fm_status = GlobalConstant.DEVICE_STATUS_PLAYING;
                        }
                        else
                        {
                            d.fm_status = GlobalConstant.DEVICE_STATUS_STOPPED;
                        }
                        if (statusBO.IsFmSwitchOn == GlobalConstant.IS_ACTIVE)
                        {
                            d.fm_connect = CONNECTED;
                        }
                        else
                        {
                            d.fm_connect = NOT_CONNECTED;
                        }
                        d.volume = statusBO.VolumeStatus;
                        d.gps = statusBO.Gps;
                    }
                }
                return new MbfDeviceStatusDto
                {
                    data = devices.ToArray(),
                    total = total
                };
            }
        }


        [HttpGet]
        public MbfDevicePlayingDto[] device_playing(int unit_id = 0, int year = 0, int month = 0, int fm_type = 2)
        {
            if (unit_id == 0 || year == 0 || month == 0)
            {
                return null;
            }
            using (var context = new Entities())
            {
                var unit = context.Unit.Where(x => x.UnitID == unit_id).Select(x => new { x.UnitID, x.UnitPath, x.ParentID }).FirstOrDefault();
                if (unit == null)
                {
                    return null;
                }
                var query = context.Device.Where(x => x.Status != GlobalConstant.IS_NOT_ACTIVE);
                string sUnitID = "/" + unit_id + "/";
                var units = context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE
                    && x.UnitLevel != GlobalConstant.UNIT_TYPE_VILLAGE && x.UnitPath.Contains(sUnitID)).OrderBy(x => x.UnitPath).ToList();
                var listData = (from p in context.DevicePlayingTime
                                join d in context.Device on p.DeviceID equals d.DeviceID
                                join u in context.Unit on d.UnitID equals u.UnitID
                                where (u.UnitID == unit_id || u.UnitPath.Contains(sUnitID))
                                && p.Year == year && p.Month == month && u.Status == GlobalConstant.IS_ACTIVE && d.Status != GlobalConstant.IS_NOT_ACTIVE
                                && ((fm_type == GlobalConstant.IS_ACTIVE && d.FmType == GlobalConstant.IS_ACTIVE) || (d.FmType == null || d.FmType == GlobalConstant.IS_NOT_ACTIVE))
                                group new { p, u } by new { u.UnitID, u.UnitPath, p.DeviceID, p.Date } into g
                                select new
                                {
                                    g.Key.DeviceID,
                                    g.Key.UnitID,
                                    g.Key.UnitPath,
                                    g.Key.Date,
                                    TotalTime = g.Sum(x => x.p.TotalMinus)
                                }).ToList();
                var listDevice = (from d in context.Device
                                  join u in context.Unit on d.UnitID equals u.UnitID
                                  where (u.UnitID == unit_id || u.UnitPath.Contains(sUnitID))
                                  && u.Status == GlobalConstant.IS_ACTIVE && d.Status != GlobalConstant.IS_NOT_ACTIVE
                                  && ((fm_type == GlobalConstant.IS_ACTIVE && d.FmType == GlobalConstant.IS_ACTIVE) || (d.FmType == null || d.FmType == GlobalConstant.IS_NOT_ACTIVE))
                                  select new
                                  {
                                      d.DeviceID,
                                      d.DeviceName,
                                      d.DeviceCode,
                                      u.UnitID,
                                      u.UnitName,
                                      u.UnitPath
                                  }).ToList();
                int days = DateTime.DaysInMonth(year, month);
                var response = new List<MbfDevicePlayingDto>();
                for (var i = 0; i < units.Count; i++)
                {
                    var u = units[i];
                    var unitData = listData.Where(x => (u.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE && x.UnitPath.Contains("/" + u.UnitID + "/"))
                    || x.UnitID == u.UnitID).ToList();
                    var listUnitDevice = listDevice.Where(x => (u.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE && x.UnitPath.Contains("/" + u.UnitID + "/"))
                    || x.UnitID == u.UnitID).ToList();
                    for (int j = 0; j < listUnitDevice.Count; j++)
                    {
                        var device = listUnitDevice[j];
                        var deviceData = unitData.Where(x => x.DeviceID == device.DeviceID).ToList();

                        for (int day = 1; day <= days; day++)
                        {
                            var playingTime = deviceData.Where(x => x.Date == day).Sum(x => x.TotalTime);
                            var obj = new MbfDevicePlayingDto
                            {
                                id = device.DeviceID,
                                code = device.DeviceCode,
                                name = device.DeviceName,
                                unit_id = device.UnitID,
                                unit_name = device.UnitName,
                                unit_path = device.UnitPath,
                                day = day,
                                month = month,
                                year = year,
                                total_time = Math.Round(playingTime.Value / 60, 1)
                            };
                            response.Add(obj);
                        }
                    }
                }
                return response.ToArray();
            }
        }

        [HttpGet]
        public MbfChannelDto[] channels(int unit_id = 0)
        {
            if (unit_id == 0)
            {
                return null;
            }
            using (var context = new Entities())
            {
                return context.ForwardChanel.Where(x => x.Status == GlobalConstant.IS_ACTIVE)
                    .Select(x => new MbfChannelDto
                    {
                        id = x.ChanelID,
                        code = x.ChannelCode,
                        name = x.ChanelName
                    }).ToArray();
            }
        }
    }
}
