﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using eStream.Common.CommonClass;
using eStream.Common.CommonObject;
using eStream.Models;
using Model;
using PagedList;
using eStream.Helpers.Filter;

namespace eStream.Controllers
{
    public class UnitManagerController : BaseController
    {
        private readonly Entities _context = new Entities();

        // GET: UnitManager
        [BreadCrumb(ControllerName = "Đơn vị", ActionName = "Danh sách", AreaName = "Quản trị hệ thống")]
        public ActionResult Index()
        {
            PrepareDataSearch(_context);
            return View();
        }

        public JsonResult LoadChild(int? id = null)
        {
            List<UnitModel> listUnit = GetListUnitChild(id);
            List<NodeBO> listNode = listUnit.Select(x => new NodeBO { id = x.UnitID, text = x.UnitName, children = x.CountChild > 0 }).ToList();
            return Json(listNode, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult SearchUnit(int? parentId, string unitName = null, int page = 1)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            unitName = string.IsNullOrWhiteSpace(unitName) ? null : unitName.Trim().ToLower();
            string sParentId = "/" + parentId.GetValueOrDefault() + "/";
            string sUnitId = "/" + user.UnitID + "/";
            var iQUnit = _context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE
                && (x.UnitPath.StartsWith(sUnitId))
                && (parentId == null || parentId == 0 || x.UnitPath.Contains(sParentId))
                && (unitName == null || x.UnitName.ToLower().Contains(unitName))).Select(x => new UnitModel
                {
                    UnitID = x.UnitID,
                    UnitName = x.UnitName,
                    PhoneNumber = x.PhoneNumber,
                    Email = x.Email,
                    Status = x.Status,
                    UnitLevel = x.UnitLevel,
                    ParentID = x.ParentID,
                    UnitPath = x.UnitPath
                }).OrderBy(x => x.UnitPath).ThenBy(x => x.UnitName);

            var listUnit = iQUnit.ToList();
            PagedList<UnitModel> paginate = new PagedList<UnitModel>(listUnit, page, GlobalConstant.PAGE_SIZE);
            ViewData["listUnit"] = paginate;
            ViewBag.UnitName = unitName;
            return PartialView("_LstUnit");
        }

        public PartialViewResult OpenAddOrEdit(int? id, int? parentId)
        {
            if (id == -1 && parentId.GetValueOrDefault() == 0)
            {
                var highest = _context.Unit.FirstOrDefault(x =>
                    x.UnitLevel == GlobalConstant.UNIT_TYPE_PROVINCE && x.Status == GlobalConstant.IS_ACTIVE);
                if (highest == null)
                {
                    throw new BusinessException("Chưa chọn cấp cha");
                }

                parentId = highest.UnitID;
            }
            UnitModel model = null;
            if (id.HasValue && id > 0)
            {
                UserInfoBO user = SessionManager.GetUserInfoBO();
                model = _context.Unit.Where(x => x.UnitID == id && x.Status == GlobalConstant.IS_ACTIVE).Select(x => new UnitModel
                {
                    UnitID = x.UnitID,
                    UnitName = x.UnitName,
                    PhoneNumber = x.PhoneNumber,
                    Email = x.Email,
                    Status = x.Status,
                    UnitPath = x.UnitPath,
                    ParentID = x.ParentID
                }).FirstOrDefault();
                if (model == null)
                {
                    throw new BusinessException("Không tồn tại đơn vị đã chọn");
                }
                string sUnitId = "/" + user.UnitID + "/";
                if (!model.UnitPath.Contains(sUnitId))
                {
                    throw new BusinessException("Bạn không có quyền quản lý đơn vị đang chọn");
                }
                parentId = model.ParentID;
            }
            if (parentId != null && parentId > 0)
            {
                UnitModel parent = _context.Unit.Where(x => x.UnitID == parentId && x.Status == GlobalConstant.IS_ACTIVE).Select(x => new UnitModel
                {
                    UnitID = x.UnitID,
                    UnitName = x.UnitName,
                    PhoneNumber = x.PhoneNumber,
                    Email = x.Email,
                    Status = x.Status,
                    UnitLevel = x.UnitLevel
                }).FirstOrDefault();
                if (parent?.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE)
                {
                    throw new BusinessException("Chỉ thêm đơn vị đến cấp xã/phường");
                }
                ViewData["parent"] = parent;
                if (id == null || id <= 0)
                {
                    model = new UnitModel { ParentID = parentId };
                }
            }
            return PartialView("_AddOrEdit", model);
        }

        private List<UnitModel> GetListUnitChild(int? parentId)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            string sUnitId = "/" + user.UnitID + "/";
            return _context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE
            && x.ParentID == parentId && x.UnitPath.StartsWith(sUnitId))
            .Select(x => new UnitModel
            {
                UnitID = x.UnitID,
                UnitName = x.UnitName,
                PhoneNumber = x.PhoneNumber,
                Email = x.Email,
                Status = x.Status,
                UnitLevel = x.UnitLevel,
                ParentID = x.ParentID,
                UnitPath = x.UnitPath,
                CountChild = _context.Unit.Count(c => c.ParentID == x.UnitID && c.Status == GlobalConstant.IS_ACTIVE)
            }).OrderBy(x => x.UnitPath).ThenBy(x => x.UnitName).ToList();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult AddOrEdit(UnitModel unit)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();

            if (ModelState.IsValid)
            {
                Unit parent = null;
                if (unit.ParentID != null && unit.ParentID > 0)
                {
                    parent = _context.Unit.FirstOrDefault(x => x.UnitID == unit.ParentID && x.Status == GlobalConstant.IS_ACTIVE);
                    if (parent == null)
                    {
                        throw new BusinessException("Không tồn tại đơn vị cha đã chọn");
                    }
                    if (parent.UnitLevel == GlobalConstant.UNIT_TYPE_VILLAGE)
                    {
                        throw new BusinessException("Hệ thống chỉ quản lý tới cấp xã/phường, không được phép chọn xã/phường là đơn vị cha");
                    }
                } 
                else
                {
                    parent = _context.Unit.FirstOrDefault(x =>
                    x.UnitLevel == GlobalConstant.UNIT_TYPE_PROVINCE && x.Status == GlobalConstant.IS_ACTIVE);
                    if (parent == null)
                    {
                        throw new BusinessException("Chưa chọn cấp cha");
                    }                    
                }
                // Kiem tra don vi
                bool isInsert = false;
                Unit entity;
                string msg;
                if (unit.UnitID > 0)
                {
                    entity = _context.Unit.FirstOrDefault(x => x.UnitID == unit.UnitID && x.Status == GlobalConstant.IS_ACTIVE);
                    if (entity == null)
                    {
                        throw new BusinessException("Không tồn tại đơn vị đã chọn");
                    }
                    entity = UpdateUnit(unit, entity);
                    msg = "Cập nhật đơn vị thành công";
                }
                else
                {
                    entity = new Unit();
                    entity = UpdateUnit(unit, entity);
                    entity.Status = GlobalConstant.IS_ACTIVE;
                    msg = "Thêm mới đơn vị thành công";
                    isInsert = true;
                }
                string sUnitId = "/" + user.UnitID + "/";
                if (parent != null && !parent.UnitPath.StartsWith(sUnitId))
                {
                    throw new BusinessException("Bạn không có quyền quản lý đơn vị đang chọn");
                }
                if (isInsert)
                {
                    _context.Unit.Add(entity);
                }
                else
                {
                    if (parent != null && !entity.UnitPath.StartsWith(sUnitId))
                    {
                        throw new BusinessException("Bạn không có quyền quản lý đơn vị đang chọn");
                    }
                }
                _context.SaveChanges();
                // Cha con
                if (isInsert)
                {
                    if (parent == null)
                    {
                        entity.UnitLevel = GlobalConstant.UNIT_TYPE_PROVINCE;
                        entity.UnitPath = "/" + entity.UnitID + "/";
                    }
                    else
                    {
                        entity.UnitLevel = parent.UnitLevel + 1;
                        entity.ParentID = parent.UnitID;
                        entity.UnitPath = parent.UnitPath + entity.UnitID + "/";
                    }
                    _context.SaveChanges();
                }
                return Json(new { msg, id = entity.ParentID }, JsonRequestBehavior.AllowGet);
            }

            throw new BusinessException("Dữ liệu không hợp lệ");
        }

        private Unit UpdateUnit(UnitModel unit, Unit entity)
        {
            entity.UnitName = unit.UnitName.Trim();
            entity.PhoneNumber = string.IsNullOrWhiteSpace(unit.PhoneNumber) ? null : unit.PhoneNumber.Trim();
            entity.Email = string.IsNullOrWhiteSpace(unit.Email) ? null : unit.Email.Trim();
            return entity;
        }

        public JsonResult Delete(int id)
        {
            Unit unit = _context.Unit.Find(id);
            if (unit == null)
            {
                throw new BusinessException("Không tồn tại đơn vị đã chọn");
            }
            unit.Status = GlobalConstant.IS_NOT_ACTIVE;
            _context.SaveChanges();
            return Json("Xóa đơn vị thành công", JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult LoadUnit(int? unitId)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            string sUnit = "/" + user.UnitID + "/";
            Unit unit = _context.Unit.FirstOrDefault(x => x.Status == GlobalConstant.IS_ACTIVE && x.UnitLevel == GlobalConstant.UNIT_TYPE_DISTRICT
                                                                                              && x.UnitID == unitId && x.UnitPath.Contains(sUnit));
            List<UnitModel> listUnit;
            if (unit != null)
            {
                sUnit = "/" + unitId + "/";
                listUnit = _context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE && x.UnitPath.Contains(sUnit) && x.UnitLevel != GlobalConstant.UNIT_TYPE_VILLAGE)
                    .Select(x => new UnitModel
                    {
                        UnitID = x.UnitID,
                        UnitName = x.UnitName,
                        PhoneNumber = x.PhoneNumber,
                        Email = x.Email,
                        Status = x.Status,
                        UnitLevel = x.UnitLevel,
                        ParentID = x.ParentID,
                        UnitPath = x.UnitPath
                    }).OrderBy(x => x.UnitPath).ThenBy(x => x.UnitName).ToList();
            }
            else
            {
                var unitPro = _context.Unit.Find(unitId);
                listUnit = new List<UnitModel>();
                if (unitPro != null)
                {
                    UnitModel m = new UnitModel();
                    m.ToModel(unitPro);
                    listUnit.Add(m);
                }
            }
            return PartialView("_Unit", listUnit);
        }
    }
}