﻿using eStream.Common.CommonObject;
using eStream.Common.CommonClass;
using eStream.Helpers.Filter;
using eStream.Models;
using Model;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eStream.Controllers
{
    public class ChannelController : BaseController
    {
        private readonly Entities _context = new Entities();
        // GET: /Channel/
        [BreadCrumb(ControllerName = "Kênh chuyển tiếp", ActionName = "Danh sách", AreaName = "Quản trị hệ thống")]
        public ActionResult Index()
        {
            PrepareDataSearch(_context);
            return View();
        }

        private string Format(string s)
        {
            if (string.IsNullOrWhiteSpace(s))
            {
                return null;
            }
            return s.Trim().ToLower();
        }

        public PartialViewResult SearchChannel(int? UnitID, string SearchText = null, int page = 1)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            SearchText = Format(SearchText);
            var query = from d in _context.ForwardChanel
                        where d.Status != GlobalConstant.IS_NOT_ACTIVE
                        select d;
            string sUnitID = "/" + user.UnitID + "/";
            if (UnitID.GetValueOrDefault() > 0)
            {
                string sUnitSearchID = "/" + UnitID.GetValueOrDefault() + "/";
                query = query.Where(o => _context.Unit.Any(u => u.Status != GlobalConstant.IS_NOT_ACTIVE
                        && u.UnitID == o.UnitID && (user.IsSupperAdmin == GlobalConstant.IS_ACTIVE || u.UnitPath.Contains(sUnitID))
                        && u.UnitPath.Contains(sUnitSearchID)));
            }
            else
            {
                query = query.Where(o => (o.UnitID == null && user.IsSupperAdmin == GlobalConstant.IS_ACTIVE)
                        || _context.Unit.Any(u => u.Status != GlobalConstant.IS_NOT_ACTIVE
                        && u.UnitID == o.UnitID && (user.IsSupperAdmin == GlobalConstant.IS_ACTIVE || u.UnitPath.Contains(sUnitID))));
            }
            if (SearchText != null)
            {
                query = query.Where(x => x.ChannelCode.Contains(SearchText) || x.ChanelName.Contains(SearchText));
            }
            var iQChannel = query.Select(x => new ChannelModel
            {
                ChanelID = x.ChanelID,
                UnitID = x.UnitID,
                UnitName = _context.Unit.Where(u => u.UnitID == x.UnitID).Select(u => u.UnitName).FirstOrDefault(),
                ChannelCode = x.ChannelCode,
                ChanelName = x.ChanelName,
                NumberOrder = x.NumberOrder,
                LinkUrl = x.LinkUrl,
                IsGlobal = x.UnitID == null,
                IsDefault = x.IsDefault == GlobalConstant.IS_ACTIVE,
                IsFm = x.Fm == GlobalConstant.IS_ACTIVE,
                FmFreq = x.FmFreq
            }).OrderByDescending(x => x.UnitID == null ? 0 : x.UnitID).ThenBy(x => x.NumberOrder).ThenBy(x => x.ChannelCode);

            PagedList<ChannelModel> paginate = new PagedList<ChannelModel>(iQChannel, page, GlobalConstant.PAGE_SIZE);
            ViewData["listChannel"] = paginate;
            ViewBag.SearchText = SearchText;
            return PartialView("_LstChannel");
        }

        public PartialViewResult OpenAddOrEdit(int? unitID, int? id)
        {
            ChannelModel model = new ChannelModel
            {
                UnitID = unitID
            };
            UserInfoBO user = SessionManager.GetUserInfoBO();
            string sUnitSearchID = "/" + unitID.GetValueOrDefault() + "/";
            string sUnitID = "/" + user.UnitID + "/";
            if (id.HasValue && id > 0)
            {
                var iquery = _context.ForwardChanel.Where(x => x.ChanelID == id && x.Status != GlobalConstant.IS_NOT_ACTIVE);
                if (unitID == null || unitID == 0)
                {
                    iquery = iquery.Where(o => (o.UnitID == null && user.IsSupperAdmin == GlobalConstant.IS_ACTIVE)
                            || _context.Unit.Any(u => u.Status != GlobalConstant.IS_NOT_ACTIVE
                            && u.UnitID == o.UnitID && (user.IsSupperAdmin == GlobalConstant.IS_ACTIVE || u.UnitPath.Contains(sUnitID))));
                }
                else
                {
                    iquery = iquery.Where(o => _context.Unit.Any(u => u.Status != GlobalConstant.IS_NOT_ACTIVE
                            && u.UnitID == o.UnitID && (user.IsSupperAdmin == GlobalConstant.IS_ACTIVE || u.UnitPath.Contains(sUnitID))
                            && u.UnitPath.Contains(sUnitSearchID)));
                }
                model = iquery.Select(x => new ChannelModel
                {
                    ChanelID = x.ChanelID,
                    UnitID = x.UnitID,
                    ChannelCode = x.ChannelCode,
                    ChanelName = x.ChanelName,
                    NumberOrder = x.NumberOrder,
                    LinkUrl = x.LinkUrl,
                    IsGlobal = x.UnitID == null,
                    IsDefault = x.IsDefault == GlobalConstant.IS_ACTIVE,
                    IsFm = x.Fm == GlobalConstant.IS_ACTIVE,
                    FmFreq = x.FmFreq
                }).FirstOrDefault();
                if (model == null)
                {
                    throw new BusinessException("Không tồn tại kênh đã chọn");
                }
            }
            return PartialView("_AddOrEdit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult AddOrEdit(ChannelModel Channel)
        {
            string msg;
            ForwardChanel entity = null;
            if (ModelState.IsValid)
            {
                UserInfoBO user = SessionManager.GetUserInfoBO();
                if (user.IsAdmin.GetValueOrDefault() == 0 && user.IsSupperAdmin.GetValueOrDefault() == 0)
                {
                    throw new BusinessException("Bạn không có quyền thực hiện chức năng này");
                }
                if (Channel.ChanelID > 0)
                {
                    if (!Channel.IsFm && string.IsNullOrWhiteSpace(Channel.LinkUrl))
                    {
                        throw new BusinessException("Đường dẫn của kênh không được để trống");
                    }
                    if (Channel.IsFm && Channel.FmFreq == null)
                    {
                        throw new BusinessException("Kênh FM không được để trống tần số");
                    }
                    if (_context.ForwardChanel.Where(x => x.ChannelCode == Channel.ChannelCode && x.Status != GlobalConstant.IS_NOT_ACTIVE
                     && (x.UnitID == null || x.UnitID == user.UnitID) && x.ChanelID != Channel.ChanelID).Any())
                    {
                        throw new BusinessException("Mã kênh đã tồn tại");
                    }
                    var iquery = _context.ForwardChanel.Where(x => x.ChanelID == Channel.ChanelID && x.Status != GlobalConstant.IS_NOT_ACTIVE);
                    var unitID = Channel.UnitID;
                    string sUnitSearchID = "/" + unitID.GetValueOrDefault() + "/";
                    string sUnitID = "/" + user.UnitID + "/";
                    if (unitID == null || unitID == 0)
                    {
                        iquery = iquery.Where(o => (o.UnitID == null && user.IsSupperAdmin == GlobalConstant.IS_ACTIVE)
                                || _context.Unit.Any(u => u.Status != GlobalConstant.IS_NOT_ACTIVE
                                && u.UnitID == o.UnitID && (user.IsSupperAdmin == GlobalConstant.IS_ACTIVE || u.UnitPath.Contains(sUnitID))));
                    }
                    else
                    {
                        iquery = iquery.Where(o => _context.Unit.Any(u => u.Status != GlobalConstant.IS_NOT_ACTIVE
                                && u.UnitID == o.UnitID && (user.IsSupperAdmin == GlobalConstant.IS_ACTIVE || u.UnitPath.Contains(sUnitID))
                                && u.UnitPath.Contains(sUnitSearchID)));
                    }
                    entity = iquery.FirstOrDefault();
                    if (entity == null)
                    {
                        throw new BusinessException("Không tồn tại kênh đã chọn");
                    }
                    Channel.UpdateEntity(entity, user);
                    msg = "Cập nhật kênh thành công";
                }
                else
                {
                    if (_context.ForwardChanel.Where(x => x.ChannelCode == Channel.ChannelCode && x.Status != GlobalConstant.IS_NOT_ACTIVE
                     && (x.UnitID == null || x.UnitID == user.UnitID)).Any())
                    {
                        throw new BusinessException("Mã kênh đã tồn tại");
                    }
                    entity = new ForwardChanel
                    {
                        Status = GlobalConstant.IS_ACTIVE
                    };
                    if (Channel.UnitID.GetValueOrDefault() == 0)
                    {
                        var root = _context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE && x.ParentID == null).FirstOrDefault();
                        if (root != null)
                        {
                            entity.UnitID = root.UnitID;
                        }
                    }
                    else
                    {
                        entity.UnitID = Channel.UnitID;
                    }
                    Channel.UpdateEntity(entity, user);
                    _context.ForwardChanel.Add(entity);
                    msg = "Thêm mới kênh thành công";
                }
                _context.SaveChanges();
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            else
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
        }

        public JsonResult Delete(int id)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            var iquery = _context.ForwardChanel.Where(x => x.ChanelID == id && x.Status != GlobalConstant.IS_NOT_ACTIVE);
            if (user.IsSupperAdmin.GetValueOrDefault() == GlobalConstant.IS_ACTIVE)
            {
                iquery = iquery.Where(x => x.UnitID == null || x.UnitID == user.UnitID);
            }
            else
            {
                iquery = iquery.Where(x => x.UnitID == user.UnitID);
            }
            var Channel = iquery.FirstOrDefault();
            if (Channel == null)
            {
                throw new BusinessException("Không tồn tại kênh chuyển tiếp đã chọn");
            }
            Channel.Status = GlobalConstant.IS_NOT_ACTIVE;
            _context.SaveChanges();
            return Json("Xóa kênh chuyển tiếp thành công", JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}