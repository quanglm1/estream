﻿using eStream.Common.CommonClass;
using eStream.Common.CommonObject;
using eStream.Dto;
using eStream.Helpers.Filter;
using eStream.Models;
using Model;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace eStream.Controllers
{
    public partial class ControlController
    {
        public PartialViewResult OpenCloneScheduleDay(int ScheduleDayID, DateTime Date)
        {
            var scheduleDay = _context.ScheduleDay.Find(ScheduleDayID);
            var schedule = _context.Schedule.Where(x => x.ScheduleID == scheduleDay.ScheduleID)
                .Select(x => new ScheduleModel
                {
                    ScheduleID = x.ScheduleID,
                    CreateDate = x.CreateDate,
                    Frequency = x.Frequency,
                    OffHour = x.OffHour,
                    OffMinus = x.OffMinus,
                    ScheduleHour = x.ScheduleHour,
                    ScheduleMinus = x.ScheduleMinus,
                    ScheduleName = x.ScheduleName,
                    Status = x.Status
                }).FirstOrDefault();
            var listSchedule = GetAllSchedule();
            var listScheduleItem = new List<SelectListItem>();
            foreach (var s in listSchedule)
            {
                listScheduleItem.Add(new SelectListItem
                {
                    Value = s.ScheduleID.ToString(),
                    Text = s.ScheduleName + " " + s.ScheduleFullTime + " - " + s.OffFullTime
                });
            }
            ViewData["Date"] = Date;
            TimeSpan TimeExecute = new TimeSpan(schedule.ScheduleHour.GetValueOrDefault(), schedule.ScheduleMinus.GetValueOrDefault(), 0);
            ViewData["ExStartDate"] = Date.Date + TimeExecute;
            ViewData["ListSchedule"] = listScheduleItem;
            ViewData["Schedule"] = schedule;
            var model = new CloneScheduleDayModel
            {
                ScheduleID = schedule.ScheduleID,
                ScheduleDayID = scheduleDay.ScheduleDayID,
                Date = Date
            };
            return PartialView("_Clone", model);
        }

        public JsonResult CloneScheduleDay(CloneScheduleDayModel model)
        {
            DateTime cloneDate;
            if (!DateTime.TryParseExact(model.CloneDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out cloneDate))
            {
                throw new BusinessException("Ngày copy không hợp lệ");
            }
            if (cloneDate.CompareTo(model.Date) == 0 && model.CloneScheduleID == model.ScheduleID)
            {
                throw new BusinessException("Copy trùng khung thời gian và ngày");
            }
            var startWeek = cloneDate.StartOfWeek(DayOfWeek.Monday);
            int day = 1 + (cloneDate - startWeek).Days;
            var cloneScheduleDay = _context.ScheduleDay.Where(x => x.DayOfWeek == day && x.ScheduleID == model.CloneScheduleID).FirstOrDefault();
            if (cloneScheduleDay == null)
            {
                throw new BusinessException("Ngày " + model.CloneDate + " không có chương trình phát");
            }
            UserInfoBO user = SessionManager.GetUserInfoBO();
            var clonePrograms = _context.Program.Where(x => x.ScheduleDayID == cloneScheduleDay.ScheduleDayID && x.UnitID == user.UnitID
                                    && DbFunctions.TruncateTime(x.ExecuteDate) == DbFunctions.TruncateTime(cloneDate)).ToList();
            foreach (var cp in clonePrograms)
            {
                Program newProgram = new Program
                {
                    ScheduleDayID = model.ScheduleDayID,
                    FileUrl = cp.FileUrl,
                    Status = cp.Status,
                    UnitID = user.UnitID.Value,
                    FileName = cp.FileName,
                    Title = cp.Title,
                    ExecuteDate = model.Date,
                    CreatedUser = user.UserName,
                    CreatedDate = DateTime.Now,
                    NumberOrder = cp.NumberOrder,
                    Duration = cp.Duration,
                    SplitedFolder = cp.SplitedFolder
                };
                _context.Program.Add(newProgram);
            }
            var programChannels = _context.ProgramChanel.Where(x => x.ScheduleDayID == cloneScheduleDay.ScheduleDayID && x.UnitID == user.UnitID
                                    && DbFunctions.TruncateTime(x.DateExecute) == DbFunctions.TruncateTime(cloneDate)).ToList();
            foreach (var pc in programChannels)
            {
                var newProChannel = new ProgramChanel
                {
                    ChanelID = pc.ChanelID,
                    DateExecute = model.Date,
                    ScheduleDayID = model.ScheduleDayID,
                    UnitID = user.UnitID.Value,
                    Status = pc.Status
                };
                _context.ProgramChanel.Add(newProChannel);
            }
            _context.SaveChanges();
            return Json("Copy chương trình phát thành công", JsonRequestBehavior.AllowGet);
        }

        private string SetSchedule(int unitID)
        {
            SystemConfig sc = _context.SystemConfig.FirstOrDefault();
            int waitingAutioDur = (sc != null && sc.IsWaitingOn.GetValueOrDefault() == GlobalConstant.IS_ACTIVE) ? GlobalConstant.WAITING_AUDIO_DURATION : 1;
            ScheduleBO currentSchedule = ScheduleCaching.GetListScheduleNow(unitID, waitingAutioDur).OrderByDescending(x => x.ScheduleID).FirstOrDefault();
            if (currentSchedule == null)
            {
                return string.Empty;
            }
            DateTime currentDt = DateTime.Now;
            long hourOfDay = currentDt.Hour;
            long minuteOfDay = currentDt.Minute;
            long minutes = 60 * hourOfDay + minuteOfDay;
            long seconds = 60 * minutes + currentDt.Second;

            int dayOfWeek = currentDt.DayOfWeek == DayOfWeek.Sunday ? dayOfWeek = 7 : (int)currentDt.DayOfWeek;
            ScheduleDay scheduleDay = (from a in _context.ScheduleDay
                                       where a.ScheduleID == currentSchedule.ScheduleID
                                       && a.DayOfWeek == dayOfWeek
                                       select a).FirstOrDefault();
            if (scheduleDay == null)
            {
                return string.Empty;
            }

            Unit unit = _context.Unit.Find(unitID);
            var loopCount = _context.ProgramLoop.Where(x => x.ScheduleDayID == scheduleDay.ScheduleDayID
                                        && x.Status == GlobalConstant.IS_ACTIVE
                                        && _context.Unit.Any(u => u.UnitID == x.UnitID && unit.UnitPath.Contains(u.UnitPath))
                                        && DbFunctions.TruncateTime(x.DateExecute) == DbFunctions.TruncateTime(currentDt))
                                        .Select(x => x.LoopCount).FirstOrDefault();
            var loop = 1;
            if (loopCount.GetValueOrDefault() > 0)
            {
                loop = loopCount.GetValueOrDefault();
            }
            OrderConfig config = _context.OrderConfig.First();
            var totalDuration = (from p in _context.Program
                                 join u in _context.Unit on p.UnitID equals u.UnitID
                                 where p.ScheduleDayID == scheduleDay.ScheduleDayID
                                 && p.Status == GlobalConstant.IS_APPROVED
                                 && DbFunctions.TruncateTime(p.ExecuteDate) == DbFunctions.TruncateTime(currentDt)
                                 && unit.UnitPath.Contains(u.UnitPath)
                                 select p.Duration).DefaultIfEmpty(0).Sum();

            long differTime = seconds - currentSchedule.StartMinus.Value * 60;
            if (differTime < loop * totalDuration.GetValueOrDefault())
            {
                return "/Control/Live?unitID=" + unitID + "&bitrate=" + GlobalConstant.DEFAULT_BIT_RATE;
            }
            // Lenh thong tin VOV
            List<int> listChanelID = (from a in _context.ProgramChanel
                                      join b in _context.Unit on a.UnitID equals b.UnitID
                                      where a.ScheduleDayID == scheduleDay.ScheduleDayID
                                      && unit.UnitPath.Contains(b.UnitPath)
                                      && DbFunctions.TruncateTime(a.DateExecute) == DbFunctions.TruncateTime(currentDt)
                                      select a.ChanelID).ToList();
            if (listChanelID.Any())
            {
                var chanelID = listChanelID[0];
                if (chanelID > 0)
                {
                    ForwardChanel fc = _context.ForwardChanel.Find(chanelID);
                    if (fc != null)
                    {
                        return Crypto.DecryptString(fc.LinkUrl);
                    }
                }
            }
            else
            {
                ForwardChanel fc = _context.ForwardChanel.Where(x => x.IsDefault == GlobalConstant.IS_ACTIVE).FirstOrDefault();
                if (fc != null)
                {
                    return Crypto.DecryptString(fc.LinkUrl);
                }
            }
            return string.Empty;
        }

        private string SetLive(int unitID)
        {
            Unit unit = _context.Unit.Find(unitID);
            OrderConfig config = _context.OrderConfig.First();
            DateTime currentDt = DateTime.Now;
            long hourOfDay = currentDt.Hour;
            long minuteOfDay = currentDt.Minute;
            long minutes = 60 * hourOfDay + minuteOfDay;
            long seconds = 60 * minutes + currentDt.Second;
            var live = (from l in _context.Live
                        join u in _context.Unit on l.UnitID equals u.UnitID
                        where l.Status >= GlobalConstant.LIVE_PLAYING
                        && unit.UnitPath.Contains(u.UnitPath)
                       && (l.LiveSource != GlobalConstant.LIVE_SOURCE_FILE
                                     || (l.TotalDuration != null && (l.PlayTime == null || DbFunctions.DiffSeconds(l.PlayTime, currentDt) < l.TotalDuration * (1 + (l.LoopCount == null ? 0 : l.LoopCount)))))
                        orderby (u.UnitLevel == GlobalConstant.UNIT_TYPE_VILLAGE ? config.VillageOrder
                                : u.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE ? config.CommuneOrder
                                : u.UnitLevel == GlobalConstant.UNIT_TYPE_DISTRICT ? config.DistrictOrder
                                : u.UnitLevel == GlobalConstant.UNIT_TYPE_PROVINCE ? config.ProvinceOrder : 0),
                                l.LiveID
                        select l).FirstOrDefault();
            if (live == null)
            {
                return string.Empty;
            }
            string link = "/Live/Live?unitID=" + unitID + "&bitrate=" + GlobalConstant.DEFAULT_BIT_RATE;
            if (live.LiveSource.GetValueOrDefault() == GlobalConstant.LIVE_SOURCE_VOV)
            {
                if (live.ChannelID != null)
                {
                    var channel = _context.ForwardChanel.Find(live.ChannelID);
                    if (channel != null)
                    {
                        link = Crypto.DecryptString(channel.LinkUrl);
                    }
                }
            }
            return link;
        }

        public PartialViewResult OpenPlay(int unitID = 0)
        {
            if (unitID == 0)
            {
                var user = SessionManager.GetUserInfoBO();
                unitID = user.UnitID.Value;
            }
            var link = SetLive(unitID);
            if (string.IsNullOrWhiteSpace(link))
            {
                link = SetSchedule(unitID);
            }
            if (string.IsNullOrWhiteSpace(link))
            {
                throw new BusinessException("Hiện tại chưa có chương trình phát");
            }
            ViewData["Link"] = link;
            return PartialView("_Play");
        }
    }
}