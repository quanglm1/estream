﻿using eStream.Common.CommonClass;
using eStream.Common.CommonObject;
using eStream.Models;
using Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace eStream.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var user = SessionManager.GetUserInfoBO();
            if (user != null)
            {
                return RedirectToAction("Index", "Control");
            }
            return RedirectToAction("LogIn", "Home");
        }
        [HttpGet]
        [AllowAnonymous]
        public ActionResult LogIn()
        {
            if (User.Identity.IsAuthenticated)
            {
                //chuyen tu cookie vao session
                try
                {
                    SessionManager.TransferSession(User.Identity.Name);
                    return RedirectToAction("Index", "Home");
                }
                catch
                {
                    return View(new LoginModel());
                }
            }

            LoginModel objLogin = new LoginModel();
            return View(objLogin);
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult LogIn(LoginModel objLogin)
        {
            if (ModelState.IsValid)
            {
                string encriptPass = GlobalCommon.Encrypt(objLogin.Password);
                using (Entities context = new Entities())
                {
                    var user = context.User.FirstOrDefault(a => a.UserName.Equals(objLogin.UserName)
                                && a.Password.Equals(encriptPass) && a.Status == GlobalConstant.IS_ACTIVE);
                    if (user != null)
                    {
                        if (user.Status == GlobalConstant.IS_NOT_ACTIVE)
                        {
                            throw new BusinessException("Tài khoản đã bị khóa, yêu cầu liên hệ với quản trị hệ thống");
                        }
                    }
                    else
                    {
                        throw new BusinessException("Sai thông tin đăng nhập");
                    }
                    var config = context.SystemConfig.First();
                    if (config != null && config.IsUseOTP.GetValueOrDefault() == GlobalConstant.IS_ACTIVE)
                    {
                        string code = GlobalCommon.GetRandomCode(6);
                        int timeExp = config.ExpOTP.HasValue ? config.ExpOTP.Value : 5;
                        string content = "Ma OTP dang nhap la " + code + ", hieu luc " + timeExp + " phut";
                        GlobalCommon.SendSmsJson(user.PhoneNumber, content, user.UnitID.GetValueOrDefault());
                        user.OTPCode = code.Trim();
                        user.OTPExpireTime = DateTime.Now.AddMinutes(timeExp);
                        context.SaveChanges();
                        var userName = Crypto.EncryptString(user.UserID.ToString());
                        ViewData["UserName"] = userName;
                        return PartialView("OTP");
                    }
                }
                //Check validate
                SessionManager.TransferSession(objLogin.UserName);
                FormsAuthentication.SetAuthCookie(objLogin.UserName, objLogin.RememberMe);
                return Json(new { RedirectUrl = Url.Action("Index", "Control") }, JsonRequestBehavior.AllowGet);
            }
            throw new BusinessException("Sai thông tin đăng nhập");
        }

        [AllowAnonymous]
        [HttpPost]
        public JsonResult OTP(string UserName, string OTP)
        {
            int userID = int.Parse(Crypto.DecryptString(UserName));
            using (Entities context = new Entities())
            {
                var user = context.User.FirstOrDefault(a => a.UserID == userID);
                if (user != null)
                {
                    if (user.Status == GlobalConstant.IS_NOT_ACTIVE)
                    {
                        throw new BusinessException("Tài khoản đã bị khóa, yêu cầu liên hệ với quản trị hệ thống");
                    }
                }
                else
                {
                    throw new BusinessException("Sai thông tin đăng nhập");
                }
                if (DateTime.Now <= user.OTPExpireTime)
                {
                    if (user.OTPCode.Trim() == user.OTPCode.Trim())
                    {
                        SessionManager.TransferSession(user.UserName);
                        FormsAuthentication.SetAuthCookie(user.UserName, false);
                        return Json(new { RedirectUrl = Url.Action("Index", "Control") }, JsonRequestBehavior.AllowGet);
                    }
                    throw new BusinessException("Mã OTP không đúng, vui lòng thử lại");
                }
                throw new BusinessException("Mã OTP hết hạn, vui lòng thử lại");
            }
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult LogOut()
        {
            UserInfoBO objUser = SessionManager.GetUserInfoBO();
            if (objUser == null)
            {
                return RedirectToAction("LogIn", "Home");
            }
            SessionManager.Clear();
            FormsAuthentication.SignOut();
            return RedirectToAction("LogIn", "Home");
        }

        [AllowAnonymous]
        public ActionResult ForgetPassword()
        {
            return View(new LoginModel());
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult ForgetPasswordStep1(string userName)
        {
            if (!string.IsNullOrEmpty(userName))
            {
                using (Entities context = new Entities())
                {
                    var obj = context.User.FirstOrDefault(a => a.UserName.ToLower().Equals(userName.ToLower().Trim()));
                    if (obj != null)
                    {
                        UserInfoBO objUserInfo = new UserInfoBO { UserID = obj.UserID };

                        if (!string.IsNullOrEmpty(obj.PhoneNumber) && obj.PhoneNumber.Length > 9)
                        {
                            if (obj.PhoneNumber.Contains("+84"))
                            {
                                obj.PhoneNumber.Replace(" ", "");
                                obj.PhoneNumber.Replace("+84", "0");
                            }
                            string temString = "";
                            for (int i = 0; i < obj.PhoneNumber.Length - 3; i++)
                            {
                                temString += "*";
                            }
                            var phone = obj.PhoneNumber.Substring(obj.PhoneNumber.Length - 2, 2);
                            objUserInfo.PhoneNumber = "0" + temString + phone;

                            return PartialView("ForgetPasswordStep1", objUserInfo);
                        }

                        throw new BusinessException("Người dùng chưa đăng ký số điện thoại, liên hệ hỗ trợ kỹ thuật");
                    }
                    throw new BusinessException("Sai thông tin đăng nhập");
                }
            }
            throw new BusinessException("Tên đăng nhập không được để trống");
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult ForgetPasswordStep2(string phonNum, int userId)
        {
            if (string.IsNullOrEmpty(phonNum))
            {
                throw new BusinessException("Chưa nhập số điện thoại");
            }

            if (userId < 1)
            {
                throw new BusinessException("Dữ liệu không hợp lệ, vui lòng thử lại");
            }

            using (Entities context = new Entities())
            {
                var user = context.User.Find(userId);
                if (user == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ, vui lòng thử lại");
                }

                if (user.PhoneNumber.Contains("+84"))
                {
                    user.PhoneNumber.Replace(" ", "");
                    user.PhoneNumber.Replace("+84", "0");
                }

                if (user.PhoneNumber == phonNum.Trim())
                {
                    string code = GlobalCommon.GetRandomCode(6);
                    //gửi tin nhắn newPass
                    string content = "Ma bao mat cua ban la: " + code;
                    if (GlobalCommon.SendSmsJson(user.PhoneNumber, content, user.UnitID.GetValueOrDefault()))
                    {
                        user.ForgetPassCode = code.Trim();
                        user.CodeExpireTime = DateTime.Now.AddMinutes(5);

                        context.SaveChanges();
                    }
                    else
                    {
                        throw new BusinessException("Có lỗi hệ thống trong quá trình gửi tin nhắn!");
                    }

                    UserInfoBO objUserInfo = new UserInfoBO { UserID = user.UserID };
                    return PartialView("ForgetPasswordStep2", objUserInfo);
                }

                throw new BusinessException("Số điện thoại không đúng, vui lòng thử lại");
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult ForgetPasswordStep3(string phonNum, int userId)
        {
            if (string.IsNullOrEmpty(phonNum))
            {
                throw new BusinessException("Chưa nhập mã bảo mật");
            }

            if (userId < 1)
            {
                throw new BusinessException("Dữ liệu không hợp lệ, vui lòng thử lại");
            }

            using (Entities context = new Entities())
            {
                var user = context.User.Find(userId);
                if (user == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ, vui lòng thử lại");
                }
                if (DateTime.Now <= user.CodeExpireTime)
                {
                    if (user.ForgetPassCode.Trim() == phonNum.Trim())
                    {
                        UserInfoBO objUserInfo = new UserInfoBO { UserID = user.UserID };
                        return PartialView("ForgetPasswordStep3", objUserInfo);
                    }
                    throw new BusinessException("Mã bảo mật không đúng, vui lòng thử lại");
                }
                throw new BusinessException("Mã bảo mật hết hạn, vui lòng thử lại");
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult ForgetPasswordStep4(string newPass, string rePass, int userId)
        {
            string message;
            if (string.IsNullOrEmpty(newPass))
            {
                throw new BusinessException("Chưa nhập mật khẩu mới");
            }

            if (!GlobalCommon.IsStrongPassword(newPass, out message))
            {
                throw new BusinessException(message);
            }

            if (string.IsNullOrEmpty(rePass))
            {
                throw new BusinessException("Chưa nhập xác nhận mật khẩu mới");
            }

            if (newPass != rePass)
            {
                throw new BusinessException("Xác nhận mật khẩu mới không đúng");
            }

            if (userId < 1)
            {
                throw new BusinessException("Dữ liệu không hợp lệ, vui lòng thử lại");
            }

            using (Entities context = new Entities())
            {
                var user = context.User.Find(userId);
                if (user == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ, vui lòng thử lại");
                }

                //cập nhật newPass trong DB
                user.LastLoginDate = null;
                user.Password = GlobalCommon.Encrypt(newPass);

                context.SaveChanges();

                return PartialView("ForgetPasswordStep4");
            }
        }

        [HttpGet]
        public PartialViewResult ChangePassword()
        {
            ChangePasswordModel objModel = new ChangePasswordModel();
            var userInfo = SessionManager.GetUserInfoBO();
            using (Entities context = new Entities())
            {
                var objUser = context.User.FirstOrDefault(a => a.UserID == userInfo.UserID);

                if (objUser != null)
                {
                    objModel.PhoneNumber = objUser.PhoneNumber;
                    objModel.Avatar = userInfo.Avatar;
                    objModel.FullName = objUser.FullName;
                }
            }

            return PartialView(objModel);
        }
        [HttpPost]
        public JsonResult ChangePassword(ChangePasswordModel model)
        {

            {
                using (Entities context = new Entities())
                {
                    int userId = SessionManager.GetUserInfoBO().UserID;
                    User entity = context.User.FirstOrDefault(x => x.UserID == userId);
                    if (entity == null)
                    {
                        throw new BusinessException("Dữ liệu không hợp lệ");
                    }
                    //Thay doi thong tin so dien thoai
                    entity.FullName = model.FullName;
                    entity.PhoneNumber = model.PhoneNumber;
                    //truong hop doi mat khau
                    if (!string.IsNullOrWhiteSpace(model.NewPassword))
                    {
                        string message;
                        //Check pass 
                        if (!GlobalCommon.Encrypt(model.Password).Equals(entity.Password))
                        {
                            throw new BusinessException("Mật khẩu cũ không đúng");
                        }
                        if (!GlobalCommon.IsStrongPassword(model.NewPassword, out message))
                        {
                            throw new BusinessException(message);
                        }
                        if (model.NewPassword.Equals(model.Password))
                        {
                            throw new BusinessException("Mật khẩu mới trùng mật khẩu cũ, vui lòng nhập lại");
                        }
                        if (!model.NewPassword.Equals(model.ConfirmNewPassword))
                        {
                            throw new BusinessException("Xác nhận lại mật khẩu không đúng");
                        }

                        entity.Password = GlobalCommon.Encrypt(model.NewPassword);
                    }
                    var upload = model.UserAvatar;
                    if (upload != null && upload.ContentLength > 0)
                    {
                        var reader = new BinaryReader(upload.InputStream);

                        entity.UserAvatar = reader.ReadBytes(upload.ContentLength);
                        entity.AvatarType = Path.GetExtension(upload.FileName);
                    }
                    context.SaveChanges();
                }
                return Json("Cập nhật thông tin thành công", JsonRequestBehavior.AllowGet);
            }

        }
        [AllowAnonymous]
        public ActionResult RoleError()
        {
            UserInfoBO objUser = SessionManager.GetUserInfoBO();
            if (objUser == null)
            {
                return RedirectToAction("LogIn", "Home");
            }
            return View();
        }
        [AllowAnonymous]
        [SkipDomainHashFilter]
        public ActionResult DomainError()
        {
            return View();
        }
    }
}