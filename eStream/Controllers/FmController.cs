﻿using eStream.Common.CommonClass;
using eStream.Common.CommonObject;
using eStream.Helpers.Filter;
using eStream.Models;
using Model;
using PagedList;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eStream.Controllers
{
    public class FmController : BaseController
    {
        private readonly Entities _context = new Entities();
        [BreadCrumb(ControllerName = "Giám sát FM", ActionName = "Danh sách", AreaName = "")]
        public ActionResult Index()
        {
            PrepareDataSearch(_context);
            SystemConfig sc = _context.SystemConfig.FirstOrDefault();
            int timeRefresh = (sc != null && sc.RefreshTime.GetValueOrDefault() > 0) ? sc.RefreshTime.GetValueOrDefault() : GlobalConstant.DEFAULT_TIME_REFRESH;
            ViewData["timeRefresh"] = timeRefresh;
            return View("index");
        }

        public PartialViewResult SearchDevice(int? UnitID, string SearchDeviceName = null, int Status = 0, int Connect = 0, int page = 1)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            if (string.IsNullOrWhiteSpace(SearchDeviceName))
            {
                SearchDeviceName = null;
            }
            else
            {
                SearchDeviceName = SearchDeviceName.Trim().ToLower();
            }
            var query = from d in _context.Device
                        where d.Status == GlobalConstant.IS_ACTIVE
                        && d.FmType == GlobalConstant.IS_ACTIVE
                        && (SearchDeviceName == null || d.DeviceCode.ToLower().Contains(SearchDeviceName) || d.DeviceName.ToLower().Contains(SearchDeviceName))
                        select d;
            string sUnitID = "/" + user.UnitID + "/";
            string sUnitSearchID = "/" + UnitID.GetValueOrDefault() + "/";
            if (UnitID == null || UnitID == 0)
            {
                query = query.Where(o => (o.UnitID == null && user.UnitLevel == GlobalConstant.UNIT_TYPE_PROVINCE)
                        || _context.Unit.Any(u => u.Status == GlobalConstant.IS_ACTIVE
                        && u.UnitID == o.UnitID && (user.IsSupperAdmin == GlobalConstant.IS_ACTIVE || u.UnitPath.Contains(sUnitID))));
            }
            else
            {
                query = query.Where(o => _context.Unit.Any(u => u.Status == GlobalConstant.IS_ACTIVE
                        && u.UnitID == o.UnitID && (user.IsSupperAdmin == GlobalConstant.IS_ACTIVE || u.UnitPath.Contains(sUnitID))
                        && u.UnitPath.Contains(sUnitSearchID)));
            }
            SystemConfig sc = _context.SystemConfig.FirstOrDefault();
            int timeout = sc != null ? sc.PlayingTimeout.GetValueOrDefault() : 0;
            var queryModel = query.Select(x => new DeviceModel
            {
                UnitID = x.UnitID,
                UnitName = _context.Unit.Where(u => u.UnitID == x.UnitID && u.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE).Select(u => u.UnitName).FirstOrDefault(),
                DeviceID = x.DeviceID,
                SDeviceCode = x.DeviceCode,
                ManagerName = x.ManagerName,
                Status = x.Status,
                CreateDate = x.CreateDate,
                DeviceName = x.DeviceName,
                Version = x.Version,
                Volume = x.Volume,
                Timeout = timeout
            }).OrderBy(x => x.UnitID).ThenBy(x => x.DeviceName).ThenBy(x => x.SDeviceCode);
            if (Status == 0 && Connect == 0)
            {
                PagedList<DeviceModel> paginate = new PagedList<DeviceModel>(queryModel, page, GlobalConstant.PAGE_SIZE);
                ViewData["listDevice"] = paginate;
            }
            else
            {
                var listDevice = queryModel.ToList();

                int[] deviceIds = listDevice.Select(x => x.DeviceID).ToArray();
                List<StatusBO> listDeviceStatus = ManagerController.GetDeviceStatus(_context, deviceIds);
                Dictionary<int, int> dicStatus = new Dictionary<int, int>();
                foreach (StatusBO nowStatus in listDeviceStatus)
                {
                    if (Status > 0 && !ManagerController.IsValidStatus(Status, nowStatus))
                    {
                        continue;
                    }
                    if (Connect > 0 && !ManagerController.IsValidConnect(Connect, nowStatus))
                    {
                        continue;
                    }
                    dicStatus[nowStatus.ID] = GlobalConstant.IS_ACTIVE;
                }
                List<DeviceModel> listFinalDevice = new List<DeviceModel>();
                foreach (var device in listDevice)
                {
                    if (dicStatus.ContainsKey(device.DeviceID))
                    {
                        listFinalDevice.Add(device);
                    }
                }
                PagedList<DeviceModel> paginate = new PagedList<DeviceModel>(listFinalDevice, page, GlobalConstant.PAGE_SIZE);
                ViewData["listDevice"] = paginate;
            }
            ViewBag.DeviceName = SearchDeviceName;
            ViewBag.UnitID = UnitID;
            ViewBag.Status = Status;
            ViewBag.Connect = Connect;
            return PartialView("_LstDevice");
        }

        public PartialViewResult OpenConfig(int deviceID)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            string sUnit = "/" + user.UnitID + "/";
            Device device = _context.Device.Where(d => d.DeviceID == deviceID && d.Status == GlobalConstant.IS_ACTIVE
            && _context.Unit.Any(x => x.Status == GlobalConstant.IS_ACTIVE
            && x.UnitID == d.UnitID && (user.IsSupperAdmin == GlobalConstant.IS_ACTIVE || x.UnitPath.Contains(sUnit)))).FirstOrDefault();
            if (device != null)
            {
                DeviceModel model = new DeviceModel();
                model.DeviceID = device.DeviceID;
                model.UnitID = device.UnitID;
                model.Volume = device.Volume;
                model.SyncTime = device.SyncTime;
                model.Bitrate = device.Bitrate;
                model.IsSMS = device.IsSMS.GetValueOrDefault() == GlobalConstant.IS_ACTIVE ? true : false;
                model.DeviceName = device.DeviceName;
                model.NoVov = device.NoVov;
                model.Timer = device.Timer;
                // cac list du lieu
                List<SelectListItem> listBitrate = new List<SelectListItem>();
                listBitrate.Add(new SelectListItem { Value = "", Text = "--Mặc định--" });
                listBitrate.Add(new SelectListItem { Value = "32", Text = "32kbps" });
                listBitrate.Add(new SelectListItem { Value = "64", Text = "64kbps" });
                listBitrate.Add(new SelectListItem { Value = "128", Text = "128kbps" });
                ViewData["listBitrate"] = listBitrate;
                return PartialView("_Config", model);
            }
            else
            {
                throw new BusinessException("Không tồn tại thiết bị hoặc thiết bị không thuộc quản lý của đơn vị bạn");
            }
        }

        public JsonResult SaveConfig(DeviceModel model)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            string sUnit = "/" + user.UnitID + "/";
            Device device = _context.Device.Where(d => d.DeviceID == model.DeviceID && d.Status == GlobalConstant.IS_ACTIVE
            && _context.Unit.Any(x => x.Status == GlobalConstant.IS_ACTIVE
            && x.UnitID == d.UnitID && (user.IsSupperAdmin == GlobalConstant.IS_ACTIVE || x.UnitPath.Contains(sUnit)))).FirstOrDefault();
            if (device != null)
            {
                device.IsChangeSyncTime = GlobalConstant.IS_ACTIVE;
                device.IsChangeVolume = GlobalConstant.IS_ACTIVE;
                DeviceCommandCaching.AddCommand(device.DeviceID, GlobalConstant.DEVICE_COMMAND_CHANGE_VOLUME);
                DeviceCommandCaching.AddCommand(device.DeviceID, GlobalConstant.DEVICE_COMMAND_CHECK_INTERVAL);
                //device.IsChangeTimer = GlobalConstant.IS_ACTIVE;
                device.Volume = model.Volume;
                device.SyncTime = model.SyncTime;
                device.Bitrate = model.Bitrate;
                device.IsSMS = null;
                device.NoVov = model.NoVov;
                device.Timer = model.Timer;
                if (model.IsSMS)
                {
                    device.IsSMS = GlobalConstant.IS_ACTIVE;
                }
                _context.SaveChanges();
                return Json("Cấu hình thành công", JsonRequestBehavior.AllowGet);
            }
            else
            {
                throw new BusinessException("Không tồn tại thiết bị hoặc thiết bị không thuộc quản lý của đơn vị bạn");
            }
        }

        public PartialViewResult ViewLog(int DeviceID, int page = 1)
        {
            Device device = _context.Device.Find(DeviceID);
            if (device == null)
            {
                throw new BusinessException("Không tồn tại thiết bị");
            }
            ViewData["deviceName"] = device.DeviceName;
            ViewData["deviceID"] = device.DeviceID;
            return PartialView("_ViewLog");
        }

        public PartialViewResult SearchLog(int DeviceID, short? LogType, string FromDate, string ToDate, int page = 1)
        {
            Device device = _context.Device.Find(DeviceID);
            if (device == null)
            {
                throw new BusinessException("Không tồn tại thiết bị");
            }
            var iQuery = _context.DeviceLog.Where(x => x.DeviceID == DeviceID && (LogType == null || x.LogCode == LogType))
                .Select(x => new DeviceLogModel
                {
                    DeviceLogID = x.DeviceLogID,
                    DeviceID = x.DeviceID,
                    LogCode = x.LogCode,
                    LogDate = x.DeviceLogDate,
                    LogID = x.LogID,
                    Status = x.Status,
                    MessageDetail = _context.LogType.Where(l => l.LogCode == x.LogCode).Select(l => l.LogName).FirstOrDefault()
                });
            if (!string.IsNullOrWhiteSpace(FromDate))
            {
                DateTime date;
                if (DateTime.TryParseExact(FromDate,
                "d/M/yyyy",
                CultureInfo.InvariantCulture,
                DateTimeStyles.None, out date))
                {
                    iQuery = iQuery.Where(x => x.LogDate >= date);
                }
            }
            if (ToDate != null)
            {
                DateTime date;
                if (DateTime.TryParseExact(ToDate,
                "d/M/yyyy",
                CultureInfo.InvariantCulture,
                DateTimeStyles.None, out date))
                {
                    date = date.AddDays(1);
                    iQuery = iQuery.Where(x => x.LogDate < date);
                }
            }
            PagedList<DeviceLogModel> paginate = new PagedList<DeviceLogModel>(iQuery.OrderByDescending(x => x.DeviceLogID), page, GlobalConstant.PAGE_SIZE);
            ViewData["listLog"] = paginate;
            ViewBag.DeviceID = DeviceID;
            ViewBag.LogType = LogType;
            ViewBag.FromDate = FromDate;
            ViewBag.ToDate = ToDate;
            return PartialView("_LstLog");
        }

        public PartialViewResult ViewInfo(int DeviceID)
        {
            DeviceModel model = _context.Device.Where(x => x.DeviceID == DeviceID
            && x.Status == GlobalConstant.IS_ACTIVE).Select(x => new DeviceModel
            {
                UnitID = x.UnitID,
                DeviceID = x.DeviceID,
                SDeviceCode = x.DeviceCode,
                DeviceName = x.DeviceName,
                Status = x.Status,
                CreateDate = x.CreateDate,
                ManagerName = x.ManagerName,
                ManagerPhone = x.ManagerPhone,
                SimNumber = x.SimNumber,
                Volume = x.Volume,
                SyncTime = x.SyncTime,
                Bitrate = x.Bitrate,
                IsSMS = x.IsSMS == GlobalConstant.IS_ACTIVE
            }).FirstOrDefault();
            if (model == null)
            {
                throw new BusinessException("Không tồn tại thiết bị đã chọn");
            }
            if (model.UnitID != null && model.UnitID > 0)
            {
                int? provinceID;
                int? districtID;
                int? communeID;
                int? villageID;
                SetAddress(_context, model.UnitID, out provinceID, out districtID, out communeID, out villageID);
                if (villageID != null && villageID > 0)
                {
                    Unit village = _context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE && x.UnitLevel == GlobalConstant.UNIT_TYPE_VILLAGE
                    && x.UnitID == villageID).FirstOrDefault();
                    if (village != null)
                    {
                        model.VillageName = village.UnitName;
                    }
                }
                if (communeID != null && communeID > 0)
                {
                    Unit commune = _context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE && x.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE
                    && x.UnitID == communeID).FirstOrDefault();
                    if (commune != null)
                    {
                        model.CommuneName = commune.UnitName;
                    }
                }
                if (districtID != null && districtID > 0)
                {
                    Unit distric = _context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE && x.UnitLevel == GlobalConstant.UNIT_TYPE_DISTRICT
                    && x.UnitID == districtID).FirstOrDefault();
                    if (distric != null)
                    {
                        model.DistrictName = distric.UnitName;
                    }
                }
            }
            return PartialView("_Info", model);
        }

        public PartialViewResult ViewChart(int DeviceID, DateTime? FromDate = null, int ReportType = GlobalConstant.REPORT_TYPE_DAY, int? Type = 0, int FmType = 0)
        {
            if (ReportType == 0)
            {
                ReportType = GlobalConstant.REPORT_TYPE_DAY;
            }
            DeviceModel model = _context.Device.Where(x => x.DeviceID == DeviceID
               && x.Status == GlobalConstant.IS_ACTIVE).Select(x => new DeviceModel
               {
                   UnitID = x.UnitID,
                   DeviceID = x.DeviceID,
                   SDeviceCode = x.DeviceCode,
                   DeviceName = x.DeviceName,
                   Status = x.Status,
                   CreateDate = x.CreateDate,
                   ManagerName = x.ManagerName,
                   ManagerPhone = x.ManagerPhone,
                   SimNumber = x.SimNumber,
                   Volume = x.Volume,
                   SyncTime = x.SyncTime,
                   Bitrate = x.Bitrate,
                   IsSMS = x.IsSMS == GlobalConstant.IS_ACTIVE
               }).FirstOrDefault();
            if (model == null)
            {
                throw new BusinessException("Không tồn tại thiết bị đã chọn");
            }
            if (FromDate == null)
            {
                FromDate = DateTime.Now;
            }
            string chartTitle = string.Empty;
            if (ReportType == GlobalConstant.REPORT_TYPE_DAY)
            {
                FromDate = FromDate.Value.StartOfWeek(DayOfWeek.Monday).Date;
                if (Type > 0)
                {
                    FromDate = FromDate.Value.AddDays(7);
                }
                else if (Type < 0)
                {
                    FromDate = FromDate.Value.AddDays(-7);
                }
                chartTitle = "tuần " + GlobalCommon.GetWeekNumber(FromDate.Value) + " (" + (FromDate.Value.ToString("dd/MM/yyyy") + " - " + FromDate.Value.AddDays(6).ToString("dd/MM/yyyy")) + ")";
            }
            else
            {
                int year = FromDate.Value.Year;
                int stepYear = ReportType == GlobalConstant.REPORT_TYPE_MONTH ? 1 : 5;
                if (Type > 0)
                {
                    year += stepYear;
                }
                else if (Type < 0)
                {
                    year -= stepYear;
                }
                else if (ReportType == GlobalConstant.REPORT_TYPE_YEAR)
                {
                    year = year - 4;
                }
                FromDate = new DateTime(year, 1, 1);
                if (ReportType == GlobalConstant.REPORT_TYPE_MONTH)
                {
                    chartTitle = "" + FromDate.Value.Year;
                }
                else
                {
                    chartTitle = "từ năm " + FromDate.Value.Year + " - " + "năm " + (FromDate.Value.Year + 4);
                }
            }
            ViewData["FromDate"] = FromDate;
            ViewData["ChartTitle"] = chartTitle;
            ViewData["ReportType"] = ReportType;
            ViewData["FmType"] = FmType;
            return PartialView("_Chart", model);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}