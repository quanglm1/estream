﻿using eStream.Common.CommonClass;
using eStream.Common.CommonObject;
using eStream.Helpers.Filter;
using eStream.Models;
using Model;
using PagedList;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eStream.Controllers
{
    public class ActionLogController : Controller
    {
        // GET: Action
        private readonly Entities context = new Entities();

        private string FormatUserName(User user)
        {
            string res = string.Empty;
            if (!string.IsNullOrWhiteSpace(user.FullName))
            {
                return user.FullName + " (" + user.UserName + ")";
            }
            return user.UserName;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type">type sub or main</param>
        /// <returns></returns>
        [BreadCrumb(ControllerName = "Giám sát hoạt động", ActionName = "Danh sách", AreaName = "Quản trị hệ thống")]
        public ActionResult Index()
        {
            var listUser = context.User.Where(x => x.Status != GlobalConstant.IS_DELETE && x.Role != GlobalConstant.USER_ROLE_SUPPER_ADMIN).ToList();
            var listSelectUser = listUser.Select(x => new SelectListItem { Text = FormatUserName(x), Value = x.UserID.ToString() }).ToList();
            ViewData["listUser"] = listSelectUser;
            return View();
        }
        public PartialViewResult Search(ActionLogModel model, int page = 1)
        {
            var objUser = SessionManager.GetUserInfoBO();
            IQueryable<ActionLogModel> IQSearch =
                from x in this.context.ActionLog
                join y in context.ActionCat on new { x.ActionName, x.ControllerName } equals new { y.ActionName, y.ControllerName }
                select new ActionLogModel
                {
                    IPAddress = x.IPAddress,
                    UserName = x.UserName,
                    ActionTime = x.ActionTime,
                    ActionDescription = y.Name
                };

            var userName = string.Empty;
            if (model.UserID > 0)
            {
                var user = context.User.Find(model.UserID);
                if (user != null)
                {
                    userName = user.UserName;
                }
            }
            if (!string.IsNullOrWhiteSpace(model.ActionName))
            {
                IQSearch = IQSearch.Where(x => x.ActionDescription.ToLower().Contains(model.ActionName.Trim().ToLower()));
            }
            if (!string.IsNullOrWhiteSpace(userName))
            {
                IQSearch = IQSearch.Where(x => x.UserName.ToLower().Contains(userName.Trim().ToLower()));
            }
            if (model.FromDate != null)
            {
                IQSearch = IQSearch.Where(x => x.ActionTime >= model.FromDate);
            }
            if (model.ToDate != null)
            {
                IQSearch = IQSearch.Where(x => x.ActionTime <= model.ToDate);
            }
            Paginate<ActionLogModel> paginate = new Paginate<ActionLogModel>();
            paginate.PageSize = GlobalConstant.PAGE_SIZE;
            paginate.Count = IQSearch.Count();
            List<ActionLogModel> listLog = IQSearch.OrderByDescending(x => x.ActionTime).Skip((page - 1) * GlobalConstant.PAGE_SIZE).Take(GlobalConstant.PAGE_SIZE).ToList();
            paginate.List = listLog;
            ViewData["listResult"] = paginate;
            return PartialView("_ListResult");
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}