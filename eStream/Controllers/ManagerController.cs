﻿using eStream.Common.CommonClass;
using eStream.Common.CommonObject;
using eStream.Dto;
using eStream.Helpers.Filter;
using eStream.Models;
using Model;
using PagedList;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eStream.Controllers
{
    public class ManagerController : BaseController
    {
        private readonly Entities _context = new Entities();
        [BreadCrumb(ControllerName = "Quản lý thiết bị", ActionName = "Danh sách", AreaName = "")]
        public ActionResult Index()
        {
            PrepareDataSearch(_context);
            SystemConfig sc = _context.SystemConfig.FirstOrDefault();
            int timeRefresh = (sc != null && sc.RefreshTime.GetValueOrDefault() > 0) ? sc.RefreshTime.GetValueOrDefault() : GlobalConstant.DEFAULT_TIME_REFRESH;
            ViewData["timeRefresh"] = timeRefresh;
            return View("index");
        }

        public JsonResult SendVersion(int[] deviceIds)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            if (user.IsSupperAdmin.GetValueOrDefault() != GlobalConstant.IS_ACTIVE)
            {
                throw new BusinessException("Bạn không có quyền thực hiện các thao tác với các thiết bị vừa chọn, vui lòng kiểm tra lại");
            }
            this.CheckDevicePermission(deviceIds);
            List<int> listDeviceID = deviceIds.Distinct().ToList();
            List<Device> listDevice = _context.Device.Where(x => listDeviceID.Contains(x.DeviceID)).ToList();
            foreach (Device device in listDevice)
            {
                device.IsSendVersion = GlobalConstant.IS_ACTIVE;
                DeviceCommandCaching.AddCommand(device.DeviceID, GlobalConstant.DEVICE_COMMAND_VERSION);
            }
            _context.SaveChanges();
            return Json("Yêu cầu gửi phiên bản của thiết bị thành công", JsonRequestBehavior.AllowGet);
        }

        public static List<StatusBO> GetDeviceStatus(Entities context, int[] deviceIds)
        {
            List<StatusBO> listDevice = new List<StatusBO>();
            if (deviceIds != null && deviceIds.Any())
            {
                List<int> listDeviceID = deviceIds.Distinct().ToList();
                listDevice.AddRange(context.Device.Where(x => listDeviceID.Contains(x.DeviceID)).Select(x =>
                new StatusBO
                {
                    ID = x.DeviceID
                }).ToList());
                int timeout = context.SystemConfig.Select(x => x.PlayingTimeout).FirstOrDefault().GetValueOrDefault();
                foreach (var status in listDevice)
                {
                    int isPlaying;
                    int isOnline;
                    int isFmSwitchOn;
                    int? fmStatus;
                    GlobalCommon.UpdateDeviceStatus(status.ID, timeout, out isPlaying, out isOnline, out fmStatus, out isFmSwitchOn);
                    status.IsPlaying = isPlaying;
                    status.IsOnline = isOnline;
                    status.IsFmSwitchOn = isFmSwitchOn;
                    status.FmStatus = fmStatus;
                    status.IsStop = StreamingCaching.IsStop(status.ID) ? GlobalConstant.IS_ACTIVE : 0;
                    status.OnlineName = status.IsOnline == GlobalConstant.IS_ACTIVE ? "Có" : "Không";
                    status.FmSwitchOnName = status.IsFmSwitchOn == GlobalConstant.IS_ACTIVE ? "Bật" : status.IsFmSwitchOn == GlobalConstant.IS_NOT_ACTIVE ? "Tắt" : "N/A";
                    status.FmStatusName = status.FmStatus == null || status.FmStatus.GetValueOrDefault() < 0 ? "N/A" : status.FmStatus == GlobalConstant.IS_ACTIVE ? "Bật" : "Tắt";
                    status.PlayingTypeName = string.Empty;
                    status.PlayingSource = string.Empty;
                    PlayingDto nowPlaying = null;
                    StreamingCaching.DicPlaying.TryGetValue(status.ID, out nowPlaying);
                    if (isOnline == GlobalConstant.IS_ACTIVE && nowPlaying != null)
                    {
                        status.PlayingType = nowPlaying.PlayingType;
                        if (nowPlaying.PlayingType > 0)
                        {
                            status.PlayingTypeName = nowPlaying.PlayingType == GlobalConstant.PLAYING_TYPE_LIVE ? "Trực tiếp" : "Theo lịch";
                        }
                        status.PlayingSource = nowPlaying.PlayingSource;
                    }
                    if (string.IsNullOrWhiteSpace(status.PlayingSource))
                    {
                        status.PlayingType = 0;
                        status.PlayingTypeName = string.Empty;
                    }
                    status.VolumeStatus = StreamingCaching.GetVolumeStatus(status.ID);
                    var ext = StreamingCaching.GetDeviceExt(status.ID, timeout);
                    var netDis = ext.Display;
                    status.IsPlaying = ext.Streaming;
                    status.PlayingName = status.IsPlaying == GlobalConstant.IS_ACTIVE ? "Bật" : status.IsStop == GlobalConstant.IS_ACTIVE ? "Tắt" : "N/A";
                    status.NetDisClass = netDis.ClassDis;
                    status.NetDisText = netDis.SortDis;
                    status.DisSms = string.IsNullOrWhiteSpace(ext.ProName) ? 0 : GlobalConstant.IS_ACTIVE;
                    var loc = ext.Loc;
                    status.DisMap = loc == null || string.IsNullOrWhiteSpace(loc.Lat) || string.IsNullOrWhiteSpace(loc.Long)
                        ? 0 : GlobalConstant.IS_ACTIVE;
                    status.Gps = loc == null ? "," : loc.Display();
                }
            }
            return listDevice;
        }

        [SkipActionLogFilter]
        public JsonResult RefreshStatus(int[] deviceIds)
        {
            List<StatusBO> listDevice = GetDeviceStatus(_context, deviceIds);
            return Json(listDevice, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RestartDevice(int[] deviceIds)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            this.CheckDevicePermission(deviceIds);
            List<int> listDeviceID = deviceIds.Distinct().ToList();
            List<Device> listDevice = _context.Device.Where(x => listDeviceID.Contains(x.DeviceID)).ToList();
            foreach (Device device in listDevice)
            {
                device.IsRestart = GlobalConstant.IS_ACTIVE;
                DeviceCommandCaching.AddCommand(device.DeviceID, GlobalConstant.DEVICE_COMMAND_RESTART);
            }
            _context.SaveChanges();
            return Json("Khởi động lại thiết bị thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult SendSmsRequest(int deviceId, int proMessageId)
        {
            if (proMessageId <= 0)
            {
                throw new BusinessException("Vui lòng chọn loại tin nhắn");
            }
            var proSms = _context.ProMessage.Find(proMessageId);
            if (proSms == null)
            {
                throw new BusinessException("Không tồn tại cú pháp tin nhắn");
            }
            UserInfoBO user = SessionManager.GetUserInfoBO();
            int[] deviceIds = new List<int> { deviceId }.ToArray();
            this.CheckDevicePermission(deviceIds);
            List<int> listDeviceID = deviceIds.Distinct().ToList();
            List<Device> listDevice = _context.Device.Where(x => listDeviceID.Contains(x.DeviceID)).ToList();
            foreach (Device device in listDevice)
            {
                device.ProSmsID = proMessageId;
            }
            _context.SaveChanges();
            return Json("Yêu cần gửi tin nhắn tới thiết bị thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateOSDevice(int[] deviceIds)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            if (user.IsSupperAdmin.GetValueOrDefault() != GlobalConstant.IS_ACTIVE)
            {
                throw new BusinessException("Bạn không có quyền thực hiện các thao tác với các thiết bị vừa chọn, vui lòng kiểm tra lại");
            }
            this.CheckDevicePermission(deviceIds);
            List<int> listDeviceID = deviceIds.Distinct().ToList();
            List<Device> listDevice = _context.Device.Where(x => listDeviceID.Contains(x.DeviceID)).ToList();
            foreach (Device device in listDevice)
            {
                device.IsUpdateOS = GlobalConstant.IS_ACTIVE;
            }
            _context.SaveChanges();
            return Json("Cập nhật OS thiết bị thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult StopStream(int[] deviceIds)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            this.CheckDevicePermission(deviceIds);
            List<int> listDeviceID = deviceIds.Distinct().ToList();
            List<Device> listDevice = _context.Device.Where(x => listDeviceID.Contains(x.DeviceID)).ToList();
            foreach (Device device in listDevice)
            {
                bool isPlaying = StreamingCaching.IsPlaying(device.DeviceID);
                if (!isPlaying)
                {
                    throw new BusinessException("Thiết bị " + device.DeviceName + " đang dừng phát, vui lòng kiểm tra lại");
                }
                device.IsRequestStop = GlobalConstant.IS_ACTIVE;
                device.IsRequestForceStop = GlobalConstant.IS_ACTIVE;
                device.IsRequestStart = null;
            }
            _context.SaveChanges();
            return Json("Yêu cầu dừng phát thiết bị thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult StartStream(int[] deviceIds)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            this.CheckDevicePermission(deviceIds);
            List<int> listDeviceID = deviceIds.Distinct().ToList();
            List<Device> listDevice = _context.Device.Where(x => listDeviceID.Contains(x.DeviceID)).ToList();
            foreach (Device device in listDevice)
            {
                bool isPlaying = StreamingCaching.IsPlaying(device.DeviceID);
                if (isPlaying)
                {
                    throw new BusinessException("Thiết bị " + device.DeviceName + " đang phát, vui lòng kiểm tra lại");
                }
                device.IsRequestStart = GlobalConstant.IS_ACTIVE;
                device.IsRequestStop = null;
                device.IsRequestForceStop = null;
            }
            _context.SaveChanges();
            return Json("Yêu cầu phát thiết bị thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult StopSwitch(int[] deviceIds)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            this.CheckDevicePermission(deviceIds);
            List<int> listDeviceID = deviceIds.Distinct().ToList();
            List<Device> listDevice = _context.Device.Where(x => listDeviceID.Contains(x.DeviceID)).ToList();
            foreach (Device device in listDevice)
            {
                device.IsFmSwitchOff = GlobalConstant.IS_ACTIVE;
                device.IsFmSwitchOn = null;
            }
            _context.SaveChanges();
            return Json("Tắt FM thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult StartSwitch(int[] deviceIds)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            this.CheckDevicePermission(deviceIds);
            List<int> listDeviceID = deviceIds.Distinct().ToList();
            List<Device> listDevice = _context.Device.Where(x => listDeviceID.Contains(x.DeviceID)).ToList();
            foreach (Device device in listDevice)
            {
                device.IsFmSwitchOn = GlobalConstant.IS_ACTIVE;
                device.IsFmSwitchOff = null;
            }
            _context.SaveChanges();
            return Json("Bật FM thành công", JsonRequestBehavior.AllowGet);
        }

        private void CheckDevicePermission(int[] deviceIds)
        {
            if (deviceIds == null || !deviceIds.Any())
            {
                throw new BusinessException("Bạn chưa lựa chọn thiết bị nào, vui lòng kiểm tra lại");
            }
            UserInfoBO user = SessionManager.GetUserInfoBO();
            string sUnit = "/" + user.UnitID + "/";
            List<int> listDeviceID = deviceIds.Distinct().ToList();
            int countDevice = _context.Device.Where(x => listDeviceID.Contains(x.DeviceID)
            && ((x.UnitID == null && user.UnitLevel == GlobalConstant.UNIT_TYPE_PROVINCE) || _context.Unit.Any(u => u.UnitID == x.UnitID && u.UnitPath.Contains(sUnit)))).Count();
            if (listDeviceID.Count != countDevice)
            {
                throw new BusinessException("Bạn không có quyền thực hiện các thao tác với các thiết bị vừa chọn, vui lòng kiểm tra lại");
            }
        }

        public static bool IsValidStatus(int status, StatusBO nowStatus)
        {
            if (status == GlobalConstant.DEVICE_STATUS_PLAYING)
            {
                return nowStatus.IsPlaying == GlobalConstant.IS_ACTIVE;
            }
            if (status == GlobalConstant.DEVICE_STATUS_STOPPED)
            {
                return nowStatus.IsStop == GlobalConstant.IS_ACTIVE;
            }
            return nowStatus.IsPlaying != GlobalConstant.IS_ACTIVE && nowStatus.IsStop != GlobalConstant.IS_ACTIVE;
        }

        public static bool IsValidConnect(int connect, StatusBO nowStatus)
        {
            if (connect == GlobalConstant.DEVICE_STATUS_PLAYING)
            {
                return nowStatus.IsOnline == GlobalConstant.IS_ACTIVE;
            }
            return nowStatus.IsOnline == 0;
        }

        public PartialViewResult SearchDevice(int? UnitID, string SearchDeviceName = null, int Status = 0, int Connect = 0, int page = 1)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            if (string.IsNullOrWhiteSpace(SearchDeviceName))
            {
                SearchDeviceName = null;
            }
            else
            {
                SearchDeviceName = SearchDeviceName.Trim().ToLower();
            }
            var query = from d in _context.Device
                        where d.Status == GlobalConstant.IS_ACTIVE
                        && (d.FmType == null || d.FmType == GlobalConstant.IS_NOT_ACTIVE)
                        && (SearchDeviceName == null || d.DeviceCode.ToLower().Contains(SearchDeviceName) || d.DeviceName.ToLower().Contains(SearchDeviceName))
                        select d;
            string sUnitID = "/" + user.UnitID + "/";
            string sUnitSearchID = "/" + UnitID.GetValueOrDefault() + "/";
            if (UnitID == null || UnitID == 0)
            {
                query = query.Where(o => (o.UnitID == null && user.UnitLevel == GlobalConstant.UNIT_TYPE_PROVINCE)
                        || _context.Unit.Any(u => u.Status == GlobalConstant.IS_ACTIVE
                        && u.UnitID == o.UnitID && (user.IsSupperAdmin == GlobalConstant.IS_ACTIVE || u.UnitPath.Contains(sUnitID))));
            }
            else
            {
                query = query.Where(o => _context.Unit.Any(u => u.Status == GlobalConstant.IS_ACTIVE
                        && u.UnitID == o.UnitID && (user.IsSupperAdmin == GlobalConstant.IS_ACTIVE || u.UnitPath.Contains(sUnitID))
                        && u.UnitPath.Contains(sUnitSearchID)));
            }
            SystemConfig sc = _context.SystemConfig.FirstOrDefault();
            int timeout = sc != null ? sc.PlayingTimeout.GetValueOrDefault() : 0;
            var queryModel = query.Select(x => new DeviceModel
            {
                UnitID = x.UnitID,
                UnitName = _context.Unit.Where(u => u.UnitID == x.UnitID && u.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE).Select(u => u.UnitName).FirstOrDefault(),
                DeviceID = x.DeviceID,
                SDeviceCode = x.DeviceCode,
                ManagerName = x.ManagerName,
                Status = x.Status,
                CreateDate = x.CreateDate,
                DeviceName = x.DeviceName,
                Version = x.Version,
                SimSer = x.SimSer,
                Volume = x.Volume,
                Timeout = timeout,
                S = x.Sound == null || x.Sound == "" ? "0000" : x.Sound
            }).OrderBy(x => x.UnitID).ThenBy(x => x.DeviceName).ThenBy(x => x.SDeviceCode);
            if (Status == 0 && Connect == 0)
            {
                PagedList<DeviceModel> paginate = new PagedList<DeviceModel>(queryModel, page, GlobalConstant.PAGE_SIZE);
                ViewData["listDevice"] = paginate;
            }
            else
            {
                var listDevice = queryModel.ToList();

                int[] deviceIds = listDevice.Select(x => x.DeviceID).ToArray();
                List<StatusBO> listDeviceStatus = GetDeviceStatus(_context, deviceIds);
                Dictionary<int, int> dicStatus = new Dictionary<int, int>();
                foreach (StatusBO nowStatus in listDeviceStatus)
                {
                    if (Status > 0 && !IsValidStatus(Status, nowStatus))
                    {
                        continue;
                    }
                    if (Connect > 0 && !IsValidConnect(Connect, nowStatus))
                    {
                        continue;
                    }
                    dicStatus[nowStatus.ID] = GlobalConstant.IS_ACTIVE;
                }
                List<DeviceModel> listFinalDevice = new List<DeviceModel>();
                foreach (var device in listDevice)
                {
                    if (dicStatus.ContainsKey(device.DeviceID))
                    {
                        listFinalDevice.Add(device);
                    }
                }
                PagedList<DeviceModel> paginate = new PagedList<DeviceModel>(listFinalDevice, page, GlobalConstant.PAGE_SIZE);
                ViewData["listDevice"] = paginate;
            }
            ViewBag.DeviceName = SearchDeviceName;
            ViewBag.UnitID = UnitID;
            ViewBag.Status = Status;
            ViewBag.Connect = Connect;
            return PartialView("_LstDevice");
        }

        public PartialViewResult OpenConfig(int deviceID)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            int timeout = _context.SystemConfig.Select(x => x.PlayingTimeout).FirstOrDefault().GetValueOrDefault();
            string sUnit = "/" + user.UnitID + "/";
            Device device = _context.Device.Where(d => d.DeviceID == deviceID && d.Status == GlobalConstant.IS_ACTIVE
            && _context.Unit.Any(x => x.Status == GlobalConstant.IS_ACTIVE
            && x.UnitID == d.UnitID && (user.IsSupperAdmin == GlobalConstant.IS_ACTIVE || x.UnitPath.Contains(sUnit)))).FirstOrDefault();
            if (device != null)
            {
                DeviceModel model = new DeviceModel();
                model.DeviceID = device.DeviceID;
                model.UnitID = device.UnitID;
                model.Volume = device.Volume;
                model.SyncTime = device.SyncTime;
                model.Bitrate = device.Bitrate;
                model.IsSMS = device.IsSMS.GetValueOrDefault() == GlobalConstant.IS_ACTIVE ? true : false;
                model.DeviceName = device.DeviceName;
                model.NoVov = device.NoVov;
                model.ConnType = device.ConnType;
                model.Timer = device.Timer;
                model.Timeout = timeout;
                model.IsSupportFm = device.IsSupportFm.GetValueOrDefault() == GlobalConstant.IS_ACTIVE ? true : false;
                // cac list du lieu
                List<SelectListItem> listBitrate = new List<SelectListItem>();
                listBitrate.Add(new SelectListItem { Value = "", Text = "--Mặc định--" });
                listBitrate.Add(new SelectListItem { Value = "32", Text = "32kbps" });
                listBitrate.Add(new SelectListItem { Value = "64", Text = "64kbps" });
                listBitrate.Add(new SelectListItem { Value = "128", Text = "128kbps" });
                ViewData["listBitrate"] = listBitrate;
                return PartialView("_Config", model);
            }
            else
            {
                throw new BusinessException("Không tồn tại thiết bị hoặc thiết bị không thuộc quản lý của đơn vị bạn");
            }
        }

        public JsonResult SaveConfig(DeviceModel model)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            string sUnit = "/" + user.UnitID + "/";
            Device device = _context.Device.Where(d => d.DeviceID == model.DeviceID && d.Status == GlobalConstant.IS_ACTIVE
            && _context.Unit.Any(x => x.Status == GlobalConstant.IS_ACTIVE
            && x.UnitID == d.UnitID && (user.IsSupperAdmin == GlobalConstant.IS_ACTIVE || x.UnitPath.Contains(sUnit)))).FirstOrDefault();
            if (device != null)
            {
                if (device.SyncTime.GetValueOrDefault() > 0 && device.SyncTime.GetValueOrDefault() != model.SyncTime)
                {
                    device.IsChangeSyncTime = GlobalConstant.IS_ACTIVE;
                }
                if (device.Volume.GetValueOrDefault() != model.Volume.GetValueOrDefault())
                {
                    device.IsChangeVolume = GlobalConstant.IS_ACTIVE;
                    DeviceCommandCaching.AddCommand(device.DeviceID, GlobalConstant.DEVICE_COMMAND_CHANGE_VOLUME);
                }
                if (device.Timer.GetValueOrDefault() != model.Timer.GetValueOrDefault())
                {
                    DeviceCommandCaching.AddCommand(device.DeviceID, GlobalConstant.DEVICE_COMMAND_CHECK_INTERVAL);
                }
                if (device.ConnType != model.ConnType)
                {
                    device.ChangeConn = GlobalConstant.IS_ACTIVE;
                }
                //device.IsChangeTimer = GlobalConstant.IS_ACTIVE;
                device.Volume = model.Volume;
                device.SyncTime = model.SyncTime;
                device.Bitrate = model.Bitrate;
                device.IsSMS = null;
                device.IsSupportFm = null;
                device.NoVov = model.NoVov;
                device.ConnType = model.ConnType;
                device.Timer = model.Timer;
                if (model.IsSMS)
                {
                    device.IsSMS = GlobalConstant.IS_ACTIVE;
                }
                if (model.IsSupportFm)
                {
                    device.IsSupportFm = GlobalConstant.IS_ACTIVE;
                }
                _context.SaveChanges();
                return Json("Cấu hình thành công", JsonRequestBehavior.AllowGet);
            }
            else
            {
                throw new BusinessException("Không tồn tại thiết bị hoặc thiết bị không thuộc quản lý của đơn vị bạn");
            }
        }

        public JsonResult SaveVolume(DeviceModel model)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            string sUnit = "/" + user.UnitID + "/";
            Device device = _context.Device.Where(d => d.DeviceID == model.DeviceID && d.Status == GlobalConstant.IS_ACTIVE
            && _context.Unit.Any(x => x.Status == GlobalConstant.IS_ACTIVE
            && x.UnitID == d.UnitID && (user.IsSupperAdmin == GlobalConstant.IS_ACTIVE || x.UnitPath.Contains(sUnit)))).FirstOrDefault();
            if (device != null)
            {
                device.IsChangeVolume = GlobalConstant.IS_ACTIVE;
                DeviceCommandCaching.AddCommand(device.DeviceID, GlobalConstant.DEVICE_COMMAND_CHANGE_VOLUME);
                device.Volume = model.Volume;
                _context.SaveChanges();
                return Json("Thay đổi âm lượng thành công", JsonRequestBehavior.AllowGet);
            }
            else
            {
                throw new BusinessException("Không tồn tại thiết bị hoặc thiết bị không thuộc quản lý của đơn vị bạn");
            }
        }

        public JsonResult SaveVolumeAll(int? Volume, List<int> DevicesID)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            string sUnit = "/" + user.UnitID + "/";
            List<Device> devices = _context.Device.Where(d => DevicesID.Contains(d.DeviceID) && d.Status == GlobalConstant.IS_ACTIVE
            && _context.Unit.Any(x => x.Status == GlobalConstant.IS_ACTIVE
            && x.UnitID == d.UnitID && (user.IsSupperAdmin == GlobalConstant.IS_ACTIVE || x.UnitPath.Contains(sUnit)))).ToList();
            if (devices != null && devices.Any())
            {
                foreach (var device in devices)
                {
                    device.IsChangeVolume = GlobalConstant.IS_ACTIVE;
                    DeviceCommandCaching.AddCommand(device.DeviceID, GlobalConstant.DEVICE_COMMAND_CHANGE_VOLUME);
                    device.Volume = Volume;
                }
                _context.SaveChanges();
                return Json("Thay đổi âm lượng thành công", JsonRequestBehavior.AllowGet);
            }
            else
            {
                throw new BusinessException("Không tồn tại thiết bị hoặc thiết bị không thuộc quản lý của đơn vị bạn");
            }
        }

        public PartialViewResult ViewLog(int DeviceID, int page = 1)
        {
            Device device = _context.Device.Find(DeviceID);
            if (device == null)
            {
                throw new BusinessException("Không tồn tại thiết bị");
            }
            ViewData["deviceName"] = device.DeviceName;
            ViewData["deviceID"] = device.DeviceID;
            return PartialView("_ViewLog");
        }

        public PartialViewResult SearchLog(int DeviceID, short? LogType, string FromDate, string ToDate, int page = 1)
        {
            Device device = _context.Device.Find(DeviceID);
            if (device == null)
            {
                throw new BusinessException("Không tồn tại thiết bị");
            }
            var iQuery = _context.DeviceLog.Where(x => x.DeviceID == DeviceID && (LogType == null || x.LogCode == LogType))
                .Select(x => new DeviceLogModel
                {
                    DeviceLogID = x.DeviceLogID,
                    DeviceID = x.DeviceID,
                    LogCode = x.LogCode,
                    LogDate = x.DeviceLogDate,
                    LogID = x.LogID,
                    Status = x.Status,
                    MessageDetail = _context.LogType.Where(l => l.LogCode == x.LogCode).Select(l => l.LogName).FirstOrDefault()
                });
            if (!string.IsNullOrWhiteSpace(FromDate))
            {
                DateTime date;
                if (DateTime.TryParseExact(FromDate,
                "d/M/yyyy",
                CultureInfo.InvariantCulture,
                DateTimeStyles.None, out date))
                {
                    iQuery = iQuery.Where(x => x.LogDate >= date);
                }
            }
            if (ToDate != null)
            {
                DateTime date;
                if (DateTime.TryParseExact(ToDate,
                "d/M/yyyy",
                CultureInfo.InvariantCulture,
                DateTimeStyles.None, out date))
                {
                    date = date.AddDays(1);
                    iQuery = iQuery.Where(x => x.LogDate < date);
                }
            }
            PagedList<DeviceLogModel> paginate = new PagedList<DeviceLogModel>(iQuery.OrderByDescending(x => x.DeviceLogID), page, GlobalConstant.PAGE_SIZE);
            ViewData["listLog"] = paginate;
            ViewBag.DeviceID = DeviceID;
            ViewBag.LogType = LogType;
            ViewBag.FromDate = FromDate;
            ViewBag.ToDate = ToDate;
            return PartialView("_LstLog");
        }

        public PartialViewResult SearchSms(int DeviceID, int page = 1)
        {
            Device device = _context.Device.Find(DeviceID);
            if (device == null)
            {
                throw new BusinessException("Không tồn tại thiết bị");
            }
            var listSms = _context.DeviceSms.Where(x => x.DeviceID == DeviceID)
                .OrderByDescending(x => x.ID)
                .Select(x => new DeviceSmsModel
                {
                    ID = x.ID,
                    Message = x.SmsContent
                }).ToList();
            var sms = string.Join("", listSms.Select(x => x.Message));
            ViewData["sms"] = sms;
            return PartialView("_LstSms");
        }

        public long CheckSearchSms(int deviceID, long lastId)
        {
            var curLastId = StreamingCaching.GetLastSms(deviceID);
            return lastId == curLastId ? 0 : curLastId;
        }

        public PartialViewResult ViewSms(int DeviceID)
        {
            DeviceModel model = _context.Device.Where(x => x.DeviceID == DeviceID
            && x.Status == GlobalConstant.IS_ACTIVE).Select(x => new DeviceModel
            {
                DeviceID = x.DeviceID,
                DeviceName = x.DeviceName
            }).FirstOrDefault();
            if (model == null)
            {
                throw new BusinessException("Không tồn tại thiết bị đã chọn");
            }
            ViewData["deviceID"] = DeviceID;
            ViewData["deviceName"] = model.DeviceName;
            var lastId = _context.DeviceSms.Where(x => x.DeviceID == DeviceID)
                .OrderByDescending(x => x.ID).Select(x => x.ID).FirstOrDefault();
            ViewData["lastSmsID"] = lastId;
            StreamingCaching.SetLastSms(DeviceID, lastId);
            return PartialView("_ViewSms", model);
        }

        public PartialViewResult ViewInfo(int DeviceID)
        {
            int timeout = _context.SystemConfig.Select(x => x.PlayingTimeout).FirstOrDefault().GetValueOrDefault();
            DeviceModel model = _context.Device.Where(x => x.DeviceID == DeviceID
            && x.Status == GlobalConstant.IS_ACTIVE).Select(x => new DeviceModel
            {
                UnitID = x.UnitID,
                DeviceID = x.DeviceID,
                SDeviceCode = x.DeviceCode,
                DeviceName = x.DeviceName,
                Status = x.Status,
                CreateDate = x.CreateDate,
                ManagerName = x.ManagerName,
                ManagerPhone = x.ManagerPhone,
                SimNumber = x.SimNumber,
                SimSer = x.SimSer,
                Volume = x.Volume,
                SyncTime = x.SyncTime,
                Bitrate = x.Bitrate,
                Version = x.Version,
                IsSMS = x.IsSMS == GlobalConstant.IS_ACTIVE
            }).FirstOrDefault();
            if (model == null)
            {
                throw new BusinessException("Không tồn tại thiết bị đã chọn");
            }
            model.Timeout = timeout;

            if (model.UnitID != null && model.UnitID > 0)
            {
                int? provinceID;
                int? districtID;
                int? communeID;
                int? villageID;
                SetAddress(_context, model.UnitID, out provinceID, out districtID, out communeID, out villageID);
                if (villageID != null && villageID > 0)
                {
                    Unit village = _context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE && x.UnitLevel == GlobalConstant.UNIT_TYPE_VILLAGE
                    && x.UnitID == villageID).FirstOrDefault();
                    if (village != null)
                    {
                        model.VillageName = village.UnitName;
                    }
                }
                if (communeID != null && communeID > 0)
                {
                    Unit commune = _context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE && x.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE
                    && x.UnitID == communeID).FirstOrDefault();
                    if (commune != null)
                    {
                        model.CommuneName = commune.UnitName;
                    }
                }
                if (districtID != null && districtID > 0)
                {
                    Unit distric = _context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE && x.UnitLevel == GlobalConstant.UNIT_TYPE_DISTRICT
                    && x.UnitID == districtID).FirstOrDefault();
                    if (distric != null)
                    {
                        model.DistrictName = distric.UnitName;
                    }
                }
            }
            return PartialView("_Info", model);
        }

        public PartialViewResult ViewChart(int DeviceID, DateTime? FromDate = null, int ReportType = GlobalConstant.REPORT_TYPE_DAY, int? Type = 0)
        {
            if (ReportType == 0)
            {
                ReportType = GlobalConstant.REPORT_TYPE_DAY;
            }
            DeviceModel model = _context.Device.Where(x => x.DeviceID == DeviceID
               && x.Status == GlobalConstant.IS_ACTIVE).Select(x => new DeviceModel
               {
                   UnitID = x.UnitID,
                   DeviceID = x.DeviceID,
                   SDeviceCode = x.DeviceCode,
                   DeviceName = x.DeviceName,
                   Status = x.Status,
                   CreateDate = x.CreateDate,
                   ManagerName = x.ManagerName,
                   ManagerPhone = x.ManagerPhone,
                   SimNumber = x.SimNumber,
                   Volume = x.Volume,
                   SyncTime = x.SyncTime,
                   Bitrate = x.Bitrate,
                   IsSMS = x.IsSMS == GlobalConstant.IS_ACTIVE,
                   Version = x.Version
               }).FirstOrDefault();
            if (model == null)
            {
                throw new BusinessException("Không tồn tại thiết bị đã chọn");
            }
            if (FromDate == null)
            {
                FromDate = DateTime.Now;
            }
            string chartTitle = string.Empty;
            if (ReportType == GlobalConstant.REPORT_TYPE_DAY)
            {
                FromDate = FromDate.Value.StartOfWeek(DayOfWeek.Monday).Date;
                if (Type > 0)
                {
                    FromDate = FromDate.Value.AddDays(7);
                }
                else if (Type < 0)
                {
                    FromDate = FromDate.Value.AddDays(-7);
                }
                chartTitle = "tuần " + GlobalCommon.GetWeekNumber(FromDate.Value) + " (" + (FromDate.Value.ToString("dd/MM/yyyy") + " - " + FromDate.Value.AddDays(6).ToString("dd/MM/yyyy")) + ")";
            }
            else
            {
                int year = FromDate.Value.Year;
                int stepYear = ReportType == GlobalConstant.REPORT_TYPE_MONTH ? 1 : 5;
                if (Type > 0)
                {
                    year += stepYear;
                }
                else if (Type < 0)
                {
                    year -= stepYear;
                }
                else if (ReportType == GlobalConstant.REPORT_TYPE_YEAR)
                {
                    year = year - 4;
                }
                FromDate = new DateTime(year, 1, 1);
                if (ReportType == GlobalConstant.REPORT_TYPE_MONTH)
                {
                    chartTitle = "" + FromDate.Value.Year;
                }
                else
                {
                    chartTitle = "từ năm " + FromDate.Value.Year + " - " + "năm " + (FromDate.Value.Year + 4);
                }
            }
            ViewData["FromDate"] = FromDate;
            ViewData["ChartTitle"] = chartTitle;
            ViewData["ReportType"] = ReportType;
            return PartialView("_Chart", model);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}