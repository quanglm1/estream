﻿using eStream.Common.CommonClass;
using eStream.Models;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using eStream.Common.CommonObject;
using eStream.Helpers.Filter;

namespace eStream.Controllers
{
    public class ChartController : BaseController
    {
        private readonly Entities _context = new Entities();
        // GET: SendSmsHis
        [BreadCrumb(ControllerName = "Biểu đồ hoạt động", AreaName = "")]
        public ActionResult Index()
        {
            PrepareDataSearch(_context);
            List<SelectListItem> listReportType = new List<SelectListItem>();
            listReportType.Add(new SelectListItem { Value = "" + GlobalConstant.REPORT_TYPE_DAY, Text = "Theo ngày" });
            listReportType.Add(new SelectListItem { Value = "" + GlobalConstant.REPORT_TYPE_MONTH, Text = "Theo tháng" });
            listReportType.Add(new SelectListItem { Value = "" + GlobalConstant.REPORT_TYPE_YEAR, Text = "Theo năm" });
            ViewData["listReportType"] = listReportType;
            return View();
        }
        public JsonResult LoadDevice(int? UnitID = null)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            var query = from d in _context.Device
                        where d.Status == GlobalConstant.IS_ACTIVE
                        select d;
            string sUnitID = "/" + user.UnitID + "/";
            string sUnitSearchID = "/" + UnitID.GetValueOrDefault() + "/";
            if (UnitID == null || UnitID == 0)
            {
                query = query.Where(o => (o.UnitID == null && user.IsSupperAdmin == GlobalConstant.IS_ACTIVE)
                        || _context.Unit.Any(u => u.Status == GlobalConstant.IS_ACTIVE
                        && u.UnitID == o.UnitID && u.UnitPath.Contains(sUnitID)));
            }
            else
            {
                query = query.Where(o => _context.Unit.Any(u => u.Status == GlobalConstant.IS_ACTIVE
                        && u.UnitID == o.UnitID
                        && (user.IsSupperAdmin == GlobalConstant.IS_ACTIVE || u.UnitPath.Contains(sUnitID))
                        && u.UnitPath.Contains(sUnitSearchID)));
            }
            var listDevice = query.Select(x => new DeviceModel
            {
                UnitID = x.UnitID,
                DeviceID = x.DeviceID,
                DeviceName = x.DeviceCode
            }).OrderBy(x => x.UnitID).ThenBy(x => x.DeviceName).ToList();

            return Json(listDevice, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadChart(int DeviceID, int ReportType, DateTime FromDate)
        {
            DateTime ToDate;
            if (ReportType == GlobalConstant.REPORT_TYPE_DAY)
            {
                FromDate = FromDate.StartOfWeek(DayOfWeek.Monday);
                ToDate = FromDate.AddDays(7);
            }
            else
            {
                int year = FromDate.Year;
                FromDate = new DateTime(year, 1, 1);
                if (ReportType == GlobalConstant.REPORT_TYPE_MONTH)
                {
                    ToDate = FromDate.AddYears(1);
                }
                else
                {
                    ToDate = FromDate.AddYears(5);
                }
            }
            var iQuery = _context.DevicePlayingTime.Where(x => x.DeviceID == DeviceID && x.LogDate >= FromDate && x.LogDate < ToDate);

            List<ChartBO> listData = new List<ChartBO>();
            if (ReportType == GlobalConstant.REPORT_TYPE_DAY)
            {
                for (int i = 0; i < 7; i++)
                {
                    DateTime nextDate = FromDate.AddDays(i);
                    listData.Add(new ChartBO
                    {
                        Label = GlobalCommon.GetNameOfDay(i + 1),
                        LabelValue = nextDate.ToString("dMyyyy")
                    });
                }
                var times = iQuery.GroupBy(x => new { x.Year, x.Month, x.Date })
                    .Select(x => new
                    {
                        Value = x.Sum(s => s.TotalMinus),
                        LabelValue = x.Key.Date + "" + x.Key.Month + "" + x.Key.Year
                    }).ToList();
                foreach (var t in times)
                {
                    ChartBO c = listData.Where(x => x.LabelValue.Equals(t.LabelValue)).FirstOrDefault();
                    c.Value = Math.Round(t.Value.GetValueOrDefault() / 60, 1);
                }
            }
            else if (ReportType == GlobalConstant.REPORT_TYPE_MONTH)
            {
                for (int i = 1; i < 13; i++)
                {
                    listData.Add(new ChartBO
                    {
                        Label = "Tháng " + i,
                        LabelValue = i + "" + FromDate.Year
                    });
                }
                var times = iQuery.GroupBy(x => new { x.Year, x.Month })
                    .Select(x => new
                    {
                        Value = x.Sum(s => s.TotalMinus),
                        LabelValue = x.Key.Month + "" + x.Key.Year
                    }).ToList();
                foreach (var t in times)
                {
                    ChartBO c = listData.Where(x => x.LabelValue.Equals(t.LabelValue)).FirstOrDefault();
                    c.Value = Math.Round(t.Value.GetValueOrDefault() / 60, 1);
                }
            }
            else if (ReportType == GlobalConstant.REPORT_TYPE_YEAR)
            {
                for (int i = 0; i < 5; i++)
                {
                    listData.Add(new ChartBO
                    {
                        Label = "Năm " + (i + FromDate.Year),
                        LabelValue = "" + (i + FromDate.Year)
                    });
                }
                var times = iQuery.GroupBy(x => new { x.Year })
                    .Select(x => new
                    {
                        Value = x.Sum(s => s.TotalMinus),
                        LabelValue = "" + x.Key.Year
                    }).ToList();
                foreach (var t in times)
                {
                    ChartBO c = listData.Where(x => x.LabelValue.Equals(t.LabelValue)).FirstOrDefault();
                    c.Value = Math.Round(t.Value.GetValueOrDefault() / 60, 1);
                }
            }
            return Json(listData, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadChartFm(int DeviceID, int ReportType, DateTime FromDate)
        {
            DateTime ToDate;
            if (ReportType == GlobalConstant.REPORT_TYPE_DAY)
            {
                FromDate = FromDate.StartOfWeek(DayOfWeek.Monday);
                ToDate = FromDate.AddDays(7);
            }
            else
            {
                int year = FromDate.Year;
                FromDate = new DateTime(year, 1, 1);
                if (ReportType == GlobalConstant.REPORT_TYPE_MONTH)
                {
                    ToDate = FromDate.AddYears(1);
                }
                else
                {
                    ToDate = FromDate.AddYears(5);
                }
            }
            var iQuery = _context.DevicePlayingFmTime.Where(x => x.DeviceID == DeviceID && x.LogDate >= FromDate && x.LogDate < ToDate);

            List<ChartBO> listData = new List<ChartBO>();
            if (ReportType == GlobalConstant.REPORT_TYPE_DAY)
            {
                for (int i = 0; i < 7; i++)
                {
                    DateTime nextDate = FromDate.AddDays(i);
                    listData.Add(new ChartBO
                    {
                        Label = GlobalCommon.GetNameOfDay(i + 1),
                        LabelValue = nextDate.ToString("dMyyyy")
                    });
                }
                var times = iQuery.GroupBy(x => new { x.Year, x.Month, x.Date })
                    .Select(x => new
                    {
                        Value = x.Sum(s => s.TotalMinus),
                        LabelValue = x.Key.Date + "" + x.Key.Month + "" + x.Key.Year
                    }).ToList();
                foreach (var t in times)
                {
                    ChartBO c = listData.Where(x => x.LabelValue.Equals(t.LabelValue)).FirstOrDefault();
                    c.Value = Math.Round(t.Value.GetValueOrDefault() / 60, 1);
                }
            }
            else if (ReportType == GlobalConstant.REPORT_TYPE_MONTH)
            {
                for (int i = 1; i < 13; i++)
                {
                    listData.Add(new ChartBO
                    {
                        Label = "Tháng " + i,
                        LabelValue = i + "" + FromDate.Year
                    });
                }
                var times = iQuery.GroupBy(x => new { x.Year, x.Month })
                    .Select(x => new
                    {
                        Value = x.Sum(s => s.TotalMinus),
                        LabelValue = x.Key.Month + "" + x.Key.Year
                    }).ToList();
                foreach (var t in times)
                {
                    ChartBO c = listData.Where(x => x.LabelValue.Equals(t.LabelValue)).FirstOrDefault();
                    c.Value = Math.Round(t.Value.GetValueOrDefault() / 60, 1);
                }
            }
            else if (ReportType == GlobalConstant.REPORT_TYPE_YEAR)
            {
                for (int i = 0; i < 5; i++)
                {
                    listData.Add(new ChartBO
                    {
                        Label = "Năm " + (i + FromDate.Year),
                        LabelValue = "" + (i + FromDate.Year)
                    });
                }
                var times = iQuery.GroupBy(x => new { x.Year })
                    .Select(x => new
                    {
                        Value = x.Sum(s => s.TotalMinus),
                        LabelValue = "" + x.Key.Year
                    }).ToList();
                foreach (var t in times)
                {
                    ChartBO c = listData.Where(x => x.LabelValue.Equals(t.LabelValue)).FirstOrDefault();
                    c.Value = Math.Round(t.Value.GetValueOrDefault() / 60, 1);
                }
            }
            return Json(listData, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}