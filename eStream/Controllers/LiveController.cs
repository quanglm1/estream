﻿using eStream.Common.CommonClass;
using eStream.Common.CommonObject;
using eStream.Helpers.Filter;
using eStream.Models;
using Model;
using PagedList;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.IO.Compression;
using NLog;
using System.Data.SqlClient;

namespace eStream.Controllers
{
    public class LiveController : BaseController
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly Entities _context = new Entities();
        [BreadCrumb(ControllerName = "Phát trực tiếp", ActionName = "Cấu hình", AreaName = "")]
        public ActionResult Index()
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            PrepareDataSearch(_context);
            var listLive = (from x in _context.Live
                            join u in _context.Unit on x.UnitID equals u.UnitID
                            where u.UnitID == user.UnitID
                            orderby x.LiveID descending
                            select new LiveModel
                            {
                                ChannelID = x.ChannelID,
                                LiveID = x.LiveID,
                                LiveType = x.LiveType,
                                Status = x.Status,
                                LiveSource = x.LiveSource,
                                UnitID = x.UnitID,
                                CreatedDate = x.CreatedDate
                            }).ToList();
            if (listLive.Any())
            {
                ViewData["nowLive"] = listLive[0];
            }
            else
            {
                var defaultChannelID = _context.ForwardChanel.Where(a => a.Status == GlobalConstant.IS_ACTIVE && a.IsDefault == GlobalConstant.IS_ACTIVE)
                    .Select(x => x.ChanelID)
                    .FirstOrDefault();

                var live = new Live
                {
                    LiveType = GlobalConstant.LIVE_TYPE_ALL,
                    CreatedDate = DateTime.Now,
                    Status = GlobalConstant.LIVE_DRAFF,
                    LiveSource = GlobalConstant.LIVE_SOURCE_VOV,
                    ChannelID = defaultChannelID,
                    UnitID = user.UnitID.Value
                };
                _context.Live.Add(live);
                _context.SaveChanges();
                ViewData["nowLive"] = new LiveModel
                {
                    ChannelID = live.ChannelID,
                    LiveID = live.LiveID,
                    LiveType = live.LiveType,
                    Status = live.Status,
                    LiveSource = live.LiveSource,
                    UnitID = live.UnitID,
                    CreatedDate = live.CreatedDate
                };
            }
            SystemConfig sc = _context.SystemConfig.FirstOrDefault();
            ViewData["systemConfig"] = sc;
            return View();
        }
        private void RemoveOneLive(Live live)
        {
            var listDevice = _context.LiveDevice.Where(x => x.LiveID == live.LiveID).ToList();
            _context.LiveDevice.RemoveRange(listDevice);
            var listFile = _context.LiveFile.Where(x => x.LiveID == live.LiveID).ToList();
            _context.LiveFile.RemoveRange(listFile);
            _context.Live.Remove(live);
        }
        public JsonResult RemoveDraffLive(long liveID)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            var live = _context.Live.Find(liveID);
            if (live.UnitID != user.UnitID)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            if (live.Status == GlobalConstant.LIVE_PLAYING)
            {
                throw new BusinessException("Chương trình đang phát không được xóa");
            }
            var listDevice = _context.LiveDevice.Where(x => _context.Live.Any(l => l.UnitID == user.UnitID && l.LiveID == x.LiveID && l.Status <= GlobalConstant.LIVE_WAIT && x.LiveID == liveID)).ToList();
            _context.LiveDevice.RemoveRange(listDevice);
            var listFile = _context.LiveFile.Where(x => _context.Live.Any(l => l.UnitID == user.UnitID && l.LiveID == x.LiveID && l.Status <= GlobalConstant.LIVE_WAIT && x.LiveID == liveID)).ToList();
            _context.LiveFile.RemoveRange(listFile);
            var lives = _context.Live.Where(x => x.Status <= GlobalConstant.LIVE_WAIT && x.LiveID == liveID).ToList();
            _context.Live.RemoveRange(lives);
            _context.SaveChanges();
            return Json("Xóa chương trình phát thành công", JsonRequestBehavior.AllowGet);
        }
        public JsonResult SaveDraffLive()
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            var defaultChannelID = _context.ForwardChanel.Where(a => a.Status == GlobalConstant.IS_ACTIVE && a.IsDefault == GlobalConstant.IS_ACTIVE)
                    .Select(x => x.ChanelID)
                    .FirstOrDefault();
            Live live = new Live
            {
                CreatedDate = DateTime.Now,
                Status = GlobalConstant.LIVE_DRAFF,
                UnitID = user.UnitID.Value,
                LiveSource = GlobalConstant.LIVE_SOURCE_VOV,
                LiveType = GlobalConstant.LIVE_TYPE_ALL,
                ChannelID = defaultChannelID
            };
            _context.Live.Add(live);
            _context.SaveChanges();
            return Json(live.LiveID, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RemoveDevice(long? liveID, int[] deviceIds)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            this.CheckDevicePermission(deviceIds);
            List<int> listDeviceID = deviceIds.Distinct().ToList();
            var listDevice = _context.LiveDevice.Where(x => listDeviceID.Contains(x.DeviceID) && x.LiveID == liveID).ToList();
            var live = _context.Live.Where(x => x.LiveID == liveID)
                .OrderByDescending(x => x.Status).FirstOrDefault();
            if (live != null)
            {

                foreach (var device in listDevice)
                {
                    _context.LiveDevice.Remove(device);
                }
                _context.SaveChanges();
            }
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public JsonResult ChooseDevice(long? liveID, int[] deviceIds)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            this.CheckDevicePermission(deviceIds);
            List<int> listDeviceID = deviceIds.Distinct().ToList();
            List<Device> listDevice = _context.Device.Where(x => listDeviceID.Contains(x.DeviceID)).ToList();
            var live = _context.Live.Where(x => x.LiveID == liveID)
                .OrderByDescending(x => x.Status).FirstOrDefault();
            if (live != null)
            {
                foreach (Device device in listDevice)
                {
                    var liveDevice = new LiveDevice();
                    liveDevice.LiveID = liveID.Value;
                    liveDevice.DeviceID = device.DeviceID;
                    _context.LiveDevice.Add(liveDevice);
                }
                _context.SaveChanges();
            }
            return Json(liveID, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CountDevice(long? liveID)
        {
            int countDevice = _context.LiveDevice.Where(x => x.LiveID == liveID).Count();
            return Json(countDevice, JsonRequestBehavior.AllowGet);
        }

        private void CheckDevicePermission(int[] deviceIds)
        {
            if (deviceIds == null || !deviceIds.Any())
            {
                throw new BusinessException("Bạn chưa lựa chọn thiết bị nào, vui lòng kiểm tra lại");
            }
            UserInfoBO user = SessionManager.GetUserInfoBO();
            string sUnit = "/" + user.UnitID + "/";
            List<int> listDeviceID = deviceIds.Distinct().ToList();
            int countDevice = _context.Device.Where(x => listDeviceID.Contains(x.DeviceID)
            && ((x.UnitID == null && user.UnitLevel == GlobalConstant.UNIT_TYPE_PROVINCE) || _context.Unit.Any(u => u.UnitID == x.UnitID && u.UnitPath.Contains(sUnit)))).Count();
            if (listDeviceID.Count != countDevice)
            {
                throw new BusinessException("Bạn không có quyền thực hiện các thao tác với các thiết bị vừa chọn, vui lòng kiểm tra lại");
            }
        }

        public PartialViewResult LoadListLive(long? liveID)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            var unit = _context.Unit.Find(user.UnitID);
            var listLive = (from x in _context.Live
                            join u in _context.Unit on x.UnitID equals u.UnitID
                            where x.Status >= GlobalConstant.LIVE_PLAYING
                            && unit.UnitPath.Contains(u.UnitPath)
                            orderby u.UnitPath descending, x.LiveID descending
                            select new LiveModel
                            {
                                ChannelID = x.ChannelID,
                                LiveID = x.LiveID,
                                LiveType = x.LiveType,
                                Status = x.Status,
                                LiveSource = x.LiveSource,
                                UnitID = x.UnitID,
                                CreatedDate = x.CreatedDate
                            }).ToList();
            if (liveID.GetValueOrDefault() == 0 && listLive.Any())
            {
                liveID = listLive[0].LiveID;
            }
            ViewData["listLive"] = listLive;
            ViewData["nowLiveID"] = liveID;
            return PartialView("_LstLive");
        }
        public PartialViewResult OpenLivePop(long? liveID, int isEdit = 0)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            LiveModel model = null;
            List<LiveFile> listFile = null;
            int countDevice = 0;
            if (liveID.GetValueOrDefault() == 0)
            {
                Live live = _context.Live.Where(x => x.UnitID == user.UnitID).FirstOrDefault();
                if (live == null)
                {
                    live = new Live
                    {
                        CreatedDate = DateTime.Now,
                        UnitID = user.UnitID.Value,
                        LiveType = GlobalConstant.LIVE_TYPE_ALL,
                        Status = GlobalConstant.LIVE_DRAFF
                    };
                    _context.Live.Add(live);
                    _context.SaveChanges();
                }
                liveID = live.LiveID;
            }
            model = _context.Live.Where(x => x.LiveID == liveID && x.UnitID == user.UnitID)
                   .Select(x => new LiveModel
                   {
                       ChannelID = x.ChannelID,
                       LiveID = x.LiveID,
                       LiveType = x.LiveType,
                       Status = x.Status,
                       LiveSource = x.LiveSource,
                       UnitID = x.UnitID,
                       LoopCount = x.LoopCount
                   }).FirstOrDefault();
            model.IsLoop = model.LoopCount.GetValueOrDefault() > 0;
            listFile = _context.LiveFile.Where(x => x.LiveID == liveID).OrderBy(x => x.NumberOrder).ThenBy(x => x.LiveFileID).ToList();
            countDevice = _context.LiveDevice.Where(x => x.LiveID == liveID).Count();
            if (countDevice == 0)
            {
                model.LiveType = GlobalConstant.LIVE_TYPE_ALL;
            }
            else
            {
                model.LiveType = GlobalConstant.LIVE_TYPE_CHOICE;
            }
            if (model.Status == GlobalConstant.LIVE_DRAFF)
            {
                isEdit = GlobalConstant.IS_ACTIVE;
            }
            var unit = _context.Unit.Find(model.UnitID);
            ViewData["unitName"] = unit.UnitName;
            ViewData["countDevice"] = countDevice;
            ViewData["listFile"] = listFile;
            ViewData["isEdit"] = GlobalConstant.IS_ACTIVE;
            return PartialView("_Live", model);
        }

        public PartialViewResult LoadFile(long? liveID)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            var unit = _context.Unit.Find(user.UnitID);
            LiveModel model = null;
            List<LiveFile> listFile = null;
            if (liveID.GetValueOrDefault() == 0)
            {
                var listLive = (from x in _context.Live
                                join u in _context.Unit on x.UnitID equals u.UnitID
                                where x.Status >= GlobalConstant.LIVE_PLAYING
                                && unit.UnitPath.Contains(u.UnitPath)
                                orderby u.UnitPath descending, x.LiveID descending
                                select new LiveModel
                                {
                                    ChannelID = x.ChannelID,
                                    LiveID = x.LiveID,
                                    LiveType = x.LiveType,
                                    Status = x.Status,
                                    LiveSource = x.LiveSource,
                                    UnitID = x.UnitID,
                                    CreatedDate = x.CreatedDate
                                }).ToList();
                if (liveID.GetValueOrDefault() == 0 && listLive.Any())
                {
                    liveID = listLive[0].LiveID;
                }
            }
            if (liveID != null)
            {
                model = _context.Live.Where(x => x.LiveID == liveID)
                    .Select(x => new LiveModel
                    {
                        ChannelID = x.ChannelID,
                        LiveID = x.LiveID,
                        LiveType = x.LiveType,
                        Status = x.Status,
                        UnitID = x.UnitID,
                        CreatedDate = x.CreatedDate
                    }).FirstOrDefault();
                listFile = _context.LiveFile.Where(x => x.LiveID == liveID).OrderBy(x => x.NumberOrder).ThenBy(x => x.LiveFileID).ToList();
            }
            ViewData["listFile"] = listFile;
            ViewData["isEdit"] = GlobalConstant.IS_ACTIVE;
            return PartialView("_File", model);
        }

        public PartialViewResult SearchDevice(int? UnitID, string SearchDeviceName = null, int Status = 0, int Connect = 0, int page = 1)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            if (string.IsNullOrWhiteSpace(SearchDeviceName))
            {
                SearchDeviceName = null;
            }
            else
            {
                SearchDeviceName = SearchDeviceName.Trim().ToLower();
            }
            var query = from d in _context.Device
                        where d.Status == GlobalConstant.IS_ACTIVE
                        where (SearchDeviceName == null || d.DeviceCode.ToLower().Contains(SearchDeviceName) || d.DeviceName.ToLower().Contains(SearchDeviceName))
                        select d;
            string sUnitID = "/" + user.UnitID + "/";
            string sUnitSearchID = "/" + UnitID.GetValueOrDefault() + "/";
            if (UnitID == null || UnitID == 0)
            {
                query = query.Where(o => (o.UnitID == null && user.UnitLevel == GlobalConstant.UNIT_TYPE_PROVINCE)
                        || _context.Unit.Any(u => u.Status == GlobalConstant.IS_ACTIVE
                        && u.UnitID == o.UnitID && (user.IsSupperAdmin == GlobalConstant.IS_ACTIVE || u.UnitPath.Contains(sUnitID))));
            }
            else
            {
                query = query.Where(o => _context.Unit.Any(u => u.Status == GlobalConstant.IS_ACTIVE
                        && u.UnitID == o.UnitID && (user.IsSupperAdmin == GlobalConstant.IS_ACTIVE || u.UnitPath.Contains(sUnitID))
                        && u.UnitPath.Contains(sUnitSearchID)));
            }
            SystemConfig sc = _context.SystemConfig.FirstOrDefault();
            int timeout = sc != null ? sc.PlayingTimeout.GetValueOrDefault() : 0;
            var unit = _context.Unit.Find(user.UnitID);
            var live = (from x in _context.Live
                        join u in _context.Unit on x.UnitID equals u.UnitID
                        where x.Status >= GlobalConstant.LIVE_PLAYING
                        && unit.UnitPath.Contains(u.UnitPath)
                        orderby u.UnitPath descending, x.LiveID descending, x.Status descending
                        select new LiveModel
                        {
                            ChannelID = x.ChannelID,
                            LiveID = x.LiveID,
                            LiveType = x.LiveType,
                            Status = x.Status,
                            LiveSource = x.LiveSource,
                            UnitID = x.UnitID,
                            CreatedDate = x.CreatedDate
                        }).FirstOrDefault();
            long liveID = 0;
            if (live != null)
            {
                liveID = live.LiveID;
            }
            var queryModel = query.Select(x => new DeviceModel
            {
                UnitID = x.UnitID,
                UnitName = _context.Unit.Where(u => u.UnitID == x.UnitID && u.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE).Select(u => u.UnitName).FirstOrDefault(),
                DeviceID = x.DeviceID,
                SDeviceCode = x.DeviceCode,
                ManagerName = x.ManagerName,
                Status = x.Status,
                CreateDate = x.CreateDate,
                DeviceName = x.DeviceName,
                Version = x.Version,
                Timeout = timeout,
                Volume = x.Volume,
                HasLive = liveID > 0 && _context.LiveDevice.Any(l => l.LiveID == liveID && l.DeviceID == x.DeviceID) ? GlobalConstant.IS_ACTIVE : 0
            }).OrderBy(x => x.UnitID).ThenBy(x => x.DeviceName).ThenBy(x => x.SDeviceCode);

            if (Status == 0 && Connect == 0)
            {
                PagedList<DeviceModel> paginate = new PagedList<DeviceModel>(queryModel, page, GlobalConstant.PAGE_SIZE);
                ViewData["listDevice"] = paginate;
            }
            else
            {
                var listDevice = queryModel.ToList();

                int[] deviceIds = listDevice.Select(x => x.DeviceID).ToArray();
                List<StatusBO> listDeviceStatus = ManagerController.GetDeviceStatus(_context, deviceIds);
                Dictionary<int, int> dicStatus = new Dictionary<int, int>();
                foreach (StatusBO nowStatus in listDeviceStatus)
                {
                    if (Status > 0 && !ManagerController.IsValidStatus(Status, nowStatus))
                    {
                        continue;
                    }
                    if (Connect > 0 && !ManagerController.IsValidConnect(Connect, nowStatus))
                    {
                        continue;
                    }
                    dicStatus[nowStatus.ID] = GlobalConstant.IS_ACTIVE;
                }
                List<DeviceModel> listFinalDevice = new List<DeviceModel>();
                foreach (var device in listDevice)
                {
                    if (dicStatus.ContainsKey(device.DeviceID))
                    {
                        listFinalDevice.Add(device);
                    }
                }
                PagedList<DeviceModel> paginate = new PagedList<DeviceModel>(listFinalDevice, page, GlobalConstant.PAGE_SIZE);
                ViewData["listDevice"] = paginate;
            }
            ViewBag.DeviceName = SearchDeviceName;
            ViewBag.UnitID = UnitID;
            ViewBag.Status = Status;
            ViewBag.Connect = Connect;
            return PartialView("_LstDevice");
        }

        private LiveFile InsertLiveFile(long liveID, string title = null, string fileName = null, string filePath = null)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            LiveFile liveFile = new LiveFile
            {
                LiveID = liveID,
                FileUrl = filePath,
                FileName = fileName,
                Title = !string.IsNullOrEmpty(fileName) ? fileName : title,
                CreatedDate = DateTime.Now,
                CreatedUser = user.UserName
            };
            // Order
            int max = _context.LiveFile.Where(x => x.LiveID == liveID).Count();
            liveFile.NumberOrder = max + 1;
            _context.LiveFile.Add(liveFile);
            return liveFile;
        }

        private void ProcessFile(long LiveID)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            string FilePath = "";
            string FileName = "";
            foreach (string objfile in Request.Files)
            {
                var fileContent = Request.Files[objfile];
                if (fileContent != null && fileContent.ContentLength > 0)
                {
                    // and optionally write the file to disk
                    FileName = Path.GetFileName(fileContent.FileName);
                    FilePath = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(GlobalConstant.FILE_PATH), user.UnitID.ToString(), "Live", LiveID.ToString(), DateTime.Now.ToString("ddMMyyyyHHmmss"));
                    //Check whether Directory (Folder) exists.
                    if (!Directory.Exists(FilePath))
                    {
                        //If Directory (Folder) does not exists. Create it.
                        Directory.CreateDirectory(FilePath);
                    }
                    // fileContent.SaveAs(Path.Combine(FilePath, FileName));
                    fileContent.SaveAs(FilePath + "\\" + FileName);
                }
                InsertLiveFile(LiveID, null, FileName, FilePath);
            }
        }

        public LiveFile ProcessConvertText(long LiveID, string Text, string Voice, double Rate, string Title)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            var Entity = InsertLiveFile(LiveID, Title);
            _context.SaveChanges();
            var isSuccess = VbeeController.ConvertTextAsync(Text, Voice, Rate, Entity.LiveFileID);
            if (!isSuccess)
            {
                throw new BusinessException("Chuyển đổi Text sang Audio thất bại, vui lòng kiểm tra lại");
            }
            return Entity;
        }
        public JsonResult Move(long LiveFileID, int Step)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            var liveFile = _context.LiveFile.Where(x => x.LiveFileID == LiveFileID).FirstOrDefault();
            var live = _context.Live.Find(liveFile.LiveID);
            if (live == null || user.UnitID != live.UnitID)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            if (liveFile.NumberOrder.GetValueOrDefault() == 0)
            {
                var listFile = _context.LiveFile.Where(x => x.LiveID == live.LiveID).OrderBy(x => x.LiveFileID).ToList();
                var idx = 1;
                foreach (var item in listFile)
                {
                    item.NumberOrder = idx++;
                }
            }
            var query = _context.LiveFile.Where(x => x.LiveID == live.LiveID);
            int nowOrder = liveFile.NumberOrder.Value;
            if (Step > 0 && nowOrder > 1)
            {
                query = query.Where(x => x.NumberOrder < nowOrder);
                liveFile.NumberOrder = nowOrder - 1;
            }
            else
            {
                query = query.Where(x => x.NumberOrder > nowOrder);
                liveFile.NumberOrder = nowOrder + 1;
            }
            var scopeFile = query.FirstOrDefault();
            if (scopeFile != null)
            {
                scopeFile.NumberOrder = nowOrder;
            }
            _context.SaveChanges();
            return Json("Thay đổi vị trí thành công", JsonRequestBehavior.AllowGet);
        }


        public JsonResult CheckConvertText(long LiveFileID)
        {
            var entity = _context.LiveFile.Find(LiveFileID);
            if (entity == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            var res = string.IsNullOrWhiteSpace(entity.FileName) ? 0 : GlobalConstant.IS_ACTIVE;
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddToPlaylist(long LiveID, string Text, string Voice, string Title, double Rate = 0)
        {
            Live live = _context.Live.Find(LiveID);
            if (live == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            CheckUnit(SessionManager.GetUserInfoBO().UnitID.GetValueOrDefault(), live.UnitID);
            LiveFile entity = null;
            if (Request.Files != null && (Text == null || Voice == null))
            {
                ProcessFile(LiveID);
            }
            else if (Text != null && Voice != null)
            {
                entity = ProcessConvertText(LiveID, Text, Voice, Rate, Title);
            }
            live.Status = GlobalConstant.LIVE_DRAFF;
            live.PlayTime = null;
            live.IsChangeFile = GlobalConstant.IS_ACTIVE;
            _context.SaveChanges();
            if (Text != null && Voice != null)
            {
                return Json(entity.LiveFileID, JsonRequestBehavior.AllowGet);
            }
            return Json("Thêm vào danh sách phát thành công", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult UpdateUnit(long liveID, int unitID)
        {
            Live live = _context.Live.Where(x => x.LiveID == liveID).FirstOrDefault();
            if (live == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            CheckUnit(SessionManager.GetUserInfoBO().UnitID.GetValueOrDefault(), live.UnitID);
            if (live.Status == GlobalConstant.LIVE_PLAYING)
            {
                throw new BusinessException("Chương trình trực tiếp đang phát, không được thay đổi phạm vi phát");
            }
            live.UnitID = unitID;
            live.LiveType = GlobalConstant.LIVE_TYPE_ALL;
            _context.SaveChanges();
            return Json("Cập nhật phạm vi phát thành công", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SaveLive(long LiveID, int? channelID, int? LiveSource, bool isLoop = false, int loopCount = GlobalConstant.LOOP_COUNT)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            Live live = _context.Live.Where(x => x.LiveID == LiveID).FirstOrDefault();
            if (live == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            CheckUnit(user.UnitID.GetValueOrDefault(), live.UnitID);
            live.LiveType = GlobalConstant.LIVE_TYPE_ALL;
            if (channelID == 0)
            {
                channelID = null;
            }
            live.ChannelID = channelID;
            live.LiveSource = LiveSource;
            live.LoopCount = null;
            if (LiveSource.GetValueOrDefault() == GlobalConstant.LIVE_SOURCE_FILE && isLoop)
            {
                live.LoopCount = loopCount;
            }
            if (live.IsChangeFile.GetValueOrDefault() == GlobalConstant.IS_ACTIVE)
            {
                var listFile = _context.LiveFile
                    .Where(a => a.LiveID == LiveID && a.FileUrl != null && a.FileName != null)
                    .ToList();
                SetupInfo PlayList = new SetupInfo();
                if (listFile.Any())
                {
                    PlayList.StartTime = DateTime.Now;
                    PlayList.Channel = LiveID.ToString();
                    var listPlayList = listFile.Select(a => new AudioInfo
                    {
                        FilePath = Path.Combine(a.FileUrl, a.FileName)
                    });
                    if (listPlayList.ToList() == null)
                    {
                        throw new BusinessException("Bạn chưa thêm danh sách phát");
                    }
                    PlayList.AudioInfos = listPlayList.ToList();
                    List<BroadcastInfo> ListSetup = ScheduleUlti.SplitAudioFile(PlayList.AudioInfos);
                    string FilePath = "";
                    double totalTime = 0;
                    foreach (var entity in listFile)
                    {
                        FilePath = Path.Combine(entity.FileUrl, entity.FileName);
                        var objBroadcast = ListSetup.Where(a => a.FilePath == FilePath).FirstOrDefault();
                        entity.Duration = objBroadcast.Duration;
                        entity.SplitedFolder = objBroadcast.TempFolder;
                        totalTime += entity.Duration.GetValueOrDefault();
                    }
                    live.TotalDuration = totalTime;
                    ScheduleUlti.SplitAudioFile(ListSetup, GlobalConstant.BITRATE_128);
                    ScheduleUlti.SplitAudioFile(ListSetup, GlobalConstant.BITRATE_32);
                }
            }
            if (live.Status.GetValueOrDefault() < GlobalConstant.LIVE_WAIT)
            {
                live.Status = GlobalConstant.LIVE_WAIT;
            }
            live.IsChangeFile = null;
            _context.SaveChanges();
            return Json("Lên lịch phát trực tiếp thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetLiveID()
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            var unit = _context.Unit.Find(user.UnitID);
            var live = (from x in _context.Live
                        join u in _context.Unit on x.UnitID equals u.UnitID
                        where x.Status >= GlobalConstant.LIVE_PLAYING
                        && unit.UnitPath.Contains(u.UnitPath)
                        orderby u.UnitPath descending, x.LiveID descending, x.Status descending
                        select new LiveModel
                        {
                            ChannelID = x.ChannelID,
                            LiveID = x.LiveID,
                            LiveType = x.LiveType,
                            Status = x.Status,
                            LiveSource = x.LiveSource,
                            UnitID = x.UnitID,
                            CreatedDate = x.CreatedDate
                        }).FirstOrDefault();
            long liveID = 0;
            if (live != null)
            {
                liveID = live.LiveID;
            }
            return Json(liveID, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetNowPlaying()
        {
            int playing = 0;
            UserInfoBO user = SessionManager.GetUserInfoBO();
            var live = (from l in _context.Live
                        join u in _context.Unit on l.UnitID equals u.UnitID
                        where l.Status >= GlobalConstant.LIVE_PLAYING
                        orderby l.LiveID
                        select l).FirstOrDefault();
            if (live != null)
            {
                playing = GlobalConstant.IS_ACTIVE;
            }
            return Json(playing, JsonRequestBehavior.AllowGet);
        }

        public FileResult Download(long LiveFileID)
        {
            var att = this._context.LiveFile.Find(LiveFileID);
            if (att == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            byte[] fileData = System.IO.File.ReadAllBytes(att.FileUrl + "\\" + att.FileName);
            return File(fileData, System.Net.Mime.MediaTypeNames.Application.Octet, att.FileName);
        }

        public static long GetStreamingCount(Entities context, Live live)
        {
            return context.LiveStreaming.LongCount(x => x.LiveId == live.LiveID);
        }

        public static StringBuilder GetStreaming(Entities context, Live live)
        {
            StringBuilder str = new StringBuilder();
            str.AppendLine("#EXTM3U");
            str.AppendLine("#EXT-X-VERSION:3");
            str.AppendLine("#EXT-X-ALLOW-CACHE:YES");
            if (live == null || live.Status < GlobalConstant.LIVE_PLAYING || live.LiveSource.GetValueOrDefault() != GlobalConstant.LIVE_SOURCE_STREAMING)
            {
                return str;
            }
            var sc = ConfigCaching.GetSystemConfig();
            int minCountFile = (sc == null || sc.StreamingCountFile.GetValueOrDefault() == 0 ? GlobalConstant.DEFAULT_STREAMING_COUNT_FILE : sc.StreamingCountFile.GetValueOrDefault());
            var liveStreamingFiles = context.LiveStreaming.Where(x => x.LiveId == live.LiveID).OrderByDescending(x => x.OrderNumber).Take(minCountFile).ToList()
                .OrderBy(x => x.OrderNumber).ToList();
            if (liveStreamingFiles.Count == minCountFile)
            {
                var interval = (sc == null || sc.StreamingInterval.GetValueOrDefault() == 0 ? GlobalConstant.DEFAULT_STREAMING_INTERVAL : sc.StreamingInterval.GetValueOrDefault());
                var currentDt = DateTime.Now;
                double differTime = (currentDt - new DateTime(currentDt.Year, 1, 1)).TotalSeconds;
                long seq = liveStreamingFiles[0].Sequence;
                str.AppendLine("#EXT-X-TARGETDURATION:" + (interval + 1));
                str.AppendLine("#EXT-X-MEDIA-SEQUENCE:" + seq);
                var path = GlobalConstant.MEDIA_SERVER + "/live/" + live.StreamingFolder + "/aac";
                foreach (var file in liveStreamingFiles)
                {
                    var duration = file.Duration;
                    if (duration == 0)
                    {
                        duration = 1.0 * interval;
                    }
                    str.AppendLine("#EXTINF:" + duration + ",");
                    str.AppendLine(path + "/" + file.FileName + ".aac");
                };
            }
            return str;
        }

        private static void LoopFiles(List<LiveFile> files, int round)
        {
            List<LiveFile> listTemp = new List<LiveFile>();
            listTemp.AddRange(files);
            for (int i = 0; i < round; i++)
            {
                files.AddRange(listTemp);
            }
        }

        public static StringBuilder GetLiveFromFile(Entities context, Live live, long bitrate)
        {
            DateTime currentDt = DateTime.Now;
            long hourOfDay = currentDt.Hour;
            long minuteOfDay = currentDt.Minute;
            long minutes = 60 * hourOfDay + minuteOfDay;
            long seconds = 60 * minutes + currentDt.Second;
            var listFile = context.LiveFile.Where(x => x.LiveID == live.LiveID).OrderBy(x => x.NumberOrder).ThenBy(x => x.LiveFileID).ToList();
            if (live.LoopCount.GetValueOrDefault() > 0)
            {
                LoopFiles(listFile, live.LoopCount.GetValueOrDefault());
            }
            long differTime = 0;
            if (live.PlayTime == null)
            {
                live.PlayTime = currentDt;
                context.SaveChanges();
            }
            if (live.Status == GlobalConstant.LIVE_PLAYING && live.PlayTime != null)
            {
                var playTime = live.PlayTime.Value;
                var minutesPlay = 60 * playTime.Hour + playTime.Minute;
                differTime = 60 * minutesPlay + playTime.Second - seconds;
            }

            //Build m3u8 file
            StringBuilder str = new StringBuilder();
            str.AppendLine("#EXTM3U");
            str.AppendLine("#EXT-X-VERSION:3");
            str.AppendLine("#EXT-X-ALLOW-CACHE:YES");
            str.AppendLine("#EXT-X-TARGETDURATION:" + (GlobalConstant.CHUNK_DURATION + 1));
            var sc = ConfigCaching.GetSystemConfig();
            int waitingAutioDur = (sc != null && sc.IsWaitingOn.GetValueOrDefault() == GlobalConstant.IS_ACTIVE) ? GlobalConstant.WAITING_AUDIO_DURATION : 1;
            double remainTime = -differTime;
            int seq = 1;
            if (differTime < waitingAutioDur)
            {
                str.AppendLine("#EXT-X-MEDIA-SEQUENCE:-1");
                if (differTime > 0)
                {
                    //@TODO: insert nhac cho here

                    int start = 0;
                    string folderPath = GlobalConstant.WAITING_AUDIO_FOLDER + "/" + bitrate;
                    string prefix = GlobalConstant.MEDIA_SERVER + "/" + folderPath + "/" + GlobalConstant.CHUNK_PREFIX;
                    List<string> lstDuration = ScheduleUlti.ReadPlayListFile(folderPath);
                    string filePathPrefix = GlobalConstant.MEDIA_FOLDER + "/" + folderPath + "/" + GlobalConstant.CHUNK_PREFIX;
                    string currentFilePath = filePathPrefix + start + GlobalConstant.CHUNK_SUFFIX;
                    while (System.IO.File.Exists(currentFilePath) && differTime > 0)
                    {
                        str.AppendLine(lstDuration[start]);
                        str.AppendLine(prefix + start + GlobalConstant.CHUNK_SUFFIX);
                        start++;
                        differTime -= GlobalConstant.CHUNK_DURATION;
                        currentFilePath = filePathPrefix + start + GlobalConstant.CHUNK_SUFFIX;
                    }
                    remainTime = 0;
                }
                int minCountFile = (sc == null || sc.StreamingCountFile.GetValueOrDefault() == 0 ? GlobalConstant.DEFAULT_STREAMING_COUNT_FILE : sc.StreamingCountFile.GetValueOrDefault());
                int totalFile = 0;
                foreach (var program in listFile)
                {
                    int start = 0;
                    string folderPath = program.SplitedFolder + "/" + bitrate;
                    string prefix = GlobalConstant.MEDIA_SERVER + "/" + folderPath + "/" + GlobalConstant.CHUNK_PREFIX;
                    if (remainTime > 0)
                    {
                        if (remainTime >= program.Duration)
                        {
                            remainTime -= program.Duration.Value;
                            seq += ScheduleUlti.ReadPlayListFile(folderPath).Count;
                            continue;
                        }
                        else
                        {
                            start = (int)Math.Floor(remainTime / GlobalConstant.CHUNK_DURATION);
                            seq += start;
                            remainTime = 0;
                        }
                    }
                    List<string> lstDuration = ScheduleUlti.ReadPlayListFile(folderPath);
                    string filePathPrefix = GlobalConstant.MEDIA_FOLDER + "/" + folderPath + "/" + GlobalConstant.CHUNK_PREFIX;
                    string currentFilePath = filePathPrefix + start + GlobalConstant.CHUNK_SUFFIX;
                    while (System.IO.File.Exists(currentFilePath) && totalFile < minCountFile)
                    {
                        str.AppendLine(lstDuration[start]);
                        str.AppendLine(prefix + start + GlobalConstant.CHUNK_SUFFIX);
                        start++;
                        currentFilePath = filePathPrefix + start + GlobalConstant.CHUNK_SUFFIX;
                        totalFile++;
                        if (totalFile >= minCountFile)
                        {
                            str.Replace("#EXT-X-MEDIA-SEQUENCE:-1", "#EXT-X-MEDIA-SEQUENCE:" + seq);
                            return str;
                        }
                    }
                }
                str.Replace("#EXT-X-MEDIA-SEQUENCE:-1", "#EXT-X-MEDIA-SEQUENCE:" + seq);
            }
            else
            {
                str.AppendLine("#EXT-X-MEDIA-SEQUENCE:0");
                str.AppendLine("#EXT-X-ALLOW-CACHE:YES");
            }
            return str;
        }
        public JsonResult DeleteFile(long LiveFileID)
        {
            // Kiem tra don vi
            UserInfoBO user = SessionManager.GetUserInfoBO();
            var objFile = _context.LiveFile.Find(LiveFileID);
            if (objFile != null)
            {
                var live = _context.Live.Find(objFile.LiveID);
                if (user.UnitID != live.UnitID)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                if (live.Status >= GlobalConstant.LIVE_PLAYING && live.LiveSource.GetValueOrDefault() == GlobalConstant.LIVE_SOURCE_FILE)
                {
                    throw new BusinessException("Bạn không được phép xóa File đang phát");
                }
                if (user.UnitID == GlobalConstant.UNIT_TYPE_PROVINCE)
                {
                    _context.LiveFile.Remove(objFile);
                }
                else
                {
                    if (user.UnitID != live.UnitID)
                    {
                        throw new BusinessException("Dữ liệu không hợp lệ");
                    }
                    else
                    {
                        _context.LiveFile.Remove(objFile);
                    }
                }
                live.Status = GlobalConstant.LIVE_DRAFF;
                live.PlayTime = null;
                live.IsChangeFile = GlobalConstant.IS_ACTIVE;
                var listFile = _context.LiveFile.Where(x => x.LiveID == live.LiveID).OrderBy(x => x.NumberOrder).ThenBy(x => x.LiveFileID).ToList();
                var idx = 1;
                foreach (var item in listFile)
                {
                    item.NumberOrder = idx++;
                }
                _context.SaveChanges();
            }
            else
            {
                throw new BusinessException("Không tồn tại chương trình hoặc chương trình đã bị xóa");
            };
            return Json("Xóa chương trình thành công", JsonRequestBehavior.AllowGet);
        }
        public static void StopStreamLive(Entities context, Live live, int ExceptDeviceID = 0)
        {
            var unitID = live.UnitID;
            StaticCheckUnit(context, unitID, live.UnitID);
            List<int> listDeviceID = null;
            List<Device> listDevice = null;
            if (live.LiveType == GlobalConstant.LIVE_TYPE_ALL)
            {
                string sUnit = "/" + unitID + "/";
                listDevice = context.Device
                    .Where(x => context.Unit.Any(u => u.UnitID == x.UnitID && u.UnitPath.Contains(sUnit))).ToList();
            }
            else
            {
                listDeviceID = context.LiveDevice.Where(x => x.LiveID == live.LiveID).Select(x => x.DeviceID).ToList();
            }
            if (listDevice == null)
            {
                listDevice = context.Device.Where(x => listDeviceID.Contains(x.DeviceID)).ToList();
            }
            if (listDevice != null && listDevice.Any())
            {
                foreach (Device device in listDevice)
                {
                    if (device.DeviceID == ExceptDeviceID)
                    {
                        continue;
                    }
                    device.IsRequestStop = GlobalConstant.IS_ACTIVE;
                    device.IsRequestStart = null;
                    device.IsFmStart = null;
                    if (StreamingCaching.DicPlaying.ContainsKey(device.DeviceID))
                    {
                        StreamingCaching.DicPlaying[device.DeviceID] = null;
                    }
                }
            }
            live.Status = GlobalConstant.LIVE_STOPED;
            live.IsStreaming = 0;
            context.SaveChanges();
        }
        public JsonResult StopStream(long liveID)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            Live live = _context.Live.Find(liveID);
            if (live == null || live.UnitID != user.UnitID)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            StopStreamLive(_context, live);
            return Json("Yêu cầu dừng phát thiết bị thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult StartStream(int liveID, int source = -1, int channelID = -1, bool isLoop = false, int loopCount = GlobalConstant.LOOP_COUNT)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            Live live = _context.Live.Find(liveID);
            CheckUnit(user.UnitID.GetValueOrDefault(), live.UnitID);
            if (source > 0)
            {
                live.LiveSource = source;
            }
            if (live.LiveSource.GetValueOrDefault() == GlobalConstant.LIVE_SOURCE_VOV)
            {
                if (channelID > 0)
                {
                    live.ChannelID = channelID;
                    live.LiveSource = GlobalConstant.LIVE_SOURCE_VOV;
                }
            }
            if (source > 0 || (channelID >= 0 && live.LiveSource.GetValueOrDefault() == GlobalConstant.LIVE_SOURCE_VOV))
            {
                _context.SaveChanges();
            }
            string msg = "Cập nhật thông tin thành công";
            if ((source < 0 && channelID < 0) || live.Status.GetValueOrDefault() == GlobalConstant.LIVE_PLAYING)
            {
                bool isFm = false;
                if (live.LiveSource.GetValueOrDefault() == GlobalConstant.LIVE_SOURCE_FILE)
                {
                    if (!_context.LiveFile.Where(a => a.LiveID == liveID).Any())
                    {
                        throw new BusinessException("Chương trình phát không có file âm thanh");
                    }
                }
                if (live.LiveSource.GetValueOrDefault() == GlobalConstant.LIVE_SOURCE_VOV)
                {
                    if (live.ChannelID.GetValueOrDefault() == 0)
                    {
                        throw new BusinessException("Chương trình phát trực tiếp không có file chuyển tiếp");
                    }
                    var channel = _context.ForwardChanel.Find(live.ChannelID);
                    if (channel == null)
                    {
                        throw new BusinessException("Không tồn tại kênh phát");
                    }
                    if (channel.Fm == GlobalConstant.IS_ACTIVE)
                    {
                        isFm = true;
                    }
                }
                // Kiem tra co chuong trinh phat khac
                var playingLive = (from l in _context.Live
                                   join u in _context.Unit on l.UnitID equals u.UnitID
                                   where l.Status >= GlobalConstant.LIVE_PLAYING
                                   && l.UnitID == user.UnitID && l.LiveID != liveID
                                   select l).FirstOrDefault();
                if (playingLive != null)
                {
                    throw new BusinessException("Chương trình trực tiếp đang phát");
                }
                List<Device> listDevice = null;
                if (live.LiveType == GlobalConstant.LIVE_TYPE_ALL)
                {
                    string sUnit = "/" + user.UnitID + "/";
                    listDevice = _context.Device
                        .Where(x => ((x.UnitID == null && user.IsSupperAdmin == GlobalConstant.IS_ACTIVE) || _context.Unit.Any(u => u.UnitID == x.UnitID && u.UnitPath.Contains(sUnit)))).ToList();
                }
                else
                {
                    listDevice = _context.Device.Where(x => _context.LiveDevice.Any(l => l.LiveID == liveID && l.DeviceID == x.DeviceID)).ToList();
                }
                if (listDevice != null && listDevice.Any())
                {
                    foreach (Device device in listDevice)
                    {
                        if (isFm)
                        {
                            device.IsFmStart = GlobalConstant.IS_ACTIVE;
                        }
                        else
                        {
                            device.IsRequestStop = GlobalConstant.IS_ACTIVE;
                            device.IsFmStart = null;
                        }
                        device.IsRequestStart = null;
                        if (StreamingCaching.DicPlaying.ContainsKey(device.DeviceID))
                        {
                            StreamingCaching.DicPlaying[device.DeviceID] = null;
                        }
                    }
                }
                if (live.Status <= GlobalConstant.LIVE_WAIT)
                {
                    live.Status = GlobalConstant.LIVE_PLAYING;
                }
                if (live.LiveSource.GetValueOrDefault() == GlobalConstant.LIVE_SOURCE_FILE)
                {
                    live.PlayTime = null;
                }
                live.LoopCount = null;
                if (live.LiveSource == GlobalConstant.LIVE_SOURCE_FILE && isLoop)
                {
                    live.LoopCount = loopCount;
                }
                if (live.LiveSource.GetValueOrDefault() == GlobalConstant.LIVE_SOURCE_STREAMING)
                {
                    live.StreamingFolder = Guid.NewGuid().ToString();
                    var FilePath = GlobalConstant.MEDIA_FOLDER + "live/" + live.StreamingFolder;
                    var WavFilePath = FilePath + "/wav";
                    var AacFilePath = FilePath + "/aac";

                    //Check whether Directory (Folder) exists.
                    if (!Directory.Exists(FilePath))
                    {
                        //If Directory (Folder) does not exists. Create it.
                        Directory.CreateDirectory(FilePath);
                    }
                    if (!Directory.Exists(WavFilePath))
                    {
                        //If Directory (Folder) does not exists. Create it.
                        Directory.CreateDirectory(WavFilePath);
                    }
                    if (!Directory.Exists(AacFilePath))
                    {
                        //If Directory (Folder) does not exists. Create it.
                        Directory.CreateDirectory(AacFilePath);
                    }

                    _context.Database.ExecuteSqlCommand("delete from LiveStreaming where LiveId = @liveId", new SqlParameter("liveId", live.LiveID));

                }
                msg = "Yêu cầu phát thiết bị thành công";
            }
            _context.SaveChanges();
            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        public JsonResult PublishLive(long LiveID)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            Live live = _context.Live.Find(LiveID);
            if (live == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            CheckUnit(user.UnitID.GetValueOrDefault(), live.UnitID);
            live.IsChangeFile = null;
            var listFile = _context.LiveFile
                .Where(a => a.LiveID == LiveID)
                .ToList();
            SetupInfo PlayList = new SetupInfo();
            //if (live.Status == GlobalConstant.LIVE_STOPED)
            //{
            //    throw new BusinessException("Chương trình đã dừng phát");
            //}
            if (live.Status >= GlobalConstant.LIVE_PLAYING)
            {
                throw new BusinessException("Chương trình đang phát");
            }
            if (!listFile.Any())
            {
                throw new BusinessException("Chương trình không có file phát thanh");
            }
            if (live.Status != GlobalConstant.LIVE_STOPED && live.Status <= GlobalConstant.LIVE_WAIT && listFile.Any())
            {
                PlayList.StartTime = DateTime.Now;
                PlayList.Channel = LiveID.ToString();
                var listPlayList = listFile.Select(a => new AudioInfo
                {
                    FilePath = Path.Combine(a.FileUrl, a.FileName)
                });
                if (listPlayList.ToList() == null)
                {
                    throw new BusinessException("Bạn chưa thêm danh sách phát");
                }
                PlayList.AudioInfos = listPlayList.ToList();
                List<BroadcastInfo> ListSetup = ScheduleUlti.SplitAudioFile(PlayList.AudioInfos);
                string FilePath = "";
                foreach (var entity in listFile)
                {
                    FilePath = Path.Combine(entity.FileUrl, entity.FileName);
                    var objBroadcast = ListSetup.Where(a => a.FilePath == FilePath).FirstOrDefault();
                    entity.Duration = objBroadcast.Duration;
                    entity.SplitedFolder = objBroadcast.TempFolder;
                }
                live.Status = GlobalConstant.LIVE_WAIT;
                _context.SaveChanges();
                Task.Factory.StartNew(() => ScheduleUlti.SplitAudioFile(ListSetup, GlobalConstant.BITRATE_128));
                Task.Factory.StartNew(() => ScheduleUlti.SplitAudioFile(ListSetup, GlobalConstant.BITRATE_32));
            }
            return Json("Lên lịch phát thành công", JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        [SkipActionLogFilter]
        public FileResult Live(long unitID, long deviceID = 0, long bitrate = 128)
        {
            if (bitrate == 0)
            {
                bitrate = GlobalConstant.DEFAULT_BIT_RATE;
            }
            StringBuilder str = null;
            using (Entities context = new Entities())
            {
                var config = context.OrderConfig.First();
                Unit unit = context.Unit.Find(unitID);
                DateTime currentDt = DateTime.Now;
                var live = (from l in context.Live
                            join u in context.Unit on l.UnitID equals u.UnitID
                            where l.Status >= GlobalConstant.LIVE_PLAYING
                            && unit.UnitPath.Contains(u.UnitPath)
                            && (l.LiveSource != GlobalConstant.LIVE_SOURCE_FILE
                                     || (l.TotalDuration != null && (l.PlayTime == null || DbFunctions.DiffSeconds(l.PlayTime, currentDt) < l.TotalDuration * (1 + (l.LoopCount == null ? 0 : l.LoopCount)))))
                            orderby (u.UnitLevel == GlobalConstant.UNIT_TYPE_VILLAGE ? config.VillageOrder
                                    : u.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE ? config.CommuneOrder
                                    : u.UnitLevel == GlobalConstant.UNIT_TYPE_DISTRICT ? config.DistrictOrder
                                    : u.UnitLevel == GlobalConstant.UNIT_TYPE_PROVINCE ? config.ProvinceOrder : 0),
                                    l.LiveID
                            select l).FirstOrDefault();
                bool living = false;
                if (live != null)
                {
                    if (live.LiveType == GlobalConstant.LIVE_TYPE_CHOICE)
                    {
                        if (context.LiveDevice.Any(x => x.LiveID == live.LiveID))
                        {
                            living = true;
                        }
                    }
                    else
                    {
                        living = true;
                    }
                }
                if (living)
                {
                    int liveSource = live.LiveSource.GetValueOrDefault();
                    switch (liveSource)
                    {
                        case GlobalConstant.LIVE_SOURCE_STREAMING:
                            str = GetStreaming(context, live);
                            break;
                        case GlobalConstant.LIVE_SOURCE_FILE:
                            str = GetLiveFromFile(context, live, bitrate);
                            break;
                        case GlobalConstant.LIVE_SOURCE_VOV:
                            if (live.ChannelID != null)
                            {
                                var channel = context.ForwardChanel.Find(live.ChannelID);
                                if (channel != null)
                                {
                                    string channelCode = channel.ChannelCode.ToLower();
                                    str = PlayController.GetVOV(channelCode, unitID);
                                }
                            }
                            break;
                    }
                }
            }
            if (str == null)
            {
                str = new StringBuilder();
                str.AppendLine("#EXTM3U");
                str.AppendLine("#EXT-X-VERSION:3");
                str.AppendLine("#EXT-X-TARGETDURATION:11");
            }
            string content = str.ToString();
            byte[] arr = Encoding.UTF8.GetBytes(content);

            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = "playlist.m3u8",
                Inline = false,
            };

            Response.AppendHeader("Content-Disposition", cd.ToString());
            return File(arr, "m3u8");
        }
        [HttpPost]
        public JsonResult StreamingUpload(long liveID)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            var live = _context.Live.Find(liveID);
            if (live.Status != GlobalConstant.LIVE_PLAYING || live.LiveSource != GlobalConstant.LIVE_SOURCE_STREAMING)
            {
                return Json("stoprecord");
            }
            if (string.IsNullOrWhiteSpace(live.StreamingFolder))
            {
                live.StreamingFolder = Guid.NewGuid().ToString();
                _context.Database.ExecuteSqlCommand("delete from LiveStreaming where LiveId = @liveId", new SqlParameter("liveId", live.LiveID));
            }
            var FilePath = GlobalConstant.MEDIA_FOLDER + "live/" + live.StreamingFolder;
            var WavFilePath = FilePath + "/wav";
            var AacFilePath = FilePath + "/aac";
            //Check whether Directory (Folder) exists.
            if (!Directory.Exists(WavFilePath))
            {
                //If Directory (Folder) does not exists. Create it.
                Directory.CreateDirectory(WavFilePath);
            }
            if (!Directory.Exists(AacFilePath))
            {
                //If Directory (Folder) does not exists. Create it.
                Directory.CreateDirectory(AacFilePath);
            }
            var fileName = "";
            foreach (string upload in Request.Files)
            {
                var ts = DateTimeOffset.Now.ToUnixTimeMilliseconds();
                fileName = ts + Guid.NewGuid().ToString();
                var wavFileName = fileName + ".wav";

                var file = Request.Files[upload];
                if (file == null) continue;
                using (var archive = new ZipArchive(file.InputStream))
                {
                    foreach (ZipArchiveEntry entry in archive.Entries)
                    {
                        using (var unzippedEntryStream = entry.Open())
                        {
                            using (var fileStream = System.IO.File.Create(Path.Combine(WavFilePath, wavFileName)))
                            {
                                unzippedEntryStream.CopyTo(fileStream);
                            }
                        }
                    }
                }
                Task.Factory.StartNew(() =>
                {
                    try
                    {
                        ScheduleUlti.WavToAac(FilePath, fileName);
                        double duration = ScheduleUlti.GetDuration(AacFilePath + "/" + fileName + ".aac");
                        using (var pContext = new Entities())
                        {
                            var lastLiveFile = pContext.LiveStreaming.Where(x => x.LiveId == live.LiveID).OrderByDescending(x => x.OrderNumber).FirstOrDefault();
                            long seq = 1;
                            if (lastLiveFile != null)
                            {
                                seq = lastLiveFile.Sequence + 1;
                            }
                            var liveStream = new LiveStreaming
                            {
                                LiveId = live.LiveID,
                                FileName = fileName,
                                Duration = duration,
                                OrderNumber = ts,
                                Sequence = seq
                            };
                            pContext.LiveStreaming.Add(liveStream);
                            pContext.SaveChanges();
                        }
                    }
                    catch (Exception e)
                    {
                        Logger.Error(e);
                    }
                });
            }
            return Json(fileName + ".wav");
        }

        public PartialViewResult GetPlayingLives(int page = 1)
        {
            var lives = from l in _context.Live
                        join u in _context.Unit on l.UnitID equals u.UnitID
                        where l.Status >= GlobalConstant.LIVE_PLAYING
                        orderby u.UnitPath descending, l.LiveID
                        select new LiveModel
                        {
                            UnitID = l.UnitID,
                            UnitName = u.UnitName,
                            LiveSource = l.LiveSource,
                            PlayingTime = l.PlayTime
                        };
            PagedList<LiveModel> paginate = new PagedList<LiveModel>(lives, page, GlobalConstant.PAGE_SIZE);
            ViewData["listPlayingLive"] = paginate;
            return PartialView("_LstPlayingLive");
        }

        public PartialViewResult PrepareConvertText(long LiveID)
        {
            ConvertTextModel model = new ConvertTextModel
            {
                LiveID = LiveID
            };
            ViewData["Voices"] = GlobalCommon.Voices();
            return PartialView("_AddText", model);
        }

        public void CheckUnit(int unitID, int liveUnitID)
        {
            StaticCheckUnit(_context, unitID, liveUnitID);
        }

        public static void StaticCheckUnit(Entities context, int unitID, int liveUnitID)
        {
            Unit unit = context.Unit.Find(unitID);
            Unit liveUnit = context.Unit.Find(liveUnitID);
            if (!liveUnit.UnitPath.Contains(unit.UnitPath))
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}