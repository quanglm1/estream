using eStream.Common.CommonClass;
using eStream.Common.CommonObject;
using eStream.Dto;
using eStream.Helpers.Filter;
using eStream.Models;
using Model;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace eStream.Controllers
{
    public partial class ControlController : Controller
    {
        private readonly Entities _context = new Entities();
        [BreadCrumb(ControllerName = "Thiết lập lịch", ActionName = "", AreaName = "")]
        // GET: Center

        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult GetSchedule(DateTime? WeekDate)
        {
            ViewData["ListSchedule"] = GetAllSchedule();
            ViewData["ListProgramInSchedule"] = GetAllProgramInSchedule(null);
            DateTime startWeek = DateTime.Now.StartOfWeek(DayOfWeek.Monday);
            ViewData["StartDateOfWeek"] = WeekDate != null ? WeekDate : startWeek;
            return PartialView("_ListSchedule");
        }

        public PartialViewResult GetScheduleDays(int ScheduleID, DateTime StartWeek, int isNext)
        {
            ScheduleModel model = _context.Schedule.Where(x => x.ScheduleID == ScheduleID).Select(x => new ScheduleModel
            {
                ScheduleID = x.ScheduleID,
                CreateDate = x.CreateDate,
                Frequency = x.Frequency,
                OffHour = x.OffHour,
                OffMinus = x.OffMinus,
                ScheduleHour = x.ScheduleHour,
                ScheduleMinus = x.ScheduleMinus,
                ScheduleName = x.ScheduleName,
                Status = x.Status
            }).FirstOrDefault();
            if (model == null)
            {
                throw new BusinessException("Không tồn tại khung giờ phát đã chọn");
            }
            StartWeek = StartWeek.StartOfWeek(DayOfWeek.Monday);
            if (isNext == 1)
            {
                StartWeek = StartWeek.AddDays(7);
            }
            else if (isNext == -1)
            {
                StartWeek = StartWeek.AddDays(-7);
            }
            ViewData["StartDateOfWeek"] = StartWeek;
            ViewData["ListScheduleDay"] = GetAllProgramInSchedule(StartWeek, ScheduleID);
            return PartialView("_Schedule", model);
        }

        public PartialViewResult ReloadScheduleDays(int ScheduleID, DateTime StartWeek)
        {
            ScheduleModel model = _context.Schedule.Where(x => x.ScheduleID == ScheduleID).Select(x => new ScheduleModel
            {
                ScheduleID = x.ScheduleID,
                CreateDate = x.CreateDate,
                Frequency = x.Frequency,
                OffHour = x.OffHour,
                OffMinus = x.OffMinus,
                ScheduleHour = x.ScheduleHour,
                ScheduleMinus = x.ScheduleMinus,
                ScheduleName = x.ScheduleName,
                Status = x.Status
            }).FirstOrDefault();
            if (model == null)
            {
                throw new BusinessException("Không tồn tại khung giờ phát");
            }
            StartWeek = StartWeek.StartOfWeek(DayOfWeek.Monday);
            ViewData["StartDateOfWeek"] = StartWeek;
            ViewData["ListScheduleDay"] = GetAllProgramInSchedule(StartWeek, ScheduleID);
            return PartialView("_Schedule", model);
        }

        public PartialViewResult ReloadScheduleDaysByProgram(int ProgramID)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            Program entity = _context.Program.Find(ProgramID);
            if (entity == null || user.UnitID != entity.UnitID || entity.ExecuteDate == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            ScheduleDay sDay = _context.ScheduleDay.Find(entity.ScheduleDayID);
            if (sDay == null)
            {
                throw new BusinessException("Không tồn tại chương trình phát đã chọn");
            }
            DateTime startExDate = entity.ExecuteDate.Value.StartOfWeek(DayOfWeek.Monday);

            ViewData["StartDateOfWeek"] = startExDate;
            ViewData["ListScheduleDay"] = GetAllProgramInSchedule(startExDate, sDay.ScheduleID);
            ScheduleModel model = _context.Schedule.Where(x => x.ScheduleID == sDay.ScheduleID).Select(x => new ScheduleModel
            {
                ScheduleID = x.ScheduleID,
                CreateDate = x.CreateDate,
                Frequency = x.Frequency,
                OffHour = x.OffHour,
                OffMinus = x.OffMinus,
                ScheduleHour = x.ScheduleHour,
                ScheduleMinus = x.ScheduleMinus,
                ScheduleName = x.ScheduleName,
                Status = x.Status
            }).FirstOrDefault();
            return PartialView("_Schedule", model);
        }

        public PartialViewResult SelectExpDis(List<int> unitIds)
        {
            if (unitIds == null)
            {
                unitIds = new List<int>();
            }
            var listDistrict = _context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE
                    && x.UnitLevel == GlobalConstant.UNIT_TYPE_DISTRICT).OrderBy(x => x.UnitName)
                    .Select(x => new UnitModel
                    {
                        UnitID = x.UnitID,
                        UnitName = x.UnitName
                    }).ToList();
            var selectUnitItems = new List<SelectListItem>();
            foreach (var d in listDistrict)
            {
                var listCommune = _context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE
                    && x.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE && x.ParentID == d.UnitID).OrderBy(x => x.UnitName)
                    .Select(x => new UnitModel
                    {
                        UnitID = x.UnitID,
                        UnitName = x.UnitName
                    }).ToList();
                var dis = new SelectListItem
                {
                    Value = d.UnitID.ToString(),
                    Text = d.UnitName,
                    Selected = unitIds != null && unitIds.Contains(d.UnitID)
                };
                selectUnitItems.Add(dis);
                if (unitIds.Contains(d.UnitID))
                {
                    continue;
                }
                foreach (var c in listCommune)
                {
                    var item = new SelectListItem
                    {
                        Value = c.UnitID.ToString(),
                        Text = "..." + c.UnitName,
                        Selected = unitIds != null && unitIds.Contains(c.UnitID),
                    };
                    selectUnitItems.Add(item);
                }
            }
            ViewData["ListUnit"] = selectUnitItems;
            return PartialView("_ListExceptUnit");
        }

        public PartialViewResult PrepareAdd(int? id)
        {
            // Kiem tra don vi
            UserInfoBO user = SessionManager.GetUserInfoBO();
            var hasPer = user.IsSupperAdmin.GetValueOrDefault() == GlobalConstant.IS_ACTIVE
                || (user.IsAdmin.GetValueOrDefault() == GlobalConstant.IS_ACTIVE
                && (user.UnitLevel == GlobalConstant.UNIT_TYPE_PROVINCE || user.UnitLevel == GlobalConstant.UNIT_TYPE_DISTRICT));
            if (!hasPer)
            {
                throw new BusinessException("Bạn không có quyền cấu hình khung thời gian phát");
            }
            ScheduleModel model = null;
            var listExceptUnitID = new List<int>();
            if (id != null && id > 0)
            {
                model = _context.Schedule.Where(x => x.ScheduleID == id).Select(x => new ScheduleModel
                {
                    ScheduleID = x.ScheduleID,
                    CreateDate = x.CreateDate,
                    Frequency = x.Frequency,
                    OffHour = x.OffHour,
                    OffMinus = x.OffMinus,
                    ScheduleHour = x.ScheduleHour,
                    ScheduleMinus = x.ScheduleMinus,
                    ScheduleName = x.ScheduleName,
                    Status = x.Status,
                    IsHidden = x.IsHidden == GlobalConstant.IS_ACTIVE,
                    IsStopStream = x.IsStopStream
                }).FirstOrDefault();
                if (model == null)
                {
                    throw new BusinessException("Không tồn tài khung thời gian phát đã chọn");
                }
                model.ListDay = _context.ScheduleDay.Where(x => x.ScheduleID == id).Select(x => x.DayOfWeek).ToList();
                listExceptUnitID = _context.ScheduleExcept.Where(x => x.ScheduleID == id &&
                _context.Unit.Any(u => u.UnitID == x.UnitID && u.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE)
                ).Select(x => x.UnitID).ToList();
                var disIds = _context.ScheduleExcept.Where(x => x.ScheduleID == id &&
                _context.Unit.Any(u => u.UnitID == x.UnitID && u.UnitLevel == GlobalConstant.UNIT_TYPE_DISTRICT)
                ).Select(x => x.UnitID).ToList().Select(x => (int?)x).ToList();
                var communes = _context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE && x.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE
                && disIds.Contains(x.ParentID)).ToList();
                foreach (var d in disIds)
                {
                    var disCommunes = communes.Where(x => x.ParentID == d).ToList();
                    var isAll = true;
                    foreach (var c in disCommunes)
                    {
                        if (!listExceptUnitID.Contains(c.UnitID))
                        {
                            isAll = false;
                            break;
                        }
                    }
                    if (isAll)
                    {
                        listExceptUnitID.RemoveAll(x => disCommunes.Any(u => u.UnitID == x));
                        listExceptUnitID.Add(d.Value);
                    }
                }
                model.ListExceptUnitID = listExceptUnitID;
            }
            ViewData["ListDayOfWeek"] = GlobalCommon.DayOfTheWeek();
            ViewData["ListFrequency"] = GlobalCommon.Frequency();
            var listDistrict = _context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE
                    && x.UnitLevel == GlobalConstant.UNIT_TYPE_DISTRICT).OrderBy(x => x.UnitName)
                    .Select(x => new UnitModel
                    {
                        UnitID = x.UnitID,
                        UnitName = x.UnitName
                    }).ToList();
            var selectUnitItems = new List<SelectListItem>();
            foreach (var d in listDistrict)
            {
                var listCommune = _context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE
                    && x.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE && x.ParentID == d.UnitID).OrderBy(x => x.UnitName)
                    .Select(x => new UnitModel
                    {
                        UnitID = x.UnitID,
                        UnitName = x.UnitName
                    }).ToList();
                var dis = new SelectListItem
                {
                    Value = d.UnitID.ToString(),
                    Text = d.UnitName,
                };
                selectUnitItems.Add(dis);
                if (listExceptUnitID.Contains(d.UnitID))
                {
                    continue;
                }
                foreach (var c in listCommune)
                {
                    var item = new SelectListItem
                    {
                        Value = c.UnitID.ToString(),
                        Text = "..." + c.UnitName,
                    };
                    selectUnitItems.Add(item);
                }
            }
            ViewData["ListUnit"] = selectUnitItems;
            return PartialView("_Add", model);
        }


        public PartialViewResult PrepareAddOrEditProgram(int ScheduleDayID, DateTime? Date)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            if (Date == null)
            {
                throw new BusinessException("Ngày tháng không hợp lệ");
            }
            ViewData["Date"] = Date;
            DateTime startDate = Date.Value;
            ScheduleDayModel model = _context.ScheduleDay
                .Where(a => a.ScheduleDayID == ScheduleDayID)
                .Select(a => new ScheduleDayModel
                {
                    ScheduleDayID = a.ScheduleDayID,
                    ScheduleID = a.ScheduleID,
                    DayOfWeek = a.DayOfWeek,
                    ListProgram = _context.Program.Where(p => p.ScheduleDayID == ScheduleDayID && p.UnitID == user.UnitID
                    && DbFunctions.TruncateTime(p.ExecuteDate) == DbFunctions.TruncateTime(startDate))
                    .OrderBy(x => x.NumberOrder).ThenBy(x => x.ProgramID)
                    .Select(p => new ScheduleProgramModel
                    {
                        ScheduleDayID = a.ScheduleDayID,
                        ProgramID = p.ProgramID,
                        Status = p.Status,
                        ExcuteDate = p.ExecuteDate
                    }).ToList()
                }).FirstOrDefault();
            if (model == null)
            {
                throw new BusinessException("Không tồn tại chương trình phải chọn");
            }
            Schedule schedule = _context.Schedule.Find(model.ScheduleID);
            if (schedule == null)
            {
                throw new BusinessException("Không tồn tại khung giờ phát");
            }
            var listChannelID =
                    (from a in _context.ProgramChanel
                     join b in _context.ScheduleDay on a.ScheduleDayID equals b.ScheduleDayID
                     where a.ScheduleDayID == ScheduleDayID && DbFunctions.TruncateTime(a.DateExecute) == DbFunctions.TruncateTime(startDate) && a.UnitID == user.UnitID
                     select a.ChanelID).ToList();
            if (listChannelID.Any())
            {
                model.ChannelID = listChannelID[0];
            }
            TimeSpan TimeExecute = new TimeSpan(schedule.ScheduleHour.GetValueOrDefault(), schedule.ScheduleMinus.GetValueOrDefault(), 0);
            ViewData["ExStartDate"] = startDate.Date + TimeExecute;
            var programLoop = _context.ProgramLoop.Where(x => x.ScheduleDayID == ScheduleDayID && x.UnitID == user.UnitID
                                        && DbFunctions.TruncateTime(x.DateExecute) == DbFunctions.TruncateTime(startDate)).FirstOrDefault();
            model.IsLoop = programLoop != null && programLoop.Status == GlobalConstant.IS_ACTIVE;
            model.LoopCount = programLoop != null && programLoop.Status == GlobalConstant.IS_ACTIVE ? programLoop.LoopCount : null;
            return PartialView("_AddPrograms", model);
        }

        public PartialViewResult PrepareConvertText(int ScheduleDayID, DateTime? Date)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            if (Date == null)
            {
                throw new BusinessException("Ngày tháng không hợp lệ");
            }
            ViewData["Date"] = Date;
            DateTime startDate = Date.Value;
            ConvertTextModel model = _context.ScheduleDay
                .Where(a => a.ScheduleDayID == ScheduleDayID)
                .Select(a => new ConvertTextModel
                {
                    ScheduleDayID = a.ScheduleDayID,
                    ScheduleID = a.ScheduleID
                }).FirstOrDefault();
            if (model == null)
            {
                throw new BusinessException("Không tồn tại chương trình phải chọn");
            }
            Schedule schedule = _context.Schedule.Find(model.ScheduleID);
            if (schedule == null)
            {
                throw new BusinessException("Không tồn tại khung giờ phát");
            }
            TimeSpan TimeExecute = new TimeSpan(schedule.ScheduleHour.GetValueOrDefault(), schedule.ScheduleMinus.GetValueOrDefault(), 0);
            ViewData["ExStartDate"] = startDate.Date + TimeExecute;
            ViewData["Voices"] = GlobalCommon.Voices();
            return PartialView("_AddText", model);
        }

        [HttpPost]
        public PartialViewResult GetPrograms(int ScheduleDayID, DateTime? Date)
        {
            if (Date != null)
            {
                Date = Date.Value.Date;
                UserInfoBO user = SessionManager.GetUserInfoBO();
                List<ScheduleProgramModel> ListSchedulePrograms =
                    (from a in _context.Program
                     join b in _context.ScheduleDay on a.ScheduleDayID equals b.ScheduleDayID
                     where a.ScheduleDayID == ScheduleDayID && DbFunctions.TruncateTime(a.ExecuteDate) == DbFunctions.TruncateTime(Date) && a.UnitID == user.UnitID
                     orderby a.NumberOrder, a.ProgramID
                     select new ScheduleProgramModel
                     {
                         ScheduleID = b.ScheduleID,
                         ScheduleDayID = a.ScheduleDayID,
                         ProgramID = a.ProgramID,
                         FileURL = a.FileUrl,
                         ExcuteDate = a.ExecuteDate,
                         Title = a.Title,
                         FileName = a.FileName,
                         CreatedDate = a.CreatedDate,
                         UserName = a.CreatedUser,
                         Status = a.Status
                     }).ToList();
                ViewData["ListSchedulePrograms"] = ListSchedulePrograms;
                ScheduleDay sDay = _context.ScheduleDay.Find(ScheduleDayID);
                if (sDay == null)
                {
                    throw new BusinessException("Không tồn tại chương trình phát đã chọn");
                }
                Schedule schedule = _context.Schedule.Find(sDay.ScheduleID);
                if (schedule == null)
                {
                    throw new BusinessException("Không tồn tại khung giờ phát");
                }
                TimeSpan TimeExecute = new TimeSpan(schedule.ScheduleHour.GetValueOrDefault(), schedule.ScheduleMinus.GetValueOrDefault(), 0);
                //DateTime exStartDate = Date.Value.Date.AddDays(sDay.DayOfWeek - 1).StartOfWeek(DayOfWeek.Monday);
                DateTime exStartDate = Date.Value.Date;
                exStartDate = exStartDate + TimeExecute;
                ViewData["ExStartDate"] = exStartDate;
                return PartialView("_ListPrograms");
            }
            else
            {
                throw new BusinessException("Ngày tháng không hợp lệ");
            }
        }

        [HttpPost]
        public PartialViewResult GetProgramChanels(int ScheduleDayID, DateTime? Date)
        {
            if (Date != null)
            {
                Date = Date.Value.Date;
                UserInfoBO user = SessionManager.GetUserInfoBO();
                List<ProgramChanelModel> ListProgramChanels =
                    (from a in _context.ProgramChanel
                     join b in _context.ScheduleDay on a.ScheduleDayID equals b.ScheduleDayID
                     join c in _context.ForwardChanel on a.ChanelID equals c.ChanelID
                     where a.ScheduleDayID == ScheduleDayID && DbFunctions.TruncateTime(a.DateExecute) == DbFunctions.TruncateTime(Date) && a.UnitID == user.UnitID
                     select new ProgramChanelModel
                     {
                         ScheduleID = b.ScheduleID,
                         ScheduleDayID = a.ScheduleDayID,
                         ProgramChanelID = a.ProgramChanelID,
                         FileURL = "",
                         ExcuteDate = a.DateExecute,
                         ChanelName = c.ChanelName
                     }).ToList();
                ViewData["ListProgramChanels"] = ListProgramChanels;
                ScheduleDay sDay = _context.ScheduleDay.Find(ScheduleDayID);
                if (sDay == null)
                {
                    throw new BusinessException("Không tồn tại chương trình phát đã chọn");
                }
                Schedule schedule = _context.Schedule.Find(sDay.ScheduleID);
                if (schedule == null)
                {
                    throw new BusinessException("Không tồn tại khung giờ phát");
                }
                TimeSpan TimeExecute = new TimeSpan(schedule.ScheduleHour.GetValueOrDefault(), schedule.ScheduleMinus.GetValueOrDefault(), 0);
                DateTime exStartDate = Date.Value.Date.AddDays(sDay.DayOfWeek - 1).StartOfWeek(DayOfWeek.Monday);
                exStartDate = exStartDate + TimeExecute;
                ViewData["ExStartDate"] = exStartDate;
                return PartialView("_ListProgramChanels");
            }
            else
            {
                throw new BusinessException("Ngày tháng không hợp lệ");
            }
        }

        public PartialViewResult ViewProgramChanels(int ScheduleDayID, DateTime? Date, int ToApprove = 0)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            Unit objUserUnit = _context.Unit.Find(user.UnitID);
            string sUnitID = "/" + objUserUnit.UnitID + "/";
            List<ProgramChanelModel> ListProgramChanels =
                   (from a in _context.ProgramChanel
                    join b in _context.ScheduleDay on a.ScheduleDayID equals b.ScheduleDayID
                    join c in _context.ForwardChanel on a.ChanelID equals c.ChanelID
                    join d in _context.Unit on a.UnitID equals d.UnitID
                    where a.ScheduleDayID == ScheduleDayID && DbFunctions.TruncateTime(a.DateExecute) == DbFunctions.TruncateTime(Date) && (user.IsSupperAdmin == GlobalConstant.IS_ACTIVE || d.UnitPath.Contains(sUnitID))
                    orderby d.UnitPath descending
                    select new ProgramChanelModel
                    {
                        ScheduleID = b.ScheduleID,
                        ScheduleDayID = a.ScheduleDayID,
                        ProgramChanelID = a.ProgramChanelID,
                        FileURL = "",
                        ExcuteDate = a.DateExecute,
                        ChanelName = c.ChanelName,
                        UnitName = d.UnitName
                    }).ToList();
            ViewData["ListProgramChanels"] = ListProgramChanels;

            List<ScheduleProgramModel> ListSchedulePrograms =
                (from a in _context.Program
                 join b in _context.ScheduleDay on a.ScheduleDayID equals b.ScheduleDayID
                 join d in _context.Unit on a.UnitID equals d.UnitID
                 where a.ScheduleDayID == ScheduleDayID && DbFunctions.TruncateTime(a.ExecuteDate) == DbFunctions.TruncateTime(Date) && (user.IsSupperAdmin == GlobalConstant.IS_ACTIVE || d.UnitPath.Contains(sUnitID))
                 orderby d.UnitPath descending, a.NumberOrder, a.ProgramID
                 select new ScheduleProgramModel
                 {
                     ScheduleID = b.ScheduleID,
                     ScheduleDayID = a.ScheduleDayID,
                     ProgramID = a.ProgramID,
                     FileURL = a.FileUrl,
                     ExcuteDate = a.ExecuteDate,
                     Title = a.Title,
                     FileName = a.FileName,
                     UnitName = d.UnitName,
                     CreatedDate = a.CreatedDate,
                     UserName = a.CreatedUser,
                     Status = a.Status
                 }).ToList();
            ViewData["ListSchedulePrograms"] = ListSchedulePrograms;
            ViewData["Date"] = Date;
            var programLoop = _context.ProgramLoop.Where(x => x.ScheduleDayID == ScheduleDayID && x.UnitID == user.UnitID
                                       && DbFunctions.TruncateTime(x.DateExecute) == DbFunctions.TruncateTime(Date)).FirstOrDefault();
            ViewData["IsLoop"] = programLoop != null && programLoop.Status == GlobalConstant.IS_ACTIVE;
            ViewData["LoopCount"] = programLoop != null && programLoop.Status == GlobalConstant.IS_ACTIVE ? programLoop.LoopCount : null;
            return PartialView("_ViewPrograms");

        }

        private void ProcessUnitExcept(int scheduleId, ScheduleModel model)
        {
            List<ScheduleExcept> listExistExcept = new List<ScheduleExcept>();
            var communeIds = new List<int>();
            var exceptUnitDistrictIDs = new List<int>();
            if (model.ListExceptUnitID != null && model.ListExceptUnitID.Any())
            {
                foreach (var unitId in model.ListExceptUnitID)
                {
                    var unit = _context.Unit.Find(unitId);
                    if (unit.UnitLevel == GlobalConstant.UNIT_TYPE_DISTRICT)
                    {
                        exceptUnitDistrictIDs.Add(unitId);
                    }
                    else if (unit.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE)
                    {
                        communeIds.Add(unitId);
                        if (unit.ParentID.HasValue)
                        {
                            if (!exceptUnitDistrictIDs.Contains(unit.ParentID.Value))
                            {
                                exceptUnitDistrictIDs.Add(unit.ParentID.Value);
                            }
                        }
                    }
                }
            }
            if (scheduleId > 0)
            {
                listExistExcept.AddRange(_context.ScheduleExcept.Where(x => x.ScheduleID == scheduleId).ToList());
                if (exceptUnitDistrictIDs.Any())
                {
                    var disIds = exceptUnitDistrictIDs.Select(x => (int?)x).ToList();
                    var communes = _context.Unit.Where(x => x.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE
                     && disIds.Contains(x.ParentID)).ToList();
                    foreach (int unitId in exceptUnitDistrictIDs)
                    {
                        var disCommunes = communes.Where(x => x.ParentID == unitId).ToList();
                        var isAll = true;
                        foreach (var c in disCommunes)
                        {
                            if (communeIds.Contains(c.UnitID))
                            {
                                isAll = false;
                                break;
                            }
                        }
                        if (isAll)
                        {
                            communeIds.AddRange(disCommunes.Select(x => x.UnitID).ToList());
                        }
                        if (listExistExcept.Any(x => x.UnitID == unitId))
                        {
                            listExistExcept.RemoveAll(x => x.UnitID == unitId);
                            continue;
                        }
                        var entity = new ScheduleExcept
                        {
                            UnitID = unitId,
                            ScheduleID = scheduleId
                        };
                        _context.ScheduleExcept.Add(entity);
                    }
                }
                if (communeIds.Any())
                {
                    foreach (int unitId in communeIds)
                    {
                        if (listExistExcept.Any(x => x.UnitID == unitId))
                        {
                            listExistExcept.RemoveAll(x => x.UnitID == unitId);
                            continue;
                        }
                        var entity = new ScheduleExcept
                        {
                            UnitID = unitId,
                            ScheduleID = scheduleId
                        };
                        _context.ScheduleExcept.Add(entity);
                    }
                }
                if (listExistExcept.Any())
                {
                    _context.ScheduleExcept.RemoveRange(listExistExcept);
                }
            }
        }

        private void CheckUnitScope(List<int> scheduleIds, ScheduleModel model)
        {
            var unitIds = _context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE
                && (x.UnitLevel == GlobalConstant.UNIT_TYPE_DISTRICT || x.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE))
                .Select(x => x.UnitID).ToList();
            var expUnitIds = _context.ScheduleExcept.Where(x => (model.ScheduleID <= 0 || x.ScheduleID != model.ScheduleID)
            && scheduleIds.Contains(x.ScheduleID)).Select(x => x.UnitID).ToList();
            var oldAppliedUnits = unitIds.Where(x => !expUnitIds.Contains(x)).ToList();
            var communeIds = new List<int>();
            var exceptUnitDistrictIDs = new List<int>();
            if (model.ListExceptUnitID != null && model.ListExceptUnitID.Any())
            {
                foreach (var unitId in model.ListExceptUnitID)
                {
                    var unit = _context.Unit.Find(unitId);
                    if (unit.UnitLevel == GlobalConstant.UNIT_TYPE_DISTRICT)
                    {
                        exceptUnitDistrictIDs.Add(unitId);
                    }
                    else if (unit.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE)
                    {
                        communeIds.Add(unitId);
                        if (unit.ParentID.HasValue)
                        {
                            if (!exceptUnitDistrictIDs.Contains(unit.ParentID.Value))
                            {
                                exceptUnitDistrictIDs.Add(unit.ParentID.Value);
                            }
                        }
                    }
                }
            }
            var purDisIds = new List<int>();
            if (exceptUnitDistrictIDs.Any())
            {
                purDisIds.AddRange(exceptUnitDistrictIDs);
                var disIds = exceptUnitDistrictIDs.Select(x => (int?)x).ToList();
                var communes = _context.Unit.Where(x => x.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE
                 && disIds.Contains(x.ParentID)).ToList();
                foreach (int unitId in exceptUnitDistrictIDs)
                {
                    var disCommunes = communes.Where(x => x.ParentID == unitId).ToList();
                    var isAll = true;
                    foreach (var c in disCommunes)
                    {
                        if (communeIds.Contains(c.UnitID))
                        {
                            isAll = false;
                            break;
                        }
                    }
                    if (isAll)
                    {
                        communeIds.AddRange(disCommunes.Select(x => x.UnitID).ToList());
                    }
                }
            }
            var curAppliedUnits = unitIds.Where(x => !communeIds.Contains(x) && !purDisIds.Contains(x)).ToList();
            foreach (var uId in oldAppliedUnits)
            {
                if (curAppliedUnits.Contains(uId))
                {
                    throw new BusinessException("Đã tồn tại lịch phát sóng trong khung giờ này");
                }
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult AddOrEdit(ScheduleModel model)
        {
            string msg;
            if (ModelState.IsValid)
            {
                // Kiem tra don vi
                UserInfoBO user = SessionManager.GetUserInfoBO();
                var hasPer = user.IsSupperAdmin.GetValueOrDefault() == GlobalConstant.IS_ACTIVE
                || (user.IsAdmin.GetValueOrDefault() == GlobalConstant.IS_ACTIVE
                && (user.UnitLevel == GlobalConstant.UNIT_TYPE_PROVINCE || user.UnitLevel == GlobalConstant.UNIT_TYPE_DISTRICT));
                if (!hasPer)
                {
                    throw new BusinessException("Bạn không có quyền cấu hình khung thời gian phát");
                }
                else
                {
                    if (model.Frequency.GetValueOrDefault() == GlobalConstant.SCHEDULE_WEEKLY && (model.ListDay == null || !model.ListDay.Any()))
                    {
                        throw new BusinessException("Bạn chưa chọn ngày phát");
                    }
                    msg = "Cấu hình khung thời gian thành công";
                    Schedule Entity = null;
                    List<ScheduleDay> listExistDay = new List<ScheduleDay>();
                    if (model.ScheduleID > 0)
                    {
                        Entity = _context.Schedule.Find(model.ScheduleID);
                        if (Entity == null)
                        {
                            throw new BusinessException("Không tồn tại khung giờ phát đã chọn");
                        }
                        listExistDay.AddRange(_context.ScheduleDay.Where(x => x.ScheduleID == model.ScheduleID).ToList());
                        model.TransferToModel(Entity);
                        var qCheck = _context.Schedule.Where(x => x.Status == GlobalConstant.IS_ACTIVE && x.ScheduleID != model.ScheduleID
                            && ((x.StartMinus <= Entity.StartMinus && x.EndMinus >= Entity.StartMinus) || (x.StartMinus <= Entity.EndMinus && x.EndMinus >= Entity.EndMinus)
                            || (x.StartMinus >= Entity.StartMinus && x.StartMinus <= Entity.EndMinus) || (x.EndMinus >= Entity.StartMinus && x.EndMinus <= Entity.EndMinus)));
                        if (Entity.Frequency != GlobalConstant.SCHEDULE_DAILY)
                        {
                            qCheck = qCheck.Where(x => _context.ScheduleDay.Any(d => d.ScheduleID == x.ScheduleID && model.ListDay.Contains(d.DayOfWeek)));
                        }
                        var oldScheduleIds = qCheck.Select(x => x.ScheduleID).ToList();
                        if (oldScheduleIds.Any())
                        {
                            CheckUnitScope(oldScheduleIds, model);
                        }
                    }
                    else
                    {
                        Entity = new Schedule();
                        model.TransferToModel(Entity);
                        var qCheck = _context.Schedule.Where(x => x.Status == GlobalConstant.IS_ACTIVE
                             && ((x.StartMinus <= Entity.StartMinus && x.EndMinus >= Entity.StartMinus) || (x.StartMinus <= Entity.EndMinus && x.EndMinus >= Entity.EndMinus)
                            || (x.StartMinus >= Entity.StartMinus && x.StartMinus <= Entity.EndMinus) || (x.EndMinus >= Entity.StartMinus && x.EndMinus <= Entity.EndMinus)));
                        if (Entity.Frequency != GlobalConstant.SCHEDULE_DAILY)
                        {
                            qCheck = qCheck.Where(x => _context.ScheduleDay.Any(d => d.ScheduleID == x.ScheduleID && model.ListDay.Contains(d.DayOfWeek)));
                        }
                        var oldScheduleIds = qCheck.Select(x => x.ScheduleID).ToList();
                        if (oldScheduleIds.Any())
                        {
                            CheckUnitScope(oldScheduleIds, model);
                        }
                        Entity.Status = GlobalConstant.IS_ACTIVE;
                        Entity.CreateDate = DateTime.Now;
                        _context.Schedule.Add(Entity);
                    }
                    _context.SaveChanges();
                    //Them thoi gian mac dinh tu thu 2 den chu nhat
                    List<ScheduleDay> ListScheduleDays = new List<ScheduleDay>();
                    if (model.Frequency == GlobalConstant.SCHEDULE_DAILY)
                    {
                        for (int i = 1; i <= GlobalConstant.MAX_DAY_OF_WEEK; i++)
                        {
                            if (listExistDay.Any(x => x.DayOfWeek == i))
                            {
                                continue;
                            }
                            ScheduleDay objScheduleDay = new ScheduleDay
                            {
                                DayOfWeek = i,
                                ScheduleID = Entity.ScheduleID
                            };
                            ListScheduleDays.Add(objScheduleDay);
                        }
                    }
                    else
                    {
                        foreach (int i in model.ListDay)
                        {
                            if (i <= 0 || i > 7)
                            {
                                throw new BusinessException("Ngày phát không hợp lệ");
                            }
                            if (listExistDay.Any(x => x.DayOfWeek == i))
                            {
                                listExistDay.RemoveAll(x => x.DayOfWeek == i);
                                continue;
                            }
                            ScheduleDay objScheduleDay = new ScheduleDay
                            {
                                DayOfWeek = i,
                                ScheduleID = Entity.ScheduleID
                            };
                            ListScheduleDays.Add(objScheduleDay);
                        }
                        if (listExistDay.Any())
                        {
                            List<int> listScheduleID = listExistDay.Select(x => x.ScheduleDayID).ToList();
                            Program program = _context.Program.FirstOrDefault(x => listScheduleID.Contains(x.ScheduleDayID));
                            if (program != null)
                            {
                                int day = listExistDay.Where(x => x.ScheduleDayID == program.ScheduleDayID).Select(x => x.DayOfWeek).FirstOrDefault();
                                throw new BusinessException("Đã tồn tại chương trình cho thứ " + GlobalCommon.GetNameOfDay(day) + " ngày " + program.ExecuteDate.Value.ToString("dd/MM/yyyy"));
                            }
                            else
                            {
                                _context.ScheduleDay.RemoveRange(listExistDay);
                            }
                        }
                    }
                    _context.ScheduleDay.AddRange(ListScheduleDays);
                    ProcessUnitExcept(Entity.ScheduleID, model);
                }
                _context.SaveChanges();
                ScheduleCaching.LoadCaching();
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            else
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
        }

        [HttpPost]
        public JsonResult DeleteScheduleDay(int ProgramID)
        {

            if (ModelState.IsValid)
            {
                // Kiem tra don vi
                UserInfoBO user = SessionManager.GetUserInfoBO();
                Program entity = _context.Program.Find(ProgramID);
                if (entity == null || user.UnitID != entity.UnitID || entity.ExecuteDate == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                var objScheduleDay = _context.ScheduleDay.Find(entity.ScheduleDayID);
                var objSchedule = _context.Schedule.Find(objScheduleDay.ScheduleID);
                DateTime currentDt = DateTime.Now;
                long hourOfDay = currentDt.Hour;
                long minuteOfDay = currentDt.Minute;
                long minutes = 60 * hourOfDay + minuteOfDay;
                if (currentDt.Date > entity.ExecuteDate.Value.Date
                    || (currentDt.Date == entity.ExecuteDate.Value.Date && minutes > objSchedule.StartMinus))
                {
                    throw new BusinessException("Bạn không được phép xóa các chương trình phát trong quá khứ");
                }
                List<Program> listProgram = _context.Program.Where(x => x.ScheduleDayID == entity.ScheduleDayID
                && x.ExecuteDate == entity.ExecuteDate && x.UnitID == user.UnitID).ToList();
                DateTime FirstDayOfWeek = DateTime.Now.StartOfWeek(DayOfWeek.Monday);
                TimeSpan TimeExecute = new TimeSpan(objSchedule.ScheduleHour.Value, objSchedule.ScheduleMinus.Value, 0);
                DateTime DateExecute = FirstDayOfWeek.AddDays(objScheduleDay.DayOfWeek - 1).Date + TimeExecute;
                foreach (Program objProgram in listProgram)
                {
                    _context.Program.Remove(objProgram);
                }
                // Xoa vov
                List<ProgramChanel> listChannel =
                   (from a in _context.ProgramChanel
                    join b in _context.ScheduleDay on a.ScheduleDayID equals b.ScheduleDayID
                    where a.ScheduleDayID == entity.ScheduleDayID && DbFunctions.TruncateTime(a.DateExecute) == DbFunctions.TruncateTime(entity.ExecuteDate) && a.UnitID == user.UnitID
                    select a).ToList();
                _context.ProgramChanel.RemoveRange(listChannel);
                // Xoa loop
                var loops = _context.ProgramLoop.Where(x => x.ScheduleDayID == entity.ScheduleDayID && x.UnitID == user.UnitID
                                        && DbFunctions.TruncateTime(x.DateExecute) == DbFunctions.TruncateTime(DateExecute)).ToList();
                _context.ProgramLoop.RemoveRange(loops);
                _context.SaveChanges();
                return Json("Xóa chương trình thành công", JsonRequestBehavior.AllowGet);
            }
            else
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
        }

        [HttpPost]
        public JsonResult DeleteProgram(int ProgramID)
        {

            if (ModelState.IsValid)
            {
                // Kiem tra don vi
                UserInfoBO user = SessionManager.GetUserInfoBO();
                Program objProgram = _context.Program.Find(ProgramID);
                if (objProgram != null)
                {
                    if (user.UnitID != objProgram.UnitID || objProgram.ExecuteDate == null)
                    {
                        throw new BusinessException("Dữ liệu không hợp lệ");
                    }
                    var objScheduleDay = _context.ScheduleDay.Find(objProgram.ScheduleDayID);
                    var objSchedule = _context.Schedule.Find(objScheduleDay.ScheduleID);
                    DateTime currentDt = DateTime.Now;
                    long hourOfDay = currentDt.Hour;
                    long minuteOfDay = currentDt.Minute;
                    long minutes = 60 * hourOfDay + minuteOfDay;
                    if (currentDt.Date > objProgram.ExecuteDate.Value.Date
                        || (currentDt.Date == objProgram.ExecuteDate.Value.Date && minutes > objSchedule.StartMinus))
                    {
                        throw new BusinessException("Bạn không được phép xóa các chương trình phát trong quá khứ");
                    }
                    if (user.UnitID == GlobalConstant.UNIT_TYPE_PROVINCE)
                    {
                        _context.Program.Remove(objProgram);
                    }
                    else
                    {
                        if (user.UnitID != objProgram.UnitID)
                        {
                            throw new BusinessException("Dữ liệu không hợp lệ");
                        }
                        else
                        {
                            _context.Program.Remove(objProgram);
                        }
                    }
                }
                else
                {
                    throw new BusinessException("Không tồn tại chương trình hoặc chương trình đã bị xóa");
                };
                var listProgram = _context.Program.Where(x => x.UnitID == user.UnitID
                    && x.ScheduleDayID == objProgram.ScheduleDayID && x.ProgramID != objProgram.ProgramID
                    && DbFunctions.TruncateTime(x.ExecuteDate) == DbFunctions.TruncateTime(objProgram.ExecuteDate))
                    .OrderBy(x => x.NumberOrder).ThenBy(x => x.ProgramID).ToList();
                var idx = 1;
                foreach (var item in listProgram)
                {
                    item.NumberOrder = idx++;
                }
                if (!listProgram.Any())
                {
                    // Xoa loop
                    var loops = _context.ProgramLoop.Where(x => x.ScheduleDayID == objProgram.ScheduleDayID && x.UnitID == user.UnitID
                                            && DbFunctions.TruncateTime(x.DateExecute) == DbFunctions.TruncateTime(objProgram.ExecuteDate)).ToList();
                    _context.ProgramLoop.RemoveRange(loops);
                }
                _context.SaveChanges();
                return Json("Xóa chương trình thành công", JsonRequestBehavior.AllowGet);
            }
            else
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
        }

        [HttpPost]
        public JsonResult ApproveScheduleDay(int id, int status)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            List<Program> programs = _context.Program.Where(x => x.ScheduleDayID == id && x.UnitID == user.UnitID).ToList();
            foreach (Program program in programs)
            {
                program.Status = status;
            }
            _context.SaveChanges();
            if (status == 0)
            {
                return Json("Hủy phê duyệt chương trình phát thành công", JsonRequestBehavior.AllowGet);
            }
            return Json("Phê duyệt chương trình phát thành công", JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult DeleteChanel(int ProgramChanelID)
        {

            if (ModelState.IsValid)
            {
                // Kiem tra don vi
                UserInfoBO user = SessionManager.GetUserInfoBO();
                ProgramChanel objProgramChanel = _context.ProgramChanel.Find(ProgramChanelID);
                if (objProgramChanel != null)
                {
                    if (user.UnitID != objProgramChanel.UnitID || objProgramChanel.DateExecute == null)
                    {
                        throw new BusinessException("Dữ liệu không hợp lệ");
                    }

                    if (objProgramChanel.DateExecute < DateTime.Now)
                    {
                        throw new BusinessException("Bạn không được phép xóa các chương trình chuyển tiếp phát trong quá khứ");
                    }
                    if (user.UnitID == GlobalConstant.UNIT_TYPE_PROVINCE)
                    {
                        _context.ProgramChanel.Remove(objProgramChanel);
                    }
                    else
                    {
                        if (user.UnitID != objProgramChanel.UnitID)
                        {
                            throw new BusinessException("Dữ liệu không hợp lệ");
                        }
                        else
                        {
                            _context.ProgramChanel.Remove(objProgramChanel);
                        }
                    }
                }
                else
                {
                    throw new BusinessException("Không tồn tại chương trình chuyển tiếp hoặc chương trình đã bị xóa");
                };
                _context.SaveChanges();
                return Json("Xóa chương trình chuyển tiếp thành công", JsonRequestBehavior.AllowGet);
            }
            else
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
        }

        [HttpPost]
        public JsonResult DeleteSchedule(int ScheduleID)
        {
            // Kiem tra don vi
            UserInfoBO user = SessionManager.GetUserInfoBO();
            if (user.IsSupperAdmin.GetValueOrDefault() == GlobalConstant.IS_ACTIVE
                || (user.IsAdmin.GetValueOrDefault() == GlobalConstant.IS_ACTIVE
                && (user.UnitLevel == GlobalConstant.UNIT_TYPE_PROVINCE || user.UnitLevel == GlobalConstant.UNIT_TYPE_DISTRICT)))
            {
                Schedule objDelete = _context.Schedule.Find(ScheduleID);
                objDelete.Status = GlobalConstant.IS_NOT_ACTIVE;
                _context.SaveChanges();
                ScheduleCaching.LoadCaching();
            }
            else
            {
                throw new BusinessException("Bạn không có quyền xóa khung thời gian");
            }

            _context.SaveChanges();
            return Json("Xóa chương trình thành công", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult PublishSchedule(int ScheduleDayID, DateTime Date, int? ChannelID, bool IsLoop = false, int? LoopCount = GlobalConstant.LOOP_COUNT)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            var objScheduleDay = _context.ScheduleDay.Find(ScheduleDayID);
            var objSchedule = _context.Schedule.Find(objScheduleDay.ScheduleID);
            DateTime currentDt = DateTime.Now;
            long hourOfDay = currentDt.Hour;
            long minuteOfDay = currentDt.Minute;
            long minutes = 60 * hourOfDay + minuteOfDay;
            if (currentDt.Date > Date.Date
                || (currentDt.Date == Date.Date && minutes > objSchedule.StartMinus))
            {
                throw new BusinessException("Bạn không được phép lên lịch các chương trình phát trong quá khứ hoặc đang phát");
            }
            SetupInfo PlayList = new SetupInfo();

            PlayList.StartTime = Date;
            PlayList.Channel = objScheduleDay.ScheduleDayID.ToString();
            List<Program> listProgram = _context.Program
                .Where(a => a.ScheduleDayID == ScheduleDayID && DbFunctions.TruncateTime(a.ExecuteDate) == DbFunctions.TruncateTime(Date)
                && a.FileUrl != null && a.FileName != null)
                .ToList();
            if (listProgram.Any(x => x.Status.GetValueOrDefault() != GlobalConstant.IS_APPROVED))
            {
                foreach (Program entity in listProgram)
                {
                    entity.Status = GlobalConstant.IS_APPROVED;
                }
                var listPlayList = listProgram.Select(a => new AudioInfo
                {
                    FilePath = Path.Combine(a.FileUrl, a.FileName)
                });
                if (listPlayList.ToList() == null)
                {
                    throw new BusinessException("Bạn chưa thêm danh sách phát");
                }
                PlayList.AudioInfos = listPlayList.ToList();
                List<BroadcastInfo> ListSetup = ScheduleUlti.SplitAudioFile(PlayList.AudioInfos);
                string FilePath = "";
                foreach (Program entity in listProgram)
                {
                    FilePath = Path.Combine(entity.FileUrl, entity.FileName);
                    var objBroadcast = ListSetup.Where(a => a.FilePath == FilePath).FirstOrDefault();
                    entity.Duration = objBroadcast.Duration;
                    entity.SplitedFolder = objBroadcast.TempFolder;
                }
                _context.SaveChanges();
                Task.Factory.StartNew(() => ScheduleUlti.SplitAudioFile(ListSetup, GlobalConstant.BITRATE_128));
                Task.Factory.StartNew(() => ScheduleUlti.SplitAudioFile(ListSetup, GlobalConstant.BITRATE_32));
            }
            if (ChannelID.HasValue)
            {
                DateTime FirstDayOfWeek = Date.StartOfWeek(DayOfWeek.Monday);
                TimeSpan TimeExecute = new TimeSpan(objSchedule.ScheduleHour.Value, objSchedule.ScheduleMinus.Value, 0);
                DateTime DateExecute = FirstDayOfWeek.AddDays(objScheduleDay.DayOfWeek - 1).Date + TimeExecute;
                if (ChannelID > 0)
                {
                    ForwardChanel fc = _context.ForwardChanel.Find(ChannelID);
                    if (fc == null)
                    {
                        throw new BusinessException("Không tồn tại kênh phát đã chọn");
                    }
                }
                ProgramChanel Entity = (from a in _context.ProgramChanel
                                        join b in _context.ScheduleDay on a.ScheduleDayID equals b.ScheduleDayID
                                        where a.ScheduleDayID == ScheduleDayID && DbFunctions.TruncateTime(a.DateExecute) == DbFunctions.TruncateTime(DateExecute) && a.UnitID == user.UnitID
                                        select a).FirstOrDefault();
                if (Entity == null)
                {
                    Entity = new ProgramChanel();
                }
                Entity.ScheduleDayID = ScheduleDayID;
                Entity.Status = GlobalConstant.IS_ACTIVE;
                Entity.ChanelID = ChannelID.Value;
                Entity.UnitID = user.UnitID.Value;
                Entity.DateExecute = DateExecute;
                if (Entity.ProgramChanelID == 0)
                {
                    _context.ProgramChanel.Add(Entity);
                }
                var programLoop = _context.ProgramLoop.Where(x => x.ScheduleDayID == ScheduleDayID && x.UnitID == user.UnitID
                                        && DbFunctions.TruncateTime(x.DateExecute) == DbFunctions.TruncateTime(DateExecute)).FirstOrDefault();
                if (IsLoop)
                {
                    if (programLoop == null)
                    {
                        programLoop = new ProgramLoop();
                        programLoop.ScheduleDayID = ScheduleDayID;
                        programLoop.DateExecute = DateExecute;
                        programLoop.UnitID = user.UnitID.Value;
                        programLoop.LoopCount = LoopCount;
                        programLoop.Status = GlobalConstant.IS_ACTIVE;
                        _context.ProgramLoop.Add(programLoop);
                    }
                    else
                    {
                        programLoop.Status = GlobalConstant.IS_ACTIVE;
                        programLoop.LoopCount = LoopCount;
                    }
                }
                else if (programLoop != null)
                {
                    programLoop.Status = 0;
                }
                _context.SaveChanges();
            }
            return Json("Lên lịch phát thành công", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult OnlyPublishSchedule(int ProgramID)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            Program program = _context.Program.Where(x => x.UnitID == user.UnitID && x.ProgramID == ProgramID).FirstOrDefault();
            if (program == null || user.UnitID != program.UnitID || program.ExecuteDate == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            var objScheduleDay = _context.ScheduleDay.Find(program.ScheduleDayID);
            var objSchedule = _context.Schedule.Find(objScheduleDay.ScheduleID);
            DateTime currentDt = DateTime.Now;
            long hourOfDay = currentDt.Hour;
            long minuteOfDay = currentDt.Minute;
            long minutes = 60 * hourOfDay + minuteOfDay;
            if (currentDt.Date > program.ExecuteDate.Value.Date
                || (currentDt.Date == program.ExecuteDate.Value.Date && minutes > objSchedule.StartMinus))
            {
                throw new BusinessException("Bạn không được phép lên lịch các chương trình phát trong quá khứ hoặc đang phát");
            }
            SetupInfo PlayList = new SetupInfo();

            PlayList.StartTime = program.ExecuteDate.Value;
            PlayList.Channel = objScheduleDay.ScheduleDayID.ToString();
            List<Program> listProgram = _context.Program
                .Where(a => a.ScheduleDayID == program.ScheduleDayID && a.ExecuteDate == program.ExecuteDate
                && a.FileUrl != null && a.FileName != null)
                .ToList();
            foreach (Program entity in listProgram)
            {
                entity.Status = GlobalConstant.IS_APPROVED;
            }
            var listPlayList = listProgram.Select(a => new AudioInfo
            {
                FilePath = Path.Combine(a.FileUrl, a.FileName)
            });
            if (listPlayList.ToList() == null)
            {
                throw new BusinessException("Bạn chưa thêm danh sách phát");
            }
            PlayList.AudioInfos = listPlayList.ToList();
            List<BroadcastInfo> ListSetup = ScheduleUlti.SplitAudioFile(PlayList.AudioInfos);
            string FilePath = "";
            foreach (Program entity in listProgram)
            {
                FilePath = Path.Combine(entity.FileUrl, entity.FileName);
                var objBroadcast = ListSetup.Where(a => a.FilePath == FilePath).FirstOrDefault();
                entity.Duration = objBroadcast.Duration;
                entity.SplitedFolder = objBroadcast.TempFolder;
            }
            _context.SaveChanges();
            Task.Factory.StartNew(() => ScheduleUlti.SplitAudioFile(ListSetup, GlobalConstant.BITRATE_128));
            Task.Factory.StartNew(() => ScheduleUlti.SplitAudioFile(ListSetup, GlobalConstant.BITRATE_32));
            return Json("Lên lịch phát thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult Move(int ProgramID, int Step)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            Program program = _context.Program.Where(x => x.UnitID == user.UnitID && x.ProgramID == ProgramID).FirstOrDefault();
            if (program == null || user.UnitID != program.UnitID || program.ExecuteDate == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            if (program.NumberOrder.GetValueOrDefault() == 0)
            {
                var listProgram = _context.Program.Where(x => x.UnitID == user.UnitID && x.ScheduleDayID == program.ScheduleDayID
                && DbFunctions.TruncateTime(x.ExecuteDate) == DbFunctions.TruncateTime(program.ExecuteDate)).OrderBy(x => x.ProgramID).ToList();
                var idx = 1;
                foreach (var item in listProgram)
                {
                    item.NumberOrder = idx++;
                }
            }
            var query = _context.Program.Where(x => x.UnitID == user.UnitID && x.ScheduleDayID == program.ScheduleDayID
                            && DbFunctions.TruncateTime(x.ExecuteDate) == DbFunctions.TruncateTime(program.ExecuteDate));
            int nowOrder = program.NumberOrder.Value;
            int newOrder = 0;
            if (Step > 0 && nowOrder > 1)
            {
                query = query.Where(x => x.NumberOrder < nowOrder).OrderByDescending(x => x.NumberOrder);
                newOrder = nowOrder - 1;
            }
            else
            {
                query = query.Where(x => x.NumberOrder > nowOrder).OrderBy(x => x.NumberOrder);
                newOrder = nowOrder + 1;
            }
            var scopeProgram = query.FirstOrDefault();
            if (scopeProgram != null)
            {
                scopeProgram.NumberOrder = nowOrder;
            }
            program.NumberOrder = newOrder;
            _context.SaveChanges();
            return Json("Thay đổi vị trí thành công", JsonRequestBehavior.AllowGet);
        }

        private Program InsertProgram(ScheduleDay objScheduleDay, DateTime DateExecute, string title = null, string fileName = null, string filePath = null)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            Program Entity = new Program();
            Entity.ScheduleDayID = objScheduleDay.ScheduleDayID;
            Entity.FileUrl = filePath;
            Entity.Status = 0;
            Entity.UnitID = user.UnitID.Value;
            Entity.FileName = fileName;
            Entity.Title = !string.IsNullOrEmpty(fileName) ? fileName : title;
            Entity.ExecuteDate = DateExecute;
            Entity.CreatedUser = user.UserName;
            Entity.CreatedDate = DateTime.Now;
            // Order
            int max = _context.Program.Where(x => x.ScheduleDayID == objScheduleDay.ScheduleDayID).Count();
            Entity.NumberOrder = max + 1;
            _context.Program.Add(Entity);
            return Entity;
        }

        private void ProcessFile(ScheduleDay objScheduleDay, DateTime DateExecute)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            string FilePath = "";
            string FileName = "";
            foreach (string objfile in Request.Files)
            {
                var fileContent = Request.Files[objfile];
                if (fileContent != null && fileContent.ContentLength > 0)
                {
                    // and optionally write the file to disk
                    FileName = Path.GetFileName(fileContent.FileName);
                    FilePath = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(GlobalConstant.FILE_PATH), user.UnitID.ToString(), objScheduleDay.ScheduleID.ToString(), DateExecute.ToString("ddMMyyyyHHmmss"));
                    //Check whether Directory (Folder) exists.
                    if (!Directory.Exists(FilePath))
                    {
                        //If Directory (Folder) does not exists. Create it.
                        Directory.CreateDirectory(FilePath);
                    }
                    // fileContent.SaveAs(Path.Combine(FilePath, FileName));
                    fileContent.SaveAs(FilePath + "\\" + FileName);
                }
                InsertProgram(objScheduleDay, DateExecute, null, FileName, FilePath);
            }
            _context.SaveChanges();
        }

        public Program ProcessConvertText(ScheduleDay objScheduleDay, DateTime DateExecute, string Text, string Voice, double Rate, string Title)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            Program Entity = InsertProgram(objScheduleDay, DateExecute, Title);
            _context.SaveChanges();
            var isSuccess = VbeeController.ConvertTextAsync(Text, Voice, Rate, Entity.ProgramID, "pro");
            if (!isSuccess)
            {
                throw new BusinessException("Chuyển đổi Text sang Audio thất bại, vui lòng kiểm tra lại");
            }
            return Entity;
        }

        public JsonResult CheckConvertText(int ProgramID)
        {
            var program = _context.Program.Find(ProgramID);
            if (program == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            var res = string.IsNullOrWhiteSpace(program.FileName) ? 0 : GlobalConstant.IS_ACTIVE;
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddToPlaylist(int ScheduleDayID, DateTime Date, string Title, string Text = null, string Voice = null, double Rate = 0)
        {
            try
            {
                //Convert datetime from ScheduleDay and schedule (hour and minus)
                DateTime FirstDayOfWeek = Date.StartOfWeek(DayOfWeek.Monday);
                ScheduleDay objScheduleDay = _context.ScheduleDay.Find(ScheduleDayID);
                Schedule objSchedule = _context.Schedule.Find(objScheduleDay.ScheduleID);
                TimeSpan TimeExecute = new TimeSpan(objSchedule.ScheduleHour.Value, objSchedule.ScheduleMinus.Value, 0);
                DateTime DateExecute = FirstDayOfWeek.AddDays(objScheduleDay.DayOfWeek - 1).Date + TimeExecute;
                if (Request.Files != null && (Text == null || Voice == null))
                {
                    ProcessFile(objScheduleDay, DateExecute);
                    return Json("Thêm vào danh sách phát thành công", JsonRequestBehavior.AllowGet);
                }
                else if (Text != null && Voice != null)
                {
                    var program = ProcessConvertText(objScheduleDay, DateExecute, Text, Voice, Rate, Title);
                    return Json(program.ProgramID, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
            }
            catch (Exception ex)
            {
                throw new BusinessException("Dữ liệu không hợp lệ, Thêm file thất bại, bạn hãy thử lại");
            }
        }

        [HttpPost]
        public ActionResult AddToChanellist(int ScheduleDayID, DateTime Date, int ChanelID, bool IsLoop = false, int? LoopCount = GlobalConstant.LOOP_COUNT)
        {
            try
            {
                UserInfoBO user = SessionManager.GetUserInfoBO();
                if (user.UnitID == null)
                {
                    throw new BusinessException("Bạn không được cấp quyền thêm chương trình chuyển tiếp");
                }
                //Convert datetime from ScheduleDay and schedule (hour and minus)
                DateTime FirstDayOfWeek = Date.StartOfWeek(DayOfWeek.Monday);
                ScheduleDay objScheduleDay = _context.ScheduleDay.Find(ScheduleDayID);
                Schedule objSchedule = _context.Schedule.Find(objScheduleDay.ScheduleID);
                TimeSpan TimeExecute = new TimeSpan(objSchedule.ScheduleHour.Value, objSchedule.ScheduleMinus.Value, 0);
                DateTime DateExecute = FirstDayOfWeek.AddDays(objScheduleDay.DayOfWeek - 1).Date + TimeExecute;

                ProgramChanel Entity = new ProgramChanel();
                Entity.ScheduleDayID = ScheduleDayID;
                Entity.Status = GlobalConstant.IS_ACTIVE;
                Entity.ChanelID = ChanelID;
                Entity.UnitID = user.UnitID.Value;
                Entity.DateExecute = DateExecute;
                _context.ProgramChanel.Add(Entity);
                var programLoop = _context.ProgramLoop.Where(x => x.ScheduleDayID == ScheduleDayID && x.UnitID == user.UnitID
                                        && DbFunctions.TruncateTime(x.DateExecute) == DbFunctions.TruncateTime(DateExecute)).FirstOrDefault();
                if (IsLoop)
                {
                    if (programLoop == null)
                    {
                        programLoop = new ProgramLoop();
                        programLoop.ScheduleDayID = ScheduleDayID;
                        programLoop.Status = GlobalConstant.IS_ACTIVE;
                        programLoop.DateExecute = DateExecute;
                        programLoop.UnitID = user.UnitID.Value;
                        programLoop.LoopCount = LoopCount;
                        _context.ProgramLoop.Add(programLoop);
                    }
                    else
                    {
                        programLoop.LoopCount = LoopCount;
                        programLoop.Status = GlobalConstant.IS_ACTIVE;
                    }
                }
                else if (programLoop != null)
                {
                    programLoop.Status = 0;
                }
                _context.SaveChanges();
            }
            catch
            {
                throw new BusinessException("Dữ liệu không hợp lệ, Thêm chương trình chuyển tiếp thất bại, bạn hãy thử lại");
            }
            return Json("Thêm vào danh sách phát thành công", JsonRequestBehavior.AllowGet);
        }

        public List<ScheduleModel> GetAllSchedule()
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            List<ScheduleModel> AllSchedule = new List<ScheduleModel>();
            AllSchedule = _context.Schedule
                .Where(a => a.Status == GlobalConstant.IS_ACTIVE && (user.IsSupperAdmin == GlobalConstant.IS_ACTIVE || a.IsHidden == null || a.IsHidden == 0)
                && (user.IsSupperAdmin == GlobalConstant.IS_ACTIVE || !_context.ScheduleExcept.Any(u => u.ScheduleID == a.ScheduleID && u.UnitID == user.UnitID)))
                .OrderBy(x => x.StartMinus)
                .Select(a => new ScheduleModel
                {
                    ScheduleID = a.ScheduleID,
                    ScheduleHour = a.ScheduleHour,
                    ScheduleName = a.ScheduleName,
                    ScheduleMinus = a.ScheduleMinus,
                    OffHour = a.OffHour,
                    OffMinus = a.OffMinus,
                    Status = a.Status,
                    CreateDate = a.CreateDate,
                    IsHidden = a.IsHidden == GlobalConstant.IS_ACTIVE
                }).ToList();
            return AllSchedule;
        }

        public List<ScheduleDayModel> GetAllProgramInSchedule(DateTime? FromDate, int? ScheduleID = null)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            if (FromDate == null)
            {
                FromDate = DateTime.Now;
            }
            FromDate = FromDate.Value.Date.StartOfWeek(DayOfWeek.Monday);
            DateTime EndDate = FromDate.Value.AddDays(7);
            List<ScheduleDayModel> result = (from a in _context.ScheduleDay
                                             join b in _context.Schedule on a.ScheduleID equals b.ScheduleID
                                             where b.Status == GlobalConstant.IS_ACTIVE && (ScheduleID == null || a.ScheduleID == ScheduleID)
                                             orderby a.DayOfWeek
                                             select new ScheduleDayModel
                                             {
                                                 DayOfWeek = a.DayOfWeek,
                                                 ScheduleID = a.ScheduleID,
                                                 ScheduleDayID = a.ScheduleDayID,
                                                 HasChannel = _context.ProgramChanel.Where(x => x.ScheduleDayID == a.ScheduleDayID && x.UnitID == user.UnitID && x.DateExecute >= FromDate && x.DateExecute < EndDate)
                                                 .Select(x => x.ChanelID).Any(),
                                                 ListProgram = _context.Program.Where(p => p.ScheduleDayID == a.ScheduleDayID && p.UnitID == user.UnitID && p.ExecuteDate >= FromDate && p.ExecuteDate < EndDate)
                                                 .OrderBy(x => x.NumberOrder).ThenBy(x => x.ProgramID).Select(p => new ScheduleProgramModel
                                                 {
                                                     ScheduleID = b.ScheduleID,
                                                     ScheduleDayID = a.ScheduleDayID,
                                                     ProgramID = p.ProgramID,
                                                     Status = p.Status,
                                                     ExcuteDate = p.ExecuteDate,
                                                     FileURL = p.FileUrl
                                                 }).ToList()
                                             }).ToList();
            var listGroupProgram = (from a in _context.ScheduleDay
                                    join b in _context.Schedule on a.ScheduleID equals b.ScheduleID
                                    join c in _context.Program on a.ScheduleDayID equals c.ScheduleDayID
                                    join d in _context.Unit on c.UnitID equals d.UnitID
                                    where b.Status == GlobalConstant.IS_ACTIVE && (ScheduleID == null || a.ScheduleID == ScheduleID)
                                    && c.ExecuteDate >= FromDate && c.ExecuteDate < EndDate
                                    group c by new { c.ScheduleDayID, d.UnitLevel } into g
                                    select new
                                    {
                                        UnitLevel = g.Key.UnitLevel,
                                        ScheduleDayID = g.Key.ScheduleDayID,
                                        CountProgram = g.Count(),
                                        Duration = g.Sum(x => x.Duration)
                                    }).ToList();
            var listGroupChanel = (from a in _context.ScheduleDay
                                   join b in _context.Schedule on a.ScheduleID equals b.ScheduleID
                                   join c in _context.ProgramChanel on a.ScheduleDayID equals c.ScheduleDayID
                                   join d in _context.Unit on c.UnitID equals d.UnitID
                                   where b.Status == GlobalConstant.IS_ACTIVE && (ScheduleID == null || a.ScheduleID == ScheduleID)
                                   && c.DateExecute >= FromDate && c.DateExecute < EndDate
                                   group c by new { c.ScheduleDayID, d.UnitLevel } into g
                                   select new
                                   {
                                       UnitLevel = g.Key.UnitLevel,
                                       ScheduleDayID = g.Key.ScheduleDayID,
                                       CountProgram = g.Count()
                                   }).ToList();

            foreach (var sd in result)
            {
                var unitPrograms = listGroupProgram.Where(x => x.ScheduleDayID == sd.ScheduleDayID).ToList();
                var unitChanels = listGroupChanel.Where(x => x.ScheduleDayID == sd.ScheduleDayID).ToList();
                if (unitPrograms != null && unitPrograms.Any())
                {
                    if (unitPrograms.Where(x => x.UnitLevel == GlobalConstant.UNIT_TYPE_VILLAGE).Sum(x => x.CountProgram) > 0)
                    {
                        sd.HasVilProgram = true;
                        var duration = unitPrograms.Where(x => x.UnitLevel == GlobalConstant.UNIT_TYPE_VILLAGE).Sum(x => x.Duration);
                        sd.VilDuration = GlobalCommon.SecondsToString(duration.GetValueOrDefault());
                    }
                    if (unitPrograms.Where(x => x.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE).Sum(x => x.CountProgram) > 0)
                    {
                        sd.HasComProgram = true;
                        var duration = unitPrograms.Where(x => x.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE).Sum(x => x.Duration);
                        sd.ComDuration = GlobalCommon.SecondsToString(duration.GetValueOrDefault());
                    }
                    if (unitPrograms.Where(x => x.UnitLevel == GlobalConstant.UNIT_TYPE_DISTRICT).Sum(x => x.CountProgram) > 0)
                    {
                        sd.HasDisProgram = true;
                        var duration = unitPrograms.Where(x => x.UnitLevel == GlobalConstant.UNIT_TYPE_DISTRICT).Sum(x => x.Duration);
                        sd.DisDuration = GlobalCommon.SecondsToString(duration.GetValueOrDefault());
                    }
                    if (unitPrograms.Where(x => x.UnitLevel == GlobalConstant.UNIT_TYPE_PROVINCE).Sum(x => x.CountProgram) > 0)
                    {
                        sd.HasPrvProgram = true;
                        var duration = unitPrograms.Where(x => x.UnitLevel == GlobalConstant.UNIT_TYPE_PROVINCE).Sum(x => x.Duration);
                        sd.PrvDuration = GlobalCommon.SecondsToString(duration.GetValueOrDefault());
                    }
                }
                if (unitChanels != null && unitChanels.Any())
                {
                    if (unitChanels.Where(x => x.UnitLevel == GlobalConstant.UNIT_TYPE_VILLAGE).Sum(x => x.CountProgram) > 0)
                    {
                        sd.HasVilProgram = true;
                    }
                    if (unitChanels.Where(x => x.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE).Sum(x => x.CountProgram) > 0)
                    {
                        sd.HasComProgram = true;
                    }
                    if (unitChanels.Where(x => x.UnitLevel == GlobalConstant.UNIT_TYPE_DISTRICT).Sum(x => x.CountProgram) > 0)
                    {
                        sd.HasDisProgram = true;
                    }
                    if (unitChanels.Where(x => x.UnitLevel == GlobalConstant.UNIT_TYPE_PROVINCE).Sum(x => x.CountProgram) > 0)
                    {
                        sd.HasPrvProgram = true;
                    }
                }
            }
            return result;
        }

        public FileResult Download(int ProgramID)
        {
            Program att = this._context.Program.Find(ProgramID);
            if (att == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            byte[] fileData = System.IO.File.ReadAllBytes(att.FileUrl + "\\" + att.FileName);
            return File(fileData, System.Net.Mime.MediaTypeNames.Application.Octet, att.FileName);
        }

        [SkipActionLogFilter]
        [AllowAnonymous]
        public FileResult Live(int unitID, long deviceID = 0, long bitrate = 128)
        {
            if (bitrate == 0)
            {
                bitrate = GlobalConstant.DEFAULT_BIT_RATE;
            }
            StringBuilder str = null;
            using (Entities context = new Entities())
            {
                str = GetStreamScheduler(context, unitID, deviceID, bitrate);
            }
            if (str == null)
            {
                str = new StringBuilder();
                str.AppendLine("#EXTM3U");
                str.AppendLine("#EXT-X-VERSION:3");
                str.AppendLine("#EXT-X-ALLOW-CACHE:YES");
                str.AppendLine("#EXT-X-TARGETDURATION:" + (GlobalConstant.CHUNK_DURATION + 1));
            }
            string content = str.ToString();
            byte[] arr = Encoding.UTF8.GetBytes(content);

            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = "playlist.m3u8",
                Inline = false,
            };

            Response.AppendHeader("Content-Disposition", cd.ToString());
            return File(arr, "m3u8");
        }

        private static void GetFullProgram(List<Program> programs, int loopUnitId, int loopCount)
        {
            int idx = -1;
            for (int i = 0; i < programs.Count; i++)
            {
                var p = programs[i];
                if (p.UnitID == loopUnitId)
                {
                    idx = i;
                    break;
                }
            }
            var unitPrograms = programs.Where(x => x.UnitID == loopUnitId).ToList();
            var loopPrograms = LoopProgram(unitPrograms, loopCount);
            programs.InsertRange(idx, loopPrograms);
        }

        private static List<Program> LoopProgram(List<Program> programs, int round)
        {
            List<Program> listTemp = new List<Program>();
            for (int i = 0; i < round; i++)
            {
                listTemp.AddRange(programs);
            }
            return listTemp;
        }

        public static StringBuilder GetStreamScheduler(Entities context, int unitID, long deviceID, long bitrate)
        {
            //Build m3u8 file
            StringBuilder str = new StringBuilder();
            str.AppendLine("#EXTM3U");
            str.AppendLine("#EXT-X-VERSION:3");
            str.AppendLine("#EXT-X-ALLOW-CACHE:YES");
            str.AppendLine("#EXT-X-TARGETDURATION:" + (GlobalConstant.CHUNK_DURATION + 1));
            str.AppendLine("#EXT-X-MEDIA-SEQUENCE:-1");
            //Calculate Timestamp
            DateTime currentDt = DateTime.Now;
            long hourOfDay = currentDt.Hour;
            long minuteOfDay = currentDt.Minute;
            long minutes = 60 * hourOfDay + minuteOfDay;
            long seconds = 60 * minutes + currentDt.Second;
            SystemConfig sc = context.SystemConfig.FirstOrDefault();
            int waitingAutioDur = (sc != null && sc.IsWaitingOn.GetValueOrDefault() == GlobalConstant.IS_ACTIVE) ? GlobalConstant.WAITING_AUDIO_DURATION : 1;
            var schedule = ScheduleCaching.GetListScheduleNow(unitID, waitingAutioDur).OrderByDescending(x => x.ScheduleID).FirstOrDefault();
            if (schedule == null)
            {
                return str;
            }

            int dayOfWeek = currentDt.DayOfWeek == DayOfWeek.Sunday ? dayOfWeek = 7 : (int)currentDt.DayOfWeek;
            ScheduleDay scheduleDay = (from a in context.ScheduleDay
                                       where a.ScheduleID == schedule.ScheduleID
                                       && a.DayOfWeek == dayOfWeek
                                       select a).FirstOrDefault();
            if (scheduleDay == null)
            {
                return str;
            }

            var unit = context.Unit.Where(x => x.UnitID == unitID).Select(x => new UnitModel
            {
                UnitID = x.UnitID,
                UnitPath = x.UnitPath
            }).FirstOrDefault();
            OrderConfig config = context.OrderConfig.First();
            var loop = (from p in context.ProgramLoop
                        join u in context.Unit on p.UnitID equals u.UnitID
                        where p.ScheduleDayID == scheduleDay.ScheduleDayID
                        && p.Status == GlobalConstant.IS_ACTIVE
                        && DbFunctions.TruncateTime(p.DateExecute) == DbFunctions.TruncateTime(currentDt)
                        && unit.UnitPath.Contains(u.UnitPath)
                        orderby (u.UnitLevel == GlobalConstant.UNIT_TYPE_VILLAGE ? config.VillageOrder
                        : u.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE ? config.CommuneOrder
                        : u.UnitLevel == GlobalConstant.UNIT_TYPE_DISTRICT ? config.DistrictOrder
                        : u.UnitLevel == GlobalConstant.UNIT_TYPE_PROVINCE ? config.ProvinceOrder : 0)
                        select p).FirstOrDefault();
            var lstProgram = (from p in context.Program
                              join u in context.Unit on p.UnitID equals u.UnitID
                              where p.ScheduleDayID == scheduleDay.ScheduleDayID
                              && p.Status == GlobalConstant.IS_APPROVED
                              && DbFunctions.TruncateTime(p.ExecuteDate) == DbFunctions.TruncateTime(currentDt)
                              && unit.UnitPath.Contains(u.UnitPath)
                              orderby (u.UnitLevel == GlobalConstant.UNIT_TYPE_VILLAGE ? config.VillageOrder
                              : u.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE ? config.CommuneOrder
                              : u.UnitLevel == GlobalConstant.UNIT_TYPE_DISTRICT ? config.DistrictOrder
                              : u.UnitLevel == GlobalConstant.UNIT_TYPE_PROVINCE ? config.ProvinceOrder : 0),
                              p.NumberOrder, p.ProgramID
                              select p).ToList();

            if (!lstProgram.Any())
            {
                return str;
            }
            if (loop != null && loop.LoopCount.GetValueOrDefault() > 0)
            {
                GetFullProgram(lstProgram, loop.UnitID, loop.LoopCount.GetValueOrDefault());
            }
            long differTime = schedule.StartMinus.Value * 60 - seconds;
            double remainTime = -differTime;
            int seq = 1;
            if (differTime < waitingAutioDur)
            {
                if (differTime > 0)
                {
                    //@TODO: insert nhac cho here
                    int start = 0;
                    string folderPath = GlobalConstant.WAITING_AUDIO_FOLDER + "/" + bitrate;
                    string prefix = GlobalConstant.MEDIA_SERVER + "/" + folderPath + "/" + GlobalConstant.CHUNK_PREFIX;
                    List<string> lstDuration = ScheduleUlti.ReadPlayListFile(folderPath);
                    string filePathPrefix = GlobalConstant.MEDIA_FOLDER + "/" + folderPath + "/" + GlobalConstant.CHUNK_PREFIX;
                    string currentFilePath = filePathPrefix + start + GlobalConstant.CHUNK_SUFFIX;
                    while (System.IO.File.Exists(currentFilePath) && differTime > 0)
                    {
                        str.AppendLine(lstDuration[start]);
                        str.AppendLine(prefix + start + GlobalConstant.CHUNK_SUFFIX);
                        start++;
                        differTime -= GlobalConstant.CHUNK_DURATION;
                        currentFilePath = filePathPrefix + start + GlobalConstant.CHUNK_SUFFIX;
                    }
                    remainTime = 0;
                }

                int minCountFile = (sc == null || sc.StreamingCountFile.GetValueOrDefault() == 0 ? GlobalConstant.DEFAULT_STREAMING_COUNT_FILE : sc.StreamingCountFile.GetValueOrDefault());
                int totalFile = 0;
                
                foreach (var program in lstProgram)
                {
                    int start = 0;
                    string folderPath = program.SplitedFolder + "/" + bitrate;
                    string prefix = GlobalConstant.MEDIA_SERVER + "/" + folderPath + "/" + GlobalConstant.CHUNK_PREFIX;
                    if (remainTime > 0)
                    {
                        if (remainTime >= program.Duration)
                        {
                            remainTime -= program.Duration.Value;
                            seq += ScheduleUlti.ReadPlayListFile(folderPath).Count;
                            continue;
                        }
                        else
                        {
                            start = (int)Math.Floor(remainTime / GlobalConstant.CHUNK_DURATION);
                            seq += start;
                            remainTime = 0;
                        }
                    }
                    List<string> lstDuration = ScheduleUlti.ReadPlayListFile(folderPath);
                    string filePathPrefix = GlobalConstant.MEDIA_FOLDER + "/" + folderPath + "/" + GlobalConstant.CHUNK_PREFIX;
                    string currentFilePath = filePathPrefix + start + GlobalConstant.CHUNK_SUFFIX;
                    while (System.IO.File.Exists(currentFilePath) && totalFile < minCountFile)
                    {
                        str.AppendLine(lstDuration[start]);
                        str.AppendLine(prefix + start + GlobalConstant.CHUNK_SUFFIX);
                        start++;
                        currentFilePath = filePathPrefix + start + GlobalConstant.CHUNK_SUFFIX;
                        totalFile++;
                        if (totalFile >= minCountFile)
                        {
                            str.Replace("#EXT-X-MEDIA-SEQUENCE:-1", "#EXT-X-MEDIA-SEQUENCE:" + seq);
                            return str;
                        }
                    }
                }
                if (remainTime > 0)
                {
                    // Lenh thong tin VOV
                    List<int> listChanelID = (from a in context.ProgramChanel
                                              join b in context.Unit on a.UnitID equals b.UnitID
                                              where a.ScheduleDayID == scheduleDay.ScheduleDayID
                                              && unit.UnitPath.Contains(b.UnitPath)
                                              && DbFunctions.TruncateTime(a.DateExecute) == DbFunctions.TruncateTime(currentDt)
                                              select a.ChanelID).ToList();
                    string chanelCode = null;
                    if (listChanelID.Any())
                    {
                        var chanelID = listChanelID[0];
                        if (chanelID > 0)
                        {
                            ForwardChanel fc = context.ForwardChanel.Find(chanelID);
                            if (fc != null)
                            {
                                chanelCode = fc.ChannelCode;
                            }
                        }
                    }
                    else
                    {
                        ForwardChanel fc = context.ForwardChanel.Where(x => x.IsDefault == GlobalConstant.IS_ACTIVE).FirstOrDefault();
                        if (fc != null)
                        {
                            chanelCode = fc.ChannelCode;
                        }
                    }
                    if (!string.IsNullOrWhiteSpace(chanelCode))
                    {
                        return PlayController.GetVOV(chanelCode.ToLower(), unitID);
                    }

                }

                str.Replace("#EXT-X-MEDIA-SEQUENCE:-1", "#EXT-X-MEDIA-SEQUENCE:" + seq);
            }
            return str;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}