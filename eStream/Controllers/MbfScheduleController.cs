﻿using eStream.Common.CommonClass;
using eStream.Common.CommonObject;
using eStream.Dto;
using Model;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace eStream.Controllers
{
    [ApiAuthorizeFilter]
    public class MbfScheduleController : ApiController
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private const int SCHEDULE_TYPE = 1;
        private const int LIVE_TYPE = 2;

        private const int RECEIVE_TYPE_UNIT = 1;
        private const int RECEIVE_TYPE_DEVICE = 2;

        private const int PLAY_TYPE_AUDIO = 1;
        private const int PLAY_TYPE_LINK = 2;

        private const int SCHEDULE_PLAYING = 1;
        private const int SCHEDULE_NOT_PLAYING = 2;

        [HttpPost]
        public MbfScheduleResponseDto Post(MbfSetScheduleDto request)
        {
            try
            {
                var listProgramId = new List<long>();
                using (var context = new Entities())
                {
                    ValidateSetScheduleRequest(context, request);
                    if (request.rec_play_mode == LIVE_TYPE)
                    {
                        var live = ProcessLive(context, request);
                        context.SaveChanges();
                        listProgramId = live.LiveFiles.Select(x => x.LiveFileID).ToList();
                    }
                    else if (request.rec_play_mode == SCHEDULE_TYPE)
                    {
                        var programs = ProcessSchedule(context, request);
                        context.SaveChanges();
                        listProgramId = programs.Select(x => (long)x.ProgramID).ToList();
                    }
                }

                if (request.rec_play_mode == LIVE_TYPE)
                {
                    Task.Factory.StartNew(() => PublicLive(listProgramId));
                }
                else if (request.rec_play_mode == SCHEDULE_TYPE)
                {
                    Task.Factory.StartNew(() => PublicSchedule(listProgramId));
                }
            }
            catch (Exception ex)
            {
                HttpResponseMessage response;
                if (ex is BusinessException)
                {
                    response = new HttpResponseMessage(HttpStatusCode.BadRequest);
                    response.Content = new StringContent("{\"message\":\"" + ex.Message + "\"}", Encoding.UTF8, "application/json");
                    throw new HttpResponseException(response);
                }
                Logger.Error(ex, "error when setting schedule");
                response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                response.Content = new StringContent("{\"message\":\"" + ex.Message + "\"}", Encoding.UTF8, "application/json");
                throw new HttpResponseException(response);
            }
            return new MbfScheduleResponseDto
            {
                rec_id = request.rec_id
            };
        }


        [HttpPost]
        public MbfScheduleResponseDto Cancel(MbfCancelScheduleDto request)
        {
            try
            {
                using (var context = new Entities())
                {
                    if (request == null)
                    {
                        throw new BusinessException("Dữ liệu rỗng");
                    }
                    if (request.rec_play_mode == SCHEDULE_TYPE && request.rec_id <= 0)
                    {
                        throw new BusinessException("Thiếu thông tin ID lịch phát");
                    }
                    if (request.rec_receipt_id <= 0)
                    {
                        throw new BusinessException("Thiếu thông tin ID đơn vị");
                    }
                    if (request.rec_play_mode != SCHEDULE_TYPE && request.rec_play_mode != LIVE_TYPE)
                    {
                        throw new BusinessException("Loại phát rec_play_mode không hợp lệ, giá trị hợp lệ thuộc [1, 2]");
                    }
                    if (request.rec_play_mode == SCHEDULE_TYPE && (string.IsNullOrWhiteSpace(request.rec_play_start) || string.IsNullOrWhiteSpace(request.rec_play_expire)))
                    {
                        throw new BusinessException("Thiếu thông tin ngày bắt đầu hoặc kết thúc của lịch phát");
                    }
                    if (!context.Unit.Any(x => x.UnitID == request.rec_receipt_id))
                    {
                        throw new BusinessException("Đơn vị phát không tồn tại");
                    }
                    if (request.rec_play_mode == SCHEDULE_TYPE)
                    {
                        CancelSchedule(context, request);
                    }
                    else if (request.rec_play_mode == SCHEDULE_TYPE)
                    {
                        StopLive(context, request.rec_receipt_id);
                    }
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                HttpResponseMessage response;
                if (ex is BusinessException)
                {
                    response = new HttpResponseMessage(HttpStatusCode.BadRequest);
                    response.Content = new StringContent("{\"message\":\"" + ex.Message + "\"}", Encoding.UTF8, "application/json");
                    throw new HttpResponseException(response);
                }
                Logger.Error(ex, "error when cancelling schedule");
                response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                response.Content = new StringContent("{\"message\":\"" + ex.Message + "\"}", Encoding.UTF8, "application/json");
                throw new HttpResponseException(response);
            }
            return new MbfScheduleResponseDto
            {
                rec_id = request.rec_id
            };
        }

        [HttpGet]
        public MbfScheduleDto Get(int id)
        {
            if (id == 0)
            {
                return null;
            }
            using (var context = new Entities())
            {
                var query = context.Schedule.Where(x => x.ScheduleID == id && x.Status == GlobalConstant.IS_ACTIVE && (x.IsHidden == null || x.IsHidden == 0));
                var entity = query.FirstOrDefault();
                if (entity == null)
                {
                    return null;
                }
                return new MbfScheduleDto
                {
                    rec_id = entity.ScheduleID,
                    rec_summary = entity.ScheduleName,
                    rec_type = 0,
                    rec_play_time = new int[] { entity.StartMinus.Value * 60, entity.EndMinus.Value * 60 },
                    rec_play_repeat_type = entity.Frequency.Value,
                    play_repeat_days = context.ScheduleDay.Where(d => d.ScheduleID == entity.ScheduleID).Select(d => d.DayOfWeek).ToArray(),
                };
            }
        }

        [HttpGet]
        public MbfScheduleDto[] List(int unit_id = 0, int type = 0, string name = null)
        {
            if (unit_id == 0)
            {
                return new MbfScheduleDto[] { };
            }
            using (var context = new Entities())
            {
                var query = context.Schedule.Where(x => x.Status == GlobalConstant.IS_ACTIVE && (x.IsHidden == null || x.IsHidden == 0));
                if (type > 0)
                {
                    query = query.Where(x => x.Frequency == type);
                }
                if (!string.IsNullOrWhiteSpace(name))
                {
                    name = name.Trim().ToLower();
                    query = query.Where(x => x.ScheduleName.ToLower().Contains(name));
                }
                var listSchedule = query.ToList();
                var schedules = new List<MbfScheduleDto>();
                foreach (var x in listSchedule)
                {
                    schedules.Add(new MbfScheduleDto
                    {
                        rec_id = x.ScheduleID,
                        rec_summary = x.ScheduleName,
                        rec_type = 0,
                        rec_play_time = new int[] { x.StartMinus.Value * 60, x.EndMinus.Value * 60 },
                        rec_play_repeat_type = x.Frequency.Value,
                        play_repeat_days = context.ScheduleDay.Where(d => d.ScheduleID == x.ScheduleID).Select(d => d.DayOfWeek).ToArray(),
                    });
                }
                return schedules.ToArray();
            }
        }

        private void ValidateSetScheduleRequest(Entities context, MbfSetScheduleDto request)
        {
            if (request == null)
            {
                throw new BusinessException("Dữ liệu rỗng");
            }
            if (request.rec_play_mode == SCHEDULE_TYPE && request.rec_id <= 0)
            {
                throw new BusinessException("Thiếu thông tin ID lịch phát");
            }
            if (request.rec_receipt_id <= 0)
            {
                throw new BusinessException("Thiếu thông tin ID đơn vị");
            }
            if (request.rec_type != PLAY_TYPE_AUDIO && request.rec_type != PLAY_TYPE_LINK)
            {
                throw new BusinessException("Loại bản tin rec_type không hợp lệ, giá trị hợp lệ thuộc [1, 2]");
            }
            if (request.rec_play_mode != SCHEDULE_TYPE && request.rec_play_mode != LIVE_TYPE)
            {
                throw new BusinessException("Loại phát rec_play_mode không hợp lệ, giá trị hợp lệ thuộc [1, 2]");
            }
            if (request.rec_receipt_type != RECEIVE_TYPE_DEVICE && request.rec_receipt_type != RECEIVE_TYPE_UNIT)
            {
                throw new BusinessException("Phạm vi phát rec_receipt_type không hợp lệ, giá trị hợp lệ thuộc [1, 2]");
            }
            if (request.rec_receipt_type == RECEIVE_TYPE_DEVICE && (request.rec_device_ids == null || !request.rec_device_ids.Any()))
            {
                throw new BusinessException("Danh sách thiết bị không được để trống đối với hình thức phát theo thiết bị (rec_receipt_id=2)");
            }
            if (request.rec_play_mode == SCHEDULE_TYPE && (string.IsNullOrWhiteSpace(request.rec_play_start) || string.IsNullOrWhiteSpace(request.rec_play_expire)))
            {
                throw new BusinessException("Thiếu thông tin ngày bắt đầu hoặc kết thúc của lịch phát");
            }
            if (string.IsNullOrWhiteSpace(request.rec_url) && (request.rec_audios == null || !request.rec_audios.Any()))
            {
                throw new BusinessException("Phải chọn thông tin file audio phát hoặc link phát chuyển tiếp");
            }
            if (!context.Unit.Any(x => x.UnitID == request.rec_receipt_id))
            {
                throw new BusinessException("Đơn vị phát không tồn tại");
            }
        }

        private List<Program> ProcessSchedule(Entities context, MbfSetScheduleDto request)
        {
            var schedule = context.Schedule.Find(request.rec_id);
            if (schedule == null || schedule.Status != GlobalConstant.IS_ACTIVE)
            {
                throw new BusinessException("Không tồn tại lịch phát");
            }
            DateTime fromDate;
            DateTime toDate;
            if (!DateTime.TryParseExact(request.rec_play_start, "yyyy-M-d", CultureInfo.InvariantCulture, DateTimeStyles.None, out fromDate))
            {
                throw new BusinessException("Thời gian bắt đầu lịch phát không đúng định dạng yyyy-M-d");
            }
            if (!DateTime.TryParseExact(request.rec_play_expire, "yyyy-M-d", CultureInfo.InvariantCulture, DateTimeStyles.None, out toDate))
            {
                throw new BusinessException("Thời gian kết thúc lịch phát không đúng định dạng yyyy-M-d");
            }
            if (fromDate.CompareTo(toDate) > 0)
            {
                throw new BusinessException("Thời gian kết thúc phải không nhỏ thua thời gian bắt đầu lịch phát");
            }
            if (fromDate.CompareTo(DateTime.Now.Date) < 0)
            {
                throw new BusinessException("Thời gian bắt đầu phát phải lớn hơn hoặc bằng ngày hiện tại");
            }
            // Neu thoi gian phat ngay hien tai dang dien ra thi chi lap lich cho ngay mai
            TimeSpan TimeExecute = new TimeSpan(schedule.ScheduleHour.Value, schedule.ScheduleMinus.Value, 0);
            if (fromDate.CompareTo(DateTime.Now.Date) == 0)
            {
                var fromTime = fromDate + TimeExecute;
                if (DateTime.Now.CompareTo(fromTime) > 0)
                {
                    fromDate = fromDate.AddDays(1);
                }
            }
            var dicFile = DownloadAudios(request);
            var scheduleDays = context.ScheduleDay.Where(x => x.ScheduleID == schedule.ScheduleID).ToList();
            var programs = new List<Program>();
            for (var date = fromDate; date <= toDate; date = date.AddDays(1))
            {
                DateTime firstDayOfWeek = date.StartOfWeek(DayOfWeek.Monday);
                ScheduleDay scheduleDay = null;
                foreach (var sd in scheduleDays)
                {
                    if ((date - firstDayOfWeek).Days == sd.DayOfWeek - 1)
                    {
                        scheduleDay = sd;
                        break;
                    }
                }
                if (scheduleDay != null)
                {
                    ClearSchedule(context, scheduleDay, date.Date, request.rec_receipt_id);
                    DateTime DateExecute = date.Date + TimeExecute;
                    var programsByDay = InsertProgram(context, scheduleDay, DateExecute, request, dicFile);
                    programs.AddRange(programsByDay);
                }
            }
            return programs;
        }

        private void CancelSchedule(Entities context, MbfCancelScheduleDto request)
        {
            DateTime fromDate;
            DateTime toDate;
            if (!DateTime.TryParseExact(request.rec_play_start, "yyyy-M-d", CultureInfo.InvariantCulture, DateTimeStyles.None, out fromDate))
            {
                throw new BusinessException("Thời gian bắt đầu lịch phát không đúng định dạng yyyy-M-d");
            }
            if (!DateTime.TryParseExact(request.rec_play_expire, "yyyy-M-d", CultureInfo.InvariantCulture, DateTimeStyles.None, out toDate))
            {
                throw new BusinessException("Thời gian kết thúc lịch phát không đúng định dạng yyyy-M-d");
            }
            if (fromDate.CompareTo(toDate) > 0)
            {
                throw new BusinessException("Thời gian kết thúc phải không nhỏ thua thời gian bắt đầu lịch phát");
            }
            if (fromDate.CompareTo(DateTime.Now.Date) < 0)
            {
                throw new BusinessException("Thời gian bắt đầu phát phải lớn hơn hoặc bằng ngày hiện tại");
            }
            // Neu thoi gian phat ngay hien tai dang dien ra thi chi lap lich cho ngay mai
            var schedule = context.Schedule.Find(request.rec_id);
            if (schedule == null)
            {
                throw new BusinessException("Không tồn tại lịch phát");
            }
            TimeSpan TimeExecute = new TimeSpan(schedule.ScheduleHour.Value, schedule.ScheduleMinus.Value, 0);
            if (fromDate.CompareTo(DateTime.Now.Date) == 0)
            {
                var fromTime = fromDate + TimeExecute;
                if (DateTime.Now.CompareTo(fromTime) > 0)
                {
                    fromDate = fromDate.AddDays(1);
                }
            }
            var scheduleDays = context.ScheduleDay.Where(x => x.ScheduleID == schedule.ScheduleID).ToList();
            for (var date = fromDate; date <= toDate; date = date.AddDays(1))
            {
                DateTime firstDayOfWeek = date.StartOfWeek(DayOfWeek.Monday);
                var exist = false;
                ScheduleDay scheduleDay = null;
                foreach (var sd in scheduleDays)
                {
                    if ((date - firstDayOfWeek).Days == sd.DayOfWeek - 1)
                    {
                        exist = true;
                        scheduleDay = sd;
                        break;
                    }
                }
                if (exist)
                {
                    List<Program> listProgram = context.Program.Where(x => x.ScheduleDayID == scheduleDay.ScheduleDayID
                        && DbFunctions.TruncateTime(x.ExecuteDate) == DbFunctions.TruncateTime(date)
                        && x.UnitID == request.rec_receipt_id && x.CreatedUser == "MBF").ToList();
                    context.Program.RemoveRange(listProgram);
                    DateTime FirstDayOfWeek = DateTime.Now.StartOfWeek(DayOfWeek.Monday);
                    DateTime DateExecute = FirstDayOfWeek.AddDays(scheduleDay.DayOfWeek - 1).Date + TimeExecute;
                    // Xoa vov
                    List<ProgramChanel> listChannel =
                       (from a in context.ProgramChanel
                        join b in context.ScheduleDay on a.ScheduleDayID equals b.ScheduleDayID
                        where a.ScheduleDayID == scheduleDay.ScheduleDayID && DbFunctions.TruncateTime(a.DateExecute) == DbFunctions.TruncateTime(date) && a.UnitID == request.rec_receipt_id
                        select a).ToList();
                    context.ProgramChanel.RemoveRange(listChannel);
                }
            }
        }

        private LiveDto ProcessLive(Entities context, MbfSetScheduleDto request)
        {
            var fileFile = DownloadAudios(request);
            var live = InsertOrUpdateLive(context, request, fileFile);
            return live;
        }

        private void StopLive(Entities context, int unitId)
        {
            var live = context.Live.Where(x => x.UnitID == unitId).FirstOrDefault();
            if (live == null)
            {
                throw new BusinessException("Chưa có chương trình phát thanh trực tiếp");
            }
            List<int> listDeviceID = null;
            List<Device> listDevice = null;
            if (live.LiveType == GlobalConstant.LIVE_TYPE_ALL)
            {
                string sUnit = "/" + unitId + "/";
                listDevice = context.Device
                    .Where(x => context.Unit.Any(u => u.UnitID == x.UnitID && u.UnitPath.Contains(sUnit))).ToList();
            }
            else
            {
                listDeviceID = context.LiveDevice.Where(x => x.LiveID == live.LiveID).Select(x => x.DeviceID).ToList();
            }
            if (listDevice == null)
            {
                listDevice = context.Device.Where(x => listDeviceID.Contains(x.DeviceID)).ToList();
            }
            if (listDevice != null && listDevice.Any())
            {
                foreach (Device device in listDevice)
                {
                    device.IsRequestStop = GlobalConstant.IS_ACTIVE;
                    device.IsRequestStart = null;
                    device.IsFmStart = null;
                    if (StreamingCaching.DicPlaying.ContainsKey(device.DeviceID))
                    {
                        StreamingCaching.DicPlaying[device.DeviceID] = null;
                    }
                }
            }
            live.Status = GlobalConstant.LIVE_STOPED;
            live.IsStreaming = 0;
        }

        public FileDto DownloadAudio(int unitId, string link)
        {
            try
            {
                using (var webClient = new WebClient())
                {
                    var ext = Path.GetExtension(link);
                    if (string.IsNullOrWhiteSpace(ext))
                    {
                        ext = ".wav";
                    }
                    string fileName = Guid.NewGuid().ToString() + ext;
                    string filePath = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(GlobalConstant.FILE_PATH), unitId.ToString(),
                        DateTimeOffset.Now.ToUnixTimeMilliseconds().ToString());
                    //Check whether Directory (Folder) exists.
                    if (!Directory.Exists(filePath))
                    {
                        //If Directory (Folder) does not exists. Create it.
                        Directory.CreateDirectory(filePath);
                    }
                    webClient.DownloadFile(link, filePath + "\\" + fileName);
                    return new FileDto { FileName = fileName, FileUrl = filePath };
                }
            }
            catch (Exception ex)
            {
                throw new BusinessException("Có lỗi trong quá trình tải file: " + ex.Message);
            }
        }

        private Dictionary<string, FileDto> DownloadAudios(MbfSetScheduleDto request)
        {
            var dicFile = new Dictionary<string, FileDto>();
            var audios = request.rec_audios;
            if (audios != null)
            {
                foreach (var a in audios)
                {
                    var file = DownloadAudio(request.rec_receipt_id, a.url);
                    dicFile[a.url] = file;
                }
            }
            return dicFile;
        }

        private List<Program> InsertProgram(Entities context, ScheduleDay objScheduleDay, DateTime DateExecute, MbfSetScheduleDto request, Dictionary<string, FileDto> dicFile)
        {
            var audios = request.rec_audios;
            var programs = new List<Program>();
            if (audios != null)
            {
                foreach (var a in audios)
                {
                    var file = dicFile[a.url];
                    Program program = new Program
                    {
                        ScheduleDayID = objScheduleDay.ScheduleDayID,
                        FileUrl = file.FileUrl,
                        Status = GlobalConstant.IS_ACTIVE,
                        UnitID = request.rec_receipt_id,
                        FileName = file.FileName,
                        Title = file.FileName,
                        ExecuteDate = DateExecute,
                        CreatedUser = "MBF",
                        CreatedDate = DateTime.Now,
                        NumberOrder = a.index
                    };
                    context.Program.Add(program);
                    programs.Add(program);
                }
            }
            if (!string.IsNullOrWhiteSpace(request.rec_url))
            {
                var programChannel = new ProgramChanel
                {
                    DateExecute = DateExecute,
                    ScheduleDayID = objScheduleDay.ScheduleDayID,
                    Status = GlobalConstant.IS_ACTIVE,
                    UnitID = request.rec_receipt_id,
                };
                var channel = context.ForwardChanel.Where(x => x.Status == GlobalConstant.IS_ACTIVE && x.ChannelCode.ToLower() == request.rec_url).FirstOrDefault();
                if (channel != null)
                {
                    programChannel.ChanelID = channel.ChanelID;
                }
                else
                {
                    programChannel.Url = request.rec_url.Trim();
                }
                context.ProgramChanel.Add(programChannel);
            }
            return programs;
        }

        private LiveDto InsertOrUpdateLive(Entities context, MbfSetScheduleDto request, Dictionary<string, FileDto> dicFile)
        {
            var res = new LiveDto() { LiveFiles = new List<LiveFile>() };
            var live = context.Live.Where(x => x.UnitID == request.rec_receipt_id).FirstOrDefault();
            if (live == null)
            {
                live = new Live
                {
                    LiveType = GlobalConstant.LIVE_TYPE_ALL,
                    CreatedDate = DateTime.Now,
                    Status = GlobalConstant.LIVE_DRAFF,
                    LiveSource = GlobalConstant.LIVE_SOURCE_VOV,
                    UnitID = request.rec_receipt_id
                };
                context.Live.Add(live);
                context.SaveChanges();
            }
            else
            {
                ClearLive(context, live);
            }
            res.Live = live;
            if (string.IsNullOrWhiteSpace(request.rec_url))
            {
                live.LiveSource = GlobalConstant.LIVE_SOURCE_FILE;
                live.ChannelID = null;
                live.Url = null;
            }
            else
            {
                live.LiveSource = GlobalConstant.LIVE_SOURCE_VOV;
                live.ChannelID = null;
                live.Url = null;
                var channel = context.ForwardChanel.Where(x => x.Status == GlobalConstant.IS_ACTIVE && x.ChannelCode.ToLower() == request.rec_url).FirstOrDefault();
                if (channel != null)
                {
                    live.ChannelID = channel.ChanelID;
                }
                else
                {
                    live.Url = request.rec_url.Trim();
                }
            }
            if (request.rec_receipt_type == RECEIVE_TYPE_UNIT)
            {
                live.LiveType = GlobalConstant.LIVE_TYPE_ALL;
            }
            else
            {
                live.LiveType = GlobalConstant.LIVE_TYPE_CHOICE;
            }
            List<Device> listDevice = null;
            if (live.LiveType == GlobalConstant.LIVE_TYPE_ALL)
            {
                string sUnit = "/" + live.UnitID + "/";
                listDevice = context.Device.Where(x => x.UnitID == null && context.Unit.Any(u => u.UnitID == x.UnitID && u.UnitPath.Contains(sUnit))).ToList();
            }
            else
            {
                string sUnit = "/" + live.UnitID + "/";
                listDevice = context.Device.Where(x => context.Unit.Any(u => u.UnitID == x.UnitID && u.UnitPath.Contains(sUnit))
                && request.rec_device_ids.Contains(x.DeviceID)).ToList();
                if (listDevice.Count != request.rec_device_ids.Count())
                {
                    throw new BusinessException("Tồn tại ID thiết bị không có trong hệ thống");
                }
            }
            foreach (Device device in listDevice)
            {
                var liveDevice = new LiveDevice();
                liveDevice.LiveID = live.LiveID;
                liveDevice.DeviceID = device.DeviceID;
                context.LiveDevice.Add(liveDevice);
            }
            var audios = request.rec_audios;
            foreach (var a in audios)
            {
                var file = dicFile[a.url];
                LiveFile liveFile = new LiveFile
                {
                    LiveID = live.LiveID,
                    FileUrl = file.FileUrl,
                    FileName = file.FileName,
                    Title = file.FileName,
                    CreatedDate = DateTime.Now,
                    CreatedUser = "MBF"
                };
                context.LiveFile.Add(liveFile);
                res.LiveFiles.Add(liveFile);
            }
            return res;
        }

        private void PublicSchedule(List<long> programIds)
        {
            using (var context = new Entities())
            {
                var programs = context.Program.Where(x => programIds.Contains(x.ProgramID)).ToList();
                foreach (var program in programs)
                {
                    // Split file
                    SetupInfo PlayList = new SetupInfo
                    {
                        StartTime = program.ExecuteDate.Value,
                        Channel = program.ScheduleDayID.ToString(),
                        AudioInfos = new List<AudioInfo> { new AudioInfo { FilePath = Path.Combine(program.FileUrl, program.FileName) } }
                    };
                    List<BroadcastInfo> ListSetup = ScheduleUlti.SplitAudioFile(PlayList.AudioInfos);
                    if (ListSetup.Any())
                    {
                        var objBroadcast = ListSetup[0];
                        program.Duration = objBroadcast.Duration;
                        program.SplitedFolder = objBroadcast.TempFolder;
                    }
                    Task.Factory.StartNew(() => ScheduleUlti.SplitAudioFile(ListSetup, GlobalConstant.BITRATE_128));
                    Task.Factory.StartNew(() => ScheduleUlti.SplitAudioFile(ListSetup, GlobalConstant.BITRATE_32));
                }
                context.SaveChanges();
            }
        }

        private void PublicLive(List<long> liveFileIds)
        {
            using (var context = new Entities())
            {
                var liveFiles = context.LiveFile.Where(x => liveFileIds.Contains(x.LiveFileID)).ToList();
                foreach (var liveFile in liveFiles)
                {
                    // Split file
                    SetupInfo PlayList = new SetupInfo
                    {
                        StartTime = DateTime.Now,
                        Channel = liveFile.LiveID.ToString(),
                        AudioInfos = new List<AudioInfo> { new AudioInfo { FilePath = Path.Combine(liveFile.FileUrl, liveFile.FileName) } }
                    };
                    List<BroadcastInfo> ListSetup = ScheduleUlti.SplitAudioFile(PlayList.AudioInfos);
                    if (ListSetup.Any())
                    {
                        var objBroadcast = ListSetup[0];
                        liveFile.Duration = objBroadcast.Duration;
                        liveFile.SplitedFolder = objBroadcast.TempFolder;
                    }
                    Task.Factory.StartNew(() => ScheduleUlti.SplitAudioFile(ListSetup, GlobalConstant.BITRATE_128));
                    Task.Factory.StartNew(() => ScheduleUlti.SplitAudioFile(ListSetup, GlobalConstant.BITRATE_32));
                    var live = context.Live.Find(liveFile.LiveID);
                    StartLive(context, live);

                }
                context.SaveChanges();
            }
        }

        private void ClearSchedule(Entities context, ScheduleDay entity, DateTime date, int unitId)
        {
            var programs = context.Program.Where(x => x.UnitID == unitId && x.ScheduleDayID == entity.ScheduleDayID
            && DbFunctions.TruncateTime(x.ExecuteDate) == DbFunctions.TruncateTime(date)).ToList();
            context.Program.RemoveRange(programs);
            var channels = context.ProgramChanel.Where(x => x.UnitID == unitId && x.ScheduleDayID == entity.ScheduleDayID).ToList();
            context.ProgramChanel.RemoveRange(channels);
        }

        private void ClearLive(Entities context, Live entity)
        {
            var devices = context.LiveDevice.Where(x => x.LiveID == entity.LiveID).ToList();
            context.LiveDevice.RemoveRange(devices);
            var files = context.LiveFile.Where(x => x.LiveID == entity.LiveID).ToList();
            context.LiveFile.RemoveRange(files);
        }

        private void StartLive(Entities context, Live live)
        {
            bool isFm = false;
            if (live.ChannelID.GetValueOrDefault() > 0)
            {
                var channel = context.ForwardChanel.Where(x => x.Status == GlobalConstant.IS_ACTIVE && x.ChanelID == live.ChannelID).FirstOrDefault();
                if (channel != null)
                {
                    live.ChannelID = channel.ChanelID;
                    if (channel.ChannelCode.ToUpper() == GlobalConstant.FM_CHANNEL_CODE)
                    {
                        isFm = true;
                    }
                }
            }
            List<Device> listDevice = null;
            if (live.LiveType == GlobalConstant.LIVE_TYPE_ALL)
            {
                string sUnit = "/" + live.UnitID + "/";
                listDevice = context.Device.Where(x => (x.UnitID == null && context.Unit.Any(u => u.UnitID == x.UnitID && u.UnitPath.Contains(sUnit)))).ToList();
            }
            else
            {
                listDevice = context.Device.Where(x => context.LiveDevice.Any(l => l.LiveID == live.LiveID && l.DeviceID == x.DeviceID)).ToList();
            }
            if (listDevice != null && listDevice.Any())
            {
                foreach (Device device in listDevice)
                {
                    if (isFm)
                    {
                        device.IsFmStart = GlobalConstant.IS_ACTIVE;
                    }
                    else
                    {
                        device.IsRequestStop = GlobalConstant.IS_ACTIVE;
                        device.IsFmStart = null;
                    }
                    device.IsRequestStart = null;
                    if (StreamingCaching.DicPlaying.ContainsKey(device.DeviceID))
                    {
                        StreamingCaching.DicPlaying[device.DeviceID] = null;
                    }
                }
            }
            if (live.Status <= GlobalConstant.LIVE_WAIT)
            {
                live.Status = GlobalConstant.LIVE_PLAYING;
            }
            if (live.LiveSource.GetValueOrDefault() == GlobalConstant.LIVE_SOURCE_FILE)
            {
                live.PlayTime = null;
            }
            if (live.LiveSource.GetValueOrDefault() == GlobalConstant.LIVE_SOURCE_STREAMING)
            {
                live.StreamingFolder = Guid.NewGuid().ToString();
                var FilePath = GlobalConstant.MEDIA_FOLDER + "live/" + live.StreamingFolder;
                var WavFilePath = FilePath + "/wav";
                var AacFilePath = FilePath + "/aac";

                //Check whether Directory (Folder) exists.
                if (!Directory.Exists(FilePath))
                {
                    //If Directory (Folder) does not exists. Create it.
                    Directory.CreateDirectory(FilePath);
                }
                if (!Directory.Exists(WavFilePath))
                {
                    //If Directory (Folder) does not exists. Create it.
                    Directory.CreateDirectory(WavFilePath);
                }
                if (!Directory.Exists(AacFilePath))
                {
                    //If Directory (Folder) does not exists. Create it.
                    Directory.CreateDirectory(AacFilePath);
                }
                context.Database.ExecuteSqlCommand("delete from LiveStreaming where LiveId = @liveId", new SqlParameter("liveId", live.LiveID));
            }
        }

        [HttpGet]
        public List<MbfScheduleStatusDto> Status(int rec_play_mode = 0, int rec_id = 0, int unit_id = 0)
        {
            using (var context = new Entities())
            {
                if (rec_play_mode == LIVE_TYPE)
                {
                    return GetLivesStatus(context, unit_id);
                }
                if (rec_play_mode == SCHEDULE_TYPE)
                {
                    return GetSchedulesStatus(context, rec_id, unit_id);
                }
                if (rec_play_mode == 0)
                {
                    var res = new List<MbfScheduleStatusDto>();
                    var lives = GetLivesStatus(context, unit_id);
                    res.AddRange(lives);
                    var schedules = GetSchedulesStatus(context, rec_id, unit_id);
                    var livingUnitIds = new List<int>();
                    foreach (var l in lives)
                    {
                        livingUnitIds.AddRange(l.units.Select(x => x.id).ToList());
                    }
                    foreach (var s in schedules)
                    {
                        var units = new List<ScheduleUnitItem>();
                        units.AddRange(s.units);
                        units.RemoveAll(x => livingUnitIds.Contains(x.id));
                        var newSchedule = new MbfScheduleStatusDto
                        {
                            rec_id = s.rec_id,
                            rec_summary = s.rec_summary,
                            rec_play_mode = s.rec_play_mode,
                            rec_play_time = s.rec_play_time,
                            rec_status = s.rec_status,
                            units = units,
                        };
                        res.Add(newSchedule);
                    }
                    return res;
                }
                return new List<MbfScheduleStatusDto>();
            }
        }

        private List<MbfScheduleStatusDto> GetLivesStatus(Entities context, int unit_id = 0)
        {
            OrderConfig config = context.OrderConfig.First();
            var schedules = new List<MbfScheduleStatusDto>();
            var units = context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE && (unit_id == 0 || x.UnitID == unit_id))
                .ToList().Select(x => new ScheduleUnitItem
                {
                    id = x.UnitID,
                    name = x.UnitName,
                    path = x.UnitPath,
                    programs = new List<ScheduleProgramItem>(),
                    devices = new List<ScheduleUnitDeviceItem>()
                }).OrderBy(x => x.path).ToList();
            var dicChannels = new Dictionary<int, string>();
            foreach (var unit in units)
            {
                var live = (from l in context.Live
                            join u in context.Unit on l.UnitID equals u.UnitID
                            where l.Status >= GlobalConstant.LIVE_PLAYING
                            && unit.path.Contains(u.UnitPath)
                            orderby (u.UnitLevel == GlobalConstant.UNIT_TYPE_VILLAGE ? config.VillageOrder
                                       : u.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE ? config.CommuneOrder
                                       : u.UnitLevel == GlobalConstant.UNIT_TYPE_DISTRICT ? config.DistrictOrder
                                       : u.UnitLevel == GlobalConstant.UNIT_TYPE_PROVINCE ? config.ProvinceOrder : 0),
                                       l.LiveID
                            select l).FirstOrDefault();
                if (live != null)
                {
                    var l = new MbfScheduleStatusDto
                    {
                        rec_id = 0,
                        rec_summary = "Phát trực tiếp",
                        rec_play_mode = LIVE_TYPE,
                        rec_play_time = new int[] { },
                        rec_status = SCHEDULE_PLAYING,
                        units = new List<ScheduleUnitItem>(),
                    };
                    int liveSource = live.LiveSource.GetValueOrDefault();
                    switch (liveSource)
                    {
                        case GlobalConstant.LIVE_SOURCE_STREAMING:
                            l.rec_summary = "Phát trực tiếp từ micro";
                            break;
                        case GlobalConstant.LIVE_SOURCE_FILE:
                            l.rec_summary = "Phát trực tiếp từ file";
                            unit.programs = context.LiveFile.Where(x => x.LiveID == live.LiveID)
                                .ToList().Select(x => new ScheduleProgramItem
                                {
                                    id = x.LiveFileID,
                                    title = x.Title,
                                    file_name = x.FileName,
                                    duration = x.Duration.GetValueOrDefault(),
                                    created_date = x.CreatedDate.Value.ToString("yyyy-M-d H:mm:ss"),
                                    created_user = x.CreatedUser,
                                }).ToList();
                            break;
                        case GlobalConstant.LIVE_SOURCE_VOV:
                            l.rec_summary = "Phát trực tiếp từ kênh chuyển tiếp";
                            if (!string.IsNullOrWhiteSpace(live.Url))
                            {
                                unit.rec_url = live.Url;
                            }
                            else if (live.ChannelID != null)
                            {
                                if (dicChannels.ContainsKey(live.ChannelID.Value))
                                {
                                    unit.rec_url = dicChannels[live.ChannelID.Value];
                                }
                                else
                                {
                                    unit.rec_url = context.ForwardChanel.Where(x => x.Status == GlobalConstant.IS_ACTIVE && x.ChanelID == live.ChannelID.Value)
                                        .Select(x => x.ChannelCode).FirstOrDefault();
                                    dicChannels[live.ChannelID.Value] = unit.rec_url;
                                }
                            }
                            break;
                    }
                    if (live.LiveType == GlobalConstant.LIVE_TYPE_CHOICE)
                    {
                        unit.devices = context.Device.Where(x => x.Status == GlobalConstant.DEVICE_STATUS_READY && x.UnitID == unit.id
                            && context.LiveDevice.Any(d => d.LiveID == live.LiveID && d.DeviceID == x.DeviceID))
                            .Select(x => new ScheduleUnitDeviceItem
                            {
                                id = x.DeviceID,
                                code = x.DeviceCode,
                                name = x.DeviceName
                            }).ToList();
                    }
                    else
                    {
                        unit.devices = context.Device.Where(x => x.Status == GlobalConstant.DEVICE_STATUS_READY && x.UnitID == unit.id)
                            .Select(x => new ScheduleUnitDeviceItem
                            {
                                id = x.DeviceID,
                                code = x.DeviceCode,
                                name = x.DeviceName
                            }).ToList();
                    }
                    l.units.Add(unit);
                }
            }
            return schedules;
        }

        private List<MbfScheduleStatusDto> GetSchedulesStatus(Entities context, int rec_id = 0, int unit_id = 0)
        {
            OrderConfig config = context.OrderConfig.First();
            var scheduleQuery = context.Schedule.Where(x => x.Status == GlobalConstant.IS_ACTIVE);
            if (rec_id > 0)
            {
                scheduleQuery = scheduleQuery.Where(x => x.ScheduleID == rec_id);
            }
            var listSchedule = scheduleQuery.Select(x => new
            {
                x.ScheduleID,
                x.ScheduleName,
                x.ScheduleHour,
                x.ScheduleMinus,
                x.StartMinus,
                x.EndMinus,
                x.Frequency,
                ScheduleDays = context.ScheduleDay.Where(d => d.ScheduleID == x.ScheduleID).ToList()
            }).ToList();
            DateTime date = DateTime.Now;
            long hourOfDay = date.Hour;
            long minuteOfDay = date.Minute;
            long minutes = 60 * hourOfDay + minuteOfDay;
            long seconds = 60 * minutes + date.Second;
            var schedules = new List<MbfScheduleStatusDto>();
            var units = context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE && (unit_id == 0 || x.UnitID == unit_id))
                .ToList().Select(x => new ScheduleUnitItem
                {
                    id = x.UnitID,
                    name = x.UnitName,
                    path = x.UnitPath,
                    programs = new List<ScheduleProgramItem>(),
                    devices = new List<ScheduleUnitDeviceItem>()
                }).OrderBy(x => x.path).ToList();
            var dicDevices = new Dictionary<int, List<ScheduleUnitDeviceItem>>();
            var dicChannels = new Dictionary<int, string>();
            foreach (var schedule in listSchedule)
            {
                var s = new MbfScheduleStatusDto
                {
                    rec_id = schedule.ScheduleID,
                    rec_summary = schedule.ScheduleName,
                    rec_play_mode = SCHEDULE_TYPE,
                    rec_play_time = new int[] { schedule.StartMinus.Value * 60, schedule.EndMinus.Value * 60 },
                    rec_status = SCHEDULE_NOT_PLAYING,
                    units = new List<ScheduleUnitItem>()
                };
                if (seconds >= schedule.StartMinus.Value * 60 && seconds <= schedule.EndMinus * 60)
                {
                    s.rec_status = SCHEDULE_PLAYING;
                }
                var scheduleDays = schedule.ScheduleDays;
                DateTime firstDayOfWeek = date.StartOfWeek(DayOfWeek.Monday);
                ScheduleDay scheduleDay = null;
                foreach (var sd in scheduleDays)
                {
                    if ((date - firstDayOfWeek).Days == sd.DayOfWeek - 1)
                    {
                        scheduleDay = sd;
                        break;
                    }
                }
                if (scheduleDay != null)
                {
                    TimeSpan TimeExecute = new TimeSpan(schedule.ScheduleHour.Value, schedule.ScheduleMinus.Value, 0);
                    DateTime DateExecute = date.Date + TimeExecute;

                    foreach (var unit in units)
                    {
                        var lstProgram = (from p in context.Program
                                          join u in context.Unit on p.UnitID equals u.UnitID
                                          where p.ScheduleDayID == scheduleDay.ScheduleDayID
                                          && p.Status == GlobalConstant.IS_APPROVED
                                          && DbFunctions.TruncateTime(p.ExecuteDate) == DbFunctions.TruncateTime(date)
                                          && unit.path.Contains(u.UnitPath)
                                          orderby (u.UnitLevel == GlobalConstant.UNIT_TYPE_VILLAGE ? config.VillageOrder
                                          : u.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE ? config.CommuneOrder
                                          : u.UnitLevel == GlobalConstant.UNIT_TYPE_DISTRICT ? config.DistrictOrder
                                          : u.UnitLevel == GlobalConstant.UNIT_TYPE_PROVINCE ? config.ProvinceOrder : 0),
                                          p.NumberOrder, p.ProgramID
                                          select p).ToList().Select(p => new ScheduleProgramItem
                                          {
                                              id = p.ProgramID,
                                              title = p.Title,
                                              file_name = p.FileName,
                                              duration = p.Duration.GetValueOrDefault(),
                                              created_date = p.CreatedDate.Value.ToString("yyyy-M-d H:mm:ss"),
                                              created_user = p.CreatedUser,
                                          }).ToList();
                        var programChanel = context.ProgramChanel.Where(x => x.Status == GlobalConstant.IS_ACTIVE && x.UnitID == unit.id
                            && x.ScheduleDayID == scheduleDay.ScheduleDayID && DbFunctions.TruncateTime(date) == DbFunctions.TruncateTime(date)).FirstOrDefault();
                        if (programChanel != null)
                        {
                            if (!string.IsNullOrWhiteSpace(programChanel.Url))
                            {
                                unit.rec_url = programChanel.Url;
                            }
                            else
                            {
                                if (dicChannels.ContainsKey(programChanel.ProgramChanelID))
                                {
                                    unit.rec_url = dicChannels[programChanel.ProgramChanelID];
                                }
                                else
                                {
                                    unit.rec_url = context.ForwardChanel.Where(x => x.Status == GlobalConstant.IS_ACTIVE && x.ChanelID == programChanel.ChanelID)
                                        .Select(x => x.ChannelCode).FirstOrDefault();
                                    dicChannels[programChanel.ProgramChanelID] = unit.rec_url;
                                }
                            }
                        }
                        if (lstProgram.Any())
                        {
                            unit.programs = lstProgram;
                            if (dicDevices.ContainsKey(unit.id))
                            {
                                unit.devices = dicDevices[unit.id];
                            }
                            else
                            {
                                unit.devices = context.Device.Where(x => x.Status == GlobalConstant.DEVICE_STATUS_READY && x.UnitID == unit.id)
                                    .Select(x => new ScheduleUnitDeviceItem
                                    {
                                        id = x.DeviceID,
                                        code = x.DeviceCode,
                                        name = x.DeviceName
                                    }).ToList();
                                dicDevices[unit.id] = unit.devices;
                            }
                        }
                        s.units.Add(unit);
                    }
                    schedules.Add(s);
                }
            }
            return schedules;
        }
    }


    public class FileDto
    {
        public string FileUrl { get; set; }
        public string FileName { get; set; }
    }

    public class LiveDto
    {
        public Live Live { get; set; }
        public List<LiveFile> LiveFiles { get; set; }
    }
}
