﻿using eStream.Common.CommonClass;
using eStream.Common.CommonObject;
using eStream.Helpers.Filter;
using eStream.Models;
using Model;
using PagedList;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace eStream.Controllers
{
    public class DeviceConfigController : BaseController
    {
        private  readonly Entities _context = new Entities();
        [BreadCrumb(ControllerName = "Cấu hình", ActionName = "", AreaName = "")]
        public ActionResult Index()
        {
            PrepareDataSearch(_context);
            return View();
        }

        public PartialViewResult SearchConfig(int? DistrictID, int? CommuneID, int page = 1)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            string sUnit = "/" + user.UnitID + "/";
            var iQConfig = _context.Unit.Where(x => x.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE && x.UnitPath.Contains(sUnit))
                .Select(x => new UnitModel
                {
                    UnitID = x.UnitID,
                    UnitPath = x.UnitPath,
                    UnitName = x.UnitName,
                    ParentName = _context.Unit.Where(u => u.UnitID == x.ParentID).Select(u => u.UnitName).FirstOrDefault(),
                    DeviceConfigID = _context.DeviceConfig.Where(c => c.UnitID == x.UnitID).Select(c => c.DeviceConfigID).FirstOrDefault()
                }).OrderBy(x => x.UnitPath).ThenBy(x => x.UnitName);

            var listConfig = iQConfig.ToList();
            PagedList<UnitModel> paginate = new PagedList<UnitModel>(listConfig, page, GlobalConstant.PAGE_SIZE);
            ViewData["listConfig"] = paginate;
            ViewBag.DistrictID = DistrictID;
            ViewBag.CommuneID = CommuneID;
            return PartialView("_LstConfig");
        }

        public PartialViewResult OpenConfig(int unitID)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            string sUnit = "/" + user.UnitID + "/";
            Unit unit = _context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE && x.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE
            && x.UnitID == unitID && x.UnitPath.Contains(sUnit)).FirstOrDefault();
            DeviceConfigModel model = new DeviceConfigModel();
            if (unit != null)
            {
                DeviceConfig entity = _context.DeviceConfig.Where(x => x.UnitID == unitID).FirstOrDefault();
                model.EntityTo(entity, unit);
                // cac list du lieu
                List<SelectListItem> listBitrate = new List<SelectListItem>();
                listBitrate.Add(new SelectListItem { Value = "", Text = "--Mặc định--" });
                listBitrate.Add(new SelectListItem { Value = "64", Text = "64kbps" });
                listBitrate.Add(new SelectListItem { Value = "128", Text = "128kbps" });
                listBitrate.Add(new SelectListItem { Value = "320", Text = "320kbps" });
                ViewData["listBitrate"] = listBitrate;
                return PartialView("_Config", model);
            }
            else
            {
                ViewData["noUnit"] = GlobalConstant.IS_ACTIVE;
                return PartialView("_Config", model);
            }
        }

        public JsonResult SaveConfig(DeviceConfigModel model)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            string sUnit = "/" + user.UnitID + "/";
            Unit unit = _context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE && x.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE
            && x.UnitID == model.UnitID && x.UnitPath.Contains(sUnit)).FirstOrDefault();
            if (unit != null)
            {
                DeviceConfig entity = null;
                if (model.DeviceConfigID != null && model.DeviceConfigID > 0)
                {
                    entity = _context.DeviceConfig.Find(model.DeviceConfigID);
                    if (entity == null)
                    {
                        throw new BusinessException("Không tồn tại cấu hình vừa chọn");
                    }
                    model.UpdateEntity(entity);
                }
                else
                {
                    entity = new DeviceConfig();
                    model.UpdateEntity(entity);
                    _context.DeviceConfig.Add(entity);
                }
                // Luu thay doi cua thiet bi
                List<Device> listDevice = _context.Device.Where(x => x.UnitID == entity.UnitID).ToList();
                foreach (Device device in listDevice)
                {
                    if (device.SyncTime.GetValueOrDefault() > 0)
                    {
                        device.IsChangeSyncTime = GlobalConstant.IS_ACTIVE;
                    }
                    device.IsChangeVolume = GlobalConstant.IS_ACTIVE;
                    DeviceCommandCaching.AddCommand(device.DeviceID, GlobalConstant.DEVICE_COMMAND_CHANGE_VOLUME);
                    DeviceCommandCaching.AddCommand(device.DeviceID, GlobalConstant.DEVICE_COMMAND_CHECK_INTERVAL);
                }
                _context.SaveChanges();
                return Json("Cấu hình thành công", JsonRequestBehavior.AllowGet);
            }
            else
            {
                throw new BusinessException("Bạn không có quyền cấu hình cho xã này");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}