﻿using eStream.Common.CommonClass;
using eStream.Common.CommonObject;
using eStream.Helpers.Filter;
using eStream.Models;
using Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eStream.Controllers
{
    public class BaseController : Controller
    {
        //private readonly Entities context = new Entities();
        // GET: User
        public void PrepareDataSearch(Entities context)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            // Lay danh sach don vi
            List<UnitModel> listUnit = context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE
            && x.ParentID == user.UnitID)
            .Select(x => new UnitModel
            {
                UnitID = x.UnitID,
                UnitName = x.UnitName,
                PhoneNumber = x.PhoneNumber,
                Email = x.Email,
                Status = x.Status,
                UnitLevel = x.UnitLevel,
                ParentID = x.ParentID,
                UnitPath = x.UnitPath
            }).OrderBy(x => x.UnitPath).ThenBy(x => x.UnitName).ToList();
            if (user.UnitLevel == GlobalConstant.UNIT_TYPE_PROVINCE)
            {
                List<UnitModel> listCommune = new List<UnitModel>();
                if (listUnit.Any())
                {
                    int firstUnitID = listUnit.First().UnitID;
                    string sDistrict = "/" + firstUnitID + "/";
                    listCommune.AddRange(
                        context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE
                        && (x.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE || x.UnitLevel == GlobalConstant.UNIT_TYPE_DISTRICT)
                        && x.UnitPath.Contains(sDistrict))
                        .Select(x => new UnitModel
                        {
                            UnitID = x.UnitID,
                            UnitName = x.UnitName,
                            PhoneNumber = x.PhoneNumber,
                            Email = x.Email,
                            Status = x.Status,
                            UnitLevel = x.UnitLevel,
                            ParentID = x.ParentID,
                            UnitPath = x.UnitPath
                        }).OrderBy(x => x.UnitPath).ThenBy(x => x.UnitName).ToList());
                }
                ViewData["listCommune"] = listCommune;
            }
            listUnit.Insert(0, context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE
                         && x.UnitID == user.UnitID)
                        .Select(x => new UnitModel
                        {
                            UnitID = x.UnitID,
                            UnitName = x.UnitName,
                            PhoneNumber = x.PhoneNumber,
                            Email = x.Email,
                            Status = x.Status,
                            UnitLevel = x.UnitLevel,
                            ParentID = x.ParentID,
                            UnitPath = x.UnitPath
                        }).OrderBy(x => x.UnitPath).ThenBy(x => x.UnitName).FirstOrDefault());
            if (user.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE)
            {
                ViewData["listCommune"] = listUnit;
            }
            else if (user.UnitLevel == GlobalConstant.UNIT_TYPE_PROVINCE)
            {
                // De tim kiem all
                listUnit.First().UnitID = 0;
                ViewData["listDistrict"] = listUnit;
            }
            else if (user.UnitLevel == GlobalConstant.UNIT_TYPE_DISTRICT)
            {
                ViewData["listCommune"] = listUnit;
            }
            ViewData["province"] = context.Unit.Where(x => x.ParentID == null).FirstOrDefault();
        }

        public void PrepareDataAddOrEdit(Entities context, int? provinceId, int? districtId, int? communeId)
        {
            UserInfoBO userInfoBo = SessionManager.GetUserInfoBO();

            #region Thong tin don vi

            List<SelectListItem> listProvince = context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE
        && x.UnitLevel == GlobalConstant.UNIT_TYPE_PROVINCE)
        .OrderBy(x => x.UnitPath).ThenBy(x => x.UnitName)
        .Select(x => new SelectListItem
        {
            Value = "" + x.UnitID,
            Text = x.UnitName
        }).ToList();
            ViewData["listProvince"] = listProvince;
            List<SelectListItem> listVillage = new List<SelectListItem> { new SelectListItem { Value = "", Text = "--Chọn thôn/xóm--" } };
            // Neu la cap thon xu ly rieng cho khoe
            if (userInfoBo.UnitLevel == GlobalConstant.UNIT_TYPE_VILLAGE)
            {
                List<SelectListItem> listDistrict = new List<SelectListItem> { new SelectListItem { Value = "", Text = "--Chọn quận/huyện--" } };
                List<SelectListItem> listCommune = new List<SelectListItem> { new SelectListItem { Value = "", Text = "--Chọn xã/phường--" } };
                Unit userUnit = context.Unit.FirstOrDefault(x => x.Status == GlobalConstant.IS_ACTIVE && x.UnitID == userInfoBo.UnitID);
                listVillage.Add(new SelectListItem { Value = "" + userUnit.UnitID, Text = userUnit.UnitName });
                Unit cUnit = context.Unit.FirstOrDefault(x => x.Status == GlobalConstant.IS_ACTIVE && x.UnitID == userUnit.ParentID);
                listCommune.Add(new SelectListItem { Value = "" + cUnit.UnitID, Text = cUnit.UnitName });
                Unit dUnit = context.Unit.FirstOrDefault(x => x.Status == GlobalConstant.IS_ACTIVE && x.UnitID == cUnit.ParentID);
                listDistrict.Add(new SelectListItem { Value = "" + dUnit.UnitID, Text = dUnit.UnitName });
                ViewData["listDistrict"] = listDistrict;
                ViewData["listCommune"] = listCommune;
                ViewData["listVillage"] = listVillage;
                return;
            }
            if (userInfoBo.UnitLevel == GlobalConstant.UNIT_TYPE_PROVINCE)
            {
                List<SelectListItem> listDistrict = new List<SelectListItem> { new SelectListItem { Value = "", Text = "--Chọn quận/huyện--" } };
                if (listProvince.Any())
                {
                    provinceId = (provinceId == null || provinceId == 0) ? int.Parse(listProvince.First().Value) : provinceId;
                    listDistrict.AddRange(context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE
                       && x.UnitLevel == GlobalConstant.UNIT_TYPE_DISTRICT && x.ParentID == provinceId)
                        .OrderBy(x => x.UnitName)
                        .Select(x => new SelectListItem
                        {
                            Value = "" + x.UnitID,
                            Text = x.UnitName
                        }).ToList());
                }
                ViewData["listDistrict"] = listDistrict;
            }
            else
            {
                Unit userUnit = context.Unit.FirstOrDefault(x => x.Status == GlobalConstant.IS_ACTIVE && x.UnitID == userInfoBo.UnitID);
                if (userUnit == null)
                {
                    throw new BusinessException("Không tồn tài đơn vị đã chọn");
                }
                List<SelectListItem> listDistrict = context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE
                       && x.UnitLevel == GlobalConstant.UNIT_TYPE_DISTRICT &&
                       ((userInfoBo.UnitLevel == GlobalConstant.UNIT_TYPE_DISTRICT && x.UnitID == userInfoBo.UnitID)
                       || (userInfoBo.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE && x.UnitID == userUnit.ParentID)))
                       .OrderBy(x => x.UnitName)
                        .Select(x => new SelectListItem
                        {
                            Value = "" + x.UnitID,
                            Text = x.UnitName
                        }).ToList();

                ViewData["listDistrict"] = listDistrict;
                if (userInfoBo.UnitLevel == GlobalConstant.UNIT_TYPE_DISTRICT)
                {
                    List<SelectListItem> listCommune = new List<SelectListItem> { new SelectListItem { Value = "", Text = "--Chọn xã/phường--" } };
                    if (listDistrict.Any())
                    {
                        districtId = (districtId == null || districtId == 0) ? int.Parse(listDistrict.First().Value) : districtId;
                    }
                }
                else if (userInfoBo.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE)
                {
                    List<SelectListItem> listCommune = context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE
                           && x.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE && x.UnitID == userInfoBo.UnitID)
                           .OrderBy(x => x.UnitName)
                            .Select(x => new SelectListItem
                            {
                                Value = "" + x.UnitID,
                                Text = x.UnitName
                            }).ToList();
                    ViewData["listCommune"] = listCommune;
                }

            }
            if (userInfoBo.UnitLevel == GlobalConstant.UNIT_TYPE_DISTRICT || userInfoBo.UnitLevel == GlobalConstant.UNIT_TYPE_PROVINCE)
            {
                List<SelectListItem> listCommune = new List<SelectListItem> { new SelectListItem { Value = "", Text = "--Chọn xã/phường--" } };
                if (districtId != null && districtId > 0)
                {
                    listCommune.AddRange(context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE
                               && x.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE && x.ParentID == districtId)
                                .OrderBy(x => x.UnitName)
                                .Select(x => new SelectListItem
                                {
                                    Value = "" + x.UnitID,
                                    Text = x.UnitName,
                                }).ToList());
                }
                ViewData["listCommune"] = listCommune;
            }
            if (communeId != null && communeId > 0)
            {
                listVillage.AddRange(context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE
                               && x.UnitLevel == GlobalConstant.UNIT_TYPE_VILLAGE && x.ParentID == communeId)
                                .OrderBy(x => x.UnitName)
                                .Select(x => new SelectListItem
                                {
                                    Value = "" + x.UnitID,
                                    Text = x.UnitName,
                                }).ToList());
            }
            ViewData["listVillage"] = listVillage;


            #endregion
        }

        public int? SetUnit(Entities context, int? provinceId, int? districtId, int? communeId, int? villageId)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            int? unitId = null;
            if (user.UnitLevel == GlobalConstant.UNIT_TYPE_VILLAGE)
            {
                unitId = user.UnitID;
            }
            else
            {
                if (villageId != null && villageId > 0)
                {
                    unitId = villageId;
                }
                else if (communeId != null && communeId > 0)
                {
                    unitId = communeId;
                }
                else if (districtId != null && districtId > 0)
                {
                    unitId = districtId;
                }
                else if (provinceId != null && provinceId > 0)
                {
                    unitId = provinceId;
                }
                // Validate
                //string sUnit = "/" + user.UnitID + "/";
                //if (!context.Unit.Any(x => x.Status == GlobalConstant.IS_ACTIVE
                //                             && x.UnitPath.Contains(sUnit) && x.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE))
                //{
                //    throw new BusinessException("Dữ liệu không hợp lệ");
                //}
            }
            return unitId;
        }

        public void SetAddress(Entities context, int? unitId, out int? provinceId, out int? districtId, out int? communeId, out int? villageId)
        {
            provinceId = null;
            districtId = null;
            communeId = null;
            villageId = null;
            UserInfoBO user = SessionManager.GetUserInfoBO();
            string sUnit = "/" + user.UnitID + "/";

            Unit unit = context.Unit.FirstOrDefault(x => x.Status == GlobalConstant.IS_ACTIVE && x.UnitID == unitId && (user.IsSupperAdmin == GlobalConstant.IS_ACTIVE || x.UnitPath.Contains(sUnit)));
            if (unit == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            if (unit.UnitLevel == GlobalConstant.UNIT_TYPE_VILLAGE)
            {
                villageId = unitId;
                string[] unitPath = unit.UnitPath.Split('/').Where(v => !string.IsNullOrWhiteSpace(v)).Select(x => x.Trim()).ToArray();
                communeId = int.Parse(unitPath[unitPath.Length - 2]);
                districtId = int.Parse(unitPath[unitPath.Length - 3]);
                provinceId = int.Parse(unitPath[unitPath.Length - 4]);
            }
            else if (unit.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE)
            {
                communeId = unitId;
                string[] unitPath = unit.UnitPath.Split('/').Where(v => !string.IsNullOrWhiteSpace(v)).Select(x => x.Trim()).ToArray();
                districtId = int.Parse(unitPath[unitPath.Length - 2]);
                provinceId = int.Parse(unitPath[unitPath.Length - 3]);
            }
            else if (unit.UnitLevel == GlobalConstant.UNIT_TYPE_DISTRICT)
            {
                districtId = unitId;
                provinceId = unit.ParentID;
            }
            else
            {
                provinceId = unit.UnitID;
            }
        }
    }
}