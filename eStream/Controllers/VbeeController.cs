using eStream.Common;
using eStream.Common.CommonClass;
using eStream.Common.CommonObject;
using eStream.Dto;
using eStream.Models;
using Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web.Http;

namespace eStream.Controllers
{
    public class VbeeController : ApiController
    {

        [HttpPost]
        [HttpGet]
        public bool GetProgramFile(int id, VbeeFileDto request)
        {
            if (request != null && request.error_code == 0 && !string.IsNullOrWhiteSpace(request.link))
            {
                DownloadProgram(id, request.link);
            }
            return true;
        }

        [HttpPost]
        [HttpGet]
        public bool GetLiveFile(int id, VbeeFileDto request)
        {
            if (request != null && request.error_code == 0 && !string.IsNullOrWhiteSpace(request.link))
            {
                DownloadLive(id, request.link);
            }
            return true;
        }

        public void DownloadProgram(int programID, string link)
        {
            using (var _context = new Entities())
            {
                using (var webClient = new WebClient())
                {
                    string fileName = DateTimeOffset.Now.ToUnixTimeMilliseconds() + ".mp3";
                    var program = _context.Program.Find(programID);
                    ScheduleDay objScheduleDay = _context.ScheduleDay.Find(program.ScheduleDayID);
                    string filePath = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(GlobalConstant.FILE_PATH), program.UnitID.ToString(),
                        objScheduleDay.ScheduleID.ToString(), program.ExecuteDate.Value.ToString("ddMMyyyyHHmmss"));
                    //Check whether Directory (Folder) exists.
                    if (!Directory.Exists(filePath))
                    {
                        //If Directory (Folder) does not exists. Create it.
                        Directory.CreateDirectory(filePath);
                    }
                    webClient.DownloadFile(link, filePath + "\\" + fileName);
                    program.FileUrl = filePath;
                    program.FileName = fileName;
                    _context.SaveChanges();
                }
            }
        }

        public void DownloadLive(long liveFileID, string link)
        {
            using (var _context = new Entities())
            {
                using (var webClient = new WebClient())
                {
                    string fileName = DateTimeOffset.Now.ToUnixTimeMilliseconds() + ".mp3";
                    var liveFile = _context.LiveFile.Find(liveFileID);
                    var live = _context.Live.Find(liveFile.LiveID);
                    string filePath = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(GlobalConstant.FILE_PATH),
                        live.UnitID.ToString(), "Live", live.LiveID.ToString(), DateTime.Now.ToString("ddMMyyyyHHmmss"));
                    //Check whether Directory (Folder) exists.
                    if (!Directory.Exists(filePath))
                    {
                        //If Directory (Folder) does not exists. Create it.
                        Directory.CreateDirectory(filePath);
                    }
                    webClient.DownloadFile(link, filePath + "\\" + fileName);
                    liveFile.FileUrl = filePath;
                    liveFile.FileName = fileName;
                    _context.SaveChanges();
                }
            }
        }

        public static bool ConvertTextAsync(string text, string voice, double rate, long id, string typeFile = "live")
        {
            using (HttpClient client = new HttpClient())
            {
                string url = GlobalConstant.VBEE_URL_CALLBACK + "/api/Vbee/";
                if (typeFile == "pro")
                {
                    url += "GetProgramFile/" + id;
                }
                else
                {
                    url += "GetLiveFile/" + id;
                }
                var values = new Dictionary<string, string>
                {
                    { "username", GlobalConstant.VBEE_USER_NAME },
                    { "input_text",  text },
                    { "dictionary_id", GlobalConstant.VBEE_DICTIONARY_ID },
                    { "application_id", GlobalConstant.VBEE_APPLICATION_ID },
                    { "voice", voice },
                    { "rate",  rate.ToString() },
                    { "audio_type",  "mp3" },
                    { "type_output", "link" },
                    { "input_type", "text" },
                    { "bit_rate", "128000"},
                    { "type_campaign",  "1" },
                    { "url_callback_api",  url }
                };
                var content = new FormUrlEncodedContent(values);
                var response = client.PostAsync(GlobalConstant.VBEE_CONVERT_API, content).Result;
                if (response.IsSuccessStatusCode)
                {
                    var responseString = response.Content.ReadAsStringAsync().Result;
                    var data = JsonConvert.DeserializeObject<VbeeConvertDto>(responseString);
                    if (data.status_code == 0)
                    {
                        return true;
                    }
                }
                return false;

            }
        }
    }
}
