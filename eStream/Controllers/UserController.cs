﻿using eStream.Common.CommonClass;
using eStream.Common.CommonObject;
using eStream.Helpers.Filter;
using eStream.Models;
using Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eStream.Controllers
{
    public class UserController : BaseController
    {
        private readonly Entities _context = new Entities();
        // GET: User
        [BreadCrumb(ControllerName = "Người dùng", ActionName = "Danh sách", AreaName = "Quản trị hệ thống")]
        public ActionResult Index()
        {
            PrepareDataSearch(_context);
            return View();
        }

        public PartialViewResult SearchUser(int? unitID = null, string searchUserName = null, int page = 1)
        {
            var user = SessionManager.GetUserInfoBO();

            var query = from u in _context.User select u;
            string sUnitID = "/" + user.UnitID + "/";
            string sUnitSearchID = "/" + unitID.GetValueOrDefault() + "/";
            if (unitID == null || unitID == 0)
            {
                query = query.Where(o => (o.UnitID == null && user.UnitLevel == GlobalConstant.UNIT_TYPE_PROVINCE)
                        || _context.Unit.Any(u => u.Status == GlobalConstant.IS_ACTIVE
                        && u.UnitID == o.UnitID && u.UnitPath.Contains(sUnitID)));
            }
            else
            {
                query = query.Where(o => _context.Unit.Any(u => u.Status == GlobalConstant.IS_ACTIVE
                        && u.UnitID == o.UnitID && u.UnitPath.Contains(sUnitID)
                        && u.UnitPath.Contains(sUnitSearchID)));
            }
            IQueryable<UserModel> iQUser = query.Where(x => x.Status != GlobalConstant.IS_DELETE && x.Role != GlobalConstant.USER_ROLE_SUPPER_ADMIN)
                .Select(x => new UserModel
                {
                    UserID = x.UserID,
                    UserName = x.UserName,
                    CreateDate = x.CreateDate,
                    LastLoginDate = x.LastLoginDate,
                    Status = x.Status,
                    Password = x.Password,
                    PhoneNumber = x.PhoneNumber,
                    Avatar = x.UserAvatar,
                    BirthDay = x.BirthDay,
                    Email = x.Email,
                    Ethnic = x.Ethnic,
                    FullName = x.FullName,
                    Position = x.Position,
                    Sex = x.Sex,
                    UnitID = x.UnitID,
                    WorkPlace = x.WorkPlace,
                    UnitName = _context.Unit.Any(o => o.UnitID == x.UnitID) ? _context.Unit.FirstOrDefault(o => o.UnitID == x.UnitID).UnitName : "",
                });

            if (!string.IsNullOrWhiteSpace(searchUserName))
            {
                iQUser = iQUser.Where(o =>
                o.UserName.ToLower().Contains(searchUserName.Trim().ToLower())
                || o.FullName.ToLower().Contains(searchUserName.Trim().ToLower())
                || o.PhoneNumber.Contains(searchUserName.Trim())
                );
            }

            Paginate<UserModel> paginate = new Paginate<UserModel>();
            paginate.PageSize = GlobalConstant.PAGE_SIZE;
            paginate.Count = iQUser.Count();
            List<UserModel> listUser = iQUser.OrderByDescending(x => x.UserID).Skip((page - 1) * GlobalConstant.PAGE_SIZE).Take(GlobalConstant.PAGE_SIZE).ToList();
            // Thiet lap lai mat khau
            listUser.ForEach(x =>
                    {
                        if (x.LastLoginDate.HasValue)
                        {
                            x.Password = "********";
                        }
                        else
                        {
                            x.Password = GlobalCommon.Decrypt(x.Password);
                        }
                    });
            paginate.List = listUser;
            ViewData["listUser"] = paginate;
            return PartialView("_LstUser");
        }

        public PartialViewResult LoadUnit(int? UnitID)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            string sUnit = "/" + user.UnitID + "/";
            List<UnitModel> listUnit = null;
            if (UnitID.GetValueOrDefault() == 0)
            {
                var highest = _context.Unit.FirstOrDefault(x => x.UnitPath.Contains(sUnit) &&
                    x.UnitLevel == GlobalConstant.UNIT_TYPE_PROVINCE && x.Status == GlobalConstant.IS_ACTIVE);
                sUnit = "/" + highest.UnitID + "/";
                listUnit = _context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE && x.UnitPath.Contains(sUnit) && x.UnitLevel <= highest.UnitLevel + 1)
                .Select(x => new UnitModel
                {
                    UnitID = x.UnitID == highest.UnitID ? 0 : x.UnitID,
                    UnitName = x.UnitName,
                    PhoneNumber = x.PhoneNumber,
                    Email = x.Email,
                    Status = x.Status,
                    UnitLevel = x.UnitLevel,
                    ParentID = x.ParentID,
                    UnitPath = x.UnitPath
                }).OrderBy(x => x.UnitPath).ThenBy(x => x.UnitName).ToList();
                ViewData["loadRoot"] = GlobalConstant.IS_ACTIVE;
                ViewData["province"] = highest;
            }
            else
            {
                Unit unit = _context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE && x.UnitLevel == GlobalConstant.UNIT_TYPE_DISTRICT
                && x.UnitID == UnitID && x.UnitPath.Contains(sUnit)).FirstOrDefault();
                if (unit != null)
                {
                    sUnit = "/" + UnitID + "/";
                    listUnit = _context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE && x.UnitPath.Contains(sUnit) && x.UnitLevel != GlobalConstant.UNIT_TYPE_VILLAGE)
                    .Select(x => new UnitModel
                    {
                        UnitID = x.UnitID,
                        UnitName = x.UnitName,
                        PhoneNumber = x.PhoneNumber,
                        Email = x.Email,
                        Status = x.Status,
                        UnitLevel = x.UnitLevel,
                        ParentID = x.ParentID,
                        UnitPath = x.UnitPath
                    }).OrderBy(x => x.UnitPath).ThenBy(x => x.UnitName).ToList();
                }
                else
                {
                    listUnit = new List<UnitModel>();
                }
            }
            return PartialView("_Unit", listUnit);
        }

        public JsonResult GetCommune(int? districtID)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            string sUnit = "/" + user.UnitID + "/";
            List<UnitModel> listUnit = new List<UnitModel> { new UnitModel { UnitID = 0, UnitName = "--Chọn xã/phường--" } };
            if (districtID != null && districtID > 0)
            {
                listUnit.AddRange(_context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE && x.ParentID == districtID
                && x.UnitPath.Contains(sUnit) && x.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE)
                    .Select(x => new UnitModel
                    {
                        UnitID = x.UnitID,
                        UnitName = x.UnitName,
                        PhoneNumber = x.PhoneNumber,
                        Email = x.Email,
                        Status = x.Status,
                        UnitLevel = x.UnitLevel,
                        ParentID = x.ParentID,
                        UnitPath = x.UnitPath
                    }).OrderBy(x => x.UnitPath).ThenBy(x => x.UnitName).ToList());
            }
            return Json(listUnit, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetVillage(int? communeID)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            string sUnit = "/" + user.UnitID + "/";
            List<UnitModel> listUnit = new List<UnitModel> { new UnitModel { UnitID = 0, UnitName = "--Chọn thôn/xóm--" } };
            if (communeID != null && communeID > 0)
            {
                listUnit.AddRange(_context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE && x.ParentID == communeID
                && x.UnitPath.Contains(sUnit) && x.UnitLevel == GlobalConstant.UNIT_TYPE_VILLAGE)
                    .Select(x => new UnitModel
                    {
                        UnitID = x.UnitID,
                        UnitName = x.UnitName,
                        PhoneNumber = x.PhoneNumber,
                        Email = x.Email,
                        Status = x.Status,
                        UnitLevel = x.UnitLevel,
                        ParentID = x.ParentID,
                        UnitPath = x.UnitPath
                    }).OrderBy(x => x.UnitPath).ThenBy(x => x.UnitName).ToList());
            }
            return Json(listUnit, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveUser(UserModel user)
        {
            UserInfoBO UserInfoBO = SessionManager.GetUserInfoBO();

            // Kiem tra trung tai khoan
            var upload = user.UserAvatar;
            if (_context.User.Any(x => x.UserID != user.UserID && x.UserName.ToLower().Equals(user.UserName.Trim().ToLower())))
            {
                throw new BusinessException("Tài khoản đã tồn tại trong hệ thống");
            }

            string msg;
            if (user.UserID > 0)
            {
                User entity = _context.User.Find(user.UserID);
                if (entity == null)
                {
                    throw new BusinessException("Không tồn tại người dùng đã chọn");
                }
                user.UpdateEntity(entity);
                if (!string.IsNullOrEmpty(entity.FullName))
                {
                    var lstName = entity.FullName.Split(' ');
                    entity.LastName = lstName[lstName.Length - 1];
                }
                if (upload != null && upload.ContentLength > 0)
                {
                    var reader = new BinaryReader(upload.InputStream);

                    entity.UserAvatar = reader.ReadBytes(upload.ContentLength);
                    entity.AvatarType = Path.GetExtension(upload.FileName);
                }
                entity.UnitID = this.SetUnit(_context, user.ProvinceID, user.DistrictID, user.CommuneID, user.VillageID);
                msg = "Cập nhật thành công";
            }
            else
            {
                if (!user.IsAutoPass && string.IsNullOrWhiteSpace(user.Password))
                {
                    throw new BusinessException("Bạn chưa nhập mật khẩu");
                }
                User entity = user.ToEntity();
                entity.UnitID = UserInfoBO.UnitID;
                entity.CreateDate = DateTime.Now;
                entity.Status = GlobalConstant.IS_ACTIVE;
                if (!string.IsNullOrEmpty(entity.FullName))
                {
                    var lstName = entity.FullName.Split(' ');
                    entity.LastName = lstName[lstName.Length - 1];
                }
                if (upload != null && upload.ContentLength > 0)
                {
                    var reader = new BinaryReader(upload.InputStream);

                    entity.UserAvatar = reader.ReadBytes(upload.ContentLength);
                    entity.AvatarType = Path.GetExtension(upload.FileName);
                }
                entity.UnitID = this.SetUnit(_context, user.ProvinceID, user.DistrictID, user.CommuneID, user.VillageID);
                _context.User.Add(entity);
                msg = "Tạo mới tài khoản thành công";
            }

            _context.SaveChanges();
            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult PrepareAddOrEditUser(int userId)
        {
            UserInfoBO UserInfoBO = SessionManager.GetUserInfoBO();
            User user = _context.User.SingleOrDefault(x => x.UserID == userId);
            UserModel model = new UserModel { UnitID = UserInfoBO.UnitID };
            if (user != null)
            {
                model.UserID = user.UserID;
                model.UserName = user.UserName;
                model.PhoneNumber = user.PhoneNumber;
                model.FullName = user.FullName;
                model.Role = user.Role;
                model.BirthDay = user.BirthDay;
                model.Email = user.Email;
                model.Ethnic = user.Ethnic;
                model.Position = user.Position;
                model.Sex = user.Sex;
                model.UnitID = user.UnitID;
                model.WorkPlace = user.WorkPlace;
                if (model.UnitID != null && model.UnitID > 0)
                {
                    string sUnitID = "/" + user.UnitID.ToString() + "/";
                    Unit unit = _context.Unit.Find(model.UnitID);
                    if (unit == null)
                    {
                        throw new BusinessException("Không tồn tài đơn vị đã chọn");
                    }
                    if (!unit.UnitPath.Contains(sUnitID))
                    {
                        throw new BusinessException("Bạn không có quyền quản lý người dùng bị của đơn vị khác");
                    }
                    ViewData["unitName"] = unit.UnitName;
                }
            }
            int? provinceID;
            int? districtID;
            int? communeID;
            int? villageID;
            SetAddress(_context, model.UnitID, out provinceID, out districtID, out communeID, out villageID);
            model.ProvinceID = provinceID;
            model.DistrictID = districtID;
            model.CommuneID = communeID;
            model.VillageID = villageID;
            PrepareDataAddOrEdit(_context, provinceID, districtID, communeID);
            return PartialView("_AddOrEditUser", model);
        }

        public JsonResult DeleteUser(int[] userIds)
        {
            List<User> listUser = _context.User.Where(x => userIds.Contains(x.UserID)).ToList();
            listUser.ForEach(x =>
            {
                x.Status = GlobalConstant.IS_DELETE;
            });
            _context.SaveChanges();
            return Json("Xóa tài khoản thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult ActiveUser(int[] userIds)
        {
            List<User> listUser = _context.User.Where(x => userIds.Contains(x.UserID) && x.Status == GlobalConstant.IS_NOT_ACTIVE).ToList();
            listUser.ForEach(x =>
            {
                x.Status = GlobalConstant.IS_ACTIVE;
            });
            _context.SaveChanges();
            return Json("Kích hoạt tài khoản thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult LockUser(int[] userIds)
        {
            List<User> listUser = _context.User.Where(x => userIds.Contains(x.UserID)).ToList();
            listUser.ForEach(x =>
            {
                x.Status = GlobalConstant.IS_NOT_ACTIVE;
            });
            _context.SaveChanges();
            return Json("Khóa tài khoản thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult ActiveUserSingle(int id)
        {
            User user = _context.User.FirstOrDefault(x => id.Equals(x.UserID));
            if (user == null)
            {
                return Json("Tài khoản không tồn tại trong hệ thống", JsonRequestBehavior.AllowGet);
            }
            else
            {
                // Neu khong thi kich hoat TK
                user.Status = GlobalConstant.IS_ACTIVE;
            }
            _context.SaveChanges();
            return Json("Kích hoạt tài khoản thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult LockUserSingle(int id)
        {
            User user = _context.User.FirstOrDefault(x => id.Equals(x.UserID));
            if (user == null)
            {
                return Json("Tài khoản không tồn tại trong hệ thống", JsonRequestBehavior.AllowGet);
            }
            else
            {
                // Neu khong thi kich hoat TK
                user.Status = GlobalConstant.IS_NOT_ACTIVE;
            }
            _context.SaveChanges();
            return Json("Khóa tài khoản thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult ResetPassword(int id)
        {
            string passReset = GlobalCommon.GetRandomString();
            User user = _context.User.FirstOrDefault(x => id.Equals(x.UserID));
            if (user == null)
            {
                return Json("Tài khoản không tồn tại trong hệ thống", JsonRequestBehavior.AllowGet);
            }
            user.LastLoginDate = null;
            user.Password = GlobalCommon.Encrypt(passReset);
            _context.SaveChanges();
            return Json("Cập nhật mật khẩu thành công", JsonRequestBehavior.AllowGet);
        }

        public ActionResult PersonalInfo(int userId)
        {
            var member = (from pd in _context.User
                          where ((pd.Status == GlobalConstant.IS_ACTIVE) || (pd.Status == null)) && pd.UserID == userId
                          orderby pd.UserID
                          select new UserModel()
                          {
                              UserID = pd.UserID,
                              FullName = pd.FullName,
                              LastName = pd.LastName,
                              BirthDay = pd.BirthDay,
                              PhoneNumber = pd.PhoneNumber,
                              Email = pd.Email,
                              Ethnic = pd.Ethnic,
                              Position = pd.Position,
                              Avatar = pd.UserAvatar,
                              AvatarType = pd.AvatarType,
                              WorkPlace = pd.WorkPlace,
                              Sex = pd.Sex
                          }).FirstOrDefault();
            if (member == null)
            {
                return new HttpNotFoundResult();
            }
            ViewData["MemberInfoModel"] = member;
            return View();
        }

        public PartialViewResult GetMemberInfo(int userId)
        {
            if (userId > 0)
            {
                var member = (from pd in _context.User
                              where ((pd.Status == GlobalConstant.IS_ACTIVE) || (pd.Status == null)) && pd.UserID == userId
                              orderby pd.UserID
                              select new UserModel
                              {
                                  UserID = pd.UserID,
                                  FullName = pd.FullName,
                                  LastName = pd.LastName,
                                  BirthDay = pd.BirthDay,
                                  PhoneNumber = pd.PhoneNumber,
                                  Email = pd.Email,
                                  Ethnic = pd.Ethnic,
                                  Position = pd.Position,
                                  Sex = pd.Sex,
                                  Avatar = pd.UserAvatar
                              }).FirstOrDefault();
                ViewData["MemberInfoModel"] = member;
            }

            return PartialView("_MemberInfo");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}