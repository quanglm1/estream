using eStream.Common;
using eStream.Common.CommonClass;
using eStream.Common.CommonObject;
using eStream.Dto;
using eStream.Models;
using Model;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace eStream.Controllers
{
    public class CommandController : ApiController
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        [HttpPost]
        [HttpGet]
        public BaseResponseDto GetCommand(CommandRequestDto request)
        {
            try
            {
                CommandResponseDto res = new CommandResponseDto();
                List<CommandDto> listCommand = new List<CommandDto>();
                List<LogDto> listLog = new List<LogDto>();
                res.listcommand = listCommand;
                res.listlog = listLog;
                string rsaid = request.rsaid;
                if (string.IsNullOrWhiteSpace(rsaid))
                {
                    return res;
                }
                var curRequest = HttpContext.Current.Request;
                var baseUrl = curRequest.Url.Authority;
                var encryptedCode = Crypto.ComputeSha1Hash(baseUrl + rsaid, GlobalConstant.VERY_SECRET + GlobalConstant.DEVICE_SECRET);
                DeviceSms sendSms = null;

                using (Entities context = new Entities())
                {
                    Device device = context.Device.Where(x => x.FullDeviceCode.Equals(encryptedCode)).FirstOrDefault();
                    if (device == null)
                    {
                        var shortCode = GlobalCommon.FormatDeviceCode(rsaid);
                        device = context.Device.Where(x => x.DeviceCode.Equals(shortCode)).FirstOrDefault();
                        if (device == null)
                        {
                            device = new Device
                            {
                                FullDeviceCode = rsaid,
                                DeviceCode = shortCode,
                                CreateDate = DateTime.Now,
                                Status = GlobalConstant.IS_ACTIVE,
                                IsChangeSyncTime = GlobalConstant.IS_ACTIVE,
                                IsChangeVolume = GlobalConstant.IS_ACTIVE,
                                Volume = GlobalConstant.DEFAULT_VOLUME,
                                SyncTime = GlobalConstant.DEFAULT_SYNC_TIME,
                                Bitrate = GlobalConstant.BITRATE_128,
                                NoVov = 0,
                                Version = request.version,
                                SimSer = request.sim_ser,
                                LastSyncDate = DateTime.Now.Date
                            };
                            context.Device.Add(device);
                            context.SaveChanges();
                            StreamingCaching.SetIsOnline(device.DeviceID, true);
                            StreamingCaching.SetVolumeStatus(device.DeviceID, request);
                            StreamingCaching.SetDeviceExt(device.DeviceID, request);
                        }
                    }
                    else
                    {
                        if (device.UnitID == null)
                        {
                            return new BaseResponseDto();
                        }
                        StreamingCaching.SetVolumeStatus(device.DeviceID, request);
                        StreamingCaching.SetDeviceExt(device.DeviceID, request);
                        SystemConfig sc = context.SystemConfig.FirstOrDefault();
                        if (device.LastSyncDate == null)
                        {
                            device.LastSyncDate = DateTime.Now.Date;
                        }
                        if (request.version != null)
                        {
                            device.Version = request.version;
                        }
                        if (request.sim_ser != null)
                        {
                            device.SimSer = request.sim_ser;
                        }
                        if (!string.IsNullOrWhiteSpace(request.sendsms))
                        {
                            sendSms = new DeviceSms { CreatedDate = DateTime.Now, DeviceID = device.DeviceID, SmsContent = request.sendsms };
                            context.DeviceSms.Add(sendSms);
                        }
                        #region Xu ly phan log de khong gui lap lenh
                        if (request.power.GetValueOrDefault() == GlobalConstant.IS_ACTIVE)
                        {
                            StreamingCaching.SetIsFmSwitchOnOrOff(device.DeviceID, GlobalConstant.IS_ACTIVE);
                        }
                        else if (request.power.HasValue && request.power.GetValueOrDefault() == GlobalConstant.IS_NOT_ACTIVE)
                        {
                            StreamingCaching.SetIsFmSwitchOnOrOff(device.DeviceID, GlobalConstant.IS_NOT_ACTIVE);
                        }
                        else
                        {
                            StreamingCaching.SetIsFmSwitchOnOrOff(device.DeviceID, GlobalConstant.N_A);
                        }
                        // Remove expired commands
                        DeviceCommandCaching.RemoveExpiredCommands(device, request.listlog);
                        if (request.listlog != null && request.listlog.Any())
                        {
                            foreach (DeviceLogDto log in request.listlog)
                            {
                                DateTime logDate;
                                if (DateTime.TryParseExact(log.datetime,
                                "yyyy-M-d HH:mm:ss",
                                CultureInfo.InvariantCulture,
                                DateTimeStyles.None, out logDate))
                                {
                                    DeviceLog deviceLog = new DeviceLog
                                    {
                                        DeviceID = device.DeviceID,
                                        LogCode = log.logcode,
                                        LogID = log.logid,
                                        LogDate = logDate,
                                        Status = log.status,
                                        DeviceLogDate = DateTime.Now
                                    };
                                    context.DeviceLog.Add(deviceLog);
                                    // Kiem tra lenh de cap nhat trang thai lenh cu da thuc hien neu trang thai la true
                                    if (log.status)
                                    {
                                        short logCode = log.logcode;
                                        switch (logCode)
                                        {
                                            case GlobalConstant.DEVICE_COMMAND_CHANGE_CONN:
                                                device.ChangeConn = null;
                                                break;
                                            case GlobalConstant.DEVICE_COMMAND_SEND_SMS:
                                                device.ProSmsID = null;
                                                break;
                                            case GlobalConstant.DEVICE_COMMAND_UPDATE_OS:
                                                device.IsUpdateOS = null;
                                                break;
                                            case GlobalConstant.DEVICE_COMMAND_CHANGE_VOLUME:
                                                device.IsChangeVolume = null;
                                                break;
                                            case GlobalConstant.DEVICE_COMMAND_CHECK_INTERVAL:
                                                device.IsChangeSyncTime = null;
                                                break;
                                            case GlobalConstant.DEVICE_COMMAND_CHANGE_DOMAIN:
                                                device.IsChangeDomain = null;
                                                break;
                                            case GlobalConstant.DEVICE_COMMAND_RESTART:
                                                device.IsRestart = null;
                                                StreamingCaching.RemoveStreamingInfo(device.DeviceID);
                                                break;
                                            case GlobalConstant.DEVICE_COMMAND_SHUTDOWN:
                                                device.IsShutdown = null;
                                                StreamingCaching.RemoveStreamingInfo(device.DeviceID);
                                                break;
                                            case GlobalConstant.DEVICE_COMMAND_START_STREAM:
                                                device.IsRequestStart = null;
                                                device.IsStoping = null;
                                                break;
                                            case GlobalConstant.DEVICE_COMMAND_FM_START:
                                                device.IsRequestStart = null;
                                                device.IsStoping = null;
                                                device.IsFmStart = null;
                                                break;
                                            case GlobalConstant.DEVICE_COMMAND_STOP_STREAM:
                                                device.IsRequestStop = null;
                                                StreamingCaching.SetIsPlaying(device.DeviceID, false);
                                                StreamingCaching.SetIsVOV(device.DeviceID, false);
                                                StreamingCaching.SetIsStop(device.DeviceID, true);
                                                // Insert thoi gian
                                                StreamingTimer.SetTimer(device, context, true);
                                                StreamingFmTimer.SetTimer(device, context, true);
                                                StreamingCaching.DicPlaying[device.DeviceID] = null;

                                                // Danh dau la co lenh stop
                                                device.IsStoping = GlobalConstant.IS_ACTIVE;
                                                break;
                                            case GlobalConstant.DEVICE_COMMAND_VERSION:
                                                device.IsSendVersion = null;
                                                break;
                                            case GlobalConstant.DEVICE_COMMAND_TIMER:
                                                device.IsChangeTimer = null;
                                                break;
                                            case GlobalConstant.DEVICE_COMMAND_TURN_ON_FM:
                                                device.IsFmSwitchOn = null;
                                                break;
                                            case GlobalConstant.DEVICE_COMMAND_TURN_OFF_FM:
                                                device.IsFmSwitchOff = null;
                                                break;
                                        }
                                    }
                                    listLog.Add(new LogDto { logid = log.logid });
                                }
                            }
                        }
                        #endregion

                        #region Cac lenh co ban
                        FmStartDto fmStart = null;
                        if (!string.IsNullOrWhiteSpace(request.version))
                        {
                            device.Version = request.version.Trim();
                        }
                        if (request.streaming.GetValueOrDefault())
                        {
                            StreamingCaching.SetIsPlaying(device.DeviceID, true, true);
                            StreamingCaching.SetIsStop(device.DeviceID, false);
                            // Set timer
                            StreamingTimer.SetTimer(device, context);
                        }
                        if (request.signal != null)
                        {
                            StreamingCaching.SetFmStatus(device.DeviceID, request.signal);
                            if (request.signal.Value == GlobalConstant.IS_ACTIVE)
                            {
                                StreamingFmTimer.SetTimer(device, context);
                            }
                        }
                        if (device.IsSendVersion.GetValueOrDefault() == GlobalConstant.IS_ACTIVE)
                        {
                            VersionDto version = new VersionDto();
                            version.version = true;
                            listCommand.Add(version);
                        }
                        if (device.IsUpdateOS.GetValueOrDefault() == GlobalConstant.IS_ACTIVE)
                        {
                            if (sc != null && !string.IsNullOrWhiteSpace(sc.OsUpdatePath))
                            {
                                UpdateOSDto updateOS = new UpdateOSDto();
                                updateOS.updateos = sc.OsUpdatePath;
                                listCommand.Add(updateOS);
                            }
                        }
                        if (device.IsChangeVolume.GetValueOrDefault() == GlobalConstant.IS_ACTIVE)
                        {
                            VolumeDto volume = new VolumeDto();
                            volume.changevolume = device.Volume.GetValueOrDefault();
                            listCommand.Add(volume);
                        }
                        if (device.IsChangeSyncTime.GetValueOrDefault() == GlobalConstant.IS_ACTIVE)
                        {
                            CheckIntervalDto checkInterval = new CheckIntervalDto();
                            checkInterval.checkinterval = device.SyncTime.GetValueOrDefault();
                            listCommand.Add(checkInterval);
                        }
                        //if (device.IsFmStart.GetValueOrDefault() == GlobalConstant.IS_ACTIVE
                        //    && device.IsSupportFm.GetValueOrDefault() == GlobalConstant.IS_ACTIVE)
                        //{
                        //    fmStart = new FmStartDto();
                        //    fmStart.startfm = device.FmFreq.GetValueOrDefault();
                        //}
                        if (request.getconfig.HasValue && request.getconfig.Value)
                        {
                            var configVolume = new ConfigVolumeDto();
                            configVolume.volume = device.Volume.GetValueOrDefault();
                            listCommand.Add(configVolume);
                            var configInterval = new ConfigIntervalDto();
                            configInterval.interval = device.SyncTime.GetValueOrDefault();
                            listCommand.Add(configInterval);
                        }
                        if (device.IsChangeDomain.GetValueOrDefault() == GlobalConstant.IS_ACTIVE)
                        {
                            if (sc != null && !string.IsNullOrWhiteSpace(sc.Domain))
                            {
                                DomainDto domain = new DomainDto();
                                domain.changedomain = sc.Domain + "/api/Command/GetCommand";
                                listCommand.Add(domain);
                            }
                        }
                        if (device.IsFmSwitchOn.GetValueOrDefault() == GlobalConstant.IS_ACTIVE)
                        {
                            SwitchDto stop = new SwitchDto();
                            stop.@switch = true;
                            listCommand.Add(stop);
                        }
                        if (device.IsFmSwitchOff.GetValueOrDefault() == GlobalConstant.IS_ACTIVE)
                        {
                            SwitchDto stop = new SwitchDto();
                            stop.@switch = false;
                            listCommand.Add(stop);
                        }
                        if (device.ProSmsID.GetValueOrDefault() > 0)
                        {
                            var proSms = context.ProMessage.Find(device.ProSmsID);
                            if (proSms != null)
                            {
                                SmsSendDto sms = new SmsSendDto();
                                sms.sendsms = proSms.MessageType + ": " + proSms.MessagePhone;
                                listCommand.Add(sms);
                            }
                            else
                            {
                                device.ProSmsID = null;
                            }
                        }
                        if (device.ChangeConn.GetValueOrDefault() == GlobalConstant.IS_ACTIVE)
                        {
                            if (GlobalConstant.CONNECTION_TYPES.Contains(device.ConnType))
                            {
                                ChangeConnDto cnn = new ChangeConnDto();
                                cnn.changeconn = device.ConnType;
                                listCommand.Add(cnn);
                            }
                        }
                        #endregion

                        #region Streaming
                        OrderConfig config = context.OrderConfig.First();
                        StreamingCaching.DicPlaying.TryGetValue(device.DeviceID, out PlayingDto nowPlaying);
                        long nowLiveID = nowPlaying != null ? nowPlaying.NowLiveID : 0;
                        StreamingBO oldStreaming = StreamingCaching.GetStreamingInfo(device.DeviceID);
                        bool isStop = oldStreaming != null && oldStreaming.IsStop;
                        bool prePlaying = oldStreaming != null && oldStreaming.IsPlaying;
                        var unit = context.Unit.Where(x => x.UnitID == device.UnitID).Select(x => new UnitModel
                        {
                            UnitID = x.UnitID,
                            UnitPath = x.UnitPath
                        }).FirstOrDefault();
                        int minCountFile = (sc == null || sc.StreamingCountFile.GetValueOrDefault() == 0 ? GlobalConstant.DEFAULT_STREAMING_COUNT_FILE : sc.StreamingCountFile.GetValueOrDefault());
                        DateTime currentDt = DateTime.Now;
                        long hourOfDay = currentDt.Hour;
                        long minuteOfDay = currentDt.Minute;
                        long minutes = 60 * hourOfDay + minuteOfDay;
                        long seconds = 60 * minutes + currentDt.Second;
                        var lives = (from l in context.Live
                                     join u in context.Unit on l.UnitID equals u.UnitID
                                     where l.Status >= GlobalConstant.LIVE_PLAYING
                                     && unit.UnitPath.Contains(u.UnitPath)
                                     && (l.LiveSource != GlobalConstant.LIVE_SOURCE_FILE
                                     || (l.TotalDuration != null && (l.PlayTime == null || DbFunctions.DiffSeconds(l.PlayTime, currentDt) < l.TotalDuration * (1 + (l.LoopCount == null ? 0 : l.LoopCount)))))
                                     orderby (u.UnitLevel == GlobalConstant.UNIT_TYPE_VILLAGE ? config.VillageOrder
                                             : u.UnitLevel == GlobalConstant.UNIT_TYPE_COMMUNE ? config.CommuneOrder
                                             : u.UnitLevel == GlobalConstant.UNIT_TYPE_DISTRICT ? config.DistrictOrder
                                             : u.UnitLevel == GlobalConstant.UNIT_TYPE_PROVINCE ? config.ProvinceOrder : 0),
                                             l.LiveID
                                     select l).ToList();
                        Live live = null;
                        var waitForStreamingFile = false;
                        foreach (var l in lives)
                        {
                            if (l.LiveSource.GetValueOrDefault() == GlobalConstant.LIVE_SOURCE_STREAMING)
                            {
                                var count = LiveController.GetStreamingCount(context, l);
                                if (count >= minCountFile)
                                {
                                    live = l;
                                    break;
                                }
                                else
                                {
                                    waitForStreamingFile = true;
                                }
                            }
                            else
                            {
                                live = l;
                                break;
                            }
                        }
                        bool living = false;
                        var isStopLiving = false;
                        if (live != null)
                        {
                            if (live.LiveType == GlobalConstant.LIVE_TYPE_CHOICE)
                            {
                                if (context.LiveDevice.Any(x => x.LiveID == live.LiveID && x.DeviceID == device.DeviceID))
                                {
                                    living = true;
                                }
                            }
                            else
                            {
                                living = true;
                            }
                            if (live.LiveID != nowLiveID && prePlaying)
                            {
                                isStopLiving = true;
                            }
                            else
                            {
                                live.LiveID = live.LiveID;
                            }
                        }
                        else if (nowLiveID > 0)
                        {
                            isStopLiving = true;
                        }

                        StartStreamDto start = null;
                        int unitID = device.UnitID.GetValueOrDefault();
                        if (device.IsRequestStart.GetValueOrDefault() == GlobalConstant.IS_ACTIVE && device.IsRequestForceStop.GetValueOrDefault() == 0)
                        {
                            if (sc != null)
                            {
                                if (living && !isStopLiving)
                                {
                                    if (live.LiveSource.GetValueOrDefault() == GlobalConstant.LIVE_SOURCE_VOV)
                                    {
                                        if (live.ChannelID != null)
                                        {
                                            var channel = context.ForwardChanel.Find(live.ChannelID);
                                            if (channel != null)
                                            {
                                                if (channel.Fm == GlobalConstant.IS_ACTIVE)
                                                {
                                                    if (device.IsSupportFm.GetValueOrDefault() == GlobalConstant.IS_ACTIVE)
                                                    {
                                                        fmStart = new FmStartDto
                                                        {
                                                            startfm = channel.FmFreq.GetValueOrDefault()
                                                        };
                                                    }
                                                }
                                                else
                                                {
                                                    start = new StartStreamDto();
                                                    start.startstream = Crypto.DecryptString(channel.LinkUrl);
                                                }
                                            }
                                        }
                                        else if (!string.IsNullOrWhiteSpace(live.Url))
                                        {
                                            start = new StartStreamDto
                                            {
                                                startstream = live.Url
                                            };
                                        }
                                    }
                                    else
                                    {
                                        start = new StartStreamDto
                                        {
                                            startstream = sc.Domain + "/Live/Live?unitId=" + unitID + "&deviceID=" + device.DeviceID + "&bitrate=" + (device.Bitrate != null ? device.Bitrate.Value : GlobalConstant.DEFAULT_BIT_RATE)
                                        };
                                    }
                                }
                                else
                                {
                                    start = new StartStreamDto
                                    {
                                        startstream = sc.Domain + "/Control/Live?unitId=" + unitID + "&deviceID=" + device.DeviceID + "&bitrate=" + (device.Bitrate != null ? device.Bitrate.Value : GlobalConstant.DEFAULT_BIT_RATE)
                                    };
                                }
                            }
                        }
                        if (device.IsRequestStop.GetValueOrDefault() == GlobalConstant.IS_ACTIVE || isStopLiving)
                        {
                            StopStreamDto stop = new StopStreamDto();
                            stop.stopstream = true;
                            listCommand.Add(stop);
                            StreamingCaching.DicPlaying[device.DeviceID] = null;
                        }

                        if (isStopLiving && nowLiveID > 0)
                        {
                            var oldLive = context.Live.Find(nowLiveID);
                            if (oldLive.LiveSource == GlobalConstant.LIVE_SOURCE_FILE && oldLive.Status == GlobalConstant.LIVE_PLAYING)
                            {
                                LiveController.StopStreamLive(context, oldLive, device.DeviceID);
                            }
                        }
                        if (living)
                        {
                            ForwardChanel channel = null;
                            if (live.LiveSource.GetValueOrDefault() == GlobalConstant.LIVE_SOURCE_VOV)
                            {
                                if (live.ChannelID != null)
                                {
                                    channel = context.ForwardChanel.Find(live.ChannelID);
                                }
                            }
                            #region Check gui stop
                            if (!isStopLiving && device.IsRequestStop.GetValueOrDefault() == 0 && device.IsRequestForceStop.GetValueOrDefault() == 0)
                            {
                                if (nowPlaying == null)
                                {
                                    nowPlaying = new PlayingDto();
                                    start = new StartStreamDto();
                                    if (live.LiveSource.GetValueOrDefault() == GlobalConstant.LIVE_SOURCE_VOV)
                                    {
                                        if (channel != null)
                                        {
                                            if (channel.Fm == GlobalConstant.IS_ACTIVE)
                                            {
                                                if (device.IsSupportFm.GetValueOrDefault() == GlobalConstant.IS_ACTIVE)
                                                {
                                                    fmStart = new FmStartDto();
                                                    fmStart.startfm = channel.FmFreq.GetValueOrDefault();
                                                }
                                                nowPlaying.PlayingSource = "FM " + Convert.ToInt32(channel.FmFreq.GetValueOrDefault()) + "MHz";
                                            }
                                            else
                                            {
                                                string vovLink = Crypto.DecryptString(channel.LinkUrl);
                                                start.startstream = vovLink;
                                                nowPlaying.PlayingSource = channel.ChannelCode;
                                            }
                                        }
                                    }
                                    else if (sc != null)
                                    {
                                        start.startstream = sc.Domain + "/Live/Live?unitId=" + unitID + "&deviceID=" + device.DeviceID + "&bitrate=" + (device.Bitrate != null ? device.Bitrate.Value : GlobalConstant.DEFAULT_BIT_RATE);
                                        if (live.LiveSource == GlobalConstant.LIVE_SOURCE_STREAMING)
                                        {
                                            nowPlaying.PlayingSource = GlobalConstant.PLAYING_SOURCE_MICRO;
                                        }
                                        else
                                        {
                                            nowPlaying.PlayingSource = GlobalConstant.PLAYING_SOURCE_FILE;
                                        }
                                    }
                                }
                                nowPlaying.PlayingType = GlobalConstant.PLAYING_TYPE_LIVE;
                                nowPlaying.LiveSource = live.LiveSource.GetValueOrDefault();
                                nowPlaying.NowLiveID = live.LiveID;
                                StreamingCaching.DicPlaying[device.DeviceID] = nowPlaying;
                            }
                            #endregion
                            // Neu co phat truc tiep xu ly rieng
                            // Xu ly doi voi truong hop thiet bi dang phat
                            // Check thoi gian
                            if (oldStreaming == null || oldStreaming.CountPlayingTime.GetValueOrDefault() == 0)
                            {
                                double totalTime = live.TotalDuration.HasValue ? live.TotalDuration.Value : 1;
                                StreamingCaching.SetPlayingTime(device.DeviceID, totalTime);
                                oldStreaming = StreamingCaching.GetStreamingInfo(device.DeviceID);
                            }
                            if (oldStreaming != null && !isStopLiving && !isStop && device.IsRequestForceStop.GetValueOrDefault() == 0)
                            {
                                bool isPlaying = StreamingCaching.UpdateStreaming(device.DeviceID, request.streaming.GetValueOrDefault(), GlobalConstant.COMMAND_PLAYING_TIME);
                                if (!isPlaying)
                                {
                                    if (live.LiveSource.GetValueOrDefault() == GlobalConstant.LIVE_SOURCE_VOV)
                                    {
                                        if (live.ChannelID != null)
                                        {
                                            if (channel != null)
                                            {
                                                if (channel.Fm == GlobalConstant.IS_ACTIVE)
                                                {
                                                    if (device.IsFmStart.GetValueOrDefault() == 0)
                                                    {
                                                        if (request.listlog != null && request.listlog.Any(x => x.logcode == GlobalConstant.DEVICE_COMMAND_FM_FREQ))
                                                        {
                                                            fmStart = new FmStartDto();
                                                            fmStart.startfm = channel.FmFreq.GetValueOrDefault();
                                                        }
                                                        else
                                                        {
                                                            FmFreqDto fmFreq = new FmFreqDto();
                                                            fmFreq.freqfm = channel.FmFreq.GetValueOrDefault();
                                                            listCommand.Add(fmFreq);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    start = new StartStreamDto();
                                                    start.startstream = Crypto.DecryptString(channel.LinkUrl);
                                                }
                                            }
                                        }
                                    }
                                    else if (sc != null)
                                    {
                                        start = new StartStreamDto();
                                        start.startstream = sc.Domain + "/Live/Live?unitId=" + unitID + "&deviceID=" + device.DeviceID + "&bitrate=" + (device.Bitrate != null ? device.Bitrate.Value : GlobalConstant.DEFAULT_BIT_RATE);
                                    }
                                    StreamingCaching.SetIsPlaying(device.DeviceID, false);
                                    // Play thi add thoi gian playing
                                    double? totalTime = live.TotalDuration.HasValue ? live.TotalDuration.Value : 1;
                                    StreamingCaching.SetPlayingTime(device.DeviceID, totalTime);
                                }
                            }
                        }
                        else if (!isStopLiving && !waitForStreamingFile)
                        {
                            if (device.IsRequestStop.GetValueOrDefault() == 0 && device.IsRequestForceStop.GetValueOrDefault() == 0)
                            {
                                if (nowPlaying == null)
                                {
                                    start = new StartStreamDto();
                                    start.startstream = sc.Domain + "/Control/Live?unitId=" + unitID + "&deviceID=" + device.DeviceID + "&bitrate=" + (device.Bitrate != null ? device.Bitrate.Value : GlobalConstant.DEFAULT_BIT_RATE);
                                    nowPlaying = new PlayingDto();
                                    nowPlaying.PlayingSource = string.Empty;
                                    nowPlaying.LiveSource = 0;
                                }
                                nowPlaying.PlayingType = GlobalConstant.PLAYING_TYPE_SCHEDULE;
                                nowPlaying.PlayingSource = GlobalConstant.PLAYING_SOURCE_FILE;
                                nowPlaying.LiveSource = 0;
                                StreamingCaching.DicPlaying[device.DeviceID] = nowPlaying;
                            }
                            int waitingAutioDur = (sc != null && sc.IsWaitingOn.GetValueOrDefault() == GlobalConstant.IS_ACTIVE) ? GlobalConstant.WAITING_AUDIO_DURATION : 1;
                            ScheduleBO currentSchedule = ScheduleCaching.GetListScheduleNow(unitID, waitingAutioDur).FirstOrDefault();

                            // Xu ly rieng cho truong hop co lenh start ma truoc do la stop
                            if (isStop && start != null && oldStreaming != null
                                && currentSchedule != null && !currentSchedule.Ishidden)
                            {
                                double scheduleSeconds = 60 * currentSchedule.StartMinus.GetValueOrDefault() + oldStreaming.CountPlayingTime.GetValueOrDefault();
                                if (scheduleSeconds < seconds)
                                {
                                    string vovLink = GetVOVUrl(sc.Domain, device, currentSchedule.ScheduleID, nowPlaying, context);
                                    SetForward(vovLink, device, out start, out fmStart);
                                }
                            }

                            // Stop stream
                            if (currentSchedule == null)
                            {
                                int shutdowTimeout = ScheduleCaching.GetEndSchedule(unitID, out ScheduleBO nearestSchedule);
                                if (shutdowTimeout > 0 && shutdowTimeout <= sc.ShutdownTimeout.GetValueOrDefault())
                                {
                                    if (nearestSchedule == null || nearestSchedule.IsStopStream.GetValueOrDefault() == 0)
                                    {
                                        ShutdownDto shutdown = new ShutdownDto();
                                        shutdown.shutdown = true;
                                        listCommand.Add(shutdown);
                                        if (device.IsShutdown != null)
                                        {
                                            device.IsShutdown = null;
                                        }
                                    }
                                    else if (device.IsStoping.GetValueOrDefault() == 0)
                                    {
                                        StopStreamDto stopStream = new StopStreamDto();
                                        stopStream.stopstream = true;
                                        listCommand.Add(stopStream);
                                    }
                                }
                                StreamingTimer.SetTimer(device, context, true);
                                StreamingFmTimer.SetTimer(device, context, true);
                                StreamingCaching.RemoveStreamingInfo(device.DeviceID);
                                StreamingCaching.DicPlaying[device.DeviceID] = null;
                                start = null;
                            }
                            else if (!isStop && !currentSchedule.Ishidden)
                            {
                                // Xu ly doi voi truong hop thiet bi dang phat
                                // Check thoi gian
                                bool isPlaying = StreamingCaching.UpdateStreaming(device.DeviceID, request.streaming.GetValueOrDefault(), GlobalConstant.COMMAND_PLAYING_TIME);
                                if (oldStreaming == null || oldStreaming.CountPlayingTime.GetValueOrDefault() == 0)
                                {
                                    this.UpdatePlayingTime(device, currentSchedule.ScheduleID, context);
                                    oldStreaming = StreamingCaching.GetStreamingInfo(device.DeviceID);
                                }
                                if (oldStreaming != null)
                                {
                                    double scheduleSeconds = 60 * currentSchedule.StartMinus.GetValueOrDefault() + oldStreaming.CountPlayingTime.GetValueOrDefault();
                                    string vovLink = string.Empty;
                                    if (scheduleSeconds < seconds)
                                    {
                                        if (nowPlaying == null)
                                        {
                                            nowPlaying = new PlayingDto();
                                        }
                                        nowPlaying.PlayingType = GlobalConstant.PLAYING_TYPE_SCHEDULE;
                                        nowPlaying.LiveSource = 0;
                                        vovLink = GetVOVUrl(sc.Domain, device, currentSchedule.ScheduleID, nowPlaying, context);
                                        StreamingCaching.DicPlaying[device.DeviceID] = nowPlaying;
                                    }
                                    if (!oldStreaming.IsVOV && scheduleSeconds < seconds)
                                    {
                                        if (string.IsNullOrEmpty(vovLink))
                                        {
                                            vovLink = GetVOVUrl(sc.Domain, device, currentSchedule.ScheduleID, nowPlaying, context);
                                        }
                                        SetForward(vovLink, device, out start, out fmStart);
                                    }
                                    else if (!isPlaying)
                                    {
                                        if (oldStreaming.IsVOV)
                                        {
                                            if (string.IsNullOrEmpty(vovLink))
                                            {
                                                vovLink = GetVOVUrl(sc.Domain, device, currentSchedule.ScheduleID, nowPlaying, context);
                                            }
                                            SetForward(vovLink, device, out start, out fmStart);
                                        }
                                        else
                                        {
                                            // Bao de play lai
                                            if (sc != null)
                                            {
                                                start = new StartStreamDto();
                                                start.startstream = sc.Domain + "/Control/Live?unitId=" + unitID + "&deviceID=" + device.DeviceID + "&bitrate=" + (device.Bitrate != null ? device.Bitrate.Value : GlobalConstant.DEFAULT_BIT_RATE);
                                                StreamingCaching.SetIsPlaying(device.DeviceID, false);
                                                // Play thi add thoi gian playing
                                                if (currentSchedule != null)
                                                {
                                                    this.UpdatePlayingTime(device, currentSchedule.ScheduleID, context);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (fmStart != null && device.IsRequestStop.GetValueOrDefault() == 0 && !isStopLiving)
                        {
                            // Neu du dieu kien gui lenh start va khong co log gui stop truoc do thi gui lenh phat FM
                            listCommand.Add(fmStart);
                        }
                        if (start != null && !string.IsNullOrWhiteSpace(start.startstream)
                            && fmStart == null && device.IsRequestStop.GetValueOrDefault() == 0 && !isStopLiving)
                        {
                            // Neu du dieu kien gui lenh start va khong co log gui stop truoc do thi gui lenh phat
                            listCommand.Add(start);
                        }
                        if (device.IsRestart.GetValueOrDefault() == GlobalConstant.IS_ACTIVE)
                        {
                            RestartDto restart = new RestartDto();
                            restart.restart = true;
                            listCommand.Add(restart);
                            device.IsRequestStart = null;
                        }
                        if (device.IsShutdown.GetValueOrDefault() == GlobalConstant.IS_ACTIVE)
                        {
                            ShutdownDto shutdown = new ShutdownDto();
                            shutdown.shutdown = true;
                            listCommand.Add(shutdown);
                            device.IsShutdown = null;
                            StreamingCaching.RemoveStreamingInfo(device.DeviceID);
                        }
                        StreamingCaching.SetIsOnline(device.DeviceID, true);
                        #endregion
                        context.SaveChanges();
                        if (sendSms != null)
                        {
                            StreamingCaching.SetLastSms(device.DeviceID, sendSms.ID);
                        }
                    }
                }
                if (res.HasValue())
                {
                    return res;
                }
                else
                {
                    return new BaseResponseDto();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                Logger.Error(ex.StackTrace);
                throw ex;
            }
        }

        private void SetForward(string vovLink, Device device, out StartStreamDto start, out FmStartDto fmStart)
        {
            start = null;
            fmStart = null;
            if (!string.IsNullOrWhiteSpace(vovLink))
            {
                var preFm = GlobalConstant.FM_CODE_FORWARD + "|";
                if (vovLink.StartsWith(preFm))
                {
                    if (double.TryParse(vovLink.Split('|')[1], out double fmFreq))
                    {
                        fmStart = new FmStartDto();
                        fmStart.startfm = fmFreq;
                    }
                }
                else
                {
                    start = new StartStreamDto();
                    start.startstream = vovLink;
                    StreamingCaching.SetIsVOV(device.DeviceID, true);
                }
            }
        }

        private string GetVOVUrl(string domain, Device device, int ScheduleID, PlayingDto nowPlaying, Entities context)
        {
            if (device.NoVov.GetValueOrDefault() == GlobalConstant.IS_ACTIVE)
            {
                if (nowPlaying != null)
                {
                    nowPlaying.PlayingSource = string.Empty;
                }
                return string.Empty;
            }
            DateTime currentDt = DateTime.Now;
            int dayOfWeek = currentDt.DayOfWeek == DayOfWeek.Sunday ? dayOfWeek = 7 : (int)currentDt.DayOfWeek;
            Unit unit = context.Unit.Find(device.UnitID);
            ScheduleDay scheduleDay = (from a in context.ScheduleDay
                                       where a.ScheduleID == ScheduleID
                                       && a.DayOfWeek == dayOfWeek
                                       select a).FirstOrDefault();

            // Lenh thong tin VOV
            var listChannel = (from a in context.ProgramChanel
                               join b in context.Unit on a.UnitID equals b.UnitID
                               where a.ScheduleDayID == scheduleDay.ScheduleDayID
                               && unit.UnitPath.Contains(b.UnitPath)
                               && DbFunctions.TruncateTime(a.DateExecute) == DbFunctions.TruncateTime(currentDt)
                               select new { a.ChanelID, a.Url }).ToList();
            if (listChannel.Any())
            {
                var channel = listChannel[0];
                if (!string.IsNullOrWhiteSpace(channel.Url))
                {
                    return channel.Url.Trim();
                }
                if (channel.ChanelID > 0)
                {
                    ForwardChanel fc = context.ForwardChanel.Find(channel.ChanelID);
                    if (fc != null && !string.IsNullOrWhiteSpace(fc.ChannelCode))
                    {
                        if (fc.Fm == GlobalConstant.IS_ACTIVE)
                        {
                            if (device.IsSupportFm.GetValueOrDefault() == 0)
                            {
                                return string.Empty;
                            }
                            if (nowPlaying != null)
                            {
                                nowPlaying.PlayingSource = "FM " + Convert.ToInt32(fc.FmFreq.GetValueOrDefault()) + "MHz";
                            }
                            return GlobalConstant.FM_CODE_FORWARD + "|" + fc.FmFreq.GetValueOrDefault();
                        }
                        else
                        {
                            if (nowPlaying != null)
                            {
                                nowPlaying.PlayingSource = fc.ChannelCode;
                            }
                            return Crypto.DecryptString(fc.LinkUrl);
                        }
                    }
                }
            }
            else
            {
                ForwardChanel fc = context.ForwardChanel.Where(x => x.IsDefault == GlobalConstant.IS_ACTIVE).FirstOrDefault();
                if (fc != null && !string.IsNullOrWhiteSpace(fc.ChannelCode))
                {
                    if (nowPlaying != null)
                    {
                        nowPlaying.PlayingSource = fc.ChannelCode;
                    }
                    return Crypto.DecryptString(fc.LinkUrl);
                }
            }
            return string.Empty;
        }

        [HttpPost]
        [HttpGet]
        public SyncTimeDto SyncTime(CommandRequestDto request)
        {
            SyncTimeDto syncTime = new SyncTimeDto();
            string rsaid = request.rsaid;
            if (string.IsNullOrWhiteSpace(rsaid))
            {
                return syncTime;
            }

            using (Entities context = new Entities())
            {
                Device device = context.Device.Where(x => x.FullDeviceCode.Equals(rsaid)).FirstOrDefault();
                if (device == null)
                {
                    return syncTime;
                }
                else
                {
                    syncTime.synctime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                }
            }
            return syncTime;
        }

        private void UpdatePlayingTime(Device device, int ScheduleID, Entities Context)
        {
            DateTime currentDt = DateTime.Now;
            int dayOfWeek = currentDt.DayOfWeek == DayOfWeek.Sunday ? dayOfWeek = 7 : (int)currentDt.DayOfWeek;
            ScheduleDay scheduleDay = (from a in Context.ScheduleDay
                                       where a.ScheduleID == ScheduleID
                                       && a.DayOfWeek == dayOfWeek
                                       select a).FirstOrDefault();
            if (scheduleDay != null)
            {
                Unit unit = Context.Unit.Find(device.UnitID);
                double? totalTime = (from p in Context.Program
                                     join u in Context.Unit on p.UnitID equals u.UnitID
                                     where p.ScheduleDayID == scheduleDay.ScheduleDayID
                                     && p.Status == GlobalConstant.IS_APPROVED
                                     && DbFunctions.TruncateTime(p.ExecuteDate) == DbFunctions.TruncateTime(currentDt)
                                     && unit.UnitPath.Contains(u.UnitPath)
                                     select p.Duration).Sum();
                var loop = Context.ProgramLoop.Where(x => x.ScheduleDayID == scheduleDay.ScheduleDayID
                                        && x.Status == GlobalConstant.IS_ACTIVE
                                        && Context.Unit.Any(u => u.UnitID == x.UnitID && unit.UnitPath.Contains(u.UnitPath))
                                        && DbFunctions.TruncateTime(x.DateExecute) == DbFunctions.TruncateTime(currentDt)).FirstOrDefault();
                if (loop != null && loop.LoopCount.GetValueOrDefault() > 0)
                {
                    totalTime = totalTime * loop.LoopCount.GetValueOrDefault();
                }
                StreamingCaching.SetPlayingTime(device.DeviceID, totalTime);
            }
        }

        [HttpGet]
        public string Encode(string deviceCode)
        {
            if (string.IsNullOrWhiteSpace(deviceCode))
            {
                return string.Empty;
            }
            return RSAUtils.Encrypt(deviceCode);
        }
    }
}
