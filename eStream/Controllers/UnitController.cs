﻿using eStream.Common.CommonObject;
using eStream.Common.CommonClass;
using eStream.Helpers.Filter;
using eStream.Models;
using Model;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eStream.Controllers
{
    public class UnitController : Controller
    {
        private readonly Entities _context = new Entities();
        //
        // GET: /Unit/
        [BreadCrumb(ControllerName = "Đơn vị", ActionName = "Danh sách", AreaName = "Quản trị hệ thống")]
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult LoadChild(int? id = null)
        {
            List<UnitModel> listUnit = this.GetListUnitChild(id);
            List<NodeBO> listNode = listUnit.Select(x => new NodeBO { id = x.UnitID, text = x.UnitName, children = x.CountChild > 0 }).ToList();
            return Json(listNode, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult SearchUnit(int? ParentID, string UnitName = null, int page = 1)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            if (string.IsNullOrWhiteSpace(UnitName))
            {
                UnitName = null;
            }
            else
            {
                UnitName = UnitName.Trim().ToLower();
            }
            string sParentID = "/" + ParentID.GetValueOrDefault().ToString() + "/";
            string sUnitID = "/" + user.UnitID.ToString() + "/";
            var iQUnit = _context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE
                && (x.UnitPath.StartsWith(sUnitID))
                && (ParentID == null || ParentID == 0 || x.UnitPath.Contains(sParentID))
                && (UnitName == null || x.UnitName.ToLower().Contains(UnitName))).Select(x => new UnitModel
                {
                    UnitID = x.UnitID,
                    UnitName = x.UnitName,
                    PhoneNumber = x.PhoneNumber,
                    Email = x.Email,
                    Status = x.Status,
                    UnitLevel = x.UnitLevel,
                    ParentID = x.ParentID,
                    UnitPath = x.UnitPath
                }).OrderBy(x => x.UnitPath).ThenBy(x => x.UnitName);

            var listUnit = iQUnit.ToList();
            PagedList<UnitModel> paginate = new PagedList<UnitModel>(listUnit, page, GlobalConstant.PAGE_SIZE);
            ViewData["listUnit"] = paginate;
            ViewBag.UnitName = UnitName;
            return PartialView("_LstUnit");
        }

        public PartialViewResult OpenAddOrEdit(int? id, int? parentID)
        {
            UnitModel model = null;
            if (id.HasValue && id > 0)
            {
                UserInfoBO user = SessionManager.GetUserInfoBO();
                model = _context.Unit.Where(x => x.UnitID == id && x.Status == GlobalConstant.IS_ACTIVE).Select(x => new UnitModel
                {
                    UnitID = x.UnitID,
                    UnitName = x.UnitName,
                    PhoneNumber = x.PhoneNumber,
                    Email = x.Email,
                    Status = x.Status,
                    UnitPath = x.UnitPath
                }).FirstOrDefault();
                if (model == null)
                {
                    throw new BusinessException("Không tồn tại đơn vị đã chọn");
                }
                string sUnitID = "/" + user.UnitID.ToString() + "/";
                if (!model.UnitPath.Contains(sUnitID))
                {
                    throw new BusinessException("Bạn không có quyền quản lý đơn vị đang chọn");
                }
                parentID = model.ParentID;
            }
            if (parentID != null && parentID > 0)
            {
                UnitModel parent = _context.Unit.Where(x => x.UnitID == parentID && x.Status == GlobalConstant.IS_ACTIVE).Select(x => new UnitModel
                {
                    UnitID = x.UnitID,
                    UnitName = x.UnitName,
                    PhoneNumber = x.PhoneNumber,
                    Email = x.Email,
                    Status = x.Status
                }).FirstOrDefault();
                ViewData["parent"] = parent;
                if (id == null || id == 0)
                {
                    model = new UnitModel { ParentID = parentID };
                }
            }
            return PartialView("_AddOrEdit", model);
        }

        private List<UnitModel> GetListUnitChild(int? parentID)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            string sUnitID = "/" + user.UnitID.ToString() + "/";
            return _context.Unit.Where(x => x.Status == GlobalConstant.IS_ACTIVE
            && x.ParentID == parentID && x.UnitPath.StartsWith(sUnitID))
            .Select(x => new UnitModel
            {
                UnitID = x.UnitID,
                UnitName = x.UnitName,
                PhoneNumber = x.PhoneNumber,
                Email = x.Email,
                Status = x.Status,
                UnitLevel = x.UnitLevel,
                ParentID = x.ParentID,
                UnitPath = x.UnitPath,
                CountChild = _context.Unit.Where(c => c.ParentID == x.UnitID && c.Status == GlobalConstant.IS_ACTIVE).Count()
            }).OrderBy(x => x.UnitPath).ThenBy(x => x.UnitName).ToList();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult AddOrEdit(UnitModel unit)
        {
            string msg;
            UserInfoBO user = SessionManager.GetUserInfoBO();
            if (user.IsSupperAdmin.GetValueOrDefault() != GlobalConstant.IS_ACTIVE)
            {
                throw new BusinessException("Bạn không có quyền thực hiện chức năng");
            }
            Unit entity = null;
            if (ModelState.IsValid)
            {
                Unit parent = null;
                if (unit.ParentID != null && unit.ParentID > 0)
                {
                    parent = _context.Unit.Where(x => x.UnitID == unit.ParentID && x.Status == GlobalConstant.IS_ACTIVE).FirstOrDefault();
                    if (parent == null)
                    {
                        throw new BusinessException("Không tồn tại đơn vị cha đã chọn");
                    }
                    if (parent.UnitLevel == GlobalConstant.UNIT_TYPE_VILLAGE)
                    {
                        throw new BusinessException("Hệ thống chỉ quản lý tới cấp xã/phường, không được phép chọn xã/phường là đơn vị cha");
                    }
                }
                // Kiem tra don vi
                bool isInsert = false;
                if (unit.UnitID > 0)
                {
                    entity = _context.Unit.Where(x => x.UnitID == unit.UnitID && x.Status == GlobalConstant.IS_ACTIVE).FirstOrDefault();
                    if (entity == null)
                    {
                        throw new BusinessException("Không tồn tại đơn vị đã chọn");
                    }
                    entity = UpdateUnit(unit, entity);
                    msg = "Cập nhật đơn vị thành công";
                }
                else
                {
                    entity = new Unit();
                    entity = UpdateUnit(unit, entity);
                    entity.Status = GlobalConstant.IS_ACTIVE;
                    msg = "Thêm mới đơn vị thành công";
                    isInsert = true;
                }
                string sUnitID = "/" + user.UnitID.ToString() + "/";
                if (parent != null && !parent.UnitPath.StartsWith(sUnitID))
                {
                    throw new BusinessException("Bạn không có quyền quản lý đơn vị đang chọn");
                }
                if (isInsert)
                {
                    _context.Unit.Add(entity);
                }
                else
                {
                    if (parent != null && !entity.UnitPath.StartsWith(sUnitID))
                    {
                        throw new BusinessException("Bạn không có quyền quản lý đơn vị đang chọn");
                    }
                }
                _context.SaveChanges();
                // Cha con
                if (isInsert)
                {
                    if (parent == null)
                    {
                        entity.UnitLevel = GlobalConstant.UNIT_TYPE_PROVINCE;
                        entity.UnitPath = "/" + entity.UnitID + "/";
                    }
                    else
                    {
                        entity.UnitLevel = parent.UnitLevel + 1;
                        entity.ParentID = parent.UnitID;
                        entity.UnitPath = parent.UnitPath + entity.UnitID + "/";
                    }
                    _context.SaveChanges();
                }
                return Json(new { msg = msg, id = entity.ParentID }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
        }

        private Unit UpdateUnit(UnitModel unit, Unit entity)
        {
            entity.UnitName = unit.UnitName.Trim();
            entity.PhoneNumber = string.IsNullOrWhiteSpace(unit.PhoneNumber) ? null : unit.PhoneNumber.Trim();
            entity.Email = string.IsNullOrWhiteSpace(unit.Email) ? null : unit.Email.Trim();
            return entity;
        }

        public JsonResult Delete(int id)
        {
            Unit unit = _context.Unit.Find(id);
            if (unit == null)
            {
                throw new BusinessException("Không tồn tại đơn vị đã chọn");
            }
            unit.Status = GlobalConstant.IS_NOT_ACTIVE;
            _context.SaveChanges();
            return Json("Xóa đơn vị thành công", JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}