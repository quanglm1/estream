﻿using eStream.Common.CommonClass;
using eStream.Models;
using Model;
using System.Linq;
using System.Web.Mvc;
using PagedList;
using eStream.Common.CommonObject;
using eStream.Helpers.Filter;

namespace eStream.Controllers
{
    public class SendSmsHisController : BaseController
    {
        private readonly Entities _context = new Entities();
        // GET: SendSmsHis
        [BreadCrumb(ControllerName = "Lịch sử gửi SMS", ActionName = "Danh sách", AreaName = "Quản trị hệ thống")]
        public ActionResult Index()
        {
            PrepareDataSearch(_context);
            return View();
        }
        public PartialViewResult SearchSmsHis(int? UnitID = null, int page = 1, int pagsize = 10, string SearchPhoneNumber = null, string SearchName = null)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            if (user.IsSupperAdmin.GetValueOrDefault() == 0)
            {
                UnitID = user.UnitID;
            }
            ViewBag.SearchPhoneNumber = SearchPhoneNumber;
            ViewBag.SearchName = SearchName;
            ViewBag.UnitID = UnitID;
            var smsHis = getListSmsHis(UnitID, SearchPhoneNumber, SearchName).OrderByDescending(x => x.SendTime);
            var model = new PagedList<SendSmsHisModel>(smsHis, page, pagsize);

            return PartialView("_LstSendSms", model);
        }

        private IQueryable<SendSmsHisModel> getListSmsHis(int? UnitID, string SearchPhoneNumber, string SearchName)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            string sUnitID = "/" + user.UnitID + "/";
            string sUnitSearchID = "/" + UnitID.GetValueOrDefault() + "/";
            var query = _context.SendSmsHis.Select(x => new SendSmsHisModel
            {
                MsIsdn = x.MsIsdn,
                Phone = x.MsIsdn != null ? "0" + x.MsIsdn.Substring(2, x.MsIsdn.Length - 2) : "",
                SendRequest = x.SendRequest,
                SendResponse = x.SendResponse,
                SendSmsHisID = x.SendSmsHisID,
                SendTime = x.SendTime,
                UnitID = x.UnitID,
                Name = _context.User.Where(u => !string.IsNullOrEmpty(u.PhoneNumber)
                && (x.MsIsdn.Equals(u.PhoneNumber) || ((x.MsIsdn != null ? "0" + x.MsIsdn.Substring(2, x.MsIsdn.Length - 2) : "").Equals(u.PhoneNumber)))).Select(u => u.FullName).FirstOrDefault()
            });
            if (UnitID == null || UnitID == 0)
            {
                query = query.Where(o => (o.UnitID == null && user.IsSupperAdmin == GlobalConstant.IS_ACTIVE)
                        || _context.Unit.Any(u => u.Status == GlobalConstant.IS_ACTIVE
                        && u.UnitID == o.UnitID && u.UnitPath.Contains(sUnitID)));
            }
            else
            {
                query = query.Where(o => _context.Unit.Any(u => u.Status == GlobalConstant.IS_ACTIVE
                        && u.UnitID == o.UnitID
                        && (user.IsSupperAdmin == GlobalConstant.IS_ACTIVE || u.UnitPath.Contains(sUnitID))
                        && u.UnitPath.Contains(sUnitSearchID)));
            }
            if (!string.IsNullOrEmpty(SearchPhoneNumber))
            {
                query = query.Where(x => !string.IsNullOrEmpty(x.MsIsdn) && !string.IsNullOrEmpty(x.Phone) && (x.Phone.Contains(SearchPhoneNumber) || x.MsIsdn.Contains(SearchPhoneNumber)));
            }
            if (!string.IsNullOrEmpty(SearchName))
            {
                query = query.Where(x => !string.IsNullOrEmpty(x.Name) && x.Name.Contains(SearchPhoneNumber));
            }

            return query;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}