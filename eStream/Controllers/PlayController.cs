﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using eStream.Common.CommonClass;
using System.Data.Entity;
using System.Text;
using System.IO;
using System.Collections.Concurrent;
using eStream.Dto;

namespace eStream.Controllers
{
    public class PlayController : Controller
    {
        // GET: Play
        [AllowAnonymous]
        public FileResult Live(int unitID, long deviceID = 0, long bitrate = 128)
        {
            StringBuilder str = null;
            using (Entities context = new Entities())
            {
                var now = DateTime.Now;
                var nowDate = DateTime.Now.Date;
                long sequence = Convert.ToInt64(Math.Ceiling((now - nowDate).TotalSeconds));
                Unit unit = context.Unit.Find(unitID);
                var live = (from l in context.Live
                            join u in context.Unit on l.UnitID equals u.UnitID
                            where l.Status >= GlobalConstant.LIVE_PLAYING
                            && unit.UnitPath.Contains(u.UnitPath)
                            orderby u.UnitPath descending, l.LiveID
                            select l).FirstOrDefault();
                bool living = false;
                if (live != null)
                {
                    if (live.LiveType == GlobalConstant.LIVE_TYPE_CHOICE)
                    {
                        if (context.LiveDevice.Any(x => x.LiveID == live.LiveID))
                        {
                            living = true;
                        }
                    }
                    else
                    {
                        living = true;
                    }
                }
                if (living)
                {
                    int liveSource = live.LiveSource.GetValueOrDefault();
                    switch (liveSource)
                    {
                        case GlobalConstant.LIVE_SOURCE_STREAMING:
                            str = LiveController.GetStreaming(context, live);
                            break;
                        case GlobalConstant.LIVE_SOURCE_FILE:
                            str = LiveController.GetLiveFromFile(context, live, bitrate);
                            break;
                        case GlobalConstant.LIVE_SOURCE_VOV:
                            if (live.ChannelID != null)
                            {
                                var channel = context.ForwardChanel.Find(live.ChannelID);
                                if (channel != null)
                                {
                                    string channelCode = channel.ChannelCode.ToLower();
                                    str = GetVOV(channelCode, unitID);
                                }
                            }
                            break;
                    }
                }
                else
                {
                    StreamingCaching.DicPlaying.TryGetValue(deviceID, out PlayingDto nowPlaying);
                    if (nowPlaying == null)
                    {
                        nowPlaying = new PlayingDto
                        {
                            PlayingType = GlobalConstant.PLAYING_TYPE_LIVE
                        };
                        StreamingCaching.DicPlaying[deviceID] = nowPlaying;
                    }
                    nowPlaying.PlayingType = GlobalConstant.PLAYING_TYPE_SCHEDULE;
                    str = ControlController.GetStreamScheduler(context, unitID, deviceID, bitrate);
                }
            }
            if (str == null)
            {
                str = new StringBuilder();
                str.AppendLine("#EXTM3U");
                str.AppendLine("#EXT-X-VERSION:3");
                str.AppendLine("#EXT-X-ALLOW-CACHE:YES");
                str.AppendLine("#EXT-X-TARGETDURATION:" + (GlobalConstant.CHUNK_DURATION + 1));
            }
            string content = str.ToString();
            byte[] arr = Encoding.UTF8.GetBytes(content);

            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = "playlist.m3u8",
                Inline = false,
            };

            Response.AppendHeader("Content-Disposition", cd.ToString());
            return File(arr, "m3u8");
        }

        [AllowAnonymous]
        public FileResult Vov(string channel)
        {
            string vovPath = GlobalConstant.MEDIA_FOLDER + channel.ToLower();
            string content = System.IO.File.ReadAllText(vovPath + "/" + "playlist.m3u8");

            byte[] arr = Encoding.UTF8.GetBytes(content);

            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = "playlist.m3u8",
                Inline = false,
            };

            Response.AppendHeader("Content-Disposition", cd.ToString());
            return File(arr, "m3u8");
        }

        public static StringBuilder GetVOV(string channel, long unitID)
        {
            StringBuilder str = new StringBuilder();
            string vovPath = GlobalConstant.MEDIA_FOLDER + channel;
            string content = System.IO.File.ReadAllText(vovPath + "/" + "playlist.m3u8");
            str.AppendLine(content);
            return str;
        }

        public static long GetVOVSequence(string channel)
        {
            string vovPath = GlobalConstant.MEDIA_FOLDER + channel;
            if (!System.IO.File.Exists(vovPath + "/" + "playlist.m3u8"))
            {
                return 0;
            }
            string content = System.IO.File.ReadAllText(vovPath + "/" + "playlist.m3u8");
            string[] arr = content.Split('\n');
            for (int i = 0; i < arr.Length; i++)
            {
                var a = arr[i];
                if (a.StartsWith("#EXT-X-MEDIA-SEQUENCE"))
                {
                    long.TryParse(a.Replace("#EXT-X-MEDIA-SEQUENCE:", "").Replace("\r", "").Trim(), out long sequence);
                    return sequence;
                }
            }
            return 0;
        }
    }

}