﻿using eStream.Common.CommonObject;
using eStream.Common.CommonClass;
using eStream.Helpers.Filter;
using eStream.Models;
using Model;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eStream.Controllers
{
    public class ConfigController : Controller
    {
        private readonly Entities _context = new Entities();
        //
        // GET: /Unit/
        [BreadCrumb(ControllerName = "Cấu hình hệ thống", ActionName = "Cấu hình", AreaName = "Quản trị hệ thống")]
        public ActionResult Index()
        {
            var config = _context.SystemConfig.FirstOrDefault();
            var model = new SystemConfigModel
            {
                IsUseOTP = config.IsUseOTP.GetValueOrDefault() == GlobalConstant.IS_ACTIVE,
                ExpOTP = config.ExpOTP
            };
            return View(model);
        }

        public int GetUnitType(OrderConfig config, int orderNumber)
        {
            if (orderNumber == config.VillageOrder)
            {
                return GlobalConstant.UNIT_TYPE_VILLAGE;
            }
            if (orderNumber == config.CommuneOrder)
            {
                return GlobalConstant.UNIT_TYPE_COMMUNE;
            }
            if (orderNumber == config.DistrictOrder)
            {
                return GlobalConstant.UNIT_TYPE_DISTRICT;
            }
            if (orderNumber == config.ProvinceOrder)
            {
                return GlobalConstant.UNIT_TYPE_PROVINCE;
            }
            return 0;
        }

        public int GetOrder(OrderConfig config, int unitType)
        {
            if (unitType == GlobalConstant.UNIT_TYPE_VILLAGE)
            {
                return config.VillageOrder;
            }
            if (unitType == GlobalConstant.UNIT_TYPE_COMMUNE)
            {
                return config.CommuneOrder;
            }
            if (unitType == GlobalConstant.UNIT_TYPE_DISTRICT)
            {
                return config.DistrictOrder;
            }
            if (unitType == GlobalConstant.UNIT_TYPE_PROVINCE)
            {
                return config.ProvinceOrder;
            }
            return 0;
        }
        public void UpdateOrder(OrderConfig config, int unitType, int orderNumber)
        {
            if (unitType == GlobalConstant.UNIT_TYPE_VILLAGE)
            {
                config.VillageOrder = orderNumber;
            }
            if (unitType == GlobalConstant.UNIT_TYPE_COMMUNE)
            {
                config.CommuneOrder = orderNumber;
            }
            if (unitType == GlobalConstant.UNIT_TYPE_DISTRICT)
            {
                config.DistrictOrder = orderNumber;
            }
            if (unitType == GlobalConstant.UNIT_TYPE_PROVINCE)
            {
                config.ProvinceOrder = orderNumber;
            }
        }

        public PartialViewResult LoadUnitOrder()
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            if (user.IsSupperAdmin.GetValueOrDefault() == 0)
            {
                throw new BusinessException("Bạn không có quyền thực hiện thao tác này");
            }
            var config = _context.OrderConfig.FirstOrDefault();
            if (config == null)
            {
                config = new OrderConfig
                {
                    VillageOrder = 1,
                    CommuneOrder = 2,
                    DistrictOrder = 3,
                    ProvinceOrder = 4
                };
                _context.OrderConfig.Add(config);
                _context.SaveChanges();
            }
            List<UnitOrderModel> listUnitOrder = new List<UnitOrderModel>();
            listUnitOrder.Add(new UnitOrderModel
            {
                NumberOrder = 1,
                UnitType = GetUnitType(config, 1)
            });
            listUnitOrder.Add(new UnitOrderModel
            {
                NumberOrder = 2,
                UnitType = GetUnitType(config, 2)
            });
            listUnitOrder.Add(new UnitOrderModel
            {
                NumberOrder = 3,
                UnitType = GetUnitType(config, 3)
            });
            listUnitOrder.Add(new UnitOrderModel
            {
                NumberOrder = 4,
                UnitType = GetUnitType(config, 4)
            });
            ViewData["listUnitOrder"] = listUnitOrder;
            return PartialView("_ListUnitOrder");
        }

        [HttpPost]
        public JsonResult Move(int unitType, int step)
        {
            var config = _context.OrderConfig.FirstOrDefault();
            int currentOrder = GetOrder(config, unitType);
            int nextOrder = currentOrder;
            currentOrder -= step;
            int nextUnitType = GetUnitType(config, currentOrder);
            UpdateOrder(config, unitType, currentOrder);
            UpdateOrder(config, nextUnitType, nextOrder);
            _context.SaveChanges();
            return Json("Sắp xếp thứ tự phát thành công", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveConfig(SystemConfigModel model)
        {
            var config = _context.SystemConfig.FirstOrDefault();
            model.UpdateEntity(config);
            _context.SaveChanges();
            ConfigCaching.LoadCaching();
            return Json("Lưu cấu hình thành công", JsonRequestBehavior.AllowGet);
        }

        [SkipDomainHashFilterAttribute]
        [AllowAnonymous]
        [SkipActionLogFilter]
        public JsonResult Hash256(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                text = string.Empty;
            }
            var path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Substring(6);
            var toolPath = System.IO.Path.Combine(path, "Tool.dll");
            System.Reflection.Assembly assembly = System.Reflection.Assembly.LoadFrom(toolPath);
            Type type = assembly.GetType("Tool.Crypto");
            object cryptoTool = Activator.CreateInstance(type);
            string methodName = "ComputeSha256Hash";
            System.Reflection.MethodInfo methodInfo = type.GetMethod(methodName);
            var parameters = new List<object>() { text, GlobalConstant.VERY_SECRET + GlobalConstant.DOMAIN_SECRET };
            var response = methodInfo.Invoke(cryptoTool, parameters.ToArray());
            return Json(new { Message = response }, JsonRequestBehavior.AllowGet);
        }

        [SkipDomainHashFilterAttribute]
        [AllowAnonymous]
        [SkipActionLogFilter]
        public JsonResult Encrypt(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                text = string.Empty;
            }
            var path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Substring(6);
            var toolPath = System.IO.Path.Combine(path, "Tool.dll");
            System.Reflection.Assembly assembly = System.Reflection.Assembly.LoadFrom(toolPath);
            Type type = assembly.GetType("Tool.Crypto");
            object cryptoTool = Activator.CreateInstance(type);
            string methodName = "EncryptString";
            System.Reflection.MethodInfo methodInfo = type.GetMethod(methodName);
            var parameters = new List<object>() { text, GlobalConstant.SECRET };
            var response = methodInfo.Invoke(cryptoTool, parameters.ToArray());
            return Json(new { Message = response }, JsonRequestBehavior.AllowGet);
        }

        [SkipDomainHashFilterAttribute]
        [AllowAnonymous]
        [SkipActionLogFilter]
        public JsonResult Decrypt(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                text = string.Empty;
            }
            var path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Substring(6);
            var toolPath = System.IO.Path.Combine(path, "Tool.dll");
            System.Reflection.Assembly assembly = System.Reflection.Assembly.LoadFrom(toolPath);
            Type type = assembly.GetType("Tool.Crypto");
            object cryptoTool = Activator.CreateInstance(type);
            string methodName = "DecryptString";
            System.Reflection.MethodInfo methodInfo = type.GetMethod(methodName);
            var parameters = new List<object>() { text, GlobalConstant.SECRET };
            var response = methodInfo.Invoke(cryptoTool, parameters.ToArray());
            return Json(new { Message = response }, JsonRequestBehavior.AllowGet);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}