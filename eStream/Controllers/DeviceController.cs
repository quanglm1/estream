﻿using eStream.Common.CommonObject;
using eStream.Common.CommonClass;
using eStream.Helpers.Filter;
using eStream.Models;
using Model;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eStream.Controllers
{
    public class DeviceController : BaseController
    {
        private readonly Entities _context = new Entities();
        // GET: /Device/
        [BreadCrumb(ControllerName = "Thiết bị", ActionName = "Danh sách", AreaName = "Quản trị hệ thống")]
        public ActionResult Index()
        {
            PrepareDataSearch(_context);
            return View();
        }

        public ActionResult Index_prototype()
        {
            return View();
        }

        public PartialViewResult SearchDevice(int? UnitID, string DeviceName = null, int page = 1)
        {
            UserInfoBO user = SessionManager.GetUserInfoBO();
            if (string.IsNullOrWhiteSpace(DeviceName))
            {
                DeviceName = null;
            }
            else
            {
                DeviceName = DeviceName.Trim().ToLower();
            }
            var query = from d in _context.Device
                        where d.Status != GlobalConstant.IS_NOT_ACTIVE
                        where (DeviceName == null || d.DeviceCode.ToLower().Contains(DeviceName) || d.DeviceName.ToLower().Contains(DeviceName))
                        select d;
            string sUnitID = "/" + user.UnitID + "/";
            string sUnitSearchID = "/" + UnitID.GetValueOrDefault() + "/";
            if (UnitID == null || UnitID == 0)
            {
                query = query.Where(o => (o.UnitID == null && user.IsSupperAdmin == GlobalConstant.IS_ACTIVE)
                        || _context.Unit.Any(u => u.Status != GlobalConstant.IS_NOT_ACTIVE
                        && u.UnitID == o.UnitID && (user.IsSupperAdmin == GlobalConstant.IS_ACTIVE || u.UnitPath.Contains(sUnitID))));
            }
            else
            {
                query = query.Where(o => _context.Unit.Any(u => u.Status != GlobalConstant.IS_NOT_ACTIVE
                        && u.UnitID == o.UnitID && (user.IsSupperAdmin == GlobalConstant.IS_ACTIVE || u.UnitPath.Contains(sUnitID))
                        && u.UnitPath.Contains(sUnitSearchID)));
            }
            if (user.IsAdmin.GetValueOrDefault() == 0)
            {
                query = query.Where(x => x.UnitID != null);
            }
            var iQDevice = query.Select(x => new DeviceModel
            {
                UnitID = x.UnitID,
                UnitName = _context.Unit.Where(u => u.UnitID == x.UnitID).Select(u => u.UnitName).FirstOrDefault(),
                DeviceID = x.DeviceID,
                SDeviceCode = x.DeviceCode,
                DeviceName = x.DeviceName,
                Status = x.Status,
                CreateDate = x.CreateDate
            }).OrderBy(x => x.UnitID).ThenBy(x => x.SDeviceCode);

            PagedList<DeviceModel> paginate = new PagedList<DeviceModel>(iQDevice, page, GlobalConstant.PAGE_SIZE);
            ViewData["listDevice"] = paginate;
            ViewBag.DeviceName = DeviceName;
            ViewBag.UnitID = UnitID;
            return PartialView("_LstDevice");
        }

        public PartialViewResult OpenAddOrEdit(int? id, int? unitID)
        {
            DeviceModel model = new DeviceModel();
            if (id.HasValue && id > 0)
            {
                UserInfoBO user = SessionManager.GetUserInfoBO();
                model = _context.Device.Where(x => x.DeviceID == id && x.Status != GlobalConstant.IS_NOT_ACTIVE).Select(x => new DeviceModel
                {
                    UnitID = x.UnitID,
                    DeviceID = x.DeviceID,
                    SDeviceCode = x.DeviceCode,
                    DeviceName = x.DeviceName,
                    Status = x.Status,
                    CreateDate = x.CreateDate,
                    ManagerName = x.ManagerName,
                    ManagerPhone = x.ManagerPhone,
                    SimNumber = x.SimNumber,
                    FmType = x.FmType == null ? GlobalConstant.IS_NOT_ACTIVE : x.FmType,
                    S = x.Sound == null || x.Sound == "" ? "0000" : x.Sound,
                    S1 = false,
                    S2 = false,
                    S3 = false,
                    S4 = false
                }).FirstOrDefault();
                var idx = 1;
                foreach (var c in model.S)
                {
                    if (c.ToString().Equals(GlobalConstant.IS_ACTIVE.ToString()))
                    {
                        if (idx == 1)
                        {
                            model.S1 = true;
                        }
                        else if (idx == 2)
                        {
                            model.S2 = true;
                        }
                        else if (idx == 3)
                        {
                            model.S3 = true;
                        }
                        else if (idx == 4)
                        {
                            model.S4 = true;
                        }
                    }
                    idx++;
                }
                if (model == null)
                {
                    throw new BusinessException("Không tồn tại thiết bị đã chọn");
                }
                if (model.UnitID != null && model.UnitID > 0)
                {
                    string sUnitID = "/" + user.UnitID.ToString() + "/";
                    Unit unit = _context.Unit.Find(model.UnitID);
                    if (!unit.UnitPath.Contains(sUnitID))
                    {
                        throw new BusinessException("Bạn không có quyền quản lý thiết bị của thiết bị khác");
                    }
                    if (unit == null)
                    {
                        throw new BusinessException("Không tồn tài thiết bị đã chọn");
                    }
                    ViewData["unitName"] = unit.UnitName;
                    SetAddress(_context, model.UnitID, out int? provinceID, out int? districtID, out int? communeID, out int? villageID);
                    model.ProvinceID = provinceID;
                    model.DistrictID = districtID;
                    model.CommuneID = communeID;
                    model.VillageID = villageID;
                    PrepareDataAddOrEdit(_context, provinceID, districtID, communeID);
                }
            }
            else
            {
                if (unitID != null && unitID > 0)
                {
                    Unit unit = _context.Unit.Find(unitID);
                    if (unit == null)
                    {
                        throw new BusinessException("Không tồn tại thiết bị đã chọn");
                    }
                    ViewData["unitName"] = unit.UnitName;
                    model = new DeviceModel { UnitID = unitID };
                }
            }
            PrepareDataAddOrEdit(_context, model.ProvinceID, model.DistrictID, model.CommuneID);
            // THong tin trang thai thiet bi
            List<SelectListItem> listStatus = new List<SelectListItem>();
            listStatus.Add(new SelectListItem { Value = "", Text = "--Trạng thái--" });
            listStatus.Add(new SelectListItem { Value = GlobalConstant.DEVICE_STATUS_READY.ToString(), Text = "Sẵn sàng" });
            listStatus.Add(new SelectListItem { Value = GlobalConstant.DEVICE_STATUS_BEAK.ToString(), Text = "Hỏng" });
            listStatus.Add(new SelectListItem { Value = GlobalConstant.DEVICE_STATUS_GUARANT.ToString(), Text = "Bảo hành" });
            ViewData["listStatus"] = listStatus;
            return PartialView("_AddOrEdit", model);
        }

        [AllowAnonymous]
        public JsonResult UpdateDeviceFullCode(string domain)
        {
            if (string.IsNullOrWhiteSpace(domain))
            {
                domain = Request.Url.Authority;
            }
            var path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Substring(6);
            var toolPath = System.IO.Path.Combine(path, "Tool.dll");
            System.Reflection.Assembly assembly = System.Reflection.Assembly.LoadFrom(toolPath);
            Type type = assembly.GetType("Tool.Device");
            object deviceTool = Activator.CreateInstance(type);
            string methodName = "UpdateFullCodes";
            System.Reflection.MethodInfo methodInfo = type.GetMethod(methodName);
            var parameters = new List<object>() { domain, GlobalConstant.VERY_SECRET + GlobalConstant.DEVICE_SECRET };
            methodInfo.Invoke(deviceTool, parameters.ToArray());
            return Json(new { Message = "Cập nhật thành công" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult AddOrEdit(DeviceModel Device)
        {
            string msg;
            Device entity = null;
            if (ModelState.IsValid)
            {
                // Kiem tra don vi
                UserInfoBO user = SessionManager.GetUserInfoBO();
                if (Device.DeviceID > 0)
                {
                    entity = _context.Device.Where(x => x.DeviceID == Device.DeviceID && x.Status != GlobalConstant.IS_NOT_ACTIVE).FirstOrDefault();
                    if (entity == null)
                    {
                        throw new BusinessException("Không tồn tại thiết bị đã chọn");
                    }
                    //entity.DeviceCode = Device.SDeviceCode;
                    entity.DeviceName = Device.DeviceName;
                    entity.ManagerName = Device.ManagerName;
                    entity.ManagerPhone = Device.ManagerPhone;
                    entity.SimNumber = Device.SimNumber;
                    entity.Status = Device.Status;
                    entity.FmType = Device.FmType;
                    int? newUnitID = this.SetUnit(_context, Device.ProvinceID, Device.DistrictID, Device.CommuneID, Device.VillageID);
                    if (entity.UnitID.GetValueOrDefault() != newUnitID.GetValueOrDefault())
                    {
                        if (entity.SyncTime.GetValueOrDefault() > 0)
                        {
                            entity.IsChangeSyncTime = GlobalConstant.IS_ACTIVE;
                        }
                        entity.IsChangeVolume = GlobalConstant.IS_ACTIVE;
                    }
                    entity.UnitID = newUnitID;
                    var sound = "0000".ToCharArray();
                    if (Device.S1)
                    {
                        sound[0] = '1';
                    }
                    if (Device.S2)
                    {
                        sound[1] = '1';
                    }
                    if (Device.S3)
                    {
                        sound[2] = '1';
                    }
                    if (Device.S4)
                    {
                        sound[3] = '1';
                    }
                    entity.Sound = new string(sound);
                    msg = "Cập nhật thiết bị thành công";
                }
                else
                {
                    //throw new BusinessException("Bạn không được thêm thiết bị bằng tay");
                    string[] arrDeviceCode = Device.SDeviceCode.Split(',');
                    int? unitID = this.SetUnit(_context, Device.ProvinceID, Device.DistrictID, Device.CommuneID, Device.VillageID);
                    foreach (string deviceCode in arrDeviceCode)
                    {
                        entity = new Device();
                        entity.UnitID = unitID;
                        entity.DeviceCode = deviceCode;
                        entity.CreateDate = DateTime.Now;
                        entity.DeviceName = Device.DeviceName;
                        entity.ManagerName = Device.ManagerName;
                        entity.ManagerPhone = Device.ManagerPhone;
                        entity.SimNumber = Device.SimNumber;
                        entity.Status = Device.Status;
                        entity.IsChangeSyncTime = GlobalConstant.IS_ACTIVE;
                        entity.IsChangeVolume = GlobalConstant.IS_ACTIVE;
                        _context.Device.Add(entity);
                    }
                    msg = "Thêm mới thiết bị thành công";
                }
                if (Device.UnitID != null && Device.UnitID > 0)
                {
                    string sUnitID = "/" + user.UnitID.ToString() + "/";
                    Unit unit = _context.Unit.Find(Device.UnitID);
                    if (!unit.UnitPath.Contains(sUnitID))
                    {
                        throw new BusinessException("Bạn không có quyền quản lý thiết bị của thiết bị khác");
                    }
                }
                _context.SaveChanges();
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            else
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
        }

        public JsonResult Delete(int id)
        {
            Device Device = _context.Device.Find(id);
            if (Device == null)
            {
                throw new BusinessException("Không tồn tại thiết bị đã chọn");
            }
            Device.Status = GlobalConstant.IS_NOT_ACTIVE;
            _context.SaveChanges();
            return Json("Xóa thiết bị thành công", JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}