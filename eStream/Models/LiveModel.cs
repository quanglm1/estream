﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace eStream.Models
{
    public class LiveModel
    {
        public long LiveID { get; set; }

        [DisplayName("Tên chương trình")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public int? LiveSource { get; set; }
        public int? Status { get; set; }
        public int UnitID { get; set; }
        public string UnitName { get; set; }
        public int LiveType { get; set; }
        public int? ChannelID { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? PlayingTime { get; set; }
        public bool IsLoop { get; set; }
        public int? LoopCount { get; set; }
    }
}