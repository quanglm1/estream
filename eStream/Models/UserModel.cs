﻿using eStream.Common;
using eStream.Common.CommonClass;
using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eStream.Models
{
    public class UserModel
    {
        public int UserID { get; set; }
        [DisplayName("Tên đăng nhập")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        [MaxLength(100, ErrorMessage = "{0} không được vượt quá 100 ký tự")]
        public string UserName { get; set; }
        [DisplayName("Mật khẩu")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public string Password { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? Status { get; set; }
        public DateTime? LastLoginDate { get; set; }
        public bool IsAutoPass { get; set; }
        [DisplayName("Số điện thoại")]
        [RegularExpression(@"^\d+$", ErrorMessage = "Số điện thoại không hợp lệ.")]
        public string PhoneNumber { get; set; }
        public int RoleID { get; set; }
        public int? ProvinceID { get; set; }
        public int? DistrictID { get; set; }
        public int? CommuneID { get; set; }
        public int? VillageID { get; set; }
        public int? UnitID { get; set; }
        public string UnitName { get; set; }
        public byte[] Avatar { get; set; }
        public HttpPostedFileWrapper UserAvatar { get; set; }
        public string AvatarType { get; set; }
        public string AvatarName { get; set; }
        public int? Role { get; set; }
        public string FullName { get; set; }
        public string LastName { get; set; }
        public string WorkPlace { get; set; }
        [DisplayName("Ngày sinh")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy", ApplyFormatInEditMode = true)]
        public DateTime? BirthDay { get; set; }
        [DisplayName("Email")]
        [EmailAddress(ErrorMessage = "{0} không hợp lệ!")]
        public string Email { get; set; }
        public int? Sex { get; set; }

        public string Ethnic { get; set; }
        public string Position { get; set; }
        public string Comment { get; set; }
        public DateTime? VotedDate { get; set; }
        public bool IsEmpower { get; set; }

        public User ToEntity()
        {
            User entity = new User
            {
                UserName = UserName.Trim().ToLower(),
                CreateDate = CreateDate,
                LastLoginDate = LastLoginDate,
                Status = Status,
                PhoneNumber = PhoneNumber,
                FullName = FullName,
                BirthDay = BirthDay,
                Email = Email,
                Ethnic = Ethnic,
                Position = Position,
                Sex = Sex,
                WorkPlace = WorkPlace,
                Role = Role
            };
            if (IsAutoPass)
            {
                entity.Password = GlobalCommon.Encrypt(GlobalCommon.GetRandomString());
            }
            else
            {
                string message;
                if (!GlobalCommon.IsStrongPassword(Password, out message))
                {
                    throw new BusinessException(message);
                }
                entity.Password = GlobalCommon.Encrypt(Password);
            }
            return entity;
        }

        internal void UpdateEntity(User entity)
        {
            entity.PhoneNumber = PhoneNumber;
            entity.FullName = FullName;
            entity.BirthDay = BirthDay;
            entity.Email = Email;
            entity.Ethnic = Ethnic;
            entity.Position = Position;
            entity.Sex = Sex;
            entity.WorkPlace = WorkPlace;
            entity.Role = Role;
            if (IsAutoPass)
            {
                entity.Password = GlobalCommon.Encrypt(GlobalCommon.GetRandomString());
            }
            else if (!string.IsNullOrWhiteSpace(Password))
            {
                string message;
                if (!GlobalCommon.IsStrongPassword(Password, out message))
                {
                    throw new BusinessException(message);
                }
                entity.Password = GlobalCommon.Encrypt(Password);
            }            
        }
    }
}