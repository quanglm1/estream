﻿using eStream.Common;
using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eStream.Models
{
    public class UnitModel
    {
        public int UnitID { get; set; }
        [DisplayName("Tên đơn vị")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        [MaxLength(200, ErrorMessage = "{0} không được vượt quá 200 ký tự")]
        public string UnitName { get; set; }
        [DisplayName("SĐT")]
        [MaxLength(50, ErrorMessage = "{0} không được vượt quá 50 ký tự")]
        public string PhoneNumber { get; set; }
        [DisplayName("Email")]
        [MaxLength(200, ErrorMessage = "{0} không được vượt quá 200 ký tự")]
        public string Email { get; set; }
        public int Status { get; set; }
        public int UnitLevel { get; set; }
        [DisplayName("Đơn vị cha")]
        public int? ParentID { get; set; }
        public string ParentName { get; set; }
        public string UnitPath { get; set; }
        public int CountChild { get; set; }
        public int? DeviceConfigID { get; set; }
        public Unit ToEntity(UnitModel model)
        {
            Unit entity = new Unit();
            entity.UnitID = this.UnitID;
            entity.UnitName = this.UnitName;
            entity.PhoneNumber = this.PhoneNumber;
            entity.Email = this.Email;
            entity.Status = this.Status;
            entity.UnitLevel = this.UnitLevel;
            entity.ParentID = this.ParentID;
            entity.UnitPath = this.UnitPath;
            return entity;
        }

        public void ToModel(Unit entity)
        {
            UnitID = entity.UnitID;
            UnitName = entity.UnitName;
            PhoneNumber = entity.PhoneNumber;
            Email = entity.Email;
            Status = entity.Status;
            UnitLevel = entity.UnitLevel;
            ParentID = entity.ParentID;
            UnitPath = entity.UnitPath;
        }
    }
}