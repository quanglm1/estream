﻿using eStream.Common;
using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eStream.Models
{
    public class DeviceLogModel
    {
        public long DeviceLogID { get; set; }
        public long? LogID { get; set; }
        public short? LogCode { get; set; }
        public string MessageDetail { get; set; }
        public System.DateTime? LogDate { get; set; }
        public int DeviceID { get; set; }
        public bool? Status { get; set; }
    }
}