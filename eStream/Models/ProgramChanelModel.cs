﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Models
{
    public class ProgramChanelModel
    {
        public int ScheduleID { get; set; }
        public int ScheduleDayID { get; set; }
        public int ProgramChanelID { get; set; }
        public string FileURL { get; set; }
        public DateTime ExcuteDate { get; set; }
        public string ChanelName { get;  set; }
        public string UnitName { get; set; }
    }
}