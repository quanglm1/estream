﻿using eStream.Common;
using eStream.Common.CommonClass;
using eStream.Common.CommonObject;
using eStream.Dto;
using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eStream.Models
{
    public class DeviceModel
    {
        public int DeviceID { get; set; }
        [DisplayName("Mã thiết bị")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public string SDeviceCode { get; set; }
        public string Version { get; set; }
        [DisplayName("Tên đơn vị")]
        public Nullable<int> UnitID { get; set; }
        public Nullable<int> IsPlaying { get; set; }
        public string UnitName { get; set; }
        public string VillageName { get; set; }
        public string CommuneName { get; set; }
        public string DistrictName { get; set; }
        public int Status { get; set; }
        public bool SignalStatus { get; set; }
        public System.DateTime CreateDate { get; set; }
        public int? ProvinceID { get; set; }
        public int? DistrictID { get; set; }
        public int? CommuneID { get; set; }
        public int? VillageID { get; set; }
        public string DeviceName { get; set; }
        public string SimSer { get; set; }
        public string DeviceNameWithVolume
        {
            get
            {
                if (string.IsNullOrWhiteSpace(S))
                {
                    return DeviceName;
                }
                if (S.Contains(GlobalConstant.IS_ACTIVE.ToString()))
                {
                    return DeviceName + " (";
                }
                return DeviceName;
            }
        }
        public int MaxS
        {
            get
            {
                if (string.IsNullOrWhiteSpace(S))
                {
                    return 0;
                }
                for (var i = 4; i > 1; i--)
                {
                    if (S[i - 1] == '1')
                    {
                        return i;
                    }
                }
                return 0;
            }
        }
        public string ManagerName { get; set; }
        [DisplayName("Số điện thoại")]
        [RegularExpression(@"^\d+$", ErrorMessage = "Số điện thoại không hợp lệ.")]
        public string ManagerPhone { get; set; }
        [DisplayName("Số thuê bao")]
        [RegularExpression(@"^\d+$", ErrorMessage = "{0} không hợp lệ.")]
        public string SimNumber { get; set; }
        public int? Volume { get; set; }
        public double VolumePercent
        {
            get
            {
                return Volume.GetValueOrDefault() * 1.0 / 100;
            }
        }

        public int? Bitrate { get; set; }
        public bool IsSMS { get; set; }
        public int? IsStopStream { get; set; }
        public int? SyncTime { get; set; }
        public int Timeout { get; set; }
        public int? NoVov { get; set; }
        public string ConnType { get; set; }
        public int? HasLive { get; set; }
        public int? Timer { get; set; }
        public int? FmType { get; set; }
        public string S { get; set; }
        public bool S1 { get; set; }
        public bool S2 { get; set; }
        public bool S3 { get; set; }
        public bool S4 { get; set; }
        public bool DisplayS1 { get { return string.IsNullOrWhiteSpace(S) ? false : S[0] == '1'; } }
        public bool DisplayS2 { get { return string.IsNullOrWhiteSpace(S) ? false : S[1] == '1'; } }
        public bool DisplayS3 { get { return string.IsNullOrWhiteSpace(S) ? false : S[2] == '1'; } }
        public bool DisplayS4 { get { return string.IsNullOrWhiteSpace(S) ? false : S[3] == '1'; } }
        public bool IsSupportFm { get; set; }
        // More info
        public DeviceExtBO DeviceExt
        {
            get
            {
                return StreamingCaching.GetDeviceExt(DeviceID, Timeout);
            }
        }

        public StatusBO StatusBO
        {
            get
            {
                StatusBO status = new StatusBO();
                int isPlaying;
                int isOnline;
                int isFmSwitchOn;
                int? fmStatus;
                GlobalCommon.UpdateDeviceStatus(DeviceID, Timeout, out isPlaying, out isOnline, out fmStatus, out isFmSwitchOn);
                status.IsPlaying = DeviceExt.Streaming;
                status.IsOnline = isOnline;
                status.IsFmSwitchOn = isFmSwitchOn;
                status.IsStop = StreamingCaching.IsStop(DeviceID) ? GlobalConstant.IS_ACTIVE : 0;
                status.FmStatus = fmStatus;
                status.PlayingName = status.IsPlaying == GlobalConstant.IS_ACTIVE ? "Bật" : status.IsStop == GlobalConstant.IS_ACTIVE ? "Tắt" : "N/A";
                status.OnlineName = status.IsOnline == GlobalConstant.IS_ACTIVE ? "Có" : "Không";
                status.FmSwitchOnName = status.IsFmSwitchOn == GlobalConstant.IS_ACTIVE ? "Bật" : status.IsFmSwitchOn == GlobalConstant.IS_NOT_ACTIVE ? "Tắt" : "N/A";
                status.FmStatusName = status.FmStatus == null || status.FmStatus.GetValueOrDefault() < 0 ? "N/A" : status.FmStatus == GlobalConstant.IS_ACTIVE ? "Bật" : "Tắt";
                status.PlayingTypeName = string.Empty;
                status.PlayingSource = string.Empty;
                PlayingDto nowPlaying = null;
                StreamingCaching.DicPlaying.TryGetValue(DeviceID, out nowPlaying);
                if (isOnline == GlobalConstant.IS_ACTIVE && nowPlaying != null)
                {
                    status.PlayingType = nowPlaying.PlayingType;
                    if (nowPlaying.PlayingType > 0)
                    {
                        status.PlayingTypeName = nowPlaying.PlayingType == GlobalConstant.PLAYING_TYPE_LIVE ? "Trực tiếp" : "Theo lịch";
                    }
                    status.PlayingSource = nowPlaying.PlayingSource;
                }
                if (string.IsNullOrWhiteSpace(status.PlayingSource))
                {
                    status.PlayingType = 0;
                    status.PlayingTypeName = string.Empty;
                }
                return status;
            }
        }
    }
}