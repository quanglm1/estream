﻿using eStream.Common.CommonClass;
using eStream.Common.CommonObject;
using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eStream.Models
{
    public class ChannelModel
    {
        public int ChanelID { get; set; }
        [DisplayName("Mã kênh")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public string ChannelCode { get; set; }
        [DisplayName("Tên kênh")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public string ChanelName { get; set; }
        [DisplayName("Tên kênh")]
        public string LinkUrl { get; set; }
        public Nullable<int> NumberOrder { get; set; }
        public Nullable<int> UnitID { get; set; }
        public string UnitName { get; set; }
        public bool IsGlobal { get; set; }
        public bool IsDefault { get; set; }
        public bool IsFm { get; set; }
        [DisplayName("Tần số FM")]
        [RegularExpression(@"^[-+]?[0-9]*\.?[0-9]$", ErrorMessage = "{0} phải là số thập phân có nhiều nhất 1 chữ số sau dấu phẩy từ 78.0MHz đến 108.0MHz")]
        [Range(GlobalConstant.FM_MIN_FREQ, GlobalConstant.FM_MAX_FREQ, ErrorMessage = "{0} phải là số thập phân có nhiều nhất 1 chữ số sau dấu phẩy từ 78.0MHz đến 108.0MHz")]
        public double? FmFreq { get; set; }

        public void UpdateEntity(ForwardChanel entity, UserInfoBO user)
        {
            entity.ChannelCode = this.ChannelCode.Trim();
            entity.ChanelName = this.ChanelName.Trim();
            entity.NumberOrder = this.NumberOrder;
            entity.LinkUrl = this.LinkUrl;
            entity.IsDefault = null;
            entity.Fm = null;
            entity.FmFreq = null;
            if (user.IsSupperAdmin.GetValueOrDefault() == GlobalConstant.IS_ACTIVE && this.IsGlobal)
            {
                entity.UnitID = null;
            }
            if (this.IsDefault)
            {
                entity.IsDefault = GlobalConstant.IS_ACTIVE;
            }
            if (this.IsFm)
            {
                entity.Fm = GlobalConstant.IS_ACTIVE;
                entity.FmFreq = this.FmFreq;
            }
        }
    }
}