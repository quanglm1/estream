﻿using eStream.Common.CommonClass;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Models
{
    public class ScheduleModel
    {
        public int ScheduleID { get; set; }
        public string ScheduleName { get; set; }
        public DateTime? StartDateOfWeek { get; set; }
        public List<ScheduleDayModel> ListScheduleDay { get; set; }
        private string _ScheduleFullTime;
        public string ScheduleFullTime
        {
            get
            {
                string s = ScheduleHour == null ? string.Empty : (ScheduleHour < 10 ? "0" + ScheduleHour : ScheduleHour + "") + ":" + (ScheduleMinus < 10 ? "0" + ScheduleMinus : ScheduleMinus + "");
                return s;
            }
            set { _ScheduleFullTime = value; }
        }
        private string _OffFullTime;
        public string OffFullTime
        {
            get
            {
                string s = OffHour == null ? string.Empty : (OffHour < 10 ? "0" + OffHour : OffHour + "") + ":" + (OffMinus < 10 ? "0" + OffMinus : OffMinus + "");
                return s;
            }
            set { _OffFullTime = value; }
        }
        public int? ScheduleHour { get; set; }
        public int? ScheduleMinus { get; set; }
        public int? OffHour { get; set; }
        public int? OffMinus { get; set; }
        public int? Status { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? UnitID { get; set; }
        public int? Frequency { get; set; }
        public bool IsHidden { get; set; }
        public int? IsStopStream { get; set; }
        public List<int> ListDay { get; set; }
        public List<int> ListExceptUnitID { get; set; }

        public void TransferToModel(Schedule Entity)
        {
            Entity.ScheduleID = ScheduleID;
            if (!string.IsNullOrWhiteSpace(_ScheduleFullTime))
            {
                var Time = _ScheduleFullTime.Split(':');
                if (!int.TryParse(Time.FirstOrDefault(), out int ScheduleHour))
                {
                    throw new BusinessException("Dữ liệu thời gian không hợp lệ");
                }
                if (!int.TryParse(Time.LastOrDefault(), out int ScheduleMinus))
                {
                    throw new BusinessException("Dữ liệu thời gian không hợp lệ");
                }
                Entity.ScheduleHour = ScheduleHour;
                Entity.ScheduleMinus = ScheduleMinus;
            }
            Entity.ScheduleName = ScheduleName;
            if (!string.IsNullOrWhiteSpace(_OffFullTime))
            {
                var OffTime = _OffFullTime.Split(':');
                if (!int.TryParse(OffTime.FirstOrDefault(), out int OffHour))
                {
                    throw new BusinessException("Dữ liệu thời gian tắt không hợp lệ");
                }
                if (!int.TryParse(OffTime.LastOrDefault(), out int OffMinus))
                {
                    throw new BusinessException("Dữ liệu thời gian tắt không hợp lệ");
                }
                Entity.OffHour = OffHour;
                Entity.OffMinus = OffMinus;
                if (Entity.ScheduleHour != null)
                {
                    if (Entity.ScheduleHour > Entity.OffHour)
                    {
                        throw new BusinessException("Thời gian kết thúc phải lớn hơn thời gian bắt đầu");
                    }
                    else if (Entity.ScheduleHour == Entity.OffHour && Entity.OffMinus <= Entity.ScheduleMinus)
                    {
                        throw new BusinessException("Thời gian kết thúc phải lớn hơn thời gian bắt đầu");
                    }
                }
            }
            Entity.StartMinus = Entity.ScheduleHour != null ? 60 * Entity.ScheduleHour + Entity.ScheduleMinus : null;
            Entity.EndMinus = Entity.OffHour != null ? 60 * Entity.OffHour + Entity.OffMinus : null;
            Entity.Frequency = Frequency;
            Entity.IsHidden = null;
            Entity.IsStopStream = GlobalConstant.IS_ACTIVE;
        }
    }
}