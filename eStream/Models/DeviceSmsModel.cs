﻿using eStream.Common;
using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eStream.Models
{
    public class DeviceSmsModel
    {
        public long ID { get; set; }
        public string Message { get; set; }
        public System.DateTime? CreatedDate { get; set; }
        public int DeviceID { get; set; }
    }
}