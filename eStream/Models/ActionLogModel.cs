﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eStream.Models
{
    public class ActionLogModel
    {
        public string IPAddress { get; set; }
        public string ActionName { get; set; }
        public string UserName { get; set; }
        public int UserID { get; set; }
        public string ActionDescription { get; set; }
        public DateTime ActionTime { get; set; }
        [DisplayName("Từ ngày")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DataType(DataType.DateTime)]
        public DateTime? FromDate { get; set; }

        [DisplayName("Tới ngày")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DataType(DataType.DateTime)]
        public DateTime? ToDate { get; set; }
    }
}