﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace eStream.Models
{
    public class ConvertTextModel
    {
        public long LiveID { get; set; }
        public int ScheduleID { get; set; }
        public int ScheduleDayID { get; set; }
        [DisplayName("Tiêu đề")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public string Title { get; set; }
        [DisplayName("Nội dung")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public string Text { get; set; }
        [DisplayName("Giọng đọc")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public string Voice { get; set; }
        [DisplayName("Tốc độ đọc")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public double Rate { get; set; }
    }
}