﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eStream.Models
{
    public class ChangePasswordModel
    {
        public int UserId { get; set; }
        [DisplayName("Mật khẩu cũ")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public string Password { get; set; }
        [DisplayName("Mật khẩu mới")]
        public string NewPassword { get; set; }
        [DisplayName("Xác nhận mật khẩu mới")]
        [Compare("NewPassword", ErrorMessage = "Xác nhận mật khẩu mới không chính xác")]
        public string ConfirmNewPassword { get; set; }
        [DisplayName("Số điện thoại")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        [RegularExpression(@"^\+?(\d[\d ]+)?(\([\d ]+\))?[\d ]+\d$", ErrorMessage ="Sai định dạng số điện thoại")]
        [StringLength(15, ErrorMessage = "Sai định dạng số điện thoại")]
        public string PhoneNumber { get; set; }
        public string FullName { get; set; }
        public bool IsMember { get; set; }
        public HttpPostedFileWrapper UserAvatar { get; set; }
        public byte[] Avatar { get; set; }
    }
}