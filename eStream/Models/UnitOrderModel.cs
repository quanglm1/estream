﻿using eStream.Common;
using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eStream.Models
{
    public class UnitOrderModel
    {
        public int UnitType { get; set; }
        public int NumberOrder { get; set; }
    }
}