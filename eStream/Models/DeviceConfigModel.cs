﻿using eStream.Common;
using eStream.Common.CommonClass;
using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eStream.Models
{
    public class DeviceConfigModel
    {
        public int? DeviceConfigID { get; set; }
        [DisplayName("Thời gian gửi log")]
        [RegularExpression("([0-9]+)", ErrorMessage = "Vui lòng nhập kiểu số")]
        public Nullable<int> SyncTime { get; set; }
        [DisplayName("Âm lượng")]
        [RegularExpression("([0-9]+)", ErrorMessage = "Vui lòng nhập kiểu số")]
        [Range(0, 100, ErrorMessage = "Please enter valid integer Number")]
        public Nullable<int> DefaultVolume { get; set; }
        [DisplayName("Xã")]
        public int UnitID { get; set; }
        public string UnitName { get; set; }
        public bool IsSMS { get; set; }
        [DisplayName("Chất lượng âm thanh (Bitrate)")]
        public Nullable<int> BitRate { get; set; }

        public void EntityTo(DeviceConfig entity, Unit unit)
        {
            if (entity != null)
            {
                this.DeviceConfigID = entity.DeviceConfigID;
                this.BitRate = entity.BitRate;
                this.DefaultVolume = entity.DefaultVolume;
                this.IsSMS = entity.IsSMS.GetValueOrDefault() == GlobalConstant.IS_ACTIVE;
                this.SyncTime = entity.SyncTime;
            }
            this.UnitID = unit.UnitID;
            this.UnitName = unit.UnitName;
        }

        public void UpdateEntity(DeviceConfig entity)
        {
            entity.BitRate = this.BitRate;
            entity.DefaultVolume = this.DefaultVolume;
            if (this.IsSMS)
            {
                entity.IsSMS = GlobalConstant.IS_ACTIVE;
            }
            else
            {
                entity.IsSMS = null;
            }
            entity.SyncTime = this.SyncTime;
            entity.UnitID = this.UnitID;
        }
    }
}