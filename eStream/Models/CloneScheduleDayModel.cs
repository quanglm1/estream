﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace eStream.Models
{
    public class CloneScheduleDayModel
    {
        [DisplayName("Khung thời gian")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public int CloneScheduleID { get; set; }

        [DisplayName("Ngày")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public string CloneDate { get; set; }
        public int ScheduleID { get; set; }
        public int ScheduleDayID { get; set; }
        public DateTime Date { get; set; }
    }
}