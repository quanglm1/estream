﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Models
{
    public class SendSmsHisModel
    {
        public int SendSmsHisID { get; set; }
        public DateTime? SendTime { get; set; }
        public string SendRequest { get; set; }
        public string SendResponse { get; set; }
        public string MsIsdn { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public int? UnitID { get; set; }
    }
}