﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Models
{
    public class ScheduleProgramModel
    {
        public int ScheduleID { get; set; }
        public int ScheduleDayID { get; set; }
        public int ProgramID { get; set; }
        public string Title { get; set; }
        public string FileName { get; set; }
        public string FileURL { get; set; }
        public int? Status { get; set; }
        public DateTime? ExcuteDate { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UnitName { get; set; }
        public string UserName { get; set; }
    }
}