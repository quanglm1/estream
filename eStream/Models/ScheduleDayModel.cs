using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Models
{
    public class ScheduleDayModel
    {
        public int DayOfWeek { get; set; }
        public int ScheduleDayID { get; set; }
        public int ScheduleID { get; set; }
        public string DayName { get; set; }
        public List<ScheduleProgramModel> ListProgram { get; set; }
        public bool HasComProgram { get; set; }
        public bool HasDisProgram { get; set; }
        public bool HasPrvProgram { get; set; }
        public bool HasVilProgram { get; set; }
        public int? ChannelID { get; set; }
        public bool HasChannel { get; set; }
        public string VilDuration { get; set; }
        public string ComDuration { get; set; }
        public string DisDuration { get; set; }
        public string PrvDuration { get; set; }
        public bool IsLoop { get; set; }
        public int? LoopCount { get; set; }
    }
}