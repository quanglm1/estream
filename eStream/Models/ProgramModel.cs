﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Models
{
    public class ProgramModel
    {
        public int ProgramID { get; set; }
        public string FileUrl { get; set; }
        public int? Status { get; set; }
        public int UnitID { get; set; }
        public int ScheduleDayID { get; set; }
        public DateTime? ExecuteDate { get; set; }
    }
}