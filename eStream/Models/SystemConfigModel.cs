﻿using eStream.Common;
using eStream.Common.CommonClass;
using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eStream.Models
{
    public class SystemConfigModel
    {
        public bool IsUseOTP { get; set; }
        public int? ExpOTP { get; set; }

        
        internal void UpdateEntity(SystemConfig config)
        {
            config.IsUseOTP = null;
            config.ExpOTP = null;
            if (IsUseOTP)
            {
                config.IsUseOTP = GlobalConstant.IS_ACTIVE;
                config.ExpOTP = ExpOTP;
            }
        }
    }
}