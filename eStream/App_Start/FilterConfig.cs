﻿using eStream.Common.CommonClass;
using System.Web;
using System.Web.Mvc;
using eStream.Helpers.Filter;

namespace eStream
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new AuthorizeFilter());
            filters.Add(new ActionLogFilter());
            filters.Add(new ExceptionFilter());
        }
    }
}
