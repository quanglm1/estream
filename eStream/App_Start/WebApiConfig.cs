﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Routing;

namespace eStream.App_Start
{
    public class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                 "ApiMbfGet",
                "api/mbf/schedules/{id}",
                new { controller = "MbfSchedule", action = "Get" },
                new { httpMethod = new HttpMethodConstraint(new HttpMethod[] { new HttpMethod("GET") }) }
           );
            config.Routes.MapHttpRoute(
                "ApiMbfList",
                "api/mbf/schedules",
                new { controller = "MbfSchedule", action = "List" },
                new { httpMethod = new HttpMethodConstraint(new HttpMethod[] { new HttpMethod("GET") }) }
            );
            config.Routes.MapHttpRoute(
                "ApiMbfPost",
                "api/mbf/schedules",
                new { controller = "MbfSchedule", action = "Post" },
                new { httpMethod = new HttpMethodConstraint(new HttpMethod[] { new HttpMethod("POST") }) }
            );
            config.Routes.MapHttpRoute(
                "ApiMbfCancel",
                "api/mbf/schedules/cancel",
                new { controller = "MbfSchedule", action = "Cancel" },
                new { httpMethod = new HttpMethodConstraint(new HttpMethod[] { new HttpMethod("POST") }) }
            );

            config.Routes.MapHttpRoute(
                "ApiMbfStatus",
                "api/mbf/schedules_status",
                new { controller = "MbfSchedule", action = "Status" },
                new { httpMethod = new HttpMethodConstraint(new HttpMethod[] { new HttpMethod("GET") }) }
            );

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
        }
    }
}