﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClosedXML.Excel;
using System.IO;

namespace eStream.Common.CommonExcel
{
    public class XVTWorkbook : IXVTWorkbook
    {
        #region Declare Variable
        public ClosedXML.Excel.XLWorkbook Workbook { get; set; }

        /// <summary>
        /// Declare Workbook
        /// </summary>
        /// <param name="workbook"></param>
        public XVTWorkbook(ClosedXML.Excel.XLWorkbook workbook)
        {
            Workbook = workbook;
        }
        #endregion

        #region Method of Object

        /// <summary>
        /// Get Worksheet by index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public IXVTWorksheet GetSheet(int index)
        {
            return new XVTWorksheet(Workbook.Worksheet(index));
        }

        /// <summary>
        /// Copy Sheet to Next Position
        /// </summary>
        /// <param name="worksheet"></param>
        /// <param name="sheetName"></param>
        /// <returns></returns>
        public IXVTWorksheet CopySheet(IXVTWorksheet worksheet, string sheetName = "New Scheet")
        {   
            // Check neu sheet name da ton tai them so vao phia sau
            if (Workbook.Worksheets.Any(a => a.Name == sheetName))
            {
                sheetName = sheetName + "01";
            }
            worksheet.CopyTo(sheetName);
            return new XVTWorksheet(Workbook.Worksheets.Last());
        }

        public void CreateSheet(string sheetName = "New Scheet") {

            if (Workbook.Worksheets.Any(a => a.Name == sheetName))
            {
                sheetName = sheetName + "01";
            }
            Workbook.Worksheets.Add(sheetName);
        }

        /// <summary>
        /// Convert Workbook to stream
        /// </summary>
        /// <returns></returns>
        public Stream ToStream()
        {
            MemoryStream outPut = new MemoryStream();
            Workbook.SaveAs(outPut);
            if (outPut != null)
            {
                // return the filestream
                // Rewind the memory stream to the beginning
                outPut.Seek(0, SeekOrigin.Begin);
            }
            return outPut;
        }
        #endregion

        #region Properties Of Object
        #endregion
    }
}
