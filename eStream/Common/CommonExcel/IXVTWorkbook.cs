﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace eStream.Common.CommonExcel
{
    public interface IXVTWorkbook
    {
        /// <summary>
        /// Get Worksheet by index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        /// <author>hungnd8</author>
        IXVTWorksheet GetSheet(int index);

        /// <summary>
        /// Copy Sheet to Next Position
        /// </summary>
        /// <param name="worksheet"></param>
        /// <param name="SheetName"></param>
        /// <returns></returns>
        /// <author>hungnd8</author>
        IXVTWorksheet CopySheet(IXVTWorksheet worksheet, string SheetName = "New Scheet");
        void CreateSheet(string SheetName = "New Scheet");
        /// <summary>
        /// Convert Workbook to stream
        /// </summary>
        /// <returns></returns>
        /// <author>hungnd8</author>
        Stream ToStream();
    }
}
