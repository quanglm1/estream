﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace eStream.Common.CommonExcel
{
    public class XVTWorksheet : IXVTWorksheet
    {
        #region Declare Variable
        public ClosedXML.Excel.IXLWorksheet Worksheet { get; set; }

        /// <summary>
        /// Declate Worksheet
        /// </summary>
        /// <param name="worksheet"></param>
        public XVTWorksheet(IXLWorksheet worksheet)
        {
            Worksheet = worksheet;
        }

        /// <summary>
        /// Declate Worksheet
        /// </summary>
        public XVTWorksheet()
        {
        }
        #endregion

        #region Method of Object
        /// <summary>
        /// Get Range by first address and last Address
        /// </summary>
        /// <param name="firstCellAddress"></param>
        /// <param name="lastCellAddress"></param>
        /// <returns></returns>
        public IXVTRange GetRange(string firstCellAddress, string lastCellAddress)
        {
            return new XVTRange(Worksheet.Range(firstCellAddress, lastCellAddress));
        }

        /// <summary>
        /// Get range by ColNum and RowNum
        /// </summary>
        /// <param name="firstRow"></param>
        /// <param name="firstCoumn"></param>
        /// <param name="lastRow"></param>
        /// <param name="lastColumn"></param>
        /// <returns></returns>
        public IXVTRange GetRange(int firstRow, int firstCoumn, int lastRow, int lastColumn)
        {
            return new XVTRange(Worksheet.Range(firstRow, firstCoumn, lastRow, lastColumn));
        }

        /// <summary>
        /// Get Range by Address
        /// </summary>
        /// <param name="cellAddress"></param>
        /// <returns></returns>
        public IXVTRange GetRange(string cellAddress)
        {
            return new XVTRange(Worksheet.Range(cellAddress));
        }

        /// <summary>
        /// Set Cell Value by Address
        /// </summary>
        /// <param name="cellAddress"></param>
        /// <param name="value"></param>
        public void SetCellValue(string cellAddress, string value)
        {
            IXVTRange xCell = new XVTRange(Worksheet.Range(cellAddress));
            xCell.Value = value;
        }

        /// <summary>
        /// Set Cell Value
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <param name="value"></param>
        public void SetCellValue(int row, int column, string value)
        {
            IXVTRange xCell = new XVTRange(Worksheet.Range(row, column, row, column));
            xCell.Value = value;
        }

        /// <summary>
        /// Delete defined Sheet
        /// </summary>
        public void Delete()
        {
            Worksheet.Delete();
        }

        /// <summary>
        /// Copy một khoảng dữ liệu (tập các Cell) sang một khoảng khác trong sheet (Độ cao của các Row sẽ không được Copy)
        /// </summary>
        /// <param name="xRange"></param>
        /// <param name="firstRow">Vị trí hàng đầu tiên của khoảng sẽ paste dữ liệu</param>
        /// <param name="firstColumn">Vị trí cột đầu tiên của khoảng sẽ paste dữ liệu</param>
        /// <param name="copyStyleOnly"></param>
        public void CopyPaste(IXVTRange xRange, int firstRow, int firstColumn, bool copyStyleOnly = false)
        {
            Worksheet.Range(firstRow, firstColumn, firstRow + xRange.RowsCount - 1,
                 firstColumn + xRange.ColumnsCount - 1);
        }

        /// <summary>
        /// Copy Anything of row to target row
        /// </summary>
        /// <param name="poisionTempRow"></param>
        /// <param name="firstRow"></param>
        /// <param name="lastRow"></param>
        public void CopyRow(int poisionTempRow, int firstRow, int lastRow)
        {
            IXLRow tmpRow = Worksheet.Row(poisionTempRow);
            for (int i = firstRow; i <= lastRow; i++)
            {
                var targetRow = Worksheet.Row(i);
                tmpRow.CopyTo(targetRow);
            }
        }

        /// <summary>
        /// Copy Sheet
        /// </summary>
        /// <param name="SheetName"></param>
        public void CopyTo(string SheetName)
        {
            Worksheet.CopyTo(SheetName);
        }

        /// <summary>
        /// Copy Sheet
        /// </summary>
        /// <param name="sheetName"></param>
        /// <param name="position"></param>
        public void CopyTo(string sheetName, int position)
        {

            Worksheet.CopyTo(sheetName, position);
        }

        /// <summary>
        /// Hide Row
        /// </summary>
        /// <param name="row"></param>
        public void HideRow(int row)
        {
            Worksheet.Row(row).Hide();
        }

        /// <summary>
        /// Delete Row
        /// </summary>
        /// <param name="row"></param>
        public void DeleteRow(int row)
        {
            Worksheet.Row(row).Delete();
        }

        public object GetValueOfCell(string address)
        {
            return Worksheet.Cell(address).Value;
        }

        public object GetValueOfCell(int row, int col)
        {
            return Worksheet.Cell(row, col).Value;
        }

        public void SetValueOfCell(string address, object value)
        {
            Worksheet.Cell(address).Value = value;
        }

        public void SetValueOfCell(int row, int col, object value)
        {
            Worksheet.Cell(row, col).Value = value;
        }

        public void AddCellComment(int row, int col, string value)
        {
            Worksheet.Cell(row, col).Style.Font.FontColor = XLColor.Red;
            if (Worksheet.Cell(row, col).Comment.Count > 0)
            {
                Worksheet.Cell(row, col).Comment.AddNewLine();
            }
            Worksheet.Cell(row, col).Comment.AddText(value);
        }

        public void SetCellFont(int row, int col, string fontName, int fontSize, bool bold, bool italic)
        {
            Worksheet.Cell(row, col).Style.Font.FontName = fontName;
            Worksheet.Cell(row, col).Style.Font.FontSize = fontSize;
            Worksheet.Cell(row, col).Style.Font.SetBold(bold);
            Worksheet.Cell(row, col).Style.Font.SetItalic(italic);
        }

        public void SetRangeFont(int firstRow, int firstCol, int lastRow, int lastCol, string fontName, int fontSize, bool bold, bool italic)
        {
            GetRange(firstRow, firstCol, lastRow, lastCol).Range.Style.Font.FontName = fontName;
            GetRange(firstRow, firstCol, lastRow, lastCol).Range.Style.Font.FontSize = fontSize;
            GetRange(firstRow, firstCol, lastRow, lastCol).Range.Style.Font.SetBold(bold);
            GetRange(firstRow, firstCol, lastRow, lastCol).Range.Style.Font.SetItalic(italic);
        }

        public void SetCellBorderThin(int row, int col)
        {
            Worksheet.Cell(row, col).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
            Worksheet.Cell(row, col).Style.Border.RightBorder = XLBorderStyleValues.Thin;
            Worksheet.Cell(row, col).Style.Border.TopBorder = XLBorderStyleValues.Thin;
            Worksheet.Cell(row, col).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
        }

        public void SetRangeBorderThin(int firstRow, int firstCol, int lastRow, int lastCol)
        {
            GetRange(firstRow, firstCol, lastRow, lastCol).Range.Style.Border.LeftBorder = XLBorderStyleValues.Thin;
            GetRange(firstRow, firstCol, lastRow, lastCol).Range.Style.Border.RightBorder = XLBorderStyleValues.Thin;
            GetRange(firstRow, firstCol, lastRow, lastCol).Range.Style.Border.TopBorder = XLBorderStyleValues.Thin;
            GetRange(firstRow, firstCol, lastRow, lastCol).Range.Style.Border.BottomBorder = XLBorderStyleValues.Thin;
        }

        public void SetCellHorizontal(int row, int col, XLAlignmentHorizontalValues align)
        {
            Worksheet.Cell(row, col).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
        }

        public void SetCellVertical(int row, int col, XLAlignmentVerticalValues valign)
        {
            Worksheet.Cell(row, col).Style.Alignment.SetVertical(valign);
        }

        public void SetBackgroundColor(int row, int col, XLColor color)
        {
            Worksheet.Cell(row, col).Style.Fill.SetBackgroundColor(color);
        }

        public void SetCellFormula(int row, int col, string formula)
        {
            Worksheet.Cell(row, col).SetFormulaA1(formula);
        }
        public void SetSheetWrapText()
        {
            Worksheet.Style.Alignment.WrapText = true;
        }
        public void SetAutoFitColumn(int row, int col)
        {
            Worksheet.Rows(row, col).AdjustToContents();
        }
        public void SetRowBold(int row)
        {
            Worksheet.Row(row).Style.Font.SetBold(true);
        }
        public void AddColumns(string cellAddress, int numberColumn)
        {
            GetRange(cellAddress).Range.InsertColumnsAfter(numberColumn);
        }
        public void SetCellFormat(int row, int column, string format)
        {
            Worksheet.Cell(row, column).Style.NumberFormat.Format = format;
        }
        public void CopyRow(int tempRow, int row)
        {
            IXLRow tmpRow = Worksheet.Row(tempRow);

            var targetRow = Worksheet.Row(row);
            tmpRow.CopyTo(targetRow);
        }
        #endregion

        #region Properties
        /// <summary>
        /// Get or Set Sheet Name
        /// </summary>
        public string SheetName
        {
            get
            {
                return Worksheet.Name;
            }
            set
            {
                Worksheet.Name = value;
            }
        }

        #endregion
    }
}
