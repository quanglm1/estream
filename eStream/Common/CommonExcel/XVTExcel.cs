﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace eStream.Common.CommonExcel
{
    public class XVTExcel
    {
        /// <summary>
        /// Create workbook ny file path
        /// </summary>
        /// <param name="templateFilePath"></param>
        /// <returns></returns>
        /// <author>hungnd8</author>
        public static IXVTWorkbook OpenWorkbook(string templateFilePath)
        {
            return new XVTWorkbook(new ClosedXML.Excel.XLWorkbook(templateFilePath));
        }
        /// <summary>
        ///  Create workbook from file stream
        /// </summary>
        /// <param name="fileStream"></param>
        /// <returns></returns>
        /// <author>hungnd8</author>
        public static IXVTWorkbook OpenWorkbook(Stream fileStream)
        {
            try
            {
                return new XVTWorkbook(new ClosedXML.Excel.XLWorkbook(fileStream));
            }
            catch (Exception)
            {
                return null;
            }
        }

        public void ReadingExcel(Stream fileUpload)
        {
            IXVTWorkbook oBook = XVTExcel.OpenWorkbook(fileUpload);
            IXVTWorksheet firstSheet = oBook.GetSheet(1);
            var endRowUsed = firstSheet.Worksheet.LastRowUsed();
            string A1 = firstSheet.GetValueOfCell("A1").ToString();
            string A2 = firstSheet.GetValueOfCell("A2").ToString();
            string A3 = firstSheet.GetValueOfCell("A3").ToString();
            string A4 = firstSheet.GetValueOfCell("A4").ToString();
            string A5 = firstSheet.GetValueOfCell("A5").ToString();


            A1 = firstSheet.GetValueOfCell(1, 1).ToString();
            A2 = firstSheet.GetValueOfCell(2, 1).ToString();
            A3 = firstSheet.GetValueOfCell(3, 1).ToString();
            A4 = firstSheet.GetValueOfCell(4, 1).ToString();
            A5 = firstSheet.GetValueOfCell(5, 1).ToString();
        }
    }
}
