﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Common.CommonObject
{
    public class VolumeStatusBO
    {
        public int S1 { get; set; }
        public int S2 { get; set; }
        public int S3 { get; set; }
        public int S4 { get; set; }
        public DateTime? LastUpdate { get; set; }
    }
}