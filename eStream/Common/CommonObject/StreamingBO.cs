﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Common.CommonObject
{
    public class StreamingBO
    {
        public bool IsPlaying { get; set; }
        public bool IsOnline { get; set; }
        public bool IsStop { get; set; }
        public bool IsVOV { get; set; }
        public int? FmStatus { get; set; }
        public DateTime? LastTime { get; set; }
        public DateTime? LastTimePlaying { get; set; }
        public DateTime? LastTimeFm { get; set; }
        public double? CountPlayingTime { get; set; }
        public DateTime? FirstTimePlaying { get; set; }
        public int IsFmSwitchOn { get; set; }

        public double GetStopingTime()
        {
            if(LastTime == null || LastTimePlaying == null)
            {
                return 0;
            }
            return (LastTime.Value - LastTimePlaying.Value).TotalSeconds;
        }
    }
}