﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Common.CommonObject
{
    public class ReqCreateProgram
    {
        public DateTime startTime { get; set; }
        public string channel { get; set; }
        public string filePaths { get; set; }
    }
}