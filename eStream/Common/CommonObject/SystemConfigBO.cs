﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Common.CommonObject
{
    public class SystemConfigBO
    {
        public int? StreamingInterval { get; set; }
        public int? StreamingCountFile { get; set; }
        public Nullable<int> IsWaitingOn { get; set; }
    }
}