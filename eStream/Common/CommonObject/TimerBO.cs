﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Common.CommonObject
{
    public class TimerBO
    {
        public int Count { get; set; }
        public DateTime FirstPlayingTime { get; set; }
    }
}