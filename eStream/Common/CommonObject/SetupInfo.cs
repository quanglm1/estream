﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Common.CommonObject
{
    public class SetupInfo
    {
        public DateTime StartTime { get; set; }
        public String Channel { get; set; }
        public List<AudioInfo> AudioInfos { get; set; }
    }
}