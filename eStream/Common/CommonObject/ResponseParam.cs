﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Common.CommonObject
{
    public class ResponseParam
    {
        public string ERROR_DESC { get; set; }

        public string name { get; set; }

        public string ERROR { get; set; }
        
    }
}