﻿using eStream.Dto;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace eStream.Common.CommonObject
{
    public class DeviceExtBO
    {
        public string RamUsage { get; set; }
        public string ProName { get; set; }
        public string ConnType { get; set; }
        public string ConnMod { get; set; }
        public string RamTotal { get; set; }
        public string CpuFreq { get; set; }
        public string CpuTemp { get; set; }
        public string CpuUsage { get; set; }
        public int SignStrength { get; set; }
        public string GpsLoc { get; set; }
        public DateTime? LastUpdate { get; set; }
        public int Streaming { get; set; }

        public ExtDisplay Display
        {
            get
            {
                var dis = new ExtDisplay();
                if (string.IsNullOrWhiteSpace(ConnType))
                {
                    return dis;
                }
                switch (ConnType.ToUpper())
                {
                    case "LAN":
                        dis.ClassDis = "lan";
                        dis.TextDis = "LAN";
                        dis.SortDis = "LAN";
                        break;
                    case "WIFI":
                        dis.ClassDis = "wifi";
                        dis.TextDis = "Wifi";
                        dis.SortDis = "Wifi";
                        break;
                    case "COMM":
                        dis.ClassDis = "signal" + SignStrength;
                        dis.TextDis = ProName + " " + ConnMod;
                        dis.SortDis = ConnMod;
                        break;
                }
                return dis;
            }
        }

        public Loc Loc
        {
            get
            {
                if (string.IsNullOrWhiteSpace(GpsLoc))
                {
                    return new Loc
                    {
                        Lat = "",
                        Long = "",
                    };
                }
                var ar = GpsLoc.Split(',');
                var lat = ar[1];
                lat = lat.Substring(0, lat.Length - 1);
                var fLat = float.Parse(lat, CultureInfo.InvariantCulture.NumberFormat);
                var lng = ar[2];
                lng = lng.Substring(0, lng.Length - 1);
                var fLng = float.Parse(lng, CultureInfo.InvariantCulture.NumberFormat);
                var dmsLoc = new DmsLocation
                {
                    Latitude = new DmsPoint(fLat),
                    Longitude = new DmsPoint(fLng)
                };
                var loc = dmsLoc.Convert();
                var l = new Loc
                {
                    Lat = loc.Latitude.ToString().Replace(",", "."),
                    Long = loc.Longitude.ToString().Replace(",", ".")
                };
                return l;
            }
        }
    }

    public class ExtDisplay
    {
        public string ClassDis { get; set; }
        public string TextDis { get; set; }
        public string SortDis { get; set; }
    }

    public class Loc
    {
        public string Lat { get; set; }
        public string Long { get; set; }

        public string Display()
        {
            string lat = string.IsNullOrWhiteSpace(Lat) ? string.Empty : Lat;
            string lng = string.IsNullOrWhiteSpace(Long) ? string.Empty : Long;
            return lat + "," + lng;
        }
    }
}