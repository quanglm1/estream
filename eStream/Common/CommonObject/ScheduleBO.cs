﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Common.CommonObject
{
    public class ScheduleBO
    {
        public int ScheduleID { get; set; }
        public int? StartMinus { get; set; }
        public int? EndMinus { get; set; }
        public bool Ishidden { get; set; }
        public int? IsStopStream { get; set; }
        public List<int> ListExceptUnitID { get; set; }
    }
}