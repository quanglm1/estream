﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Common.CommonObject
{
    public class BroadcastInfo
    {
        public string FilePath { get; internal set; }
        public DateTime StartTime { get; internal set; }
        public double Duration { get; internal set; }
        public DateTime EndTime { get; internal set; }
        public long NumberOfSplitFile { get; internal set; }
        public string TempFolder { get; internal set; }
        public double StartTimestamp { get; internal set; }
        public double EndTimeStamp { get; internal set; }
    }
}