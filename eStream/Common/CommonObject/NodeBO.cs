﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Common.CommonObject
{
    public class NodeBO
    {
        public int id { get; set; }
        public string text { get; set; }
        public bool children { get; set; }
    }
}