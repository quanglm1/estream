﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Common.CommonObject
{
    public class ChartBO
    {
        public double Value { get; set; }
        /// <summary>
        /// Dung de hien thi
        /// </summary>
        public string Label { get; set; }
        /// <summary>
        /// Dung de so sanh
        /// </summary>
        public string LabelValue { get; set; }
    }
}