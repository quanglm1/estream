﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
namespace eStream.Common.CommonObject
{
    public class ReqJson
    {
        [JsonProperty("RQST")]
        public RequestParam Rqst { get; set; }
    }
}