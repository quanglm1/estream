﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Common.CommonObject
{
    public class AuthorizeBO
    {
        public int ActionID { get; set; }
        public string ActionUrl { get; set; }
    }
}