﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Common.CommonObject
{
    public class ReqDeleteProgram
    {
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string Channel { get; set; }
    }
}