﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Common.CommonObject
{
    public class RequestParam
    {
        [JsonProperty("PARAMS")]
        public List<ReqParam> Params { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("REQID")]
        public string Reqid { get; set; }

        [JsonProperty("LABELID")]
        public string Labelid { get; set; }
        [JsonProperty("CONTRACTTYPEID")]
        public string Contracttypeid { get; set; }
        [JsonProperty("CONTRACTID")]
        public string Contractid { get; set; }
        [JsonProperty("TEMPLATEID")]
        public string Templateid { get; set; }
        [JsonProperty("SCHEDULETIME")]
        public string Scheduletime { get; set; }
        [JsonProperty("MOBILELIST")]
        public string Mobilelist { get; set; }
        [JsonProperty("ISTELCOSUB")]
        public string Istelcosub { get; set; }
        [JsonProperty("AGENTID")]
        public string Agentid { get; set; }
        [JsonProperty("APIUSER")]
        public string Apiuser { get; set; }
        [JsonProperty("APIPASS")]
        public string Apipass { get; set; }
        [JsonProperty("USERNAME")]
        public string Username { get; set; }
        [JsonProperty("MAXITEM")]
        public string Maxitem { get; set; }
    }
}