﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Common.CommonObject
{
    public class ReqParam
    {
        [JsonProperty("NUM")]
        public string Num { get; set; }

        [JsonProperty("CONTENT")]
        public string Content { get; set; }

    }
}