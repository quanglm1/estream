﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Common.CommonObject
{
    public class StatusBO
    {
        public int ID { get; set; }
        public int IsPlaying { get; set; }
        public string PlayingName { get; set; }
        public int IsOnline { get; set; }
        public string OnlineName { get; set; }
        public int IsStop { get; set; }
        public int IsFmSwitchOn { get; set; }
        public string FmSwitchOnName { get; set; }
        public int? FmStatus { get; set; }
        public string FmStatusName { get; set; }
        public int PlayingType { get; set; }
        public string PlayingTypeName { get; set; }
        public string PlayingSource { get; set; }
        public string VolumeStatus { get; set; }
        public string NetDisClass { get; set; }
        public string NetDisText { get; set; }
        public string ProName { get; set; }
        public int DisSms { get; set; }
        public int DisMap { get; set; }
        public string Gps { get; set; }

        // pos start from 1
        public bool IsVolumeOn(int pos)
        {
            var s = !string.IsNullOrWhiteSpace(VolumeStatus) ? VolumeStatus.Trim() : "0000";
            return s[pos - 1] != '0';
        }
    }
}