﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Common.CommonObject
{
    public class UserInfoBO
    {
        public int UserID { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Password { get; set; }
        public string PasswordSalt { get; set; }
        public List<int> Role { get; set; }
        public int? UnitID { get; set; }
        public int UnitLevel { get; set; }
        public string RoleName { get; set; }
        public string PhoneNumber { get; set; }
        public byte[] Avatar { get; set; }
        public int? IsAdmin { get; set; }
        public int? IsManager { get; set; }
        public int? IsSupperAdmin { get; set; }
    }
}