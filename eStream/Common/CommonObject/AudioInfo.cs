﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Common.CommonObject
{
    public class AudioInfo
    {
        public string FilePath { get; set; }
        public decimal? Volume { get; set; }
        public long? Bitrate { get; set; }
    }
}