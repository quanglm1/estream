﻿using eStream.Common.CommonObject;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Model;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace eStream.Common.CommonClass
{
    public class StreamingFmTimer
    {
        private static ConcurrentDictionary<int, TimerBO> DicDeviceFmTimer = new ConcurrentDictionary<int, TimerBO>();

        public static TimerBO GetTimer(int deviceID)
        {
            DicDeviceFmTimer.TryGetValue(deviceID, out TimerBO result);
            return result;
        }

        public static void RemoveTimer(int deviceID)
        {
            DicDeviceFmTimer.TryRemove(deviceID, out TimerBO _);
        }

        public static void SetTimer(Device device, Entities context, bool onlyInsert = false)
        {
            int deviceID = device.DeviceID;
            TimerBO timer = GetTimer(deviceID);
            if (timer == null)
            {
                timer = new TimerBO
                {
                    FirstPlayingTime = DateTime.Now,
                    Count = 0
                };
                DicDeviceFmTimer[deviceID] = timer;
            }
            if (!onlyInsert)
            {
                timer.Count += 1;
            }
            if (onlyInsert || timer.Count >= GlobalConstant.MAX_COUNT_PLAYING_TIME)
            {
                // Count time
                DateTime now = DateTime.Now;
                var seconds = (now - timer.FirstPlayingTime).TotalSeconds;
                var standard = timer.Count * (device.SyncTime == null ? GlobalConstant.DEFAULT_SYNC_TIME : device.SyncTime.Value);
                if (seconds > standard * GlobalConstant.COEFFICIENT_PLAYING_TIME)
                {
                    seconds = 0;
                    if (GlobalConstant.IS_STANDARD_PLAYING_TIME)
                    {
                        seconds = standard;
                    }
                }
                if (seconds > 0)
                {
                    DevicePlayingFmTime dt = new DevicePlayingFmTime
                    {
                        DeviceID = deviceID,
                        LogDate = now.Date,
                        Date = now.Day,
                        Month = now.Month,
                        Year = now.Year,
                        FromTime = timer.FirstPlayingTime,
                        ToTime = now,
                        TotalMinus = Math.Round(1.0 * seconds / 60, 1)
                    };
                    context.DevicePlayingFmTime.Add(dt);
                }
                RemoveTimer(deviceID);
                // Dong bo
                if (device.LastSyncDate != null)
                {
                    int days = (DateTime.Now.Date - device.LastSyncDate.Value).Days;
                    if (days > 0)
                    {
                        Task.Run(() => SyncPlayingTime(device));
                    }
                }
            }
        }

        private static void SyncPlayingTime(Device device)
        {
            var lastSyncDate = device.LastSyncDate.Value;
            int days = (DateTime.Now.Date - lastSyncDate).Days;
            if (days <= 0)
            {
                return;
            }
            using (Entities context = new Entities())
            {
                using (var dbContextTransaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        var deviceID = device.DeviceID;
                        DateTime now = DateTime.Now;
                        device.LastSyncDate = now.Date;
                        for (int i = 0; i < days; i++)
                        {
                            var date = lastSyncDate.AddDays(i);
                            // Dong bo cho ngay nay
                            var totalPlayingTime = context.DevicePlayingFmTime.Where(x => x.DeviceID == deviceID && x.LogDate == date).Sum(x => x.TotalMinus);
                            if (totalPlayingTime.GetValueOrDefault() > 0)
                            {
                                DevicePlayingFmTime dt = new DevicePlayingFmTime
                                {
                                    DeviceID = deviceID,
                                    LogDate = date,
                                    Date = date.Day,
                                    Month = date.Month,
                                    Year = date.Year,
                                    FromTime = date,
                                    ToTime = date.AddDays(1).AddSeconds(-1),
                                    TotalMinus = Math.Round(totalPlayingTime.GetValueOrDefault(), 1)
                                };
                                context.DevicePlayingFmTime.Add(dt);
                                // Xoa
                                var deviceIDParam = new SqlParameter("@DeviceID", deviceID);
                                var logDateParam = new SqlParameter("@LogDate", date);
                                context.Database.ExecuteSqlCommand("DELETE FROM DevicePlayingFmTime WHERE DeviceID = @DeviceID AND LogDate = @LogDate", deviceIDParam, logDateParam);
                            }
                        }
                        context.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception)
                    {
                        dbContextTransaction.Rollback();
                        throw;
                    }
                }
            }
        }

        public static bool HasDevice(int deviceID)
        {
            return DicDeviceFmTimer.Any(x => x.Key == deviceID);
        }
    }
}