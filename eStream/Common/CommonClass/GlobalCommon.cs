﻿using eStream.Common.CommonObject;
using eStream.Models;
using Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace eStream.Common.CommonClass
{
    public class GlobalCommon
    {
        private const string PassPhrase = "EeStream123@$%^XYZ";
        private const string PassChar = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        private const string CodeChar = "0123456789";
        private const int PassLength = 8;
        // This constant is used to determine the keysize of the encryption algorithm in bits.
        // We divide this by 8 within the code below to get the equivalent number of bytes.
        private const int Keysize = 256;
        // This constant determines the number of iterations for the password bytes generation function.
        private const int DerivationIterations = 1000;
        public static string Encrypt(string clearText)
        {
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(PassPhrase, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                if (encryptor != null)
                {
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(clearBytes, 0, clearBytes.Length);
                            cs.Close();
                        }
                        clearText = Convert.ToBase64String(ms.ToArray());
                    }
                }
            }
            return clearText;
        }
        public static string Decrypt(string cipherText)
        {
            cipherText = cipherText.Replace(" ", "+");
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(PassPhrase, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                if (encryptor == null) return cipherText;
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }
        internal static void GetLoginInformation()
        {
            throw new NotImplementedException();
        }
        private static byte[] Generate256BitsOfRandomEntropy()
        {
            var randomBytes = new byte[32]; // 32 Bytes will give us 256 bits.
            using (var rngCsp = new RNGCryptoServiceProvider())
            {
                // Fill the array with cryptographically secure random bytes.
                rngCsp.GetBytes(randomBytes);
            }
            return randomBytes;
        }
        public static List<SelectListItem> DayOfTheWeek()
        {
            List<SelectListItem> DaysOfWeek = new List<SelectListItem>();
            DaysOfWeek.Add(new SelectListItem() { Text = "Thứ hai", Value = "1" });
            DaysOfWeek.Add(new SelectListItem() { Text = "Thứ ba", Value = "2" });
            DaysOfWeek.Add(new SelectListItem() { Text = "Thứ tư", Value = "3" });
            DaysOfWeek.Add(new SelectListItem() { Text = "Thứ năm", Value = "4" });
            DaysOfWeek.Add(new SelectListItem() { Text = "Thứ sáu", Value = "5" });
            DaysOfWeek.Add(new SelectListItem() { Text = "Thứ bảy", Value = "6" });
            DaysOfWeek.Add(new SelectListItem() { Text = "Chủ nhật", Value = "7" });
            return DaysOfWeek;
        }
        public static List<SelectListItem> Frequency()
        {
            List<SelectListItem> Frequency = new List<SelectListItem>();
            Frequency.Add(new SelectListItem() { Text = "Hàng Ngày", Value = GlobalConstant.SCHEDULE_DAILY.ToString() });
            Frequency.Add(new SelectListItem() { Text = "Hàng Tuần", Value = GlobalConstant.SCHEDULE_WEEKLY.ToString() });
            return Frequency;
        }
        public static List<SelectListItem> Logs()
        {
            List<SelectListItem> Logs = new List<SelectListItem>();
            Logs.Add(new SelectListItem() { Text = "--Tất cả--", Value = "" });
            Logs.Add(new SelectListItem() { Text = "Bắt đầu phát", Value = GlobalConstant.DEVICE_COMMAND_START_STREAM.ToString() });
            Logs.Add(new SelectListItem() { Text = "Cập nhật OS", Value = GlobalConstant.DEVICE_COMMAND_UPDATE_OS.ToString() });
            Logs.Add(new SelectListItem() { Text = "Thay đổi Volume", Value = GlobalConstant.DEVICE_COMMAND_CHANGE_VOLUME.ToString() });
            Logs.Add(new SelectListItem() { Text = "Thay đổi thời gian", Value = GlobalConstant.DEVICE_COMMAND_CHECK_INTERVAL.ToString() });
            Logs.Add(new SelectListItem() { Text = "Thay đổi Domain", Value = GlobalConstant.DEVICE_COMMAND_CHANGE_DOMAIN.ToString() });
            Logs.Add(new SelectListItem() { Text = "Khởi động lại", Value = GlobalConstant.DEVICE_COMMAND_RESTART.ToString() });
            Logs.Add(new SelectListItem() { Text = "Tắt thiết bị", Value = GlobalConstant.DEVICE_COMMAND_SHUTDOWN.ToString() });
            Logs.Add(new SelectListItem() { Text = "Dừng phát", Value = GlobalConstant.DEVICE_COMMAND_STOP_STREAM.ToString() });
            Logs.Add(new SelectListItem() { Text = "Phiên bản", Value = GlobalConstant.DEVICE_COMMAND_VERSION.ToString() });
            return Logs;
        }
        public static List<SelectListItem> ProMessages(int deviceId)
        {
            List<SelectListItem> types = new List<SelectListItem> {
                new SelectListItem() { Text = "--Chọn loại tin nhắn--", Value = "-1" }
            };
            using (var Context = new Entities())
            {
                int timeout = Context.SystemConfig.Select(x => x.PlayingTimeout).FirstOrDefault().GetValueOrDefault();
                var proName = StreamingCaching.GetDeviceExt(deviceId, timeout).ProName.ToLower();
                var proMessages = Context.ProMessage.Where(x => x.ProName.ToLower() == proName).ToList();
                foreach (var v in proMessages)
                {
                    types.Add(new SelectListItem() { Text = v.MessageName + " (" + v.MessageType + " - " + v.MessagePhone + ")", Value = v.ID.ToString() });
                }
            }
            return types;
        }
        public static List<SelectListItem> Voices()
        {
            List<SelectListItem> Voices = new List<SelectListItem>();
            using (var Context = new Entities())
            {
                var listVoice = Context.Voice.ToList();
                foreach (var v in listVoice)
                {
                    Voices.Add(new SelectListItem() { Text = v.DisplayName, Value = v.Code });
                }
            }
            return Voices;
        }
        public static List<SelectListItem> DeviceStatus()
        {
            List<SelectListItem> Status = new List<SelectListItem>();
            Status.Add(new SelectListItem() { Text = "--Tất cả--", Value = "" });
            Status.Add(new SelectListItem() { Text = "Bật", Value = GlobalConstant.DEVICE_STATUS_PLAYING.ToString() });
            Status.Add(new SelectListItem() { Text = "Tắt", Value = GlobalConstant.DEVICE_STATUS_STOPPED.ToString() });
            Status.Add(new SelectListItem() { Text = "N/A", Value = GlobalConstant.DEVICE_STATUS_UNDEFINED.ToString() });
            return Status;
        }
        public static List<SelectListItem> Connects()
        {
            List<SelectListItem> Status = new List<SelectListItem>();
            Status.Add(new SelectListItem() { Text = "--Tất cả--", Value = "" });
            Status.Add(new SelectListItem() { Text = "Có", Value = GlobalConstant.DEVICE_STATUS_PLAYING.ToString() });
            Status.Add(new SelectListItem() { Text = "Không", Value = GlobalConstant.DEVICE_STATUS_STOPPED.ToString() });
            return Status;
        }
        public static List<SelectListItem> Years()
        {
            List<SelectListItem> Years = new List<SelectListItem>();
            int nowYear = DateTime.Now.Year;
            for (int year = nowYear; year >= 2015; year--)
            {
                Years.Add(new SelectListItem() { Text = year.ToString(), Value = year.ToString() });
            }
            return Years;
        }
        public static List<SelectListItem> Months()
        {
            List<SelectListItem> Months = new List<SelectListItem>();
            int nowYear = DateTime.Now.Year;
            int nowMonth = DateTime.Now.Month;
            for (int month = 1; month <= 12; month++)
            {
                if (month == nowMonth)
                {
                    Months.Add(new SelectListItem() { Text = "Tháng " + month.ToString(), Value = month.ToString(), Selected = true });
                }
                else
                {
                    Months.Add(new SelectListItem() { Text = "Tháng " + month.ToString(), Value = month.ToString() });
                }
            }
            return Months;
        }

        public static List<SelectListItem> GetChanels()
        {
            List<SelectListItem> Chanels = new List<SelectListItem>();
            UserInfoBO user = SessionManager.GetUserInfoBO();
            using (var Context = new Entities())
            {
                var ListChanels = Context.ForwardChanel.Where(a => a.Status == GlobalConstant.IS_ACTIVE && (a.UnitID == null || a.UnitID == user.UnitID))
                    .OrderByDescending(x => x.UnitID == null ? 0 : x.UnitID).OrderBy(x => x.NumberOrder).ThenBy(x => x.ChanelID).ToList();
                var hasDefault = false;
                foreach (var objChanel in ListChanels)
                {
                    var isDefault = objChanel.IsDefault.GetValueOrDefault() == GlobalConstant.IS_ACTIVE;
                    if (hasDefault)
                    {
                        isDefault = false;
                    }
                    else if (isDefault)
                    {
                        hasDefault = true;
                    }
                    Chanels.Add(new SelectListItem() { Text = objChanel.ChanelName, Value = objChanel.ChanelID.ToString(), Selected = isDefault });
                }
            }
            return Chanels;
        }

        public static SystemConfigBO GetSystemConfig()
        {
            using (var context = new Entities())
            {
                var systemConfig = context.SystemConfig.FirstOrDefault();
                if (systemConfig == null)
                {
                    return null;
                }
                return new SystemConfigBO
                {
                    StreamingCountFile = systemConfig.StreamingCountFile,
                    StreamingInterval = systemConfig.StreamingInterval,
                    IsWaitingOn = systemConfig.IsWaitingOn
                };
            }
        }

        public static Dictionary<DayOfWeek, List<ScheduleBO>> GetSchedule()
        {
            Dictionary<DayOfWeek, List<ScheduleBO>> dicSchedule = new Dictionary<DayOfWeek, List<ScheduleBO>>();
            using (var Context = new Entities())
            {
                var listSchedule = (from a in Context.Schedule
                                    join b in Context.ScheduleDay on a.ScheduleID equals b.ScheduleID
                                    where a.Status == GlobalConstant.IS_ACTIVE
                                    group a by b.DayOfWeek into g
                                    select new
                                    {
                                        DayOfWeek = g.Key,
                                        ListSchedule = g.Select(x => new ScheduleBO
                                        {
                                            ScheduleID = x.ScheduleID,
                                            StartMinus = x.StartMinus,
                                            EndMinus = x.EndMinus,
                                            Ishidden = x.IsHidden == GlobalConstant.IS_ACTIVE,
                                            IsStopStream = x.IsStopStream,
                                            ListExceptUnitID = Context.ScheduleExcept.Where(s => s.ScheduleID == x.ScheduleID).Select(s => s.UnitID).ToList()
                                        }).ToList()
                                    }).ToList();
                foreach (var schedule in listSchedule)
                {
                    dicSchedule[GetDayOfWeek(schedule.DayOfWeek)] = schedule.ListSchedule;
                }
            }
            return dicSchedule;
        }
        public static string GetNameOfDay(int Day)
        {
            switch (Day)
            {
                case 1:
                    return "Thứ 2";
                case 2:
                    return "Thứ 3";
                case 3:
                    return "Thứ 4";
                case 4:
                    return "Thứ 5";
                case 5:
                    return "Thứ 6";
                case 6:
                    return "Thứ 7";
                default:
                    return "Chủ nhật";
            }
        }
        public static DayOfWeek GetDayOfWeek(int Day)
        {
            switch (Day)
            {
                case 1:
                    return DayOfWeek.Monday;
                case 2:
                    return DayOfWeek.Tuesday;
                case 3:
                    return DayOfWeek.Wednesday;
                case 4:
                    return DayOfWeek.Thursday;
                case 5:
                    return DayOfWeek.Friday;
                case 6:
                    return DayOfWeek.Saturday;
                default:
                    return DayOfWeek.Sunday;
            }
        }
        public static string GetRandomString()
        {
            string s = "";
            using (RNGCryptoServiceProvider provider = new RNGCryptoServiceProvider())
            {
                while (s.Length != PassLength)
                {
                    byte[] oneByte = new byte[1];
                    provider.GetBytes(oneByte);
                    char character = (char)oneByte[0];
                    if (PassChar.Contains(character))
                    {
                        s += character;
                    }
                }
            }
            return s;
        }
        public static string GetRandomCode(int length)
        {
            string s = "";
            using (RNGCryptoServiceProvider provider = new RNGCryptoServiceProvider())
            {
                while (s.Length != length)
                {
                    byte[] oneByte = new byte[1];
                    provider.GetBytes(oneByte);
                    char character = (char)oneByte[0];
                    if (CodeChar.Contains(character))
                    {
                        s += character;
                    }
                }
            }
            return s;
        }
        public static bool IsStrongPassword(string password, out string errorMessage)
        {
            errorMessage = string.Empty;
            // Minimum and Maximum Length of field - 6 to 12 Characters
            if (password.Length < 6 || password.Length > 18)

            {
                errorMessage = "Mật khẩu không được nhỏ hơn 6 ký tự hoặc không được lớn hơn 18 ký tự";
                return false;
            }
            // Check white space in pass
            if (password.Any(char.IsWhiteSpace))
            {
                errorMessage = "Mật khẩu không được chứa dấu cách";
                return false;
            }
            // Numeric Character - At least one character
            if (!password.Any(char.IsLetter))
            {
                errorMessage = "Mật khẩu phải chứa ít nhất một ký tự chữ";
                return false;
            }
            if (!password.Any(char.IsNumber))
            {
                errorMessage = "Mật khẩu phải bao gồm số";
                return false;
            }
            return true;
        }
        /// <summary>
        /// Gui SMS
        /// </summary>
        /// <param name="isdn"></param>
        /// <param name="content"></param>
        /// <param name="unitId"></param>
        /// <returns></returns>
        public static bool SendSmsJson(string isdn, string content, int unitId)
        {
            string smsContent = "";
            string s = GlobalCommon.ConvertToUnSign(content);
            int smsLength = Convert.ToInt32(ConfigurationManager.AppSettings["SMS_CONTENT_LENGTH"]);

            if (s.Length > smsLength)
            {
                smsContent = s.Substring(0, smsLength);
                while (!smsContent.EndsWith(" "))
                {
                    smsContent = smsContent.Substring(0, smsContent.Length - 1);
                }
                smsContent = smsContent.TrimEnd() + "...";
            }
            else
            {
                smsContent = s;
            }

            bool reVal = false;
            using (Entities context = new Entities())
            {
                SmsServiceConfig smsConfig = context.SmsServiceConfig.FirstOrDefault();
                string msisdn = StandardizeIsdn(isdn);
                if (string.IsNullOrWhiteSpace(msisdn))
                {
                    return false;
                }

                if (smsConfig != null)
                {
                    List<ReqParam> lst = new List<ReqParam>();
                    ReqParam obj = new ReqParam
                    {
                        Num = "1",
                        Content = smsContent
                    };
                    lst.Add(obj);

                    var reqParam = new RequestParam
                    {
                        Params = lst,
                        Name = "send_sms_list",
                        Reqid = DateTime.Now.Millisecond.ToString(),
                        Labelid = smsConfig.LabelID,
                        Contracttypeid = "1",
                        Contractid = smsConfig.ContractId,
                        Templateid = smsConfig.TemplateID,
                        Scheduletime = "",
                        Mobilelist = "84" + msisdn,
                        Istelcosub = "0",
                        Agentid = smsConfig.AgentID,
                        Apiuser = smsConfig.ApiUser,
                        Apipass = smsConfig.ApiPass,
                        Username = smsConfig.Username,
                        Maxitem = "0"
                    };
                    var payload =
                        new ReqJson
                        {
                            Rqst = reqParam
                        };

                    var stringPayload = Newtonsoft.Json.JsonConvert.SerializeObject(payload);

                    //    string unixdate = GetUnixDateNow();
                    string baseAddress = smsConfig.ApiUrl;
                    string apiPath = smsConfig.ApiResource;

                    var request = (HttpWebRequest)WebRequest.Create(baseAddress + apiPath);

                    request.Method = "POST";
                    request.ContentType = "application/json";
                    var data = Encoding.UTF8.GetBytes(stringPayload);
                    request.ContentLength = data.Length;
                    string message = request.Method + baseAddress + apiPath + request.ContentType;

                    //post data
                    using (var stream = request.GetRequestStream())
                    {
                        stream.Write(data, 0, data.Length);
                    }
                    //get response
                    WebResponse response = request.GetResponse();
                    // Get the stream containing content returned by the server.
                    Stream dataStream = response.GetResponseStream();
                    // Read the content.
                    if (((HttpWebResponse)response).StatusDescription == "OK")
                    {
                        using (var stream = response.GetResponseStream())
                        {
                            StreamReader reader = new StreamReader(stream);
                            var responseMessage = reader.ReadToEnd();
                            ResponeJson resObj = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponeJson>(responseMessage);

                            SendSmsHis entity = new SendSmsHis
                            {
                                SendTime = DateTime.Now,
                                MsIsdn = "84" + msisdn,
                                SendRequest = stringPayload,
                                SendResponse = responseMessage,
                                UnitID = unitId
                            };
                            context.SendSmsHis.Add(entity);
                            context.SaveChanges();

                            if (GlobalConstant.API_ERROR_SUCCESS.Equals(resObj.RPLY.ERROR))
                            {
                                reVal = true;
                            }
                            // Ghi log gui SMS
                        }
                    }
                }
                else
                {
                    throw new BusinessException("Cấu hình gửi tin nhắn chưa được thiết lập, vui lòng liên hệ quản trị");
                }
            }
            return reVal;
        }
        public static string ConvertToUnSign(string s)
        {
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = s.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }
        public static string StandardizeIsdn(string isdn)
        {
            if (isdn != null && isdn.Length > 0)
            {
                if (isdn.StartsWith("0") && (11 >= isdn.Length))
                {
                    isdn = isdn.Substring(1, isdn.Length - 1);
                    //  isdn = isdn.Replace("0", "");
                }
                else if (isdn.StartsWith("84"))
                {
                    isdn = isdn.Substring(2, isdn.Length - 2);
                }
                else if (isdn.StartsWith("0084"))
                {
                    isdn = isdn.Substring(4, isdn.Length - 4);

                }
                else if (isdn.StartsWith("084"))
                {
                    isdn = isdn.Substring(3, isdn.Length - 3);
                    // isdn = isdn.Replace("084", "");
                }
                else if (isdn.StartsWith("+84"))
                {
                    isdn = isdn.Replace("+84", "");
                }
            }

            return isdn;
        }
        public static string ExecuteRequest(object objectRequest, string RequestUrl)
        {
            //Convert object to request string
            string RequestStringObject = JsonConvert.SerializeObject(objectRequest);
            //Creating request
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(RequestUrl);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = RequestStringObject.Length;
            StreamWriter requestWriter = new StreamWriter(request.GetRequestStream(), System.Text.Encoding.ASCII);
            requestWriter.Write(RequestStringObject);
            requestWriter.Close();

            try
            {
                WebResponse webResponse = request.GetResponse();
                Stream webStream = webResponse.GetResponseStream();
                StreamReader responseReader = new StreamReader(webStream);
                string response = responseReader.ReadToEnd();
                responseReader.Close();
                return response;
            }
            catch (Exception e)
            {
                throw e;
            }

        }
        public static long UnixTimestampFromDateTime(DateTime date)
        {
            long unixTimestamp = date.Ticks - new DateTime(1970, 1, 1).Ticks;
            unixTimestamp /= TimeSpan.TicksPerSecond;
            return unixTimestamp;
        }
        public static DateTime TimeFromUnixTimestamp(long unixTimestamp)
        {
            DateTime unixYear0 = new DateTime(1970, 1, 1);
            long unixTimeStampInTicks = unixTimestamp * TimeSpan.TicksPerSecond;
            DateTime dtUnix = new DateTime(unixYear0.Ticks + unixTimeStampInTicks);
            return dtUnix;
        }
        public static long AdjustTimeToFloorBase(long seconds)
        {
            return seconds - (seconds % GlobalConstant.CHUNK_DURATION);
        }
        public static long AdjustTimeToCeilBase(long seconds)
        {
            var remain = seconds % GlobalConstant.CHUNK_DURATION;
            return remain == 0 ? seconds : seconds - (seconds % GlobalConstant.CHUNK_DURATION) + GlobalConstant.CHUNK_DURATION;
        }
        public static int GetWeekNumber(DateTime WeekDate)
        {
            CultureInfo ciCurr = CultureInfo.CurrentCulture;
            int weekNum = ciCurr.Calendar.GetWeekOfYear(WeekDate, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
            return weekNum;
        }

        public static string SecondsToString(double secs)
        {
            TimeSpan t = TimeSpan.FromSeconds(Convert.ToInt64(secs));

            string answer = string.Format("{0:D2}:{1:D2}:{2:D2}",
                            t.Hours,
                            t.Minutes,
                            t.Seconds);
            return answer;
        }
        public static string FormatDeviceCode(string code)
        {
            if (string.IsNullOrWhiteSpace(code))
            {
                return code;
            }
            code = code.Trim().ToUpper().Replace(":", string.Empty);
            if (code.Length < 6)
            {
                return code;
            }
            return code.Substring(code.Length - 6);
        }

        public static void UpdateDeviceStatus(int deviceID, int timeout, out int isPlaying, out int isOnline, out int? fmStatus, out int isFmSwitchOn)
        {
            StreamingBO str = StreamingCaching.GetStreamingInfo(deviceID);
            if (str == null)
            {
                isPlaying = 0;
                isOnline = 0;
                isFmSwitchOn = 0;
                fmStatus = null;
            }
            else
            {
                if (str.LastTimePlaying == null)
                {
                    StreamingCaching.SetOnlyPlaying(deviceID, false);
                }
                else
                {
                    if ((DateTime.Now - str.LastTimePlaying.Value).TotalSeconds > timeout)
                    {
                        StreamingCaching.SetOnlyPlaying(deviceID, false);
                    }
                }
                if (str.LastTime == null)
                {
                    StreamingCaching.SetOnlyOnline(deviceID, false);
                }
                else
                {
                    if ((DateTime.Now - str.LastTime.Value).TotalSeconds > timeout)
                    {
                        StreamingCaching.SetOnlyOnline(deviceID, false);
                    }
                }
                if (str.LastTimeFm == null)
                {
                    StreamingCaching.SetFmStatus(deviceID, null);
                }
                else
                {
                    if ((DateTime.Now - str.LastTime.Value).TotalSeconds > timeout)
                    {
                        StreamingCaching.SetFmStatus(deviceID, null);
                    }
                }
                StreamingCaching.SetVolumeStatus(deviceID, null, timeout);
                isPlaying = (str.IsPlaying ? GlobalConstant.IS_ACTIVE : 0);
                isOnline = (str.IsOnline ? GlobalConstant.IS_ACTIVE : 0);
                isFmSwitchOn = str.IsFmSwitchOn;
                fmStatus = str.FmStatus;
            }
        }

        public static string GetExcelColumnName(int columnNumber)
        {
            int dividend = columnNumber;
            string columnName = String.Empty;
            int modulo;

            while (dividend > 0)
            {
                modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = (int)((dividend - modulo) / 26);
            }

            return columnName;
        }

        public static string ToRoman(int number)
        {
            String[] roman = new String[] { "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I" };
            int[] decimals = new int[] { 1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1 };

            string romanvalue = String.Empty;

            for (int i = 0; i < 13; i++)
            {
                while (number >= decimals[i])
                {
                    number -= decimals[i];
                    romanvalue += roman[i];
                }
            }
            return romanvalue;
        }

        private static readonly string[] VietnameseSigns = new string[]
        {

            "aAeEoOuUiIdDyY",

            "áàạảãâấầậẩẫăắằặẳẵ",

            "ÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴ",

            "éèẹẻẽêếềệểễ",

            "ÉÈẸẺẼÊẾỀỆỂỄ",

            "óòọỏõôốồộổỗơớờợởỡ",

            "ÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠ",

            "úùụủũưứừựửữ",

            "ÚÙỤỦŨƯỨỪỰỬỮ",

            "íìịỉĩ",

            "ÍÌỊỈĨ",

            "đ",

            "Đ",

            "ýỳỵỷỹ",

            "ÝỲỴỶỸ"
        };

        public static string RemoveSign4VietnameseString(string str)
        {
            for (int i = 1; i < VietnameseSigns.Length; i++)
            {
                for (int j = 0; j < VietnameseSigns[i].Length; j++)
                    str = str.Replace(VietnameseSigns[i][j], VietnameseSigns[0][i - 1]);
            }
            return str;
        }

        public static string ToGuid(long value)
        {
            byte[] bytes = new byte[16];
            BitConverter.GetBytes(value).CopyTo(bytes, 0);
            return new Guid(bytes).ToString().Replace("-", "");
        }
    }
}