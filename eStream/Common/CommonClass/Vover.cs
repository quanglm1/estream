﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using eStream.Common.CommonObject;
using Model;
using System.Data.Entity;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Text;

namespace eStream.Common.CommonClass
{
    [PersistJobDataAfterExecution]
    public class Vover : IJob
    {
        public Task Execute(IJobExecutionContext context)
        {
            return Task.Factory.StartNew(() => Download(context, 0));
        }

        private void Download(IJobExecutionContext context, int times)
        {
            if (times >= 5)
            {
                return;
            }
            try
            {
                JobDataMap dataMap = context.JobDetail.JobDataMap;
                string vovCode = dataMap.GetString("channelCode");
                string downloadUrl = dataMap.GetString("downloadUrl");
                using (var webClient = new WebClient())
                {
                    string vovPath = GlobalConstant.MEDIA_FOLDER + "/" + vovCode;
                    // Xoa file cu
                    var listFile = Directory.GetFiles(vovPath);
                    var removeTime = DateTime.Now.AddMinutes(-5);
                    foreach (string filePath in listFile)
                    {
                        var lastTime = File.GetLastWriteTime(filePath);
                        if (lastTime < removeTime)
                        {
                            File.Delete(filePath);
                        }
                    }
                    string chunk = string.Empty;
                    StringBuilder str = new StringBuilder();
                    string line = webClient.DownloadString(downloadUrl + "playlist.m3u8");
                    if (!string.IsNullOrWhiteSpace(line) && line.ToLower().Contains("chunklist"))
                    {
                        int pos = line.IndexOf("chunklist");
                        chunk = Regex.Replace(line.Substring(pos), @"\t|\n|\r", "");
                    }
                    if (!string.IsNullOrWhiteSpace(chunk))
                    {
                        using (var webClientChunk = new WebClient())
                        {
                            string chunkFile = webClientChunk.DownloadString(downloadUrl + chunk);
                            string[] arr = chunkFile.Split('\n');
                            foreach (var a in arr)
                            {
                                if (a.StartsWith("media_"))
                                {
                                    using (var webClientFile = new WebClient())
                                    {
                                        webClientFile.DownloadFile(downloadUrl + a, vovPath + "/" + a);
                                        str.AppendLine("/media/" + vovCode + "/" + a);
                                    }
                                }
                                else
                                {
                                    str.AppendLine(a);
                                }
                            }
                        }
                    }
                    File.WriteAllText(vovPath + "/" + "playlist.m3u8", str.ToString());
                }
            }
            catch
            {
                System.Threading.Thread.Sleep(500);
                // Ignored
                Download(context, times + 1);
            }
        }
    }
}