﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using eStream.Common.CommonObject;
using Model;

namespace eStream.Common.CommonClass
{
    [PersistJobDataAfterExecution]
    public class SmsSender : IJob
    {
        public Task Execute(IJobExecutionContext context)
        {
            return Task.Factory.StartNew(() => SendSms(context));
        }

        private void SendSms(IJobExecutionContext jContext)
        {
            try
            {
                int smsTime = 0;
                using (Entities context = new Entities())
                {
                    SystemConfig sc = context.SystemConfig.FirstOrDefault();
                    smsTime = (sc != null ? sc.SmsRefreshTime.GetValueOrDefault() : 0);
                    int waitingAutioDur = (sc != null && sc.IsWaitingOn.GetValueOrDefault() == GlobalConstant.IS_ACTIVE) ? GlobalConstant.WAITING_AUDIO_DURATION : 1;
                    ScheduleBO currentSchedule = ScheduleCaching.GetListScheduleNow(0, waitingAutioDur).FirstOrDefault();
                    string smsContent = sc != null ? sc.SmsContent : null;
                    if (!string.IsNullOrWhiteSpace(smsContent) && currentSchedule != null)
                    {
                        List<Device> listDevice = context.Device.Where(x => x.Status == GlobalConstant.DEVICE_STATUS_READY
                        && x.ManagerPhone != null && x.UnitID != null && x.IsSMS == GlobalConstant.IS_ACTIVE).ToList();
                        DateTime now = DateTime.Now.Date;
                        bool isUpdate = false;
                        foreach (Device device in listDevice)
                        {
                            if (string.IsNullOrWhiteSpace(device.ManagerPhone) || (device.SmsScheduleID.GetValueOrDefault() == currentSchedule.ScheduleID
                                && (device.SmsTime != null && device.SmsTime.Value == now)))
                            {
                                // Da gui truoc do
                                continue;
                            }
                            if (!StreamingCaching.UpdateIsOnline(device.DeviceID, sc.PlayingTimeout.GetValueOrDefault()))
                            {
                                // Send sms
                                smsContent = string.Format(smsContent, device.DeviceName);
                                GlobalCommon.SendSmsJson(device.ManagerPhone, smsContent, device.UnitID.Value);
                                device.SmsScheduleID = currentSchedule.ScheduleID;
                                device.SmsTime = now;
                                isUpdate = true;
                            }
                        }
                        if (isUpdate)
                        {
                            context.SaveChanges();
                        }
                    }
                }
                if (smsTime > 0)
                {
                    int timeInterval = jContext.JobDetail.JobDataMap.GetIntValue("SmsScheduleID");
                    if (timeInterval > 0)
                    {
                        if (smsTime != timeInterval)
                        {
                            jContext.JobDetail.JobDataMap["SmsScheduleID"] = smsTime;
                            ITrigger trigger = TriggerBuilder.Create()
                                .WithIdentity("trigger", "group")
                                .StartAt(DateBuilder.FutureDate(smsTime, IntervalUnit.Second))
                                .WithSimpleSchedule(x => x
                                    .WithIntervalInSeconds(smsTime)
                                    .RepeatForever())
                                .Build();
                            jContext.Scheduler.RescheduleJob(jContext.Trigger.Key, trigger);
                        }
                    }
                    else
                    {
                        jContext.JobDetail.JobDataMap.Put("SmsScheduleID", smsTime);
                    }
                }
            }
            catch
            {
                // ignored
            }
        }
    }
}