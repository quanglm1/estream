﻿using Model;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;


namespace eStream.Common.CommonClass
{
    public class VovScheduler
    {
        public static async Task Start()
        {
            using (var context = new Entities())
            {
                var vovs = context.ForwardChanel.Where(x => x.Status == GlobalConstant.IS_ACTIVE && x.UnitID == null && x.DownloadUrl != null).ToList();
                //var cf = context.SystemConfig.FirstOrDefault();
                int downloadTime = 20;
                //if (cf != null)
                //{
                //    downloadTime = cf.VovDownloadTime.GetValueOrDefault();
                //}
                foreach (var vov in vovs)
                {
                    // Check duong dan
                    if (!string.IsNullOrWhiteSpace(vov.ChannelCode))
                    {
                        string vovPath = GlobalConstant.MEDIA_FOLDER + vov.ChannelCode;
                        if (!Directory.Exists(vovPath))
                        {
                            Directory.CreateDirectory(vovPath);
                        }
                        IScheduler scheduler = await StdSchedulerFactory.GetDefaultScheduler();
                        await scheduler.Start();
                        IJobDetail job = JobBuilder.Create<Vover>()
                        .WithIdentity(vov.ChannelCode + "Job", "group")
                            .UsingJobData("channelCode", vov.ChannelCode)
                            .UsingJobData("downloadUrl", vov.DownloadUrl)
                            .Build();
                        ITrigger trigger = TriggerBuilder.Create()
                            .WithIdentity(vov.ChannelCode + "SchedulerTrigger", "group")
                            .StartNow()
                            .WithSimpleSchedule(x => x
                                .WithIntervalInSeconds(downloadTime)
                                .RepeatForever())
                            .Build();

                        await scheduler.ScheduleJob(job, trigger);
                    }
                }
            }
        }
    }
}