﻿using eStream.Common.CommonObject;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Common.CommonClass
{
    public class ConfigCaching
    {
        public static void LoadCaching()
        {
            CacheHelper.Delete(GlobalConstant.SYSTEM_CONFIG_CACHE_KEY);
            CacheHelper.Add(GlobalConstant.SYSTEM_CONFIG_CACHE_KEY, GlobalCommon.GetSystemConfig(), DateTimeOffset.UtcNow.AddHours(1));
        }
        public static SystemConfigBO GetSystemConfig()
        {
            var config = CacheHelper.GetValue(GlobalConstant.SYSTEM_CONFIG_CACHE_KEY) as SystemConfigBO;
            if (config == null)
            {
                LoadCaching();
                config = CacheHelper.GetValue(GlobalConstant.SYSTEM_CONFIG_CACHE_KEY) as SystemConfigBO;
            }
            return config;
        }
    }
}