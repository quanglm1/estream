using System.Collections.Generic;
using System.Configuration;

namespace eStream.Common.CommonClass
{
    public class GlobalConstant
    {

        public const string SECRET = "EstreamttM2MVMastpQYQYAlnAd12345";
        public const string VERY_SECRET = "!@#E1s@t3r$e5a^m7t*19B!1t2D#$^Hfl4z2&*a(H)2H6e!2l3$Pk)Y*1G#3A4*u2G!j@E";
        /// <summary>
        /// Khai bao cac hang so dung dung
        /// </summary>
        public const int IS_ACTIVE = 1;
        public const int IS_NOT_ACTIVE = 0;
        public const int N_A = -1;
        public const int IS_DELETE = 2;
        public const int PAGE_SIZE = 10;
        public const int PAGE_SIZE_MAX = 20;
        public const int PAGE_SIZE_LITE = 5;
        public const int IS_TRUE = 1;
        public const int IS_FALSE = 0;
        public const int OTP_MINUTES = 5;
        public const int LOOP_COUNT = 2;

        /// <summary>
        /// Loai bao cao
        /// </summary>
        public const int REPORT_TYPE_DAY = 1;
        public const int REPORT_TYPE_MONTH = 2;
        public const int REPORT_TYPE_YEAR = 3;


        /// <summary>
        /// Loai ket noi
        /// </summary>
        public const string CONN_TYPE_3G = "3G";
        public const string CONN_TYPE_4G = "4G";
        public const string CONN_TYPE_AUTO = "AUTO";

        /// <summary>
        /// Loai nguoi dung
        /// </summary>
        public const int USER_ROLE_ADMIN = 1;
        public const int USER_ROLE_MANAGER = 2;
        public const int USER_ROLE_SUPPER_ADMIN = 3;
        public const int USER_ROLE_MONITOR = 4;

        public const int IS_DRAF = 0;
        public const int IS_APPROVED = 1;

        public const int SCHEDULE_DAILY = 1;
        public const int SCHEDULE_WEEKLY = 2;

        /// <summary>
        /// Loai phat truc tiep
        /// </summary>
        public const int LIVE_TYPE_CHOICE = 1;
        public const int LIVE_TYPE_ALL = 2;

        /// <summary>
        /// Nguon phat truc tiep
        /// </summary>
        public const int LIVE_SOURCE_VOV = 1;
        public const int LIVE_SOURCE_FILE = 2;
        public const int LIVE_SOURCE_STREAMING = 3;

        /// <summary>
        /// Cac loai phat
        /// </summary>
        public const int PLAYING_TYPE_LIVE = 1;
        public const int PLAYING_TYPE_SCHEDULE = 2;

        public const string PLAYING_SOURCE_FILE = "Từ file";
        public const string PLAYING_SOURCE_MICRO = "Micro";

        public const string FM_CODE_FORWARD = "FM_FORWARD_FOR_DEVICE_ONLY_ONE_SO_SET_TOO_LONG_TOO_LONG";

        /// Tan so FM
        /// </summary>
        public const double FM_MIN_FREQ = 78.0;
        public const double FM_MAX_FREQ = 108.0;
        public const string FM_CHANNEL_CODE = "FM";

        /// <summary>
        /// Trang thai phat truc tiep
        /// </summary>
        public const int LIVE_STOPED = -2;
        public const int LIVE_DRAFF = -1;
        public const int LIVE_WAIT = 0;
        public const int LIVE_PLAYING = 1;

        /// <summary>
        /// Loai don vi hanh chi
        /// </summary>
        public const int UNIT_TYPE_PROVINCE = 1;
        public const int UNIT_TYPE_DISTRICT = 2;
        public const int UNIT_TYPE_COMMUNE = 3;
        public const int UNIT_TYPE_VILLAGE = 4;
        /// <summary>
        /// Ten ca tinh
        /// </summary>
        public const string ALL_PROVINCE_NAME = "Tỉnh Tuyên Quang";
        /// <summary>
        /// Trang thai cua thiet bi
        /// </summary>
        public const int DEVICE_STATUS_READY = 1;
        public const int DEVICE_STATUS_BEAK = 2;
        public const int DEVICE_STATUS_GUARANT = 3;
        /// <summary>
        /// Trang thai phat cua thiet bi
        /// </summary>
        public const int DEVICE_STATUS_PLAYING = 1;
        public const int DEVICE_STATUS_STOPPED = 2;
        public const int DEVICE_STATUS_UNDEFINED = 3;

        /// <summary>
        /// Bitrate default
        /// </summary>
        public const int DEFAULT_BIT_RATE = 64;
        public const int BITRATE_128 = 128;
        public const int BITRATE_32 = 32;

        public const int DEFAULT_TIME_REFRESH = 60;
        public const int DEFAULT_VOLUME = 100;
        public const int DEFAULT_SYNC_TIME = 5;
        public const int DEFAULT_STREAMING_INTERVAL = 10;
        public const int DEFAULT_STREAMING_BITRATE = 128;
        public const int DEFAULT_STREAMING_STREO = 2;
        public const int DEFAULT_STREAMING_COUNT_FILE = 3;

        /// <summary>
        /// Ten lenh dieu khien tu server
        /// </summary>
        public const short DEVICE_COMMAND_SEND_SMS = 6;
        public const short DEVICE_COMMAND_CHANGE_CONN = 7;
        public const short DEVICE_COMMAND_FM_START = 8;
        public const short DEVICE_COMMAND_FM_FREQ = 9;
        public const short DEVICE_COMMAND_START_STREAM = 10;
        public const short DEVICE_COMMAND_UPDATE_OS = 11;
        public const short DEVICE_COMMAND_CHANGE_VOLUME = 12;
        public const short DEVICE_COMMAND_CHECK_INTERVAL = 13;
        public const short DEVICE_COMMAND_CHANGE_DOMAIN = 14;
        public const short DEVICE_COMMAND_RESTART = 15;
        public const short DEVICE_COMMAND_SHUTDOWN = 16;
        public const short DEVICE_COMMAND_STOP_STREAM = 20;
        public const short DEVICE_COMMAND_VERSION = 21;
        public const short DEVICE_COMMAND_TIMER = 24;
        public const short DEVICE_COMMAND_TURN_ON_FM = 25;
        public const short DEVICE_COMMAND_TURN_OFF_FM = 26;

        public static List<string> CONNECTION_TYPES = new List<string> { CONN_TYPE_3G, CONN_TYPE_4G, CONN_TYPE_AUTO };

        public static int MAX_COUNT_PLAYING_TIME = int.Parse(ConfigurationManager.AppSettings["MAX_COUNT_PLAYING_TIME"]);
        public static double COEFFICIENT_PLAYING_TIME = double.Parse(ConfigurationManager.AppSettings["COEFFICIENT_PLAYING_TIME"]);
        public static bool IS_STANDARD_PLAYING_TIME = bool.Parse(ConfigurationManager.AppSettings["IS_STANDARD_PLAYING_TIME"]);

        public const int MAX_DAY_OF_WEEK = 7;
        public const string FILE_PATH = "~/App_Data/Audio/";

        #region Dinh nghia ma loi tra ve goi api SMS
        public static string API_ERROR_SUCCESS = "0";
        public static string API_ERROR_EXCEPTION = "-1";
        public static string API_ERROR_INVALID = "1";
        public static string API_ERROR_WRONG_DATETIME_FORMAT = "2";
        public static string API_ERROR_ID_METHOD_INVALID = "3";
        public static string API_ERROR_TEMPLATE_INVALID = "7";
        public static string API_ERROR_INVALID_TIME = "8";
        public static string API_ERROR_CONTRACTTYPE_INVALID = "9";
        public static string API_ERROR_USERNAME_INVALID = "10";
        public static string API_ERROR_LENGTH_MESSAGE = "11";
        public static string API_ERROR_TIME_POLICY_INVALID = "12";
        public static string API_ERROR_CONTRACT_INVALID = "13";
        public static string API_ERROR_LABELID_INVALID = "14";
        #endregion

        public static int CHUNK_DURATION = int.Parse(ConfigurationManager.AppSettings["CHUNK_DURATION"]);
        public const string CHUNK_PREFIX = "audio_";
        public const string CHUNK_SUFFIX = ".aac";
        public readonly static string MEDIA_SERVER = ConfigurationManager.AppSettings["MEDIA_SERVER"];
        public readonly static string MEDIA_FOLDER = ConfigurationManager.AppSettings["MEDIA_FOLDER"];
        public readonly static string TEMP_MEDIA_FOLDER = ConfigurationManager.AppSettings["TEMP_MEDIA_FOLDER"];
        public readonly static bool DELETE_TEMP_FILE = bool.Parse(ConfigurationManager.AppSettings["DELETE_TEMP_FILE"]);
        public static int WAITING_AUDIO_DURATION = int.Parse(ConfigurationManager.AppSettings["WAITING_AUDIO_DURATION"]);
        public static string WAITING_AUDIO_FOLDER = ConfigurationManager.AppSettings["WAITING_AUDIO_FOLDER"];
        public static int COMMAND_EXPIRED_PERIOD = int.Parse(ConfigurationManager.AppSettings["COMMAND_EXPIRED_PERIOD"]);
        public static int COMMAND_PLAYING_TIME = int.Parse(ConfigurationManager.AppSettings["COMMAND_PLAYING_TIME"]);
        // VBEE
        public readonly static string VBEE_CONVERT_API = Crypto.DecryptString(ConfigurationManager.AppSettings["VBEE_CONVERT_API"]);
        public readonly static string VBEE_USER_NAME = Crypto.DecryptString(ConfigurationManager.AppSettings["VBEE_USER_NAME"]);
        public readonly static string VBEE_DICTIONARY_ID = Crypto.DecryptString(ConfigurationManager.AppSettings["VBEE_DICTIONARY_ID"]);
        public readonly static string VBEE_APPLICATION_ID = Crypto.DecryptString(ConfigurationManager.AppSettings["VBEE_APPLICATION_ID"]);
        public readonly static string VBEE_URL_CALLBACK = ConfigurationManager.AppSettings["VBEE_URL_CALLBACK"];
        // SECRET
        public readonly static string DEVICE_SECRET = ConfigurationManager.AppSettings["DEVICE_SECRET"];
        public readonly static string DOMAIN_SECRET = ConfigurationManager.AppSettings["DOMAIN_SECRET"];
        public readonly static string DOMAIN_HASH = ConfigurationManager.AppSettings["DOMAIN_HASH"].ToUpper();

        // Cache thong tin lich
        public const string SCHEDULE_CACHE_KEY = "ESTREAM_SCHEDULE_CACHE_KEY";
        public const string SYSTEM_CONFIG_CACHE_KEY = "ESTREAM_SYSTEM_CONFIG_CACHE_KEY";
    }
}