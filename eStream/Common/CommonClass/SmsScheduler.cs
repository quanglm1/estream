﻿using Model;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace eStream.Common.CommonClass
{
    public class SmsScheduler
    {
        public static async Task Start()
        {
            IScheduler scheduler = await StdSchedulerFactory.GetDefaultScheduler();
            await scheduler.Start();
            int smsTime = 0;
            using (Entities context = new Entities())
            {
                SystemConfig sc = context.SystemConfig.FirstOrDefault();
                smsTime = (sc != null ? sc.SmsRefreshTime.GetValueOrDefault() : 0);
            }
            if (smsTime == 0)
            {
                // Mac dinh 300s
                smsTime = 300;
            }
            IJobDetail job = JobBuilder.Create<SmsSender>()
            .WithIdentity("SmsSenderJob", "group")
                .Build();
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("SmsSenderJobTrigger", "group")
                .StartNow()
                .WithSimpleSchedule(x => x
                    .WithIntervalInSeconds(smsTime)
                    .RepeatForever())
                .Build();

            await scheduler.ScheduleJob(job, trigger);
        }
    }
}