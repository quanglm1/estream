﻿using Model;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace eStream.Common.CommonClass
{
    public class RemoveFileScheduler
    {
        public static async Task Start()
        {
            IScheduler scheduler = await StdSchedulerFactory.GetDefaultScheduler();
            await scheduler.Start();
            IJobDetail job = JobBuilder.Create<FileRemover>()
            .WithIdentity("FileRemoverJob", "group")
                .Build();
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("FileRemoverJobTrigger", "group")
                //.StartAt(DateBuilder.DateOf(23, 0, 0))
                .StartNow()
                .WithSimpleSchedule(x => x
                    .WithIntervalInHours(24)
                    .RepeatForever())
                .Build();

            await scheduler.ScheduleJob(job, trigger);
        }
    }
}