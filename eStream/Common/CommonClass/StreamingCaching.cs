﻿using eStream.Common.CommonObject;
using eStream.Dto;
using System;
using System.Collections.Concurrent;
using System.Linq;

namespace eStream.Common.CommonClass
{
    public class StreamingCaching
    {
        public static ConcurrentDictionary<long, PlayingDto> DicPlaying = new ConcurrentDictionary<long, PlayingDto>();
        private static ConcurrentDictionary<int, StreamingBO> DicDeviceStreaming = new ConcurrentDictionary<int, StreamingBO>();
        private static ConcurrentDictionary<int, VolumeStatusBO> DicDeviceVolumeStatus = new ConcurrentDictionary<int, VolumeStatusBO>();
        private static ConcurrentDictionary<int, DeviceExtBO> DicDeviceExt = new ConcurrentDictionary<int, DeviceExtBO>();
        private static ConcurrentDictionary<int, long> DicLastSms = new ConcurrentDictionary<int, long>();

        public static void SetLastSms(int deviceID, long lastId)
        {
            DicLastSms[deviceID] = lastId;
        }

        public static long GetLastSms(int deviceID)
        {
            DicLastSms.TryGetValue(deviceID, out long result);
            return result;
        }

        public static void SetDeviceExt(int deviceID, CommandRequestDto req, int timeout = 0)
        {
            DicDeviceExt.TryGetValue(deviceID, out DeviceExtBO result);
            if (result == null)
            {
                result = new DeviceExtBO();
                DicDeviceExt[deviceID] = result;
            }
            if (req != null)
            {
                result.RamUsage = req.ram_usage;
                result.ProName = req.pro_name;
                result.ConnType = req.conn_type;
                result.ConnMod = req.conn_mod;
                result.RamTotal = req.ram_total;
                result.CpuFreq = req.cpu_freq;
                result.CpuTemp = req.cpu_temp;
                result.CpuUsage = req.cpu_usage;
                result.SignStrength = req.sign_strength.GetValueOrDefault();
                result.GpsLoc = req.gps_loc;
                result.LastUpdate = DateTime.Now;
                result.Streaming = req.streaming.GetValueOrDefault() ? GlobalConstant.IS_ACTIVE : 0;
            }
            else
            {
                if (result.LastUpdate != null)
                {
                    if ((DateTime.Now - result.LastUpdate.Value).TotalSeconds > timeout)
                    {
                        result.RamUsage = string.Empty;
                        result.ProName = string.Empty;
                        result.ConnType = string.Empty;
                        result.ConnMod = string.Empty;
                        result.RamTotal = string.Empty;
                        result.CpuFreq = string.Empty;
                        result.CpuTemp = string.Empty;
                        result.CpuUsage = string.Empty;
                        result.SignStrength = 0;
                        result.Streaming = 0;
                        result.GpsLoc = string.Empty;
                    }
                }
            }
        }

        public static DeviceExtBO GetDeviceExt(int deviceID, int timeout)
        {
            DicDeviceExt.TryGetValue(deviceID, out DeviceExtBO result);
            if (result == null)
            {
                return new DeviceExtBO
                {
                    RamUsage = string.Empty,
                    ProName = string.Empty,
                    ConnType = string.Empty,
                    ConnMod = string.Empty,
                    RamTotal = string.Empty,
                    CpuFreq = string.Empty,
                    CpuTemp = string.Empty,
                    CpuUsage = string.Empty,
                    SignStrength = 0,
                    Streaming = 0,
                    GpsLoc = string.Empty
                };
            }
            if (timeout > 0 && (DateTime.Now - result.LastUpdate.Value).TotalSeconds > timeout)
            {
                DicDeviceExt.TryRemove(deviceID, out DeviceExtBO _);
                return new DeviceExtBO
                {
                    RamUsage = string.Empty,
                    ProName = string.Empty,
                    ConnType = string.Empty,
                    ConnMod = string.Empty,
                    RamTotal = string.Empty,
                    CpuFreq = string.Empty,
                    CpuTemp = string.Empty,
                    CpuUsage = string.Empty,
                    SignStrength = 0,
                    Streaming = 0,
                    GpsLoc = string.Empty
                };
            }
            return result;
        }

        public static void SetVolumeStatus(int deviceID, CommandRequestDto req, int timeout = 0)
        {
            DicDeviceVolumeStatus.TryGetValue(deviceID, out VolumeStatusBO result);
            if (result == null)
            {
                result = new VolumeStatusBO();
                DicDeviceVolumeStatus[deviceID] = result;
            }
            if (req != null)
            {
                result.S1 = req.s1.GetValueOrDefault();
                result.S2 = req.s2.GetValueOrDefault();
                result.S3 = req.s3.GetValueOrDefault();
                result.S4 = req.s4.GetValueOrDefault();
                result.LastUpdate = DateTime.Now;
            }
            else
            {
                if (result.LastUpdate != null && (result.S1 != 0 || result.S2 != 0 || result.S3 != 0 || result.S4 != 0))
                {
                    if ((DateTime.Now - result.LastUpdate.Value).TotalSeconds > timeout)
                    {
                        result.S1 = 0;
                        result.S2 = 0;
                        result.S3 = 0;
                        result.S4 = 0;
                    }
                }
            }
        }

        public static string GetVolumeStatus(int deviceID)
        {
            DicDeviceVolumeStatus.TryGetValue(deviceID, out VolumeStatusBO result);
            if (result == null)
            {
                return "0000";
            }
            return "" + result.S1 + result.S2 + result.S3 + result.S4;
        }

        public static bool UpdateStreaming(int deviceID, bool isPlaying, int timeout)
        {
            StreamingBO streamingBO = GetStreamingInfo(deviceID);
            if (streamingBO == null)
            {
                streamingBO = new StreamingBO();
            }
            streamingBO.LastTime = DateTime.Now;
            if (isPlaying)
            {
                streamingBO.IsPlaying = true;
                streamingBO.LastTimePlaying = streamingBO.LastTime;
            }
            else if (timeout > 0 && streamingBO.GetStopingTime() > (double)timeout)
            {
                streamingBO.IsPlaying = false;
            }
            if (isPlaying && streamingBO.FirstTimePlaying == null)
            {
                streamingBO.FirstTimePlaying = DateTime.Now;
            }
            DicDeviceStreaming[deviceID] = streamingBO;
            return streamingBO.IsPlaying;
        }

        public static bool UpdateIsOnline(int deviceID, int timeout)
        {
            StreamingBO streamingInfo = GetStreamingInfo(deviceID);
            if (streamingInfo == null)
            {
                return false;
            }
            if (streamingInfo.LastTime == null)
            {
                streamingInfo.IsOnline = false;
                DicDeviceStreaming[deviceID] = streamingInfo;
            }
            else
            {
                if ((DateTime.Now - streamingInfo.LastTime.Value).TotalSeconds > timeout)
                {
                    streamingInfo.IsOnline = false;
                    DicDeviceStreaming[deviceID] = streamingInfo;
                }
            }
            return streamingInfo.IsOnline;
        }

        public static void SetPlayingTime(int deviceID, double? playingTime)
        {
            StreamingBO streamingBO = GetStreamingInfo(deviceID);
            if (streamingBO == null)
            {
                streamingBO = new StreamingBO
                {
                    CountPlayingTime = playingTime
                };
            }
            else
            {
                streamingBO.CountPlayingTime = playingTime;
            }
            DicDeviceStreaming[deviceID] = streamingBO;
        }

        public static bool IsPlaying(int deviceID)
        {
            StreamingBO streamingInfo = GetStreamingInfo(deviceID);
            return streamingInfo?.IsPlaying ?? false;
        }

        public static bool IsStop(int deviceID)
        {
            StreamingBO streamingInfo = GetStreamingInfo(deviceID);
            return streamingInfo?.IsStop ?? false;
        }

        public static bool IsOnline(int deviceID)
        {
            StreamingBO streamingInfo = GetStreamingInfo(deviceID);
            return streamingInfo?.IsOnline ?? false;
        }

        public static int? FmStatus(int deviceID)
        {
            StreamingBO streamingInfo = GetStreamingInfo(deviceID);
            return streamingInfo?.FmStatus ?? null;
        }

        public static int? IsFmSwitchOn(int deviceID)
        {
            StreamingBO streamingInfo = GetStreamingInfo(deviceID);
            return streamingInfo?.IsFmSwitchOn ?? GlobalConstant.N_A;
        }

        public static void SetOnlyPlaying(int deviceID, bool isPlaying)
        {
            StreamingBO streamingInfo = GetStreamingInfo(deviceID);
            if (streamingInfo != null)
            {
                streamingInfo.IsPlaying = isPlaying;
                DicDeviceStreaming[deviceID] = streamingInfo;
            }
        }

        public static void SetOnlyOnline(int deviceID, bool isOnline)
        {
            StreamingBO streamingInfo = GetStreamingInfo(deviceID);
            if (streamingInfo != null)
            {
                streamingInfo.IsOnline = isOnline;
                DicDeviceStreaming[deviceID] = streamingInfo;
            }
        }

        public static void SetIsPlaying(int deviceID, bool isPlaying, bool firstPlaying = false)
        {
            StreamingBO streamingBO = GetStreamingInfo(deviceID);
            if (streamingBO == null)
            {
                streamingBO = new StreamingBO
                {
                    IsPlaying = isPlaying
                };
            }
            else
            {
                streamingBO.IsPlaying = isPlaying;
            }
            if (firstPlaying)
            {
                streamingBO.FirstTimePlaying = DateTime.Now;
            }
            DicDeviceStreaming[deviceID] = streamingBO;
        }

        public static void SetFmStatus(int deviceID, int? value)
        {
            StreamingBO streamingBO = GetStreamingInfo(deviceID);
            if (streamingBO == null)
            {
                streamingBO = new StreamingBO
                {
                    FmStatus = value
                };
            }
            else
            {
                streamingBO.FmStatus = value;
            }
            if (value != null)
            {
                streamingBO.LastTimeFm = DateTime.Now;
            }
            DicDeviceStreaming[deviceID] = streamingBO;
        }

        public static void SetIsOnline(int deviceID, bool isOnline)
        {
            StreamingBO streamingBO = GetStreamingInfo(deviceID);
            if (streamingBO == null)
            {
                streamingBO = new StreamingBO
                {
                    IsOnline = isOnline
                };
            }
            else
            {
                streamingBO.IsOnline = isOnline;
            }
            if (isOnline)
            {
                streamingBO.LastTime = DateTime.Now;
            }
            DicDeviceStreaming[deviceID] = streamingBO;
        }

        public static void SetIsStop(int deviceID, bool isStop)
        {
            StreamingBO streamingBO = GetStreamingInfo(deviceID);
            if (streamingBO == null)
            {
                streamingBO = new StreamingBO
                {
                    IsStop = isStop
                };
            }
            else
            {
                streamingBO.IsStop = isStop;
            }
            DicDeviceStreaming[deviceID] = streamingBO;
        }

        public static void SetIsVOV(int deviceID, bool isVov)
        {
            StreamingBO streamingBO = GetStreamingInfo(deviceID);
            if (streamingBO == null)
            {
                streamingBO = new StreamingBO
                {
                    IsVOV = isVov
                };
            }
            else
            {
                streamingBO.IsVOV = isVov;
            }
            DicDeviceStreaming[deviceID] = streamingBO;
        }

        public static StreamingBO GetStreamingInfo(int deviceID)
        {
            DicDeviceStreaming.TryGetValue(deviceID, out StreamingBO result);
            return result;
        }

        public static void RemoveStreamingInfo(int deviceID)
        {
            StreamingBO streamingBO = GetStreamingInfo(deviceID);
            if (streamingBO != null)
            {
                streamingBO.FirstTimePlaying = null;
                streamingBO.CountPlayingTime = null;
                streamingBO.IsOnline = false;
                streamingBO.IsPlaying = false;
                streamingBO.IsStop = false;
                streamingBO.IsVOV = false;
                streamingBO.LastTime = null;
                streamingBO.LastTimePlaying = null;
            }
        }

        public static void RemoveFirstPlayingTime(int deviceID)
        {
            StreamingBO streamingBO = GetStreamingInfo(deviceID);
            if (streamingBO != null)
            {
                streamingBO.FirstTimePlaying = null;
                DicDeviceStreaming[deviceID] = streamingBO;
            }
        }

        public static bool HasDevice(int deviceID)
        {
            return DicDeviceStreaming.Any(x => x.Key == deviceID);
        }

        public static void SetIsFmSwitchOnOrOff(int deviceID, int value)
        {
            StreamingBO streamingBO = GetStreamingInfo(deviceID);
            if (streamingBO == null)
            {
                streamingBO = new StreamingBO
                {
                    IsFmSwitchOn = value
                };
            }
            else
            {
                streamingBO.IsFmSwitchOn = value;
            }
            DicDeviceStreaming[deviceID] = streamingBO;
        }
    }
}