﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Common.CommonClass
{
    public class Paginate<T>
    {
        public int Count { get; set; }
        public int PageSize { get; set; }
        public List<T> List { get; set; }
    }
}