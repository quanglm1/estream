﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eStream.Common.CommonObject;
using Model;

namespace eStream.Common.CommonClass
{
    public class SessionManager
    {
        public const string USER_INFO = "UserInfoBO";
        public const string LIST_ACTION = "LIST_ACTION";

        /// <summary>
        /// Dung cho luu tru ten file khi nguoi dung them / sua tai lieu
        /// </summary>
        public const string KEY_FILE_NAME = "LIST_FILE_NAME";
        public const string KEY_ORIGIN_FILE_NAME = "LIST_ORIGIN_FILE_NAME";
        public const string KEY_NOTI_ATTCH_FILE_NAME = "LIST_NOTI_ATTACH_FILE_NAME";
        public const string PAGE_INDEX = "PAGE_INDEX";
        public static void SetValue(string key, object value)
        {
            HttpContext context = HttpContext.Current;
            context.Session[key] = value;
        }
        public static object GetValue(string key)
        {
            HttpContext context = HttpContext.Current;
            return context.Session[key];
        }
        public static void Remove(string key)
        {
            HttpContext context = HttpContext.Current;
            context.Session.Remove(key);
        }
        public static void Clear()
        {
            HttpContext context = HttpContext.Current;
            context.Session.RemoveAll();
        }
        public static bool HasValue(string key)
        {
            HttpContext context = HttpContext.Current;
            return context.Session[key] != null;
        }
        public static UserInfoBO GetUserInfoBO()
        {
            HttpContext context = HttpContext.Current;
            if (context.Session[USER_INFO] == null)
            {
                context.Session[USER_INFO] = context.Cache[context.Session.SessionID];
                if (context.Session[USER_INFO] == null
                    && context.User.Identity.IsAuthenticated && !string.IsNullOrWhiteSpace(context.User.Identity.Name))
                {
                    SessionManager.TransferSession(context.User.Identity.Name);
                }
            }
            return context.Session[USER_INFO] as UserInfoBO;
        }
        public static void SetUserInfoBO(UserInfoBO UserInfoBO)
        {
            HttpContext context = HttpContext.Current;
            if (context.Session[USER_INFO] == null)
            {
                context.Session[USER_INFO] = UserInfoBO;
            }
        }
        public static List<AuthorizeBO> GetListPermistion()
        {
            HttpContext context = HttpContext.Current;
            if (context.Session[LIST_ACTION] == null)
            {
                var UserInfoBO = GetUserInfoBO();
                if (UserInfoBO != null)
                {
                    //using (Entities entityContext = new Entities())
                    //{
                    //    List<AuthorizeBO> lstActionPermission = (from a in entityContext.RoleAction
                    //                                             join b in entityContext.ActionPermisstion on a.ActionID equals b.ActionID
                    //                                             select new AuthorizeBO
                    //                                             {
                    //                                                 ActionID = a.ActionID,
                    //                                                 ActionUrl = b.ActiontionPath
                    //                                             })
                    //        .ToList();

                    //    context.Session[LIST_ACTION] = lstActionPermission.Select(a => new AuthorizeBO
                    //    {
                    //        ActionID = a.ActionID,
                    //        ActionUrl = a.ActionUrl.Replace("\r\n", "")
                    //    }).ToList();
                    //}
                }
                else
                {
                    context.Session[LIST_ACTION] = new List<AuthorizeBO>();
                }
            }
            return context.Session[LIST_ACTION] as List<AuthorizeBO>;
        }
        public static void TransferSession(string userName)
        {
            using (Entities context = new Entities())
            {
                var obj = context.User.FirstOrDefault(a => a.UserName.Equals(userName));
                if (obj != null)
                {
                    var objUserInfoBO = new UserInfoBO
                    {
                        UserID = obj.UserID,
                        UserName = obj.UserName,
                        Avatar = obj.UserAvatar,
                        FullName = obj.FullName,
                        UnitID = obj.UnitID,
                        IsAdmin = (obj.Role == GlobalConstant.USER_ROLE_ADMIN ? GlobalConstant.IS_ACTIVE : 0),
                        IsManager = (obj.Role == GlobalConstant.USER_ROLE_MANAGER ? GlobalConstant.IS_ACTIVE : 0),
                        IsSupperAdmin = (obj.Role == GlobalConstant.USER_ROLE_SUPPER_ADMIN ? GlobalConstant.IS_ACTIVE : 0),
                        PhoneNumber = obj.PhoneNumber,
                        UnitLevel = context.Unit.Where(u => u.UnitID == obj.UnitID).Select(x => x.UnitLevel).FirstOrDefault()
                    };
                    if (objUserInfoBO.IsSupperAdmin.GetValueOrDefault() == GlobalConstant.IS_ACTIVE)
                    {
                        objUserInfoBO.IsAdmin = GlobalConstant.IS_ACTIVE;
                    }
                    SetUserInfoBO(objUserInfoBO);
                    //Set lai gia tri dang nhap
                    obj.LastLoginDate = DateTime.Now;
                    context.SaveChanges();
                }
            }
        }
    }
}