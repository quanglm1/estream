﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using eStream.Common.CommonObject;
using Model;
using System.Data.Entity;
using System.IO;

namespace eStream.Common.CommonClass
{
    [PersistJobDataAfterExecution]
    public class FileRemover : IJob
    {
        public Task Execute(IJobExecutionContext context)
        {
            return Task.Factory.StartNew(() => RemoveFile(context));
        }

        private void RemoveFile(IJobExecutionContext jContext)
        {
            try
            {
                int days = 0;
                List<string> listDirectory = new List<string>();
                using (Entities context = new Entities())
                {
                    SystemConfig sc = context.SystemConfig.FirstOrDefault();
                    days = (sc != null ? sc.DayRemoveFile.GetValueOrDefault() : 0);
                    if (days > 0)
                    {
                        DateTime fromDate = DateTime.Now.Date.AddDays(-days);
                        List<Program> listProgram = context.Program.Where(x => DbFunctions.TruncateTime(x.ExecuteDate) <= DbFunctions.TruncateTime(fromDate)).ToList();
                        foreach (Program p in listProgram)
                        {
                            if (!string.IsNullOrWhiteSpace(p.FileUrl))
                            {
                                listDirectory.Add(p.FileUrl);
                            }
                            if (!string.IsNullOrWhiteSpace(p.SplitedFolder))
                            {
                                listDirectory.Add(GlobalConstant.MEDIA_FOLDER + p.SplitedFolder);
                            }
                        }
                    }
                }
                if (days > 0 && listDirectory.Any())
                {
                    foreach (string path in listDirectory)
                    {
                        if (Directory.Exists(path))
                        {
                            //Delete all files from the Directory
                            foreach (string file in Directory.GetFiles(path))
                            {
                                File.Delete(file);
                            }
                            //Delete all child Directories
                            foreach (string directory in Directory.GetDirectories(path))
                            {
                                //Delete a Directory
                                Directory.Delete(directory, true);
                            }
                        }
                    }
                }
            }
            catch
            {
                // ignored
            }
        }
    }
}