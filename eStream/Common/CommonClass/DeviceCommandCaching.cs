﻿using System.Linq;
using eStream.Dto;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using Model;

namespace eStream.Common.CommonClass
{
    public class DeviceCommandCaching
    {
        private static ConcurrentDictionary<int, List<DeviceCommandDto>> DicDeviceCommand = new ConcurrentDictionary<int, List<DeviceCommandDto>>();
        public static void AddCommand(int deviceID, short logcode)
        {
            DicDeviceCommand.TryGetValue(deviceID, out List<DeviceCommandDto> listCommand);
            if (listCommand == null)
            {
                listCommand = new List<DeviceCommandDto>();
                DicDeviceCommand[deviceID] = listCommand;
            }
            var command = new DeviceCommandDto
            {
                LogCode = logcode,
                ReceiveTime = DateTime.Now
            };
            listCommand.Add(command);
        }

        private static void RemoveExpiredCommand(Device device, short logcode)
        {
            switch (logcode)
            {
                case GlobalConstant.DEVICE_COMMAND_CHANGE_VOLUME:
                    device.IsChangeVolume = null;
                    break;
                case GlobalConstant.DEVICE_COMMAND_CHECK_INTERVAL:
                    device.IsChangeSyncTime = null;
                    break;
                case GlobalConstant.DEVICE_COMMAND_RESTART:
                    device.IsRestart = null;
                    StreamingCaching.RemoveStreamingInfo(device.DeviceID);
                    break;
                case GlobalConstant.DEVICE_COMMAND_SHUTDOWN:
                    device.IsShutdown = null;
                    StreamingCaching.RemoveStreamingInfo(device.DeviceID);
                    break;
                case GlobalConstant.DEVICE_COMMAND_VERSION:
                    device.IsSendVersion = null;
                    break;
            }
        }

        public static void RemoveExpiredCommands(Device device, List<DeviceLogDto> listlog)
        {
            DicDeviceCommand.TryGetValue(device.DeviceID, out List<DeviceCommandDto> listCommand);
            if (listCommand != null && listCommand.Any())
            {
                if (listlog != null && listlog.Any())
                {
                    listCommand.RemoveAll(x => listlog.Any(l => l.logcode == x.LogCode));
                }
                var now = DateTime.Now;
                foreach (var command in listCommand)
                {
                    var limitedTime = command.ReceiveTime.AddMinutes(GlobalConstant.COMMAND_EXPIRED_PERIOD);
                    if (limitedTime < now)
                    {
                        RemoveExpiredCommand(device, command.LogCode);
                    }
                }
                listCommand.RemoveAll(x => x.ReceiveTime.AddMinutes(GlobalConstant.COMMAND_EXPIRED_PERIOD) < now);
            }
        }
    }
}