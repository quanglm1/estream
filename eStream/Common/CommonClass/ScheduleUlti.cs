﻿using eStream.Common.CommonObject;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;

namespace eStream.Common.CommonClass
{
    public class ScheduleUlti
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public static List<BroadcastInfo> SplitAudioFile(List<AudioInfo> audioInfos)
        {
            List<BroadcastInfo> res = new List<BroadcastInfo>();


            foreach (AudioInfo audioInfo in audioInfos)
            {
                audioInfo.Bitrate = GlobalConstant.DEFAULT_BIT_RATE;
                string filePath = audioInfo.FilePath;
                string tempFolder = Guid.NewGuid().ToString();
                string tempPath = GlobalConstant.MEDIA_FOLDER + tempFolder + "/" + audioInfo.Bitrate;
                Directory.CreateDirectory(tempPath);
                BroadcastInfo bi = new BroadcastInfo
                {
                    FilePath = filePath
                };
                if (!File.Exists(filePath))
                {
                    throw new BusinessException(string.Format("File {0} không tồn tại", filePath));
                }

                string volume = audioInfo.Volume == null ?
                    "" : string.Format(CultureInfo.InvariantCulture, "-af \"volume={0:0.##}\" ", audioInfo.Volume);

                string bitrate = audioInfo.Bitrate == null ?
                    "" : string.Format(CultureInfo.InvariantCulture, "-b:a {0}k ", audioInfo.Bitrate);

                string rtp = string.Format("-i \"{0}\" {1}-vn -ac 2 -acodec aac {2}-f segment" +
                    " -segment_format mpegaac -segment_time {3} -segment_list" +
                    " \"{4}\\playlist.m3u8\" \"{4}/{5}%d.aac\"", filePath, volume, bitrate,
                    GlobalConstant.CHUNK_DURATION, tempPath, GlobalConstant.CHUNK_PREFIX);
                var psi = new ProcessStartInfo(@"ffmpeg.exe")
                {
                    Arguments = rtp,
                    UseShellExecute = false,
                    CreateNoWindow = true
                };
                Process p = Process.Start(psi);
                p.WaitForExit();

                string playlist = string.Format("{0}\\playlist.m3u8", tempPath);
                double duration = 0;
                long fileCount = 0;
                string[] lines = File.ReadAllLines(playlist);
                foreach (var line in lines)
                {
                    if (line.StartsWith("#EXTINF"))
                    {
                        string dur = line.Split(':')[1].TrimEnd(',').Replace(',', '.');
                        duration += double.Parse(dur, NumberStyles.Any, CultureInfo.InvariantCulture);
                        fileCount++;
                    }
                }
                bi.Duration = duration;
                bi.NumberOfSplitFile = fileCount;
                bi.TempFolder = tempFolder;
                res.Add(bi);
            }

            return res;
        }

        public static void SplitAudioFile(List<BroadcastInfo> broadcastInfos, long bitrate)
        {
            List<BroadcastInfo> res = new List<BroadcastInfo>();
            foreach (BroadcastInfo audioInfo in broadcastInfos)
            {
                string filePath = audioInfo.FilePath;
                string tempFolder = audioInfo.TempFolder;
                string tempPath = GlobalConstant.MEDIA_FOLDER + tempFolder + "/" + bitrate;
                Directory.CreateDirectory(tempPath);
                BroadcastInfo bi = new BroadcastInfo
                {
                    FilePath = filePath
                };
                if (!File.Exists(filePath))
                {
                    throw new BusinessException(string.Format("File {0} không tồn tại", filePath));
                }

                string volume = "";
                string bitrateS = string.Format(CultureInfo.InvariantCulture, "-b:a {0}k ", bitrate);

                string rtp = string.Format("-i \"{0}\" {1}-vn -ac 2 -acodec aac {2}-f segment" +
                    " -segment_format mpegaac -segment_time {3} -segment_list" +
                    " \"{4}\\playlist.m3u8\" \"{4}\\{5}%d.aac\"", filePath, volume, bitrateS,
                    GlobalConstant.CHUNK_DURATION, tempPath, GlobalConstant.CHUNK_PREFIX);

                var psi = new ProcessStartInfo(@"ffmpeg.exe")
                {
                    Arguments = rtp,
                    UseShellExecute = false,
                    CreateNoWindow = true
                };
                Process p = Process.Start(psi);
                p.WaitForExit();

            }
        }

        public static void WavToAac(string path, string name)
        {
            var psi = new ProcessStartInfo(@"ffmpeg.exe")
            {
                Arguments = "-i " + path + "/wav/" + name + ".wav -acodec aac " + path + "/aac/" + name + ".aac",
                UseShellExecute = false,
                CreateNoWindow = true
            };
            Process p = Process.Start(psi);
            p.WaitForExit();
        }

        public static double GetDuration(string path)
        {
            var psi = new ProcessStartInfo(@"ffprobe.exe")
            {
                Arguments = "-i " + path + " -show_entries format=duration -v quiet -of csv=\"p = 0\"",
                UseShellExecute = false,
                CreateNoWindow = true,
                RedirectStandardOutput = true
            };
            Process p = Process.Start(psi);
            string line = p.StandardOutput.ReadToEnd().Replace(',', '.');
            double.TryParse(line, NumberStyles.Any, CultureInfo.InvariantCulture, out double duration);
            p.WaitForExit();
            return duration;
        }

        public static List<string> ReadPlayListFile(string folderPath)
        {
            string path = GlobalConstant.MEDIA_FOLDER + "/" + folderPath + "/" + "playlist.m3u8";
            string[] lines = File.ReadAllLines(path);
            return lines.Where(x => x.StartsWith("#EXTINF")).ToList();
        }
        public static List<string> ReadPlayListChanel(string ForwardChanelUrl)
        {
            string[] lines = File.ReadAllLines(ForwardChanelUrl);
            return lines.Where(x => x.StartsWith("#EXTINF")).ToList();
        }

    }
}