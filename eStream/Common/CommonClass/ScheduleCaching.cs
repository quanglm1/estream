﻿using eStream.Common.CommonObject;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Common.CommonClass
{
    public class ScheduleCaching
    {
        public static void LoadCaching(bool isEmpty = false)
        {
            if (!isEmpty)
            {
                CacheHelper.Delete(GlobalConstant.SCHEDULE_CACHE_KEY);
            }
            CacheHelper.Add(GlobalConstant.SCHEDULE_CACHE_KEY, GlobalCommon.GetSchedule(), DateTimeOffset.UtcNow.AddHours(1));
        }
        public static List<ScheduleBO> GetListScheduleNow(int exceptUnitId, int secondBefore = 0)
        {
            Dictionary<DayOfWeek, List<ScheduleBO>> dicSchedule = CacheHelper.GetValue(GlobalConstant.SCHEDULE_CACHE_KEY) as Dictionary<DayOfWeek, List<ScheduleBO>>;
            if (dicSchedule == null)
            {
                LoadCaching(true);
                dicSchedule = CacheHelper.GetValue(GlobalConstant.SCHEDULE_CACHE_KEY) as Dictionary<DayOfWeek, List<ScheduleBO>>;
            }
            DateTime now = DateTime.Now;
            int hourOfDay = now.Hour;
            int minuteOfDay = now.Minute;
            int minutes = 60 * hourOfDay + minuteOfDay;
            int seconds = 60 * minutes + now.Second;
            List<ScheduleBO> listSchedule = new List<ScheduleBO>();
            if (dicSchedule != null && dicSchedule[now.DayOfWeek] != null)
            {
                foreach (var schedule in dicSchedule[now.DayOfWeek])
                {
                    if ((60 * schedule.StartMinus.GetValueOrDefault() - secondBefore) <= seconds && 60 * schedule.EndMinus.GetValueOrDefault() >= seconds)
                    {
                        var listExceptUnitId = schedule.ListExceptUnitID;
                        if (exceptUnitId <= 0 || listExceptUnitId == null || !listExceptUnitId.Contains(exceptUnitId))
                        {
                            listSchedule.Add(schedule);
                        }
                    }
                }
            }
            return listSchedule;
        }
        public static int GetEndSchedule(int exceptUnitId, out ScheduleBO nearestSchedule)
        {
            Dictionary<DayOfWeek, List<ScheduleBO>> dicSchedule = CacheHelper.GetValue(GlobalConstant.SCHEDULE_CACHE_KEY) as Dictionary<DayOfWeek, List<ScheduleBO>>;
            if (dicSchedule == null)
            {
                LoadCaching(true);
                dicSchedule = CacheHelper.GetValue(GlobalConstant.SCHEDULE_CACHE_KEY) as Dictionary<DayOfWeek, List<ScheduleBO>>;
            }
            ScheduleBO lastSchedule = null;
            int minDifTime = int.MaxValue;
            nearestSchedule = null;
            if (dicSchedule != null)
            {
                DateTime now = DateTime.Now;
                int hourOfDay = now.Hour;
                int minuteOfDay = now.Minute;
                int secondOfDay = now.Second;
                int seconds = 60 * 60 * hourOfDay + 60 * minuteOfDay + secondOfDay;
                List<ScheduleBO> listSchedule = dicSchedule[now.DayOfWeek];
                if (listSchedule != null)
                {
                    foreach (var schedule in dicSchedule[now.DayOfWeek])
                    {
                        var listExceptUnitId = schedule.ListExceptUnitID;
                        if (exceptUnitId > 0 && (listExceptUnitId == null || !listExceptUnitId.Contains(exceptUnitId)))
                        {
                            int difTime = seconds - 60 * schedule.EndMinus.GetValueOrDefault();
                            if (difTime > 0 && difTime < minDifTime)
                            {
                                minDifTime = difTime;
                                lastSchedule = schedule;
                                nearestSchedule = schedule;
                            }
                        }
                    }
                }
            }
            if (lastSchedule != null)
            {
                return minDifTime;
            }
            return 0;
        }
    }
}