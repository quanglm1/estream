﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Dto
{
    public class SwitchDto : CommandDto
    {
        public bool @switch { get; set; }
    }
}