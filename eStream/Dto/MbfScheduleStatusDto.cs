﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Dto
{
    public class MbfScheduleStatusDto
    {
        public long rec_id { get; set; }
        public string rec_summary { get; set; }
        public int rec_play_mode { get; set; }
        public int rec_status { get; set; }
        public int[] rec_play_time { get; set; }
        public List<ScheduleUnitItem> units { get; set; }
    }

    public class ScheduleProgramItem
    {
        public long id { get; set; }
        public string title { get; set; }
        public string file_name { get; set; }
        public string created_user { get; set; }
        public string created_date { get; set; }
        public double? duration { get; set; }
    }

    public class ScheduleUnitItem
    {
        public int id { get; set; }
        public string name { get; set; }
        public string path { get; set; }
        public string rec_url { get; set; }
        public List<ScheduleProgramItem> programs { get; set; }
        public List<ScheduleUnitDeviceItem> devices { get; set; }
    }

    public class ScheduleUnitDeviceItem
    {
        public int id { get; set; }
        public string code { get; set; }
        public string name { get; set; }

    }
}