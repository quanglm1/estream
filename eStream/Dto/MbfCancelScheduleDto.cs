﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Dto
{
    public class MbfCancelScheduleDto
    {
        public long rec_id { get; set; }
        public int rec_receipt_id { get; set; }
        public int rec_play_mode { get; set; }
        public string rec_play_start { get; set; }
        public string rec_play_expire { get; set; }
    }
}