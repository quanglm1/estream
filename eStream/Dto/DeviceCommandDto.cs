﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Dto
{
    public class DeviceCommandDto
    {
        public short LogCode { get; set; }
        public DateTime ReceiveTime { get; set; }
    }
}