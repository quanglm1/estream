﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Dto
{
    public class LogRequestDto
    {
        public string rsaid { get; set; }
        public List<DeviceLogDto> listlog { get; set; }
    }
}