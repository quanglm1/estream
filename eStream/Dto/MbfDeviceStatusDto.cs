﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Dto
{
    public class MbfDeviceStatusDto
    {
        public MbfDeviceStatus[] data { get; set; }
        public int total { get; set; }
    }

    public class MbfDeviceStatus
    {
        public int id { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public int status { get; set; }
        public int? unit_id { get; set; }
        public string unit_name { get; set; }
        public int connect { get; set; }
        public int playing_type { get; set; }
        public string playing_source { get; set; }
        public int fm_status { get; set; }
        public int fm_connect { get; set; }
        public string volume { get; set; }
        public string gps { get; set; }
    }
}