﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Dto
{
    public class MbfDevicePlayingDto
    {
        public int id { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public int? unit_id { get; set; }
        public string unit_name { get; set; }
        public string unit_path { get; set; }
        public int day { get; set; }
        public int month { get; set; }
        public int year { get; set; }
        public double total_time { get; set; }
    }
}