﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Dto
{
    public class ShutdownDto : CommandDto
    {
        public bool shutdown { get; set; }
    }
}