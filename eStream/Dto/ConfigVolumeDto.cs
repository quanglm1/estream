﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Dto
{
    public class ConfigVolumeDto : CommandDto
    {
        public int volume { get; set; }
    }
}