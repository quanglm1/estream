﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Dto
{
    public class MbfSetScheduleDto
    {
        public long rec_id { get; set; }
        public string rec_summary { get; set; }
        public int rec_type { get; set; }
        public string rec_header { get; set; }
        public MbfAudioDto[] rec_audios { get; set; }
        public int rec_audio_codec { get; set; }
        public string rec_url { get; set; }
        public int rec_play_mode { get; set; }
        public int rec_priority { get; set; }
        public int[] rec_play_time { get; set; }
        public int rec_play_repeat_type { get; set; }
        public int[] rec_play_repeat_days { get; set; }
        public string rec_play_start { get; set; }
        public string rec_play_expire { get; set; }
        public int rec_receipt_type { get; set; }
        public int rec_receipt_id { get; set; }
        public int[] rec_device_ids { get; set; }
    }
}