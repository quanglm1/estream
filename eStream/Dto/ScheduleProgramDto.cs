﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Dto
{
    public class ScheduleProgramDto
    {
        public DateTime ScheduleDatetime { get; set; }
        public int ScheduleHour { get; set; }
        public int ScheduleMinutes { get; set; }
        public string FileURL { get; set; }
    }
}