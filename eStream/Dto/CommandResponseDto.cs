﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Dto
{
    public class CommandResponseDto : BaseResponseDto
    {
        public List<CommandDto> listcommand { get; set; }
        public List<LogDto> listlog { get; set; }
        public bool HasValue()
        {
            if (listcommand != null && listcommand.Any())
            {
                return true;
            }
            if (listlog != null && listlog.Any())
            {
                return true;
            }
            return false;
        }
    }
}