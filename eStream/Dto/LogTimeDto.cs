﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Dto
{
    public class LogTimeDto : CommandDto
    {
        public int loginterval { get; set; }
    }
}