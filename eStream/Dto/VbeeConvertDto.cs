﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Dto
{
    public class ConvertDataDto
    {
        public string input_text { get; set; }
        public string audio_type { get; set; }
        public long id { get; set; }
    }
    public class VbeeConvertDto
    {
        public long status_code { get; set; }
        public string message { get; set; }
        public ConvertDataDto data { get; set; }
    }
}