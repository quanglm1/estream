﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Dto
{
    public class VbeeFileDto
    {
        public int program_id { get; set; }
        public long error_code { get; set; }
        public string message { get; set; }
        public string link { get; set; }
    }
}