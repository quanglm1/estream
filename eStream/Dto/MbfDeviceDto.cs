﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Dto
{
    public class MbfDeviceDto
    {
        public MbfDevice[] data { get; set; }
        public int total { get; set; }
    }

    public class MbfDevice
    {
        public int id { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public int status { get; set; }
        public int? unit_id { get; set; }
        public string unit_name { get; set; }
    }
}