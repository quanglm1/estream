﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Dto
{
    public class MbfUnitDto
    {
        public int id { get; set; }
        public int level_type { get; set; }
        public string name { get; set; }
        public int? parent_id { get; set; }
        public string phone_number { get; set; }
        public string email { get; set; }
        public string path { get; set; }
    }
}