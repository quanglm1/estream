﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Dto
{
    public class VersionDto : CommandDto
    {
        public bool version { get; set; }
    }
}