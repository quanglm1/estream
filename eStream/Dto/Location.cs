﻿namespace eStream.Dto
{
    class FloatLocation
    {
        public float Latitude { get; set; }
        public float Longitude { get; set; }
    }

    class DmsLocation
    {
        public DmsPoint Latitude { get; set; }
        public DmsPoint Longitude { get; set; }

        public FloatLocation Convert()
        {
            return new FloatLocation
            {
                Latitude = Calculatefloat(Latitude),
                Longitude = Calculatefloat(Longitude)
            };
        }

        private static float Calculatefloat(DmsPoint point)
        {
            if (point == null)
            {
                return default(float);
            }
            return point.Degrees + (float)point.Minutes / 60 + (float)point.Seconds / 3600;
        }
    }

    class DmsPoint
    {
        public int Degrees { get; set; }
        public int Minutes { get; set; }
        public float Seconds { get; set; }

        public DmsPoint(float val)
        {
            var iVal = (int)val;
            Minutes = iVal - (iVal / 100) * 100;
            Degrees = (iVal - Minutes) / 100;
            Seconds = (val - iVal) * 60.0f;
        }
    }
}