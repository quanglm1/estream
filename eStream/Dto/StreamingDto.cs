﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Dto
{
    public class StreamingDto
    {
        public long FileCount { get; set; }
        public long NowSequence { get; set; }
    }
}