﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Dto
{
    public class CheckIntervalDto : CommandDto
    {
        public int checkinterval { get; set; }
    }
}