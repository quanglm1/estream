﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Dto
{
    public class FileStreamDto
    {
        public long FileSequence { get; set; }
        public long NowSequence { get; set; }
    }
}