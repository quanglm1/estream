﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Dto
{
    public class CommandRequestDto
    {
        public string rsaid { get; set; }
        public string version { get; set; }
        public bool? streaming { get; set; }
        public bool? getconfig { get; set; }
        public int? signal { get; set; }
        public int? power { get; set; }
        public int? s1 { get; set; }
        public int? s2 { get; set; }
        public int? s3 { get; set; }
        public int? s4 { get; set; }
        public string ram_usage { get; set; }
        public string pro_name { get; set; }
        public string conn_type { get; set; }
        public string conn_mod { get; set; }
        public string ram_total { get; set; }
        public string cpu_freq { get; set; }
        public string cpu_temp { get; set; }
        public string cpu_usage { get; set; }
        public int? sign_strength { get; set; }
        public string sim_ser { get; set; }
        public string gps_loc { get; set; }
        public string sendsms { get; set; }
        public List<DeviceLogDto> listlog { get; set; }
    }
}