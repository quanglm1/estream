﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Dto
{
    public class VovDto
    {
        public string VovCode { get; set; }
        public long VovSequence { get; set; }
        public long NowSequence { get; set; }
    }
}