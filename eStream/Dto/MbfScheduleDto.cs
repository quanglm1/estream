﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Dto
{
    public class MbfScheduleDto
    {
        public long rec_id { get; set; }
        public string rec_summary { get; set; }
        public int rec_type { get; set; }
        public int[] rec_play_time { get; set; }
        public int rec_play_repeat_type { get; set; }
        public int[] play_repeat_days { get; set; }
    }
}