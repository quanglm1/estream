﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Dto
{
    public class PlayingDto
    {
        public int PlayingType { get; set; }
        public int LiveSource { get; set; }
        public string PlayingSource { get; set; }
        public long NowLiveID { get; set; }
    }
}