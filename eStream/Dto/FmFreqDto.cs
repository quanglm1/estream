﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Dto
{
    public class FmFreqDto : CommandDto
    {
        public double freqfm { get; set; }
    }
}