﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Dto
{
    public class StopStreamDto : CommandDto
    {
        public bool stopstream { get; set; }
    }
}