﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Dto
{
    public class StartStreamDto: CommandDto
    {
        public string startstream { get; set; }
    }
}