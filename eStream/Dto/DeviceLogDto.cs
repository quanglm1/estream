﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Dto
{
    public class DeviceLogDto
    {
        public short logcode { get; set; }
        public long logid { get; set; }
        public string datetime { get; set; }
        public bool status { get; set; }
    }
}