﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eStream.Dto
{
    public class MbfAudioDto
    {
        public string url { get; set; }
        public int index { get; set; }
    }
}