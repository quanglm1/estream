//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Device
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Device()
        {
            this.DeviceLog = new HashSet<DeviceLog>();
        }
    
        public int DeviceID { get; set; }
        public string DeviceCode { get; set; }
        public string DeviceName { get; set; }
        public string ManagerName { get; set; }
        public int Status { get; set; }
        public string ManagerPhone { get; set; }
        public string SimNumber { get; set; }
        public Nullable<int> IsOnline { get; set; }
        public Nullable<int> IsUpdateOS { get; set; }
        public Nullable<int> IsRestart { get; set; }
        public Nullable<int> UnitID { get; set; }
        public System.DateTime CreateDate { get; set; }
        public string OSPath { get; set; }
        public Nullable<int> IsShutdown { get; set; }
        public Nullable<int> IsChangeVolume { get; set; }
        public Nullable<int> IsChangeSyncTime { get; set; }
        public Nullable<int> IsChangeDomain { get; set; }
        public Nullable<int> IsSendVersion { get; set; }
        public string Version { get; set; }
        public Nullable<int> IsRequestStop { get; set; }
        public Nullable<int> IsRequestStart { get; set; }
        public Nullable<int> Volume { get; set; }
        public Nullable<int> Bitrate { get; set; }
        public Nullable<int> IsSMS { get; set; }
        public Nullable<int> SyncTime { get; set; }
        public Nullable<int> SmsScheduleID { get; set; }
        public Nullable<System.DateTime> SmsTime { get; set; }
        public Nullable<int> NoVov { get; set; }
        public Nullable<int> IsStoping { get; set; }
        public Nullable<int> IsChangeTimer { get; set; }
        public Nullable<int> Timer { get; set; }
        public string FullDeviceCode { get; set; }
        public Nullable<int> IsRequestForceStop { get; set; }
        public Nullable<System.DateTime> LastSyncDate { get; set; }
        public Nullable<int> FmType { get; set; }
        public Nullable<int> IsFmSwitchOn { get; set; }
        public Nullable<int> IsFmSwitchOff { get; set; }
        public Nullable<int> IsFmStart { get; set; }
        public Nullable<int> IsSupportFm { get; set; }
        public string Sound { get; set; }
        public Nullable<double> FmFreq { get; set; }
        public Nullable<int> IsFmFreq { get; set; }
        public string SimSer { get; set; }
        public Nullable<int> ProSmsID { get; set; }
        public Nullable<int> ChangeConn { get; set; }
        public string ConnType { get; set; }
    
        public virtual Unit Unit { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DeviceLog> DeviceLog { get; set; }
    }
}
