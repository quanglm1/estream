﻿using System.Linq;
using Model;
using Tool.Helpers;

namespace Tool
{
    public class Device
    {
        public void UpdateFullCodes(string domain, string secret)
        {
            using (var context = new Entities())
            {
                var devices = context.Device.ToList();
                foreach (var d in devices)
                {
                    string fullCode = d.FullDeviceCode;
                    if (!string.IsNullOrWhiteSpace(fullCode))
                    {
                        var a = fullCode.Split(':');
                        if (a.Length != 6)
                        {
                            continue;
                        }
                        bool isUpdate = true;
                        foreach(string s in a)
                        {
                            if (s.Length != 2)
                            {
                                isUpdate = false;
                                break;
                            }
                        }
                        if (isUpdate)
                        {
                            var encryptedFullCode = Utils.ComputeSha1Hash(domain + d.FullDeviceCode, secret);
                            d.FullDeviceCode = encryptedFullCode;
                        }
                    }
                }
                context.SaveChanges();
            }
        }
    }
}
