﻿using System.Security.Cryptography;
using System.Text;

namespace Tool.Helpers
{
    class Utils
    {
        public static string ComputeSha1Hash(string rawText, string secret)
        {
            // Create a SHA256   
            using (var sha1 = SHA1.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha1.ComputeHash(Encoding.UTF8.GetBytes(secret + rawText));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString().ToUpper();
            }
        }
    }
}
